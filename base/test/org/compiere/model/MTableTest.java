package org.compiere.model;

import static org.junit.jupiter.api.Assertions.*;

import org.compiere.util.Env;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class MTableTest
{

	private static MTable test_table = null;

	@BeforeAll
	public static void setUp() throws Exception, Exception
	{
		org.compiere.Adempiere.startupEnvironment(false);
		test_table = MTable.get(Env.getCtx(), "Test");
	}

	/**
	 * Test MTable.getCreateSQL() method
	 */
	@Test
	public void testCreateTable()
	{
		String sql = test_table.getSQLCreate();
		assertEquals("CREATE TABLE Test (Account_Acct NUMERIC(10) DEFAULT NULL ,"
				+ " AD_Client_ID NUMERIC(10) NOT NULL, AD_Org_ID NUMERIC(10) NOT NULL,"
				+ " BinaryData BYTEA DEFAULT NULL , C_BPartner_ID NUMERIC(10) DEFAULT NULL ,"
				+ " C_Currency_ID NUMERIC(10) DEFAULT NULL , CharacterData TEXT DEFAULT NULL ,"
				+ " C_Location_ID NUMERIC(10) DEFAULT NULL , C_Payment_ID NUMERIC(10) DEFAULT NULL ,"
				+ " Created TIMESTAMP NOT NULL, CreatedBy NUMERIC(10) NOT NULL,"
				+ " C_UOM_ID NUMERIC(10) DEFAULT NULL , Description TEXT DEFAULT NULL ,"
				+ " Help TEXT DEFAULT NULL ,"
				+ " IsActive CHAR(1) DEFAULT 'Y' CHECK (IsActive IN ('Y','N')) NOT NULL,"
				+ " M_Locator_ID NUMERIC(10) DEFAULT NULL , M_Product_ID NUMERIC(10) DEFAULT NULL ,"
				+ " Name TEXT NOT NULL, Processed CHAR(1) DEFAULT NULL  CHECK (Processed IN ('Y','N')),"
				+ " Processing CHAR(1) DEFAULT NULL , T_Amount NUMERIC DEFAULT NULL ,"
				+ " T_Date TIMESTAMP DEFAULT NULL , T_DateTime TIMESTAMP DEFAULT NULL , "
				+ "Test_ID NUMERIC(10) NOT NULL, T_Integer NUMERIC(10) DEFAULT NULL ,"
				+ " T_Number NUMERIC DEFAULT NULL , T_Qty NUMERIC DEFAULT NULL , "
				+ "Updated TIMESTAMP NOT NULL, UpdatedBy NUMERIC(10) NOT NULL,"
				+ " CONSTRAINT Test_Key PRIMARY KEY (Test_ID))", sql);
	}

}	//	MTableTest
