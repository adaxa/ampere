package org.compiere.model;

import org.compiere.util.Trx;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

abstract class AbstractAdempiereTest {

	protected Trx trx = null;

	@BeforeAll
	private static void setUpClass() throws Exception {
		org.compiere.Adempiere.startupEnvironment(true);
	}

	public AbstractAdempiereTest() {
		super();
	}

	@BeforeEach
	void setUp() throws Exception {
		
		trx = Trx.get(Trx.createTrxName("AdempiereTest"), true);
	}

	@AfterEach
	void tearDown() throws Exception {
		trx.rollback();
		trx.close();
	}

}