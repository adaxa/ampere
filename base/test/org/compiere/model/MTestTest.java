package org.compiere.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

import org.compiere.util.Env;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class MTestTest
{


	private static Properties ctx;

	@BeforeAll
	public static void setUp() throws Exception, Exception
	{
		org.compiere.Adempiere.startupEnvironment(false);

		ctx = Env.getCtx();
	}

	/**
	 * Test MTest save
	 */
	@Test
	public void testTestSave()
	{
		
		MTest test = new MTest(ctx, "test", 1);
		test.saveEx();
		
		assertEquals(test.getName(), "test_1");
	}
	
	/**
	 * Test MTest CLOB
	 */
	@Test
	public void testCLOB()
	{
		String charData = "Long Text JJ";
		/** Test CLOB	*/
		MTest t1 = new MTest (ctx, 0, null);
		t1.setName("Test1");
		t1.saveEx();
		t1.setCharacterData(charData);
		t1.saveEx();
		int Test_ID = t1.getTest_ID();
		//
		MTest t2 = new MTest (Env.getCtx(), Test_ID, null);

		assertEquals(t2.getCharacterData(), charData);
		t2.delete(true);
		
	}

	@Disabled
	@Test 
	void testVolume() {
		
		//String trxName = Trx.createTrxName();

		//Trx trx = Trx.get(trxName, true);

		long start = System.nanoTime();
		for (int i = 1; i < 20000; i++)
		{
			new MTest (ctx, "test", i).saveEx();
		}		
		
		 long elapsed = System.nanoTime() - start;
		 System.out.println( "Elapsed (ms):" + elapsed/1000000 );
	}
}	//	MTestTest
