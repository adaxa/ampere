package org.compiere.model;

import static org.junit.jupiter.api.Assertions.*;

import org.compiere.util.Env;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class MSetupTest {

	private static MSetup ms;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		

		org.compiere.Adempiere.startupEnvironment(true);
		if ( ms == null )
			ms = new MSetup();

		ms.initialize(Env.getCtx(), 9999);
			
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		ms.rollback();
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Disabled
	@Test
	void testMSetup() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testMSetupPropertiesInt() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testInitialize() {
		fail("Not yet implemented");
	}

	@Test
	void testCreateClient() {
		 assertTrue(ms.createClient("testClient", "testOrg", "testOrgName", "adminUser", "normalUser"
					, "phone", "phone2", "fax", "email", "taxId", "duns", null, 0 ));
	}

	@Disabled
	@Test
	void testCreateAccounting() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testImportChart() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testImportReportLineSet() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateEntities() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreatePaymentTerms() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateBPGroups() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateTax() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateDocumentStatus() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testGetNextID() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testGetAD_Client_ID() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testGetAD_Org_ID() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testGetAD_User_ID() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testGetInfo() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testClose() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testRollback() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateBank() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testCreateRequestParameter() {
		fail("Not yet implemented");
	}

}
