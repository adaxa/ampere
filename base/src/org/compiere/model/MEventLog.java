package org.compiere.model;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;

import org.json.JSONObject;

public class MEventLog extends X_AD_EventLog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5258467945754740435L;

	public MEventLog(Properties ctx, int AD_EventLog_ID, String trxName) {
		super(ctx, AD_EventLog_ID, trxName);
	}

	public void addData(String fieldName, Object oldValue, Object newValue) {
		
		JSONObject data = getEventData();
		if ( data == null )
			data = new JSONObject();
	}

	/*
	 * Record the insertion of PO in event log
	 */
	public static void logInsertPO (PO po, HashMap<String, Object> newValues, MSession session)
	{
		if ( isLogPO(po) )
		{
			MEventLog elog = new MEventLog(po.getCtx(), 0, po.get_TrxName());
			elog.setClientOrg(po);
			elog.setAD_Table_ID(po.p_info.getAD_Table_ID());
			elog.setRecord_ID(po.get_ID());
			elog.setEventType(MEventLog.EVENTTYPE_InsertPO);
			if ( session != null )
				elog.setAD_Session_ID(session.getAD_Session_ID());
			JSONObject values = cleanPOValues(po, newValues);
			JSONObject data = new JSONObject();
			data.put("eventtype", "po.insert");
			data.put("new_data", values);
			elog.setEventData(data);
			elog.saveEx(po.get_TrxName());
		}
	}

	/*
	 * Record update of PO in event log
	 */
	public static void logUpdatePO (PO po, HashMap<String, Object> newValues, HashMap<String, Object> oldValues, MSession session)
	{
		if ( isLogPO(po) )
		{
			MEventLog elog = new MEventLog(po.getCtx(), 0, po.get_TrxName());
			elog.setClientOrg(po);
			elog.setAD_Table_ID(po.p_info.getAD_Table_ID());
			elog.setRecord_ID(po.get_ID());
			elog.setEventType(MEventLog.EVENTTYPE_UpdatePO);
			if ( session != null )
				elog.setAD_Session_ID(session.getAD_Session_ID());
			JSONObject values = cleanPOValues(po, newValues);
			JSONObject oldvalues = cleanPOValues(po, oldValues);
			JSONObject data = new JSONObject();
			data.put("eventtype", "po.update");
			data.put("new_data", values);
			data.put("old_data", oldvalues);
			elog.setEventData(data);
			elog.saveEx(po.get_TrxName());
		}
	}

	/*
	 * Record the deletion of PO in event log
	 */
	public static void logDeletePO (PO po, HashMap<String, Object> oldValues, MSession session)
	{
		if ( isLogPO(po) )
		{
			MEventLog elog = new MEventLog(po.getCtx(), 0, po.get_TrxName());
			elog.setClientOrg(po);
			elog.setAD_Table_ID(po.p_info.getAD_Table_ID());
			elog.setRecord_ID(po.get_ID());
			elog.setEventType(MEventLog.EVENTTYPE_DeletePO);
			if ( session != null )
				elog.setAD_Session_ID(session.getAD_Session_ID());
			JSONObject values = cleanPOValues(po, oldValues);
			JSONObject data = new JSONObject();
			data.put("eventtype", "po.delete");
			data.put("old_data", values);
			elog.setEventData(data);
			elog.saveEx(po.get_TrxName());
		}
	}
	
	/*
	 * Check if PO table is loggable
	 */
	private static boolean isLogPO(PO po)
	{
		if ( MEventLog.Table_Name.equals(po.get_TableName()) )
			return false;
		else if ( MTable.get(po.getCtx(), po.get_Table_ID()).isChangeLog() )
			return true;
		else
			return false;
	}

	/*
	 * Create JSONObject from PO values, excluding confidential and not logged columns 
	 */
	private static JSONObject cleanPOValues(PO po, HashMap<String, Object> values) {

		JSONObject result = new JSONObject();

		for (Entry<String, Object> value : values.entrySet() )
		{
			int i = po.p_info.getColumnIndex(value.getKey());

			if (po.p_info.isAllowLogging(i)		//	logging allowed
					&& !po.p_info.isEncrypted(i)		//	not encrypted
					&& !po.p_info.isVirtualColumn(i)	//	no virtual column
					&& !"Password".equals(value.getKey()))
			{
				result.put(value.getKey(), value.getValue());
			}
		}

		return result;
	}

}
