package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.DB;

public class MFreightRegion extends X_M_FreightRegion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4541833111449682448L;

	public MFreightRegion(Properties ctx, int M_FreightRegion_ID, String trxName) {
		super(ctx, M_FreightRegion_ID, trxName);
	}

	public MFreightRegion(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public static MFreightRegion getByPostal(Properties ctx, int M_Shipper_ID, String postal)
	{

		String whereClause = "M_Shipper_ID=? AND M_FreightRegion_ID IN (SELECT l.M_FreightRegion_ID FROM " +
				"M_FreightRegion_Location l WHERE l.Postal = ? AND l.AD_Client_ID=AD_Client_ID) ";
		MFreightRegion region = new Query(ctx, Table_Name, whereClause, null)
										.setParameters(new Object[]{M_Shipper_ID, postal})
										.setOnlyActiveRecords(true)
										.first();
		return region;
	}
	
	public static MFreightRegion getByRegion(Properties ctx, int M_Shipper_ID, int C_Region_ID)
	{

		String whereClause = "M_Shipper_ID=? AND M_FreightRegion_ID IN (SELECT l.M_FreightRegion_ID FROM " +
				"M_FreightRegion_Location l WHERE l.C_Region_ID = ? AND l.AD_Client_ID=AD_Client_ID) ";
		MFreightRegion region = new Query(ctx, Table_Name, whereClause, null)
										.setParameters(new Object[]{M_Shipper_ID, C_Region_ID})
										.setOnlyActiveRecords(true)
										.first();
		return region;
	}

	public static MFreightRegion getForLocation(Properties ctx, int M_Shipper_ID, int C_Location_ID) {
		
		MLocation l = MLocation.get(ctx, C_Location_ID, null);
		
		if ( l == null )
			return null;
		
		MFreightRegion region = null;
		if ( l.getPostal() != null )
			region = getByPostal(ctx, M_Shipper_ID, l.getPostal());
		
		if ( region == null && l.getC_Region_ID() != 0 )
			region = getByRegion(ctx, M_Shipper_ID, l.getC_Region_ID());
			
		return region;
	}
	
	@Override
	protected boolean beforeDelete() {
		
		
		String sql = "DELETE FROM M_FreightRegion_Location WHERE M_FreightRegion_ID = ?";
		int no = DB.executeUpdateEx(sql, new Object[] {getM_FreightRegion_ID()}, get_TrxName());
		log.log(Level.FINE, "Deleted " + no + " locations");
		
		return true;
	}
}
