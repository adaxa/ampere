/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * Postgresql Table Attribute model
 */
public class MTableAttribute extends X_AD_Table_Attribute
{

	private static final long serialVersionUID = -4476205391859124186L;
	
	/**
	 * 	Get MTableAttribute from Cache
	 *	@param ctx context
	 * 	@param AD_Table_Attribute_ID id
	 *	@return MTableAttribute
	 */
	public static MTableAttribute get (Properties ctx, int AD_Table_Attribute_ID)
	{
		Integer key = AD_Table_Attribute_ID;
		MTableAttribute retValue = (MTableAttribute) s_cache.get (key);
		if (retValue != null)
			return retValue;
		retValue = new MTableAttribute (ctx, AD_Table_Attribute_ID, null);
		if (retValue.get_ID () != 0)
			s_cache.put (key, retValue);
		return retValue;
	}	//	get
	
	/**
	 * 
	 * 
	 */
	/**	Cache						*/
	private static CCache<Integer,MTableAttribute>	s_cache	= new CCache<Integer,MTableAttribute>("AD_Table_Attribute", 200);
	
	/**	Static Logger	*/
	private static CLogger	s_log	= CLogger.getCLogger (MTableAttribute.class);
	
	/**************************************************************************
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param AD_Table_Attribute_ID
	 *	@param trxName transaction
	 */
	public MTableAttribute (Properties ctx, int AD_Table_Attribute_ID, String trxName)
	{
		super (ctx, AD_Table_Attribute_ID, trxName);
		if (AD_Table_Attribute_ID == 0)
		{
		}
	}	//	MTableAttribute

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MTableAttribute (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MTableAttribute
	
	/**
	 * 	Parent Constructor
	 *	@param parent table
	 */
	public MTableAttribute (MTable parent)
	{
		this (parent.getCtx(), 0, parent.get_TrxName());
		setClientOrg(parent);
		setAD_Table_ID (parent.getAD_Table_ID());
	}	//	MTableAttribute
	

	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{		
		setName(getName().toLowerCase());
		
		if ( TABLEATTRIBUTETYPE_UniqueConstraint.equals(getTableAttributeType()) 
				&& !getTableAttributeDefinition()
				.toUpperCase().trim().startsWith("UNIQUE") )
		{
			throw new AdempiereException("Unique Constraint must start with UNIQUE");
		}
		else if ( TABLEATTRIBUTETYPE_ForeignKey.equals(getTableAttributeType()) 
				&& !getTableAttributeDefinition()
				.toUpperCase().trim().startsWith("FOREIGN KEY") )
		{
			throw new AdempiereException("Foreign Key Constraint must start with FOREIGN KEY");
		}
		else if ( TABLEATTRIBUTETYPE_PrimaryKey.equals(getTableAttributeType()) 
				&& !getTableAttributeDefinition()
				.toUpperCase().trim().startsWith("PRIMARY KEY") )
		{
			throw new AdempiereException("Primary Key Constraint must start with PRIMARY KEY");
		}
		else if ( TABLEATTRIBUTETYPE_CheckConstraint.equals(getTableAttributeType()) 
				&& !getTableAttributeDefinition()
				.toUpperCase().trim().startsWith("CHECK") )
		{
			throw new AdempiereException("Check Constraint must start with CHECK");
		}
		else if ( TABLEATTRIBUTETYPE_ExclusionConstraint.equals(getTableAttributeType()) 
				&& !getTableAttributeDefinition()
				.toUpperCase().trim().startsWith("EXCLUDE") )
		{
			throw new AdempiereException("Exclusion Constraint must start with EXCLUDE");
		}
		return true;
	}	//	beforeSave

	
	/**
	 * 	After Save
	 *	@param newRecord new
	 *	@param success success
	 *	@return success
	 */
	protected boolean afterSave (boolean newRecord, boolean success)
	{
		if ( newRecord )
		{
			DB.executeUpdateEx(getCreateDDL(), get_TrxName());
		}
		
		return success;
	}	//	afterSave

	/**
	 * 	Get existing database DDL
	 *	@return DDL or null
	 */
	public String getExistingDDL()
	{
		String DDL = null;
		MTable t = MTable.get(getCtx(), getAD_Table_ID());
		
		if ( getTableAttributeType().startsWith("I") )  // indexes
		{
			String sql = "SELECT position(reverse('.' || pi.tablename) in reverse(pi.indexdef)) - 1) "
					+ "FROM pg_indexes pi\n"
					+ "JOIN ad_table t ON (pi.tablename=lower(t.tablename))\n"
					+ "WHERE t.tablename = ? and pi.indexname = ? ";
			
			DDL = DB.getSQLValueString(get_TrxName(), sql,
					t.getTableName().toLowerCase(),
					getName().toLowerCase());
		}
		else  // constraints
		{
			String sql = "SELECT pg_get_constraintdef(oid)\n "
					+ "FROM  pg_constraint\n "
					+ "WHERE conrelid::regclass = ?::regclass "
					+ "AND conname = ?";
			
			DDL = DB.getSQLValueStringEx(null, sql,
					t.get_TableName().toLowerCase(),
					getName().toLowerCase());
		}
		
		return DDL;
	}

	/**
	 * 	Get DDL to create
	 *	@return create DDL
	 */
	public String getCreateDDL()
	{
		if ( getTableAttributeType().startsWith("I") )  // indexes
			return getCreateIndexDDL(false);
		else
			return getCreateConstraintDDL();
	}
	

	/**
	 * 	Get DDL to drop
	 *	@return drop DDL
	 */
	public String getDropDDL()
	{
		if ( getTableAttributeType().startsWith("I") )  // indexes
			return getDropIndexDDL(false);
		else
			return getDropConstraintDDL();
	}
	
	/**
	 * 	Get DDL to create Index
	 *	@return CREATE ... INDEX ... ON
	 */
	public String getCreateIndexDDL(boolean concurrently)
	{
		MTable t = MTable.get(getCtx(), getAD_Table_ID());
		StringBuilder sql = new StringBuilder ("CREATE INDEX ");

		if ( concurrently )
			sql.append("CONCURRENTLY ");

		sql.append("IF NOT EXISTS ")		
		.append(getName())
		.append(" ON ")
		.append(t.getTableName())
		.append(" ")
		.append(getTableAttributeDefinition());

		return sql.toString();
	}	//	getCreatIndexDDL	
	
	/**
	 * 	Get DDL to drop Index
	 *	@return DROP Index IF EXISTS...
	 */
	public String getDropIndexDDL(boolean concurrently)
	{
		StringBuilder sql = new StringBuilder ("DROP INDEX ");
		
		if ( concurrently )
			sql.append("CONCURRENTLY ");
		
		sql.append("IF EXISTS ")
		.append(getName());
		
		return sql.toString();
	}	//	getDropIndexDDL	
	
	/**
	 * 	Get DDL to create constraint
	 *	@return ALTER TABLE ADD CONSTRAINT ...
	 */
	public String getCreateConstraintDDL()
	{
		MTable t = MTable.get(getCtx(), getAD_Table_ID());
		StringBuilder sql = new StringBuilder ("ALTER TABLE ")
		.append(t.getTableName())
		.append( " ADD CONSTRAINT ")
		.append(getName())
		.append(" ")
		.append(getTableAttributeDefinition());
		
		return sql.toString();
	}	//	getCreateConstraintDDL	

	
	/**
	 * 	Get DDL to drop constraint
	 *	@return ALTER TABLE DROP CONSTRAINT ...
	 */
	public String getDropConstraintDDL()
	{
		MTable t = MTable.get(getCtx(), getAD_Table_ID());
		StringBuilder sql = new StringBuilder ("ALTER TABLE ")
		.append(t.getTableName())
		.append( " DROP CONSTRAINT ")
		.append("IF EXISTS ")
		.append(getName());
		
		return sql.toString();
	}	//	getDropConstraintDDL	
	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder ("MTableAttribute[");
		sb.append (get_ID()).append ("-").append (getName()).append ("]");
		return sb.toString ();
	}	//	toString

	public String syncDatabase() {
		
		String existing = getExistingDDL();
		StringBuilder ddl = new StringBuilder();
		
		if ( existing == null )
		{
			ddl.append(getCreateDDL());
			DB.executeUpdateEx(getCreateDDL(), get_TrxName());
		}
		else 
		{
			if ( !existing.equalsIgnoreCase(getTableAttributeDefinition()))
			{
				ddl.append(getDropDDL());
				DB.executeUpdateEx(getCreateDDL(), get_TrxName());
				
				ddl.append(";\n");
				ddl.append(getCreateDDL());
			}
			else
			{
				return "No changes";
			}
		}
		
		return ddl.toString();
	}
	
}	//	MTableAttribute
