/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;

/**
 *  Info Class for Lookup SQL (ValueObject)
 *
 * 	@author 	Jorg Janke
 * 	@version 	$Id: MLookupInfo.java,v 1.3 2006/07/30 00:58:37 jjanke Exp $
 */
public class MLookupInfo implements Serializable, Cloneable
{
	/**************************************************************************
	 *  Constructor.
	 * 	(called from MLookupFactory)
	 *  @param sqlQuery SQL query
	 *  @param tableName table name
	 *  @param keyColumn key column
	 *  @param zoomWindow zoom window
	 *  @param zoomWindowPO PO zoom window
	 *  @param zoomQuery zoom query
	 * @param isAlert 
	 */
	public MLookupInfo (String sqlQuery, String tableName, String keyColumn, 
		int zoomWindow, int zoomWindowPO, MQuery zoomQuery, boolean isAlert)
	{
		if (sqlQuery == null)
			throw new IllegalArgumentException("SqlQuery is null");
		Query = sqlQuery;
		if (keyColumn == null)
			throw new IllegalArgumentException("KeyColumn is null");
		TableName = tableName;
		KeyColumn = keyColumn;
		ZoomWindow = zoomWindow;
		ZoomWindowPO = zoomWindowPO;
		ZoomQuery = zoomQuery;
		IsAlert  = isAlert;
	}   //  MLookupInfo
	
	static final long serialVersionUID = -7958664359250070233L;

	/** SQL Query       */
	public String       Query = null;
	/** Table Name      */
	public String       TableName = "";
	/** Key Column      */
	public String       KeyColumn = "";
	/** Zoom Window     */
	public int          ZoomWindow;
	/** Zoom Window     */
	public int          ZoomWindowPO;
	/** Zoom Query      */
	public MQuery       ZoomQuery = null;

	/** Direct Access Query 	*/
	public String       QueryDirect = "";
	/** Parent Flag     */
	public boolean      IsParent = false;
	/** Key Flag     	*/
	public boolean      IsKey = false;
	/** Validation code */
	public String       ValidationCode = "";
	/** Validation flag */
	public boolean      IsValidated = true;
	
	/** Context			*/
	public Properties   ctx = null;
	/** WindowNo		*/
	public int          WindowNo;

	/**	AD_Column_Info or AD_Process_Para	*/
	public int          Column_ID;
	/** AD_Reference_ID						*/
	public int          DisplayType;
	/** Real AD_Reference_ID				*/
	public int 			AD_Reference_Value_ID;
	/** CreadtedBy/updatedBy					*/
	public boolean		IsCreatedUpdatedBy = false;

	public String		parsedValidationCode = "";

	public String		InfoFactoryClass = 	null;

	public boolean		IsAlert = false;

	public String		DisplayColumn;

	public String		SearchSQL = "";	

	/**
	 * String representation
	 * @return info
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer ("MLookupInfo[")
			.append(KeyColumn)
			.append("-Direct=").append(QueryDirect)
			.append("]");
		return sb.toString();
	}	//	toString

	/**
	 * 	Clone
	 *	@return deep copy
	 */
	public MLookupInfo cloneIt()
	{
		try
		{
			MLookupInfo clone = (MLookupInfo)super.clone();
			if ( ZoomQuery != null )
				clone.ZoomQuery = ZoomQuery.deepCopy();
			if ( ctx != null )
				clone.ctx = (Properties) ctx.clone();
			return clone;
		}
		catch (Exception e)
		{
			CLogger.get().log(Level.SEVERE, "", e);
		}
		return null;
	}	//	clone

}   //  MLookupInfo
