package org.compiere.model;

import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import org.compiere.print.MPrintFont;

public class MPrintFontLibrary extends X_AD_PrintFont_Library {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5152820816920988308L;

	public MPrintFontLibrary(Properties ctx, int AD_PrintFont_Library_ID, String trxName) {
		super(ctx, AD_PrintFont_Library_ID, trxName);
	}

	public MPrintFontLibrary(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	/**
	 * 	Get Invoice Lines of Invoice
	 * 	@param whereClause starting with AND
	 * 	@return lines
	 */
	public MPrintFont[] getFonts ()
	{
		String whereClauseFinal = "AD_PrintFont_Library_ID=? ";
		List<MPrintFont> list = new Query(getCtx(), I_AD_PrintFont.Table_Name, whereClauseFinal, get_TrxName())
										.setParameters(getAD_PrintFont_Library_ID())
										.setOrderBy(I_AD_PrintFont.COLUMNNAME_FontSize)
										.list();
		return list.toArray(new MPrintFont[list.size()]);
	}	//	getLines
}
