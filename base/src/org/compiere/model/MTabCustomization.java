package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;

public class MTabCustomization extends X_AD_Tab_Customization
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= -8856837077506056538L;

	public static final int		SUPERUSER			= 100;

	public MTabCustomization(Properties ctx, int AD_Tab_Customization_ID, String trxName)
	{
		super(ctx, AD_Tab_Customization_ID, trxName);
	}

	public MTabCustomization(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}

	public static MTabCustomization get(Properties ctx, int AD_User_ID, int AD_Tab_ID, String trxName)
	{
		if (MTable.get(ctx, Table_Name) == null) {
			return  null;
		}
		Query query = new Query(ctx, Table_Name, " AD_User_ID=? AND AD_Tab_ID=? ", trxName);
		query.setClient_ID();
		return query.setParameters(new Object[] { AD_User_ID, AD_Tab_ID }).first();
	}

	/**
	 * Save Tab Customization Data
	 * 
	 * @author Sachin Bhimani
	 * @param ctx
	 * @param AD_Tab_ID
	 * @param AD_User_ID
	 * @param Custom
	 * @param GridView
	 * @param trxName
	 * @return
	 */
	public static boolean saveData(Properties ctx, int AD_Tab_ID, int AD_User_ID, String Custom, String GridView,
			 String trxName)
	{
		return saveData(ctx, AD_Tab_ID, AD_User_ID, Custom, GridView, trxName, null);
	}

	/**
	 * Save Tab Customization Data
	 * 
	 * @param ctx
	 * @param AD_Tab_ID
	 * @param AD_User_ID
	 * @param Custom
	 * @param GridView
	 * @param trxName
	 * @param tabHeight
	 * @return
	 */
	public static boolean saveData(Properties ctx, int AD_Tab_ID, int AD_User_ID, String Custom, String GridView,
			String trxName, BigDecimal tabHeight)
	{
		MTabCustomization tabCust = get(ctx, AD_User_ID, AD_Tab_ID, trxName);

		if (tabCust != null && tabCust.getAD_Tab_Customization_ID() > 0)
		{
			tabCust.setcustom(Custom);
			tabCust.setIsDisplayedGrid(GridView);
			tabCust.setIncludedTabHeight(tabHeight);
		}
		else
		{
			tabCust = new MTabCustomization(ctx, 0, trxName);
			tabCust.setAD_Tab_ID(AD_Tab_ID);
			tabCust.setAD_User_ID(AD_User_ID);
			tabCust.setcustom(Custom);
			tabCust.setIsDisplayedGrid(GridView);
			tabCust.setIncludedTabHeight(tabHeight);
		}

		return tabCust.save();

	} // saveTabCustomization
	
	/**
	 * set info-window Customization Data
	 * 
	 * @param ctx
	 * @param AD_InfoWindow_ID
	 * @param AD_User_ID
	 * @param custom
	 * @param trxName
	 * @return
	 */
	public static boolean saveInfoData(Properties ctx, int AD_InfoWindow_ID, int AD_User_ID, String custom,
			String trxName)
	{
		return saveInfoData(ctx, AD_InfoWindow_ID, 0, AD_User_ID, custom, trxName);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_InfoWindow_ID
	 * @param AD_User_ID
	 * @param AD_Tab_ID 
	 * @param custom
	 * @param trxName
	 * @return
	 */
	public static boolean saveInfoData(Properties ctx, int AD_InfoWindow_ID, int AD_Tab_ID, int AD_User_ID, String custom,
			String trxName)
	{
		MTabCustomization tabCust = getInfo(ctx, AD_User_ID, AD_InfoWindow_ID, AD_Tab_ID, trxName);

		if (tabCust != null && tabCust.getAD_Tab_Customization_ID() > 0)
		{
			tabCust.setcustom(custom);
		}
		else
		{
			tabCust = new MTabCustomization(ctx, 0, trxName);
			tabCust.setAD_Org_ID(0);
			tabCust.setAD_InfoWindow_ID(AD_InfoWindow_ID);
			tabCust.setAD_User_ID(AD_User_ID);
			tabCust.setAD_Tab_ID(AD_Tab_ID);
			tabCust.setcustom(custom);
		}

		return tabCust.save();
	} // saveInfoData

	/**
	 * get info-window Customization Data
	 * 
	 * @param ctx
	 * @param AD_User_ID
	 * @param AD_InfoWindow_ID
	 * @param trxName
	 * @return
	 */
	public static MTabCustomization getInfo(Properties ctx, int AD_User_ID, int AD_InfoWindow_ID, String trxName)
	{
		return getInfo(ctx, AD_User_ID, AD_InfoWindow_ID, 0, trxName);
	}
	
	/**
	 * 
	 * @param ctx
	 * @param AD_User_ID
	 * @param AD_InfoWindow_ID
	 * @param AD_Tab_ID - subtab index in Info Window as presented, not persisted in the database
	 * @param trxName
	 * @return
	 */
	public static MTabCustomization getInfo(Properties ctx, int AD_User_ID, int AD_InfoWindow_ID, int AD_Tab_ID, String trxName)
	{
		if (MTable.get(ctx, Table_Name) == null)
		{
			return null;
		}
		Query query = new Query(ctx, Table_Name, " AD_User_ID=? AND AD_InfoWindow_ID=? AND COALESCE(AD_Tab_ID, 0)=?", trxName);
		query.setClient_ID();
		return query.setParameters(new Object[] { AD_User_ID, AD_InfoWindow_ID, AD_Tab_ID }).first();
	} // getInfo

}
