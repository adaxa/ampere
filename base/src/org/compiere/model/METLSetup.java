package org.compiere.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.compiere.util.DB;

public class METLSetup extends X_ETL_Setup {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3765999298841812333L;
	List<X_ETL_SetupLine>  m_SetupLines = null;
	public METLSetup(Properties ctx, int ETL_Setup_ID, String trxName) {
		super(ctx, ETL_Setup_ID, trxName);

		if (ETL_Setup_ID == 0) {
			
		}
	}

	public METLSetup(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}
	
	public void addSetupLine(String key, String value)
	{
		X_ETL_SetupLine newLine = new X_ETL_SetupLine(getCtx(), 0, get_TrxName());
		newLine.setETL_Setup_ID(getETL_Setup_ID());
		newLine.setAD_Client_ID(getAD_Client_ID());
		newLine.setAD_Org_ID(getAD_Org_ID());
		newLine.setName(key);
		newLine.setValue(value);
		newLine.saveEx();
		if (m_SetupLines == null) {
			m_SetupLines = new ArrayList<X_ETL_SetupLine>();
		}
		m_SetupLines.add(newLine);
	}

	public List<X_ETL_SetupLine> getLines()
	{
		return m_SetupLines;
	}

	public List<X_ETL_SetupLine> getSetupLines() {
		return m_SetupLines;
	}
	
	public void deleteSetupLines()
	{
		int no = DB.executeUpdate("DELETE FROM ETL_SetupLine WHERE ETL_Setup_ID=?", getETL_Setup_ID(), get_TrxName() );
		if (no > 0) {
			log.warning("Existing setup lines. " + no + " items deleted. ETL_Setup_ID=" + getETL_Setup_ID() );
		}
		m_SetupLines = new ArrayList<X_ETL_SetupLine>();
	}
	
	public static METLSetup get(Properties p_ctx, String trxName, String name, int AD_Org_ID)
	{
		METLSetup setup = new Query(p_ctx, METLSetup.Table_Name, "Name=? AND AD_Org_ID=?", trxName)
			.setParameters(name, AD_Org_ID)
			.first();
		
		return setup;
	}

	@Override
	protected boolean beforeDelete() {
		deleteSetupLines();
		return super.beforeDelete();
	}
}
