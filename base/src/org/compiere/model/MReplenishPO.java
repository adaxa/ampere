package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Properties;

public class MReplenishPO extends X_T_Replenish_PO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2213265440236013529L;
	private MProductPricing m_productPrice;

	public MReplenishPO(Properties ctx, int T_Replenish_PO_ID, String trxName) {
		super(ctx, T_Replenish_PO_ID, trxName);
	}

	public MReplenishPO(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

	public void setDefaultPOPrice(int M_Pricelist_ID) {
		BigDecimal price = null;
		MProductPricing pprice = getProductPricing(M_Pricelist_ID);
		
		price = pprice.getPriceList();
		if (price.intValue() == 0) {
			price = pprice.getPriceStd();
		}
		setPOPrice(price);
	}
	
	private MProductPricing getProductPricing (int M_PriceList_ID)
	{
		
		m_productPrice = new MProductPricing (getM_Product_ID(), 
			getC_BPartner_ID(), getPOQty(), false);
		m_productPrice.setM_PriceList_ID(M_PriceList_ID);
		Date date= new java.util.Date();
		m_productPrice.setPriceDate(new Timestamp(date.getTime()));
		//
		m_productPrice.calculatePrice();
		return m_productPrice;
	}	//	getProductPrice

}
