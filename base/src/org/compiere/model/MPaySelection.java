/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * AP Payment Selection
 *
 * @author Jorg Janke
 * @version $Id: MPaySelection.java,v 1.3 2006/07/30 00:51:03 jjanke Exp $
 */
public class MPaySelection extends X_C_PaySelection implements DocAction, DocOptions
{

	/**
     * 
     */
	private static final long			serialVersionUID	= -6521282913549455131L;

	/** Process Message */
	private String						processMessage		= null;
	/** Paid completely */
	private boolean						isPaidCompletely	= false;
	/** Used completely */
	private boolean						isUsedCompletely	= false;
	/** Is Paid */
	private boolean						isPaid				= false;
	/** Is Used */
	private boolean						isUsed				= false;
	/** Cache for validation for Lines Paid */
	private HashMap<Integer, Boolean>	linesPaid			= null;
	/** Cache for validation for Lines Used */
	private HashMap<Integer, Boolean>	linesUsed			= null;
	/** Just Prepared Flag */
	private boolean						justPrepared		= false;

	/**
	 * Default Constructor
	 *
	 * @param ctx context
	 * @param C_PaySelection_ID id
	 * @param trxName transaction
	 */
	public MPaySelection(Properties ctx, int C_PaySelection_ID, String trxName)
	{
		super(ctx, C_PaySelection_ID, trxName);
		if (C_PaySelection_ID == 0)
		{
			// setC_BankAccount_ID (0);
			// setName (null); // @#Date@
			// setPayDate (new Timestamp(System.currentTimeMillis())); //
			// @#Date@
			setTotalAmt(Env.ZERO);
			setIsApproved(false);
			setProcessed(false);
			setProcessing(false);
		}
	} // MPaySelection

	/**
	 * Load Constructor
	 *
	 * @param ctx context
	 * @param rs result set
	 * @param trxName transaction
	 */
	public MPaySelection(Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	} // MPaySelection

	/** Lines */
	private MPaySelectionLine[]		m_lines				= null;
	/** Currency of Bank Account */
	private int						m_C_Currency_ID		= 0;

	private List<MPaySelectionLine>	paySelectionLines	= new ArrayList<>();

	/**
	 * Get Lines
	 *
	 * @param requery requery
	 * @return lines
	 */
	public MPaySelectionLine[] getLines(boolean requery)
	{
		if (m_lines != null && !requery)
		{
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		// FR: [ 2214883 ] Remove SQL code and Replace for Query - red1
		final String whereClause = "C_PaySelection_ID=?";
		List<MPaySelectionLine> list = new Query(getCtx(), I_C_PaySelectionLine.Table_Name, whereClause, get_TrxName())
				.setParameters(getC_PaySelection_ID()).setOrderBy("Line").list();
		//
		m_lines = new MPaySelectionLine[list.size()];
		list.toArray(m_lines);
		return m_lines;
	} // getLines

	/**
	 * Get Lines with where clause and order by clause, re-query
	 * 
	 * @param whereClause
	 * @param orderClause
	 * @return
	 */
	public List<MPaySelectionLine> getLines(String whereClause, String orderClause)
	{
		// FR: [ 2214883 ] Remove SQL code and Replace for Query - red1
		StringBuffer whereClauseFinal = new StringBuffer(I_C_PaySelection.COLUMNNAME_C_PaySelection_ID + " = ? ");
		if (!Util.isEmpty(whereClause, true))
			whereClauseFinal.append(whereClause);
		if (Util.isEmpty(orderClause, true))
			orderClause = I_C_PaySelectionLine.COLUMNNAME_Line;
		paySelectionLines = new Query(getCtx(), I_C_PaySelectionLine.Table_Name, whereClauseFinal.toString(),
				get_TrxName()).setParameters(getC_PaySelection_ID()).setOrderBy(orderClause).list();
		return paySelectionLines;
	}// getLines

	/**
	 * Get Currency of Bank Account
	 *
	 * @return C_Currency_ID
	 */
	public int getC_Currency_ID()
	{
		if (m_C_Currency_ID == 0)
		{
			String sql = "SELECT C_Currency_ID FROM C_BankAccount " + "WHERE C_BankAccount_ID=?";
			m_C_Currency_ID = DB.getSQLValue(null, sql, getC_BankAccount_ID());
		}
		return m_C_Currency_ID;
	} // getC_Currency_ID

	/**
	 * String Representation
	 *
	 * @return info
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer("MPaySelection[");
		sb.append(get_ID()).append(",").append(getName()).append("]");
		return sb.toString();
	} // toString

	@Override
	public int customizeValidActions(String docStatus, Object processing, String orderType, String isSOTrx,
			int AD_Table_ID, String[] docAction, String[] options, int index)
	{
		// Valid Document Action
		if (AD_Table_ID == Table_ID)
		{
			if (docStatus.equals(DocumentEngine.STATUS_Drafted) || docStatus.equals(DocumentEngine.STATUS_InProgress)
					|| docStatus.equals(DocumentEngine.STATUS_Invalid))
			{
				options[index++] = DocumentEngine.ACTION_Prepare;
			}
			// Complete .. CO
			else if (docStatus.equals(DocumentEngine.STATUS_Completed))
			{
				options[index++] = DocumentEngine.ACTION_ReActivate;
				options[index++] = DocumentEngine.ACTION_Void;
			}
		}
		// Default
		return index;
	}

	@Override
	public boolean processIt(String action) throws Exception
	{
		processMessage = null;
		DocumentEngine engine = new DocumentEngine(this, getDocStatus());
		return engine.processIt(action, getDocAction());
	}

	@Override
	public boolean unlockIt()
	{
		log.info("unlockIt - " + toString());
		// setProcessing(false);
		return true;
	}

	@Override
	public boolean invalidateIt()
	{
		log.info("invalidateIt - " + toString());
		setDocAction(DOCACTION_Prepare);
		return true;
	}

	@Override
	public String prepareIt()
	{
		log.info(toString());
		processMessage = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (processMessage != null)
			return DocAction.STATUS_Invalid;

		MDocType docType = MDocType.get(getCtx(), getC_DocType_ID());

		// Std Period open?
		if (!MPeriod.isOpen(getCtx(), getDateDoc(), docType.getDocBaseType(), getAD_Org_ID()))
		{
			processMessage = "@PeriodClosed@";
			return DocAction.STATUS_Invalid;
		}
		// Add up Amounts
		processMessage = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (processMessage != null)
			return DocAction.STATUS_Invalid;
		justPrepared = true;
		if (!DOCACTION_Complete.equals(getDocAction()))
			setDocAction(DOCACTION_Complete);
		return DocAction.STATUS_InProgress;
	}

	@Override
	public boolean approveIt()
	{
		log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}

	@Override
	public boolean rejectIt()
	{
		log.info("rejectIt - " + toString());
		setIsApproved(false);
		return true;
	}

	@Override
	public String completeIt()
	{
		// Re-Check
		if (!justPrepared)
		{
			String status = prepareIt();
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		processMessage = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (processMessage != null)
			return DocAction.STATUS_Invalid;

		// Implicit Approval
		if (!isApproved())
			approveIt();
		log.info(toString());
		// Validate Amount
		if (getTotalAmt() == null || getTotalAmt().equals(Env.ZERO))
		{
			processMessage = "@TotalAmt@ = 0";
			return DocAction.STATUS_Invalid;
		}
		// Validate conversion
		// String erroMsg = validateConversion(); TODO ask to keep
		// if (erroMsg != null)
		// {
		// processMessage = erroMsg;
		// return DocAction.STATUS_Invalid;
		// }
		// Create Checks
		createChecks();
		// User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			processMessage = valid;
			return DocAction.STATUS_Invalid;
		}
		// Set Definitive Document No
		setDefiniteDocumentNo();

		setProcessed(true);
		setDocAction(DOCACTION_Close);
		return DocAction.STATUS_Completed;
	}

	@Override
	public boolean voidIt()
	{
		log.info("voidIt - " + toString());
		// Validate if is paid
		if (isPaid())
			throw new AdempiereException("@C_PaySelection_ID@ @IsPaid@");
		//
		setPaymentToZero();
		setProcessed(true);
		setDocAction(DOCACTION_None);
		setDocStatus(DOCSTATUS_Voided);
		return closeIt();
	}

	@Override
	public boolean closeIt()
	{
		log.info("closeIt - " + toString());

		// Close Not delivered Qty
		setDocAction(DOCACTION_None);
		return true;
	}

	@Override
	public boolean reverseCorrectIt()
	{
		log.info("reverseCorrectIt - " + toString());
		return false;
	}

	@Override
	public boolean reverseAccrualIt()
	{
		log.info("reverseAccrualIt - " + toString());
		return false;
	}

	@Override
	public boolean reActivateIt()
	{
		log.info("reActivateIt - " + toString());
		// Validate if is paid
		if (isPaid())
			throw new AdempiereException("@C_PaySelection_ID@ @IsPaid@");
		//
		setProcessed(false);
		// Delete check
		deleteChecks();
		return true;
	}
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return save
	 */
	protected boolean beforeSave (boolean newRecord)
	{

		// Bank Account Is mandatory
		if ( getC_BankAccount_ID() <= 0 ) {
			log.saveError("Error", Msg.parseTranslation(getCtx(), "@Mandatory@: @C_BankAccount_ID@"));
			return false;
		}
		
		//	Document Type/Receipt
		if (getC_DocType_ID() == 0)
			setC_DocType_ID();

		if (getC_DocType_ID() == 0)
		{
			log.saveError("Error", Msg.parseTranslation(getCtx(), "@Mandatory@: @C_DocType_ID@"));
			return false;
		}

		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		setIsSOTrx(dt.isSOTrx());

		//
		return true;
	}	//	beforeSave

	/**
	 * 	Set Doc Type bases on IsReceipt
	 */
	private void setC_DocType_ID ()
	{
		setC_DocType_ID(isSOTrx());
	}	//	setC_DocType_ID

	/**
	 * 	Set Doc Type
	 * 	@param isReceipt is receipt
	 */
	public void setC_DocType_ID (boolean isSOTrx)
	{
		setIsSOTrx(isSOTrx);
		String sql = "SELECT C_DocType_ID "
				+ "FROM C_DocType "
				+ "WHERE IsActive='Y' "
				+ "AND AD_Client_ID=? AND "
				+ "DocBaseType=? "
				+ "ORDER BY IsDefault DESC";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, get_TrxName());
			pstmt.setInt(1, getAD_Client_ID());
			if (isSOTrx)
				pstmt.setString(2, X_C_DocType.DOCBASETYPE_ARPaymentSelection);
			else
				pstmt.setString(2, X_C_DocType.DOCBASETYPE_APPaymentSelection);
			rs = pstmt.executeQuery();
			if (rs.next())
				setC_DocType_ID(rs.getInt(1));
			else
				log.warning ("setDocType - NOT found - isSOTrx=" + isSOTrx);
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
	}	//	setC_DocType_ID

	@Override
	public String getSummary()
	{
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(getDocumentNo());
		// - Description
		if (getDescription() != null && getDescription().length() > 0)
			stringBuffer.append(" - ").append(getDescription());
		return stringBuffer.toString();
	}

	@Override
	public String getDocumentInfo()
	{
		MDocType docType = MDocType.get(getCtx(), getC_DocType_ID());
		return docType.getName() + " " + getDocumentNo();
	}

	@Override
	public File createPDF()
	{
		try
		{
			File temp = File.createTempFile(get_TableName() + get_ID() + "_", ".pdf");
			return createPDF(temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}

	@Override
	public String getProcessMsg()
	{
		return processMessage;
	}

	@Override
	public int getDoc_User_ID()
	{
		return 0;
	}

	@Override
	public BigDecimal getApprovalAmt()
	{
		return null;
	}

	/**
	 * Create PDF file
	 *
	 * @param file output file
	 * @return file if success
	 */
	public File createPDF(File file)
	{
		// ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE,
		// getC_Invoice_ID());
		// if (re == null)
		return null;
		// return re.getPDF(file);
	} // createPDF

	/**
	 * Verify if is completely payment
	 * 
	 * @return
	 */
	public boolean isCompletelyPaid()
	{
		// Load
		verifyIsPaid();
		return isPaidCompletely;
	}

	/**
	 * Verify if is not paid
	 */
	private void verifyIsPaid()
	{
		if (linesPaid != null)
			return;
		// Get
		linesPaid = new HashMap<Integer, Boolean>();
		// Get from Validation
		KeyNamePair[] pairs = DB.getKeyNamePairs("SELECT psl.C_PaySelectionLine_ID, " 
				+ "(CASE WHEN COALESCE(SUM(CASE WHEN p.DocStatus NOT IN('VO', 'RE') " 
				+ "							    THEN 1 "
				+ "							    ELSE 0 END), 0) > 0 " 
				+ "		THEN 'Y' ELSE 'N' END) AS IsPaid " 
				+ "FROM C_PaySelectionLine psl "
				+ "LEFT JOIN C_PaySelectionCheck psc ON(psc.C_PaySelectionCheck_ID = psl.C_PaySelectionCheck_ID) "
				+ "LEFT JOIN C_Payment p ON(p.C_Payment_ID = psc.C_Payment_ID) " + "WHERE psl.C_PaySelection_ID = ? "
				+ "GROUP BY psl.C_PaySelectionLine_ID", false, getC_PaySelection_ID());
		// Get Values
		boolean paidCompletely = true;
		boolean paid = false;
		for (KeyNamePair pair : pairs)
		{
			linesPaid.put(pair.getKey(), pair.getName().equals("Y"));
			// Fill Use
			if (!pair.getName().equals("Y") && paidCompletely)
			{
				paidCompletely = false;
			}
			else if (pair.getName().equals("Y") && !paid)
			{
				paid = true;
			}
		}
		//
		isPaidCompletely = paid && paidCompletely;
		isPaid = paid;
	}

	/**
	 * Delete Checks if the payments is not generated
	 */
	private void deleteChecks()
	{
		for (MPaySelectionCheck paySelectionCheck : MPaySelectionCheck.get(getCtx(), getC_PaySelection_ID(),
				get_TrxName()))
		{
			// Valid Payment
			if (paySelectionCheck.getC_Payment_ID() != 0)
			{
				MPayment payment = new MPayment(getCtx(), paySelectionCheck.getC_Payment_ID(), get_TrxName());
				if (!payment.getDocStatus().equals(X_C_Payment.DOCSTATUS_Voided)
						&& !payment.getDocStatus().equals(X_C_Payment.DOCSTATUS_Reversed))
				{
					continue;
				}
			}
			// Update reference
			DB.executeUpdate("UPDATE C_PaySelectionLine " + "SET C_PaySelectionCheck_ID = NULL "
					+ "WHERE C_PaySelectionCheck_ID = ?", paySelectionCheck.getC_PaySelectionCheck_ID(), get_TrxName());
			// Delete Check
			paySelectionCheck.deleteEx(true);
		}
	}

	/**
	 * Verify if is paid
	 * 
	 * @return
	 */
	public boolean isPaid()
	{
		verifyIsPaid();
		return isPaid;
	}

	/**
	 * Verify if a line is paid
	 * 
	 * @param paySelectionLineId
	 * @return
	 */
	private boolean isPaid(int paySelectionLineId)
	{
		// Load
		verifyIsPaid();
		Boolean isPaid = linesPaid.get(paySelectionLineId);
		//
		if (isPaid == null)
			return false;
		// default not nul
		return isPaid;
	}

	/**
	 * Create Check from line
	 * 
	 * @throws Exception for invalid bank accounts FR [ 297 ] Copied from
	 *             org.compiere.process.PaySelectionCreateCheck
	 */
	private void createChecks()
	{
		// Don't have account
		if (getC_BankAccount_ID() == 0)
			return;
		// Values
		// int chargeId = 0;
		// boolean isPrepayment = false;
		MPaySelectionCheck paySelectionCheck = null;
		List<MPaySelectionLine> paySelectionLines = getLines(null, "C_BPartner_ID, PaymentRule");
		// Try to find one
		for (MPaySelectionLine paySelectionLine : paySelectionLines)
		{
			// already check reference
			if (paySelectionLine.getC_PaySelectionCheck_ID() != 0)
				continue;
			// Instance new
			if (paySelectionCheck == null
					|| paySelectionCheck.getC_BPartner_ID() != paySelectionLine.getC_BPartner_ID()
					|| !paySelectionCheck.getPaymentRule().equals(paySelectionLine.getPaymentRule()))
			{
				paySelectionCheck = new MPaySelectionCheck(paySelectionLine, paySelectionLine.getPaymentRule());
			}
			else
			{
				paySelectionCheck.addLine(paySelectionLine);
			}
			// Save and reference
			paySelectionCheck.saveEx();
			paySelectionLine.setC_PaySelectionCheck_ID(paySelectionCheck.getC_PaySelectionCheck_ID());
			paySelectionLine.saveEx();
		}
	} // createCheck

	/**
	 * Set the definite document number after completed
	 */
	private void setDefiniteDocumentNo()
	{
		MDocType docType = MDocType.get(getCtx(), getC_DocType_ID());
		if (docType.isOverwriteDateOnComplete())
		{
			setDateDoc(new Timestamp(System.currentTimeMillis()));
		}
		if (docType.isOverwriteSeqOnComplete())
		{
			String value = null;
			String colName = null;
			if (p_info.getColumnIndex("C_DocType_ID") > -1 )
				colName = "C_DocType_ID";
			if ( p_info.getColumnIndex("C_DocTypeTarget_ID") > -1 )
				colName = "C_DocTypeTarget_ID";
			if (colName != null) // get based on Doc Type (might return null)
				value = DB.getDocumentNo(get_ValueAsInt(colName), get_TrxName(), true);
			if (value != null)
			{
				setDocumentNo(value);
			}
		}
	}

	/**
	 * 	Add to Description
	 *	@param description text
	 */
	public void addDescription (String description)
	{
		String desc = getDescription();
		if (desc == null)
			setDescription(description);
		else
			setDescription(desc + " | " + description);
	}	//	addDescription

	private String setPaymentToZero()
	{

		if (zeroPaySelectionLines())
		{
			setTotalAmt(Env.ZERO);
			addDescription(Msg.getMsg(getCtx(), "Voided"));
			saveEx();
		}
		return null;
	}

	private boolean zeroPaySelectionLines()
	{
		for ( MPaySelectionLine line: getLines(true) )
		{
			line.setPayAmt(Env.ZERO);
			line.setC_PaySelectionCheck_ID(0);
			line.saveEx();
		}
		
		for (MPaySelectionCheck paySelectionCheck : MPaySelectionCheck.get(getCtx(), getC_PaySelection_ID(),
				get_TrxName()))
		{
			paySelectionCheck.setPayAmt(Env.ZERO);
			paySelectionCheck.saveEx();
		}
		return true;
	}

	/**
	 * Set Processed to Line and header
	 */
	public void setProcessed(boolean processed)
	{
		super.setProcessed(processed);
		// Change Processed in line
		for (MPaySelectionLine paySelectionLine : getLines(true))
		{
			// isUsed(paySelectionLine.getC_PaySelectionLine_ID()) ||
			if (!processed && (isPaid(paySelectionLine.getC_PaySelectionLine_ID())))
				continue;
			//
			paySelectionLine.setProcessed(processed);
			paySelectionLine.saveEx();
		}
	}

	/**
	 * Get Last line No
	 * 
	 * @return
	 */
	public int getLastLineNo()
	{
		int lastLineNo = DB.getSQLValue(get_TrxName(), "SELECT COALESCE(MAX(psl.line),0) As Line "
				+ "FROM C_PaySelectionLine psl " + "WHERE psl.C_PaySelection_ID = ?", getC_PaySelection_ID());
		if (lastLineNo == -1)
			lastLineNo = 0;
		// Return
		return lastLineNo;
	}

} // MPaySelection
