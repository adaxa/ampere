/**
 * 
 */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 * @author Ashley G Ramdass
 * 
 */
public class MGAASAssetJournalLineDet extends X_GAAS_AssetJournalLine_Det
{
    private static final long serialVersionUID = 5746969514183310119L;
    private static final CLogger logger = CLogger
            .getCLogger(MGAASAssetJournalLineDet.class);

    public MGAASAssetJournalLineDet(Properties ctx,
            int GAAS_AssetJournalLine_Detail_ID, String trxName)
    {
        super(ctx, GAAS_AssetJournalLine_Detail_ID, trxName);
    }

    public MGAASAssetJournalLineDet(Properties ctx, ResultSet rs, String trxName)
    {
        super(ctx, rs, trxName);
    }
    
    protected boolean beforeSave(boolean newRecord)
    {
        return true;
    }
    
    public BigDecimal getQty()
    {
        if (getC_InvoiceLine_ID() <= 0)
        {
            return Env.ZERO;
        }
        
        String sql = "SELECT QtyInvoiced FROM C_InvoiceLine " +
                "WHERE C_InvoiceLine_ID=?";
        
        return DB.getSQLValueBD(null, sql, getC_InvoiceLine_ID());
    }

    @Override
    protected boolean afterSave(boolean newRecord, boolean success)
    {
        if (!success)
            return success;
        return updateLine();
    } // afterSave

    private boolean updateLine()
    {

        String sql = "SELECT COALESCE(SUM(ajld.CostAmt),0),"
                + " COALESCE(SUM(ajld.AccumulatedDepreciationAmt),0)"
                + " FROM GAAS_AssetJournalLine_Det ajld"
                + " WHERE ajld.GAAS_AssetJournalLine_ID=? AND ajld.IsActive='Y'";

        BigDecimal costSum = Env.ZERO;
        BigDecimal accumDeprSum = Env.ZERO;

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getGAAS_AssetJournalLine_ID());
            rs = pstmt.executeQuery();

            if (rs.next())
            {
                costSum = rs.getBigDecimal(1);
                accumDeprSum = rs.getBigDecimal(2);
            }
        }
        catch (Exception ex)
        {
            logger.log(Level.SEVERE, "Could not get sum of lines details", ex);
            return false;
        }
        finally
        {
            DB.close(rs, pstmt);
        }

        MGAASAssetJournalLine journalLine = new MGAASAssetJournalLine(getCtx(),
                getGAAS_AssetJournalLine_ID(), get_TrxName());
        journalLine.setCostAmt(costSum);
        journalLine.setAccumulatedDepreciationAmt(accumDeprSum);
        journalLine.saveEx();

        return true;
    }
}
