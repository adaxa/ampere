package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MFreightRegionLocation extends X_M_FreightRegion_Location {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6095084394794054576L;

	public MFreightRegionLocation(Properties ctx,
			int M_FreightRegion_Location_ID, String trxName) {
		super(ctx, M_FreightRegion_Location_ID, trxName);
	}

	public MFreightRegionLocation(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
