/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.compiere.model.I_M_Replenish;
import org.compiere.model.MPriceList;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPO;
import org.compiere.model.MProductPrice;
import org.compiere.model.MReplenish;
import org.compiere.model.Query;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;

/**
 *	Generate Default Product Replenishment
 *
 *	@author jobriant	
 */
public class CreateDefaultProductReplenishment extends SvrProcess
{
	
	/** Static Logger 				*/
	private static CLogger s_log = CLogger.getCLogger(CreateDefaultProductReplenishment.class);
	
	/** Product Category		*/
	private int			p_M_Product_Category_ID = 0;
	/** Update Existing				*/
	private static boolean		p_UpdateExisting = false;		

	private int AD_Client_ID = 0 ; 

	/** The Query sql			*/
	private String 		m_sql = null;


	private static int created = 0;
	private static int updated = 0;
	private static int createdPO = 0;
	private static int updatedPO = 0;
	private String where = null;

	private static MProductPO templatePO[] = null;
	private static List<MReplenish> templateReplenish = null;
	private static MProduct templateProduct = null;
	
	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else if (name.equals("M_Product_Category_ID"))
				p_M_Product_Category_ID = para[i].getParameterAsInt();
			else if (name.equals("UpdateExisting"))
				p_UpdateExisting = "Y".equals(para[i].getParameter());			
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
			
		}
		AD_Client_ID = Env.getAD_Client_ID(getCtx());
		where  = "ISACTIVE = 'Y' AND AD_CLIENT_ID = " +  AD_Client_ID;

	}	//	prepare

	/**
	 * 	Generate Shipments
	 *	@return info
	 *	@throws Exception
	 */
	protected String doIt () throws Exception
	{
		log.info("M_Product_Category_ID=" + p_M_Product_Category_ID);
		if (p_M_Product_Category_ID == 0) {
			int ids[] = MProductCategory.getAllIDs("M_Product_Category", where,  get_TrxName());
			for (int id : ids) {
				resetCategory(id);
			}
		}
		else {
			resetCategory(p_M_Product_Category_ID);
		}
		
		addLog("Replenishment Records Created = " + created);
		addLog("Replenishment Records Updated = " + updated);
		addLog("Product Purchasing Records Created = " + createdPO);
		addLog("Product Purchasing Records Updated = " + updatedPO);
		return "@OK@";


	}	//	doIt
	
	
	private void resetCategory (int M_Product_Category_ID) throws AdempiereUserError {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "SELECT m_product_id, value FROM m_product WHERE m_product_category_id = ?";
		pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
		int templateProductID = DB.getSQLValue(null, 
				"SELECT COALESCE(pc.M_ProductTemplate_ID,o.M_ProductTemplate_ID, c.M_ProductTemplate_ID, 0) " +
				"FROM M_Product_Category pc " + 
				"INNER JOIN AD_ClientInfo c ON pc.ad_client_id=c.ad_client_id " +
				"LEFT JOIN AD_OrgInfo o ON pc.ad_org_id=o.ad_org_id " +
				"WHERE pc.M_Product_Category_ID=?", M_Product_Category_ID);
		
		if (templateProductID == 0) {
			throw new AdempiereUserError("No Product Template defined at least for Client");
		}

		templatePO = MProductPO.getOfProduct(Env.getCtx(), templateProductID, null);
		templateReplenish = getForProduct(Env.getCtx(), templateProductID, null);

		try {
			pstmt.setInt(1, M_Product_Category_ID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				
				resetProduct(rs.getInt(1),rs.getString(2), null);
			}
		} catch (SQLException e) {
			log.severe(e.getMessage());
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;

		}
	}
	
	public static void resetProduct(int M_Product_ID, String pvalue, String trxName) throws AdempiereUserError {
		//MProductPO
		if (templateReplenish == null || templatePO == null) {
			int templateProductID  = 0;
			templateProductID = DB.getSQLValue(null, 
					"SELECT COALESCE(pc.M_ProductTemplate_ID, o.M_ProductTemplate_ID, c.M_ProductTemplate_ID, 0) " +
					"FROM M_Product_Category pc INNER JOIN AD_ClientInfo c ON pc.ad_client_id=c.ad_client_id " +
					"INNER JOIN M_Product p ON p.M_Product_Category_ID = pc.M_Product_Category_ID " +
					"LEFT JOIN AD_OrgInfo o ON p.ad_org_id=o.ad_org_id " +
					"WHERE p.M_Product_ID=?", M_Product_ID);

			if (templateProductID <= 0) {
				//throw new AdempiereUserError("No Product Template defined at least for Client");
				return;
			}
			templatePO = MProductPO.getOfProduct(Env.getCtx(), templateProductID, null);
			templateReplenish = getForProduct(Env.getCtx(), templateProductID, null);
		}
		if (templateReplenish.size() > 0) {
			resetProductReplenishment(M_Product_ID, trxName);
		}
		if (templatePO.length > 0) {
			resetProductPO(M_Product_ID, pvalue, trxName);
		}
		
	}
	public static void setProductTemplate(int templateProductID) {
		if (templateProductID == 0) {
			templatePO = null;
			templateReplenish = null;
			return;
		}
			
		if (templatePO == null || templatePO.length == 0 ) {
			templatePO = MProductPO.getOfProduct(Env.getCtx(), templateProductID, null);
		}
		if (templateReplenish == null || templateReplenish.size() == 0 ) {
			templateReplenish = getForProduct(Env.getCtx(), templateProductID, null);
		}
	}
	
	private static void resetProductPO(int M_Product_ID, String pvalue, String trxName) {
		MProductPO po[] =MProductPO.getOfProduct(Env.getCtx(), M_Product_ID, trxName);
		
		if (po.length > 0 && !p_UpdateExisting) {
			return;
		}


		//create new
		if (po.length == 0) {
			int i=1;
			for (MProductPO from : templatePO) {
				if (from.isActive()) {
					MProductPO to = null;
					if (to == null) {
						to = new MProductPO(Env.getCtx(), 0, trxName);
						to.setM_Product_ID(M_Product_ID);
						if (pvalue != null) {
							to.setVendorProductNo(pvalue + "-" + 1);
							i++;
						}
					}
					copyProductPO(from, to);
					//add to purchase pricelist
					if (to.getC_BPartner() != null
							&& to.getC_BPartner().getPO_PriceList() != null) {
						MPriceList pl = (MPriceList) to.getC_BPartner().getPO_PriceList();
						if (pl == null) {
							s_log.severe("NPE - Price List is null");
							
						} else {
							MProductPrice pp = null;
							pp = MProductPrice.get(Env.getCtx()
									, pl.getPriceListVersion(null).getM_PriceList_Version_ID(), M_Product_ID, trxName);
							if (pp == null) {
								pp = new MProductPrice(Env.getCtx()
										, pl.getPriceListVersion(null).getM_PriceList_Version_ID(), M_Product_ID, trxName);
							}
							BigDecimal dp = new BigDecimal(0.01);
							pp.setPrices(dp, dp, dp);
							pp.save();
						}

					}
					createdPO++;
				}
			}
			return;
		}
		//update existing
		for (MProductPO to : po) {
			for (MProductPO from : templatePO) {
				if (to.getC_BPartner_ID() == from.getC_BPartner_ID() && from.isActive()) {
					copyProductPO(from, to);
					updatedPO++;
				}
			}
		}		
	} 	
	
	private static void resetProductReplenishment(int M_Product_ID, String trxName) {
		List<MReplenish> po = getForProduct(Env.getCtx(), M_Product_ID, trxName);
		
		if (po.size() > 0 && !p_UpdateExisting) {
			return;
		}

		//create new
		if (po.size() == 0) {
			for (MReplenish from : templateReplenish) {
				if (from.isActive()) {
					MReplenish to = new MReplenish(Env.getCtx(), 0, trxName);
					to.setM_Product_ID(M_Product_ID);
					copyReplenishment(from, to);
					created++;
				}
			}
			return;
		}
		//update existing
		for (MReplenish to : po) {
			for (MReplenish from : templateReplenish) {
				if (to.getM_Warehouse_ID() == from.getM_Warehouse_ID() && from.isActive()) {
					copyReplenishment(from, to);
					updated++;
				}
			}
		}

	}
	
	
	private static void copyProductPO (MProductPO from, MProductPO to) {
		to.setAD_Client_ID(from.getAD_Client_ID());
		to.setAD_Org_ID(from.getAD_Org_ID());
		to.setIsActive(from.isActive());
		to.setC_BPartner_ID(from.getC_BPartner_ID());
		to.setC_Currency_ID(from.getC_Currency_ID());
		to.setC_UOM_ID(from.getC_UOM_ID());
		to.setOrder_Min(from.getOrder_Min());
		to.setOrder_Pack(from.getOrder_Pack());
		to.setIsCurrentVendor(from.isCurrentVendor());
		to.setOrder_Min_PO(from.getOrder_Min_PO());
		to.set_ValueOfColumn("Order_Pack_PO", from.get_Value("Order_Pack_PO"));
//		to.setVendorProductNo("DEFAULT" + to.getM_Product_ID());
		try {
			to.save();
		}
		catch (Exception e) {
			s_log.severe(e.getMessage());
		}
	
	}
	
	private static void copyReplenishment(MReplenish from, MReplenish to) {
		to.setAD_Client_ID(from.getAD_Client_ID());
		to.setAD_Org_ID(from.getAD_Org_ID());
		to.setIsActive(from.isActive());
		to.setM_Warehouse_ID(from.getM_Warehouse_ID());
		to.setM_Locator_ID(from.getM_Locator_ID());
		to.setReplenishType(from.getReplenishType());
		to.setLevel_Max(from.getLevel_Max());
		to.setLevel_Min(from.getLevel_Min());
		to.setQtyBatchSize(from.getQtyBatchSize());
		to.setAX_Seasonality_ID(from.getAX_Seasonality_ID());
		try {
			to.save();
		}
		catch (Exception e) {
			s_log.severe(e.getMessage());
		}

	}
	
    private static List<MReplenish> getForProduct(Properties ctx, int M_ProductID, String trxName) {
    	final String whereClause= "M_Product_ID=? ";         
    	return new Query(ctx, I_M_Replenish.Table_Name, whereClause, trxName)
    	.setParameters(M_ProductID)
    	.setClient_ID()
    	.setOrderBy("AD_Org_ID")
    	.setOnlyActiveRecords(true)
    	.list();
    }
		
}	//	CreateDefaultProductReplenishment
