/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.process;

import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;

/**
 *	Set accounting date
 */
public class SetDateAcct extends SvrProcess
{
	
	/** Record ID of PO */
	private int m_Record_ID = 0;
	private int m_AD_Table_ID = 0;
	private Timestamp p_DateAcct = null;


	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null && para[i].getParameter_To() == null)
				;
			else if (name.equals("DateAcct"))
				p_DateAcct  = para[i].getParameterAsTimestamp();
			else if (name.equals("Record_ID"))
				m_Record_ID  = para[i].getParameterAsInt();
			else if (name.equals("AD_Table_ID"))
				m_AD_Table_ID   = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "prepare - Unknown Parameter: " + name);
		}
		
		if ( m_AD_Table_ID == 0 )
			m_AD_Table_ID = getTable_ID();
		
		if ( m_Record_ID == 0)
			m_Record_ID = getRecord_ID();
	}	//	prepare

	/**
	 *  Perform process.
	 *  @return Message
	 *  @throws Exception
	 */
	protected String doIt() throws java.lang.Exception
	{
		PO po = new MTable(getCtx(), m_AD_Table_ID, get_TrxName()).getPO(m_Record_ID, get_TrxName());
		
		if ( po.get_ColumnIndex("DateAcct") <= 0 )
		{
			throw new AdempiereSystemError("Record does not contain Date Account column");
		}
		if ( "Y".equals(po.get_ValueAsString("Posted")) )
		{
			throw new AdempiereSystemError("Cannot update accounting date on posted document, reset posting first");
		}
		if ( p_DateAcct == null )
		{
			throw new AdempiereUserError("Date Account required");
		}
		
		po.set_ValueOfColumn("DateAcct", p_DateAcct);
		po.saveEx();
		
		return "@OK@";
	}	//	doIt

}	//	update dateacct
