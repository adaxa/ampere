package org.compiere.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;

import org.compiere.model.MConvCurrency;
import org.compiere.model.MConversionRate;
import org.compiere.util.DB;

public class CreateCurrencyRate extends SvrProcess
{

	private int					m_Conversion_Rate_ID			= 0;

	private MConvCurrency		currow							= null;

	private final static String	SQL_CONVERT_FROMCURR_TO_TOCURR	= "SELECT xc.C_Currency_ID,xcr.C_Currency_ID,xcr.multiplyrate"
																		+ " FROM X_Conv_Currency xc INNER JOIN X_Conv_Currency_Rate xcr"
																		+ " ON xc.C_Currency_ID<>xcr.C_Currency_ID AND xc.X_Conv_Currency_ID=xcr.X_Conv_Currency_ID "
																		+ " AND xcr.isActive='Y' AND xc.X_Conv_Currency_ID=?";

	private final static String	SQL_CONVERT_TOCURR_TOCURR		= "SELECT xc.C_Currency_ID,xcr.C_Currency_ID,xc.multiplyrate,xcr.multiplyrate"
																		+ " FROM X_Conv_Currency_Rate xc"
																		+ " CROSS JOIN X_Conv_Currency_Rate xcr"
																		+ " WHERE xc.c_currency_id <> xcr.c_currency_id"
																		+ " AND xc.isActive='Y' AND xcr.isActive='Y' AND xc.x_conv_currency_id=? AND xcr.x_conv_currency_id=?"
																		+ " AND xc.AD_Client_ID=? AND xcr.AD_Client_ID=?";

	private final static String	SQL_CHECK_CONVERSTION_EXISTS	= "SELECT C_Conversion_Rate_ID FROM C_Conversion_Rate "
																		+ " WHERE C_Currency_ID= ? AND C_Currency_ID_to= ? AND validfrom= ? AND C_Conversiontype_ID=?";

	private final static String	SQL_UPDATE_EXISTING_CURRENCY	= " UPDATE C_Conversion_Rate SET"
																		+ " multiplyrate= ?, dividerate=?"
																		+ " Where C_Conversion_Rate_ID=?";

	@Override
	protected void prepare()
	{
		currow = new MConvCurrency(getCtx(), getRecord_ID(), get_TrxName());
	}

	@Override
	protected String doIt() throws Exception
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;

		// Conversion from Header currency To Inner tab Currency
		try
		{
			stmt = DB.prepareStatement(SQL_CONVERT_FROMCURR_TO_TOCURR.toString(), get_TrxName());
			stmt.setInt(1, currow.getX_Conv_Currency_ID());

			rs = stmt.executeQuery();

			while (rs.next())
			{
				insertData(rs.getInt(1), rs.getInt(2), currow.getC_ConversionType_ID(), rs.getDouble(3),
						currow.getValidFrom());
			}
		}
		catch (SQLException e)
		{
			log.log(Level.SEVERE, "Create Header  Rate - " + SQL_CONVERT_FROMCURR_TO_TOCURR, e);
			return "UnSuccessfull Currency Conversion";
		}
		finally
		{
			DB.close(rs, stmt);
			stmt = null;
			rs = null;
		}

		// Conversion from Child currency To Child Currency
		try
		{
			stmt = DB.prepareStatement(SQL_CONVERT_TOCURR_TOCURR, get_TrxName());
			stmt.setInt(1, getRecord_ID());
			stmt.setInt(2, getRecord_ID());
			stmt.setInt(3, currow.getAD_Client_ID());
			stmt.setInt(4, currow.getAD_Client_ID());
			rs = stmt.executeQuery();

			while (rs.next())
			{
				insertData(rs.getInt(1), rs.getInt(2), currow.getC_ConversionType_ID(),
						(rs.getDouble(4) / rs.getDouble(3)), currow.getValidFrom());
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Create Sub Table Rate - " + SQL_CONVERT_TOCURR_TOCURR, e);
			return "UnSuccessfull Currency Conversion";
		}
		finally
		{
			DB.close(rs, stmt);
			stmt = null;
			rs = null;
		}

		// Conversion from Child currency To Header Currency
		try
		{

			stmt = DB.prepareStatement(SQL_CONVERT_FROMCURR_TO_TOCURR, get_TrxName());
			stmt.setInt(1, getRecord_ID());

			rs = stmt.executeQuery();

			while (rs.next())
			{
				insertData(rs.getInt(2), rs.getInt(1), currow.getC_ConversionType_ID(), (1 / rs.getDouble(3)),
						currow.getValidFrom());
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Create Child to Parent Rate - " + SQL_CONVERT_FROMCURR_TO_TOCURR, e);
			return "UnSuccessfull Currency Conversion";
		}
		finally
		{
			DB.close(rs, stmt);
			stmt = null;
			rs = null;
		}

		return "Currency Conversion Successfully";
	}

	/**
	 * Insert or update Currency Rate
	 * 
	 * @param from_currency
	 * @param to_currency
	 * @param conversiontype_id
	 * @param multiply_rate
	 * @param valid_from
	 */
	private void insertData(int from_currency, int to_currency, int conversiontype_id, double multiply_rate,
			Timestamp valid_from)
	{
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try
		{
			m_Conversion_Rate_ID = DB.getSQLValue(get_TrxName(), SQL_CHECK_CONVERSTION_EXISTS, from_currency,
					to_currency, valid_from, currow.getC_ConversionType_ID());

			if (m_Conversion_Rate_ID != -1)
			{
				stmt = DB.prepareStatement(SQL_UPDATE_EXISTING_CURRENCY, get_TrxName());

				BigDecimal mulRate = new BigDecimal(multiply_rate);
				BigDecimal divRate = new BigDecimal((1 / multiply_rate));

				stmt.setBigDecimal(1, mulRate);
				stmt.setBigDecimal(2, divRate);
				stmt.setInt(3, m_Conversion_Rate_ID);
				DB.executeUpdateEx(SQL_UPDATE_EXISTING_CURRENCY,
						new Object[] { mulRate, divRate, m_Conversion_Rate_ID }, get_TrxName());
			}
			else
			{
				MConversionRate mrate = new MConversionRate(currow, currow.getC_ConversionType_ID(), from_currency,
						to_currency, new java.math.BigDecimal(String.valueOf(multiply_rate)), valid_from);
				mrate.saveEx();
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Insert Rate - " + SQL_UPDATE_EXISTING_CURRENCY, e);
			return;
		}
		finally
		{
			DB.close(rs, stmt);
			stmt = null;
			rs = null;
		}
	}
}
