package org.compiere.process;

import java.util.logging.Level;

import org.compiere.model.MTableAttribute;
import org.compiere.util.AdempiereUserError;

/**
 *	Synchronize with Database
 */
public class TableAttributeSync extends SvrProcess
{
	private int			p_AD_Table_Attribute_ID = 0;

	/**
	 *  Prepare - e.g., get Parameters.
	 */
	protected void prepare()
	{
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;			
			else if (name.equals("AD_Table_Attribute_ID"))
			{
				p_AD_Table_Attribute_ID = para[i].getParameterAsInt();
			}
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		if ( getRecord_ID() > 0 )
			p_AD_Table_Attribute_ID = getRecord_ID();
	}	//	prepare

	/**
	 * 	Process
	 *	@return message
	 *	@throws Exception
	 */
	protected String doIt() throws Exception
	{
		log.info("AD_Table_Attribute_ID=" + p_AD_Table_Attribute_ID);
		if (p_AD_Table_Attribute_ID == 0)
			throw new AdempiereUserError("@No@ @AD_Table_Attribute_ID@");

		String sql = null;
		if ( p_AD_Table_Attribute_ID > 0)
		{
			MTableAttribute att = new MTableAttribute (getCtx(), p_AD_Table_Attribute_ID, get_TrxName());
			if (att.get_ID() == 0)
				throw new AdempiereUserError("@NotFound@ @AD_Table_Attribute_ID@ " + p_AD_Table_Attribute_ID);

			sql = att.syncDatabase();
			
			addLog(sql);
		}
		
		
		return sql;
	}	//	doIt
	
}	//	TableAttributeSync
