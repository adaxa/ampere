package org.compiere.process;

import java.util.logging.Level;

import org.adempiere.process.InitialClientSetup;
import org.compiere.model.MSetup;
import org.compiere.util.Env;

/**
 * 
 * @author jobriant
 *
 */

public class SetupRequest extends SvrProcess {

	private MSetup ms = null;

	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (para[i].getParameter() == null)
				;
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}

	@Override
	protected String doIt() throws Exception {
		
		if ( ms == null ) {
			ms = new MSetup();
		}

		ms.initialize(Env.getCtx(), InitialClientSetup.WINDOW_THIS_PROCESS);
		ms.createRequestParameter(true, true);
		
		return "@OK@";
	}

}
