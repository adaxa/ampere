package org.compiere.process;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MPaySelectionCheck;
import org.compiere.model.MPaySelectionLine;
import org.compiere.model.MPayment;
import org.compiere.model.MTable;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.process.SvrProcess;
import org.compiere.util.Env;

public class PaySelectionCheckReverse extends SvrProcess
{

	@Override
	protected void prepare( )
	{

	}

	@Override
	protected String doIt( ) throws Exception
	{

		if ( getTable_ID() != MPaySelectionCheck.Table_ID )
			throw new AdempiereException("Process only valid for C_PaySelectionCheck");
		
		List<Integer> recordIDs = getSelectionKeys();

		if ((recordIDs == null || recordIDs.isEmpty()) && getRecord_ID() > 0)
		{
			recordIDs = new ArrayList<Integer>();
			recordIDs.add(getRecord_ID());
		}

		if (recordIDs == null || recordIDs.isEmpty())
			return "No selection to process";
		
		int count = 0;
		for (int recordId : recordIDs)
		{
			MPaySelectionCheck paySelectionCheck = 
					(MPaySelectionCheck) MTable.get(getCtx(), MPaySelectionCheck.Table_ID).getPO(recordId, get_TrxName());
			paySelectionCheck.setPayAmt(Env.ZERO);
			paySelectionCheck.saveEx();

			if ( paySelectionCheck.getC_Payment_ID() > 0 )
			{
				MPayment payment = (MPayment) paySelectionCheck.getC_Payment();
				
				if (!payment.getDocStatus().equals(DocAction.STATUS_Reversed) && !payment.getDocStatus().equals(DocAction.STATUS_Voided))
				{
					payment.setDocAction(DocAction.ACTION_Reverse_Correct);
					if (!payment.processIt(DocAction.ACTION_Reverse_Correct))
					{
						throw new AdempiereException("Unable to reverse Payment " + payment.getDocumentNo() 
						+ ": " + payment.getProcessMsg());

					}
					payment.saveEx();
					addLog("Reversing Payment #" + payment.getDocumentNo());
				}
			}

			List <MPaySelectionLine> paySelectionLineList = new Query(getCtx(), MTable.get(getCtx(), MPaySelectionLine.Table_ID),
					"C_PaySelectionCheck_ID=? AND Processed='Y'", get_TrxName()).setParameters(paySelectionCheck.get_ID()).list();
			for (MPaySelectionLine paySelectionLine : paySelectionLineList)
			{
				paySelectionLine.setPayAmt(Env.ZERO);
				paySelectionLine.saveEx();
				addLog("Updated Payment Selection Line #" + paySelectionLine.getLine());
			}
			
			addLog("Prepared Payment #" + paySelectionCheck.getDocumentNo() + " reversed");
			count++;
		}
		
		return "Prepared Payments reversed: " + count;
	}

}
