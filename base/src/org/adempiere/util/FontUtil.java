package org.adempiere.util;

public final class FontUtil
{
	/**
	 * Retrieve font family from font name
	 * 
	 * @param fontName
	 * @return font family
	 */
	public static final String getFontFamily(String fontName)
	{
		if (fontName.equalsIgnoreCase("sansserif"))
			fontName = "sans-serif";
		else if (fontName.equalsIgnoreCase("monospaced"))
			fontName = "monospace";
		else if (fontName.equalsIgnoreCase("dialog"))
			fontName = "cursive";
		else if (fontName.equalsIgnoreCase("dialoginput"))
			fontName = "fantasy";
		else if (fontName.equalsIgnoreCase("Century Gothic"))
			fontName = "Century Gothic";
		else
			fontName = "Open Sans";

		return fontName;
	} // getFontFamily
}
