package org.adempiere.model;

import javax.swing.tree.DefaultMutableTreeNode;


/**
 * 
 * @author jtrinidad
 *
 */
public class MInfoTreeNode extends DefaultMutableTreeNode {



	/**	Summary			*/
	private boolean 	m_isSummary;
	
	private boolean		m_isRoot;
	/** Node Level to indicate which tree it belongs to*/
	private int			m_NodeLevel; // 0 is the root
	/** Name			*/
	private String  	m_name;
	/** Description		*/
	private String  	m_description;
	/** Node Key = TableName + - + INT_ID */
	private String		m_NodeKey;
	/** Parent Key */
	private String		m_parentKey;
	/** Node is Read Only - if set to true, you cannot delete or rearrange **/
	private boolean 	m_isReadOnly;	
	
	private String		m_tableName;
	/** Parent column of the table  to be used when moving to another parent table */
	private String		m_parentColumn;
	/**Record_ID  note: id is not enough as you could have several tables with same id*/
	private int			m_id;
	private int			m_window_ID = 0;
	


	public MInfoTreeNode(String name, String description, String parentKey, String tableName
			, int id, int nodeLevel, String parentColumn, boolean isReadOnly) {
		super();
		this.m_name = name;
		this.m_description = description;
		this.m_parentKey = parentKey;
		this.m_tableName = tableName;
		this.m_id = id;
		this.m_NodeLevel = nodeLevel;
		this.m_NodeKey = tableName + "-" + id;
		this.m_parentColumn = parentColumn;
		this.m_isReadOnly = isReadOnly;

	}

	public String getNodeKey() {
		return m_NodeKey;
	}



	public String getParentKey() {
		return m_parentKey;
	}

	public String getName() {
		return m_name;
	}

	public int getID() {
		return m_id;
	}

	public int getADWindow_ID() {
		return m_window_ID;
	}

	public void setADWindowID(int ad_Window_ID) {
		this.m_window_ID = ad_Window_ID;
	}

	public int getNodeLevel() {
		return m_NodeLevel;
	}

	public String getTableName() {
		return m_tableName;
	}

	public String getParentColumn() {
		return m_parentColumn;
	}

	public boolean isReadOnly() {
		return m_isReadOnly;
	}
	

}
