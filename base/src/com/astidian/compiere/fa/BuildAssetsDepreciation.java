/**
 * 
 */
package com.astidian.compiere.fa;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.CLogger;
import org.compiere.util.DB;

/**
 * @author Ashley G Ramdass
 * 
 */
public class BuildAssetsDepreciation extends SvrProcess
{
    private static final CLogger logger = CLogger
            .getCLogger(BuildAssetsDepreciation.class);
    private int p_assetId;
    private int p_assetGroupId;

    @Override
    protected void prepare()
    {
        ProcessInfoParameter[] para = getParameter();
        for (int i = 0; i < para.length; i++)
        {
            String name = para[i].getParameterName();
            if (para[i].getParameter() == null)
            {
                ;
            }
            else if (name.equals("A_Asset_ID"))
            {
                p_assetId = para[i].getParameterAsInt();
            }
            else if (name.equals("A_Asset_Group_ID"))
            {
                p_assetGroupId = para[i].getParameterAsInt();
            }
            else
            {
                log.warning("Unknown parameter: " + name);
            }
        }
    }

    @Override
    protected String doIt() throws Exception
    {
        String sql = "SELECT a.A_Asset_ID, aa.GAAS_Asset_Acct_ID, "
                + "aa.GAAS_Depreciation_Type, (SELECT COUNT(*) FROM GAAS_Depreciation_Expense dep "
                + "WHERE dep.GAAS_Asset_Acct_ID=aa.GAAS_Asset_Acct_ID AND dep.Processed='Y') "
                + "FROM A_Asset a INNER JOIN GAAS_Asset_Acct aa ON a.A_Asset_ID=aa.A_Asset_ID "
                + "WHERE a.IsDepreciated='Y' AND a.IsActive='Y' AND a.AD_Client_ID=? AND aa.IsActive='Y'";

        if (p_assetId > 0)
        {
            sql += " AND a.A_Asset_ID=?";
        }
        else if (p_assetGroupId > 0)
        {
            sql += " AND a.A_Asset_Group_ID=?";
        }
        
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        int updatedCount = 0;
        int buildCount = 0;
        
        try
        {
            pstmt = DB.prepareStatement(sql, get_TrxName());
            pstmt.setInt(1, getAD_Client_ID());
            if (p_assetId > 0)
            {
                pstmt.setInt(2, p_assetId);
            }
            else if (p_assetGroupId > 0)
            {
                pstmt.setInt(2, p_assetGroupId);
            }
            
            rs = pstmt.executeQuery();
            
            while (rs.next())
            {
                int assetId = rs.getInt(1);
                int assetAcctId = rs.getInt(2);
                String depreciationType = rs.getString(3);
                int processedCount = rs.getInt(4);
                
                Depreciation.updateDepreciationPeriods(getCtx(), assetId, null);
                updatedCount++;
                
                if (processedCount == 0)
                {
                    Depreciation depreciation;

                    if ("SL".equals(depreciationType))
                    {
                        depreciation = new SLDepreciation(getCtx(), assetId,
                                get_TrxName());
                    }
                    else if ("RB".equals(depreciationType))
                    {
                        depreciation = new RBDepreciation(getCtx(), assetId,
                                get_TrxName());
                    }
                    else
                    {
                        throw new IllegalStateException(
                                "Depreciation type not yet implemented");
                    }
                    depreciation.updateDepreciationExpenses(assetAcctId);
                    buildCount++;
                }
            }
        }
        catch (SQLException ex)
        {
            logger.log(Level.SEVERE, "Could not get asset data", ex);
            throw new IllegalStateException("Could not get data");
        }
        finally
        {
            DB.close(rs, pstmt);
        }
        
        return "Updated #: " + updatedCount + ", Built #: " + buildCount;
    }
}
