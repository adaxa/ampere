/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for T_MRP_RUN_Capacity_Summary
 *  @author Adempiere (generated) 
 *  @version $
{
project.version}

 */
public interface I_T_MRP_RUN_Capacity_Summary 
{

    /** TableName=T_MRP_RUN_Capacity_Summary */
    public static final String Table_Name = "T_MRP_RUN_Capacity_Summary";

    /** AD_Table_ID=54226 */
    public static final int Table_ID = MTable.getTable_ID(Table_Name);

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 3 - Client - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(3);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organisation.
	  * Organisational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organisation.
	  * Organisational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name CapacityQty */
    public static final String COLUMNNAME_CapacityQty = "CapacityQty";

	/** Set Capacity Qty	  */
	public void setCapacityQty (BigDecimal CapacityQty);

	/** Get Capacity Qty	  */
	public BigDecimal getCapacityQty();

    /** Column name ConfirmedQty */
    public static final String COLUMNNAME_ConfirmedQty = "ConfirmedQty";

	/** Set Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty);

	/** Get Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CumulatedCapacity */
    public static final String COLUMNNAME_CumulatedCapacity = "CumulatedCapacity";

	/** Set Accumulated Capacity	  */
	public void setCumulatedCapacity (BigDecimal CumulatedCapacity);

	/** Get Accumulated Capacity	  */
	public BigDecimal getCumulatedCapacity();

    /** Column name CumulatedDemand */
    public static final String COLUMNNAME_CumulatedDemand = "CumulatedDemand";

	/** Set Accumulated Demand	  */
	public void setCumulatedDemand (BigDecimal CumulatedDemand);

	/** Get Accumulated Demand	  */
	public BigDecimal getCumulatedDemand();

    /** Column name DateStart */
    public static final String COLUMNNAME_DateStart = "DateStart";

	/** Set Date Start.
	  * Date Start for this Order
	  */
	public void setDateStart (Timestamp DateStart);

	/** Get Date Start.
	  * Date Start for this Order
	  */
	public Timestamp getDateStart();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public I_M_Product getM_Product() throws RuntimeException;

    /** Column name MRP_Run_ID */
    public static final String COLUMNNAME_MRP_Run_ID = "MRP_Run_ID";

	/** Set MRP_Run ID	  */
	public void setMRP_Run_ID (int MRP_Run_ID);

	/** Get MRP_Run ID	  */
	public int getMRP_Run_ID();

	public I_MRP_Run getMRP_Run() throws RuntimeException;

    /** Column name M_WorkCentre_ID */
    public static final String COLUMNNAME_M_WorkCentre_ID = "M_WorkCentre_ID";

	/** Set Work Centre	  */
	public void setM_WorkCentre_ID (int M_WorkCentre_ID);

	/** Get Work Centre	  */
	public int getM_WorkCentre_ID();

	public I_M_WorkCentre getM_WorkCentre() throws RuntimeException;

    /** Column name RemainingCumulativeCapacity */
    public static final String COLUMNNAME_RemainingCumulativeCapacity = "RemainingCumulativeCapacity";

	/** Set Remaining Cumulative Capacity	  */
	public void setRemainingCumulativeCapacity (BigDecimal RemainingCumulativeCapacity);

	/** Get Remaining Cumulative Capacity	  */
	public BigDecimal getRemainingCumulativeCapacity();

    /** Column name SuggestedQty */
    public static final String COLUMNNAME_SuggestedQty = "SuggestedQty";

	/** Set Suggested Qty	  */
	public void setSuggestedQty (BigDecimal SuggestedQty);

	/** Get Suggested Qty	  */
	public BigDecimal getSuggestedQty();

    /** Column name SurplusShortageQty */
    public static final String COLUMNNAME_SurplusShortageQty = "SurplusShortageQty";

	/** Set Surplus Shortage Qty	  */
	public void setSurplusShortageQty (BigDecimal SurplusShortageQty);

	/** Get Surplus Shortage Qty	  */
	public BigDecimal getSurplusShortageQty();

    /** Column name T_MRP_RUN_Capacity_Summary_ID */
    public static final String COLUMNNAME_T_MRP_RUN_Capacity_Summary_ID = "T_MRP_RUN_Capacity_Summary_ID";

	/** Set MRP RUN Capacity Summary ID	  */
	public void setT_MRP_RUN_Capacity_Summary_ID (int T_MRP_RUN_Capacity_Summary_ID);

	/** Get MRP RUN Capacity Summary ID	  */
	public int getT_MRP_RUN_Capacity_Summary_ID();

    /** Column name TotalDemandQty */
    public static final String COLUMNNAME_TotalDemandQty = "TotalDemandQty";

	/** Set Total DemandQty	  */
	public void setTotalDemandQty (BigDecimal TotalDemandQty);

	/** Get Total DemandQty	  */
	public BigDecimal getTotalDemandQty();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Week */
    public static final String COLUMNNAME_Week = "Week";

	/** Set Week	  */
	public void setWeek (String Week);

	/** Get Week	  */
	public String getWeek();
}
