/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2007 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;

/** Generated Model for T_MRP_RUN_Capacity_Summary
 *  @author Adempiere (generated) 
 *  @version ${project.version} - $Id$ */
public class X_T_MRP_RUN_Capacity_Summary extends PO implements I_T_MRP_RUN_Capacity_Summary, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20220307L;

    /** Standard Constructor */
    public X_T_MRP_RUN_Capacity_Summary (Properties ctx, int T_MRP_RUN_Capacity_Summary_ID, String trxName)
    {
      super (ctx, T_MRP_RUN_Capacity_Summary_ID, trxName);
      /** if (T_MRP_RUN_Capacity_Summary_ID == 0)
        {
			setMRP_Run_ID (0);
			setM_WorkCentre_ID (0);
			setT_MRP_RUN_Capacity_Summary_ID (0);
        } */
    }

    /** Load Constructor */
    public X_T_MRP_RUN_Capacity_Summary (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_T_MRP_RUN_Capacity_Summary[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Capacity Qty.
		@param CapacityQty Capacity Qty	  */
	public void setCapacityQty (BigDecimal CapacityQty)
	{
		set_Value (COLUMNNAME_CapacityQty, CapacityQty);
	}

	/** Get Capacity Qty.
		@return Capacity Qty	  */
	public BigDecimal getCapacityQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CapacityQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed Quantity.
		@param ConfirmedQty 
		Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty)
	{
		set_Value (COLUMNNAME_ConfirmedQty, ConfirmedQty);
	}

	/** Get Confirmed Quantity.
		@return Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ConfirmedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Accumulated Capacity.
		@param CumulatedCapacity Accumulated Capacity	  */
	public void setCumulatedCapacity (BigDecimal CumulatedCapacity)
	{
		set_Value (COLUMNNAME_CumulatedCapacity, CumulatedCapacity);
	}

	/** Get Accumulated Capacity.
		@return Accumulated Capacity	  */
	public BigDecimal getCumulatedCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CumulatedCapacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Accumulated Demand.
		@param CumulatedDemand Accumulated Demand	  */
	public void setCumulatedDemand (BigDecimal CumulatedDemand)
	{
		set_Value (COLUMNNAME_CumulatedDemand, CumulatedDemand);
	}

	/** Get Accumulated Demand.
		@return Accumulated Demand	  */
	public BigDecimal getCumulatedDemand () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CumulatedDemand);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Date Start.
		@param DateStart 
		Date Start for this Order
	  */
	public void setDateStart (Timestamp DateStart)
	{
		set_Value (COLUMNNAME_DateStart, DateStart);
	}

	/** Get Date Start.
		@return Date Start for this Order
	  */
	public Timestamp getDateStart () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateStart);
	}

	public I_M_Product getM_Product() throws RuntimeException
    {
		return (I_M_Product)MTable.get(getCtx(), I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_MRP_Run getMRP_Run() throws RuntimeException
    {
		return (I_MRP_Run)MTable.get(getCtx(), I_MRP_Run.Table_Name)
			.getPO(getMRP_Run_ID(), get_TrxName());	}

	/** Set MRP_Run ID.
		@param MRP_Run_ID MRP_Run ID	  */
	public void setMRP_Run_ID (int MRP_Run_ID)
	{
		if (MRP_Run_ID < 1) 
			set_Value (COLUMNNAME_MRP_Run_ID, null);
		else 
			set_Value (COLUMNNAME_MRP_Run_ID, Integer.valueOf(MRP_Run_ID));
	}

	/** Get MRP_Run ID.
		@return MRP_Run ID	  */
	public int getMRP_Run_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_MRP_Run_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_WorkCentre getM_WorkCentre() throws RuntimeException
    {
		return (I_M_WorkCentre)MTable.get(getCtx(), I_M_WorkCentre.Table_Name)
			.getPO(getM_WorkCentre_ID(), get_TrxName());	}

	/** Set Work Centre.
		@param M_WorkCentre_ID Work Centre	  */
	public void setM_WorkCentre_ID (int M_WorkCentre_ID)
	{
		if (M_WorkCentre_ID < 1) 
			set_Value (COLUMNNAME_M_WorkCentre_ID, null);
		else 
			set_Value (COLUMNNAME_M_WorkCentre_ID, Integer.valueOf(M_WorkCentre_ID));
	}

	/** Get Work Centre.
		@return Work Centre	  */
	public int getM_WorkCentre_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_WorkCentre_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Remaining Cumulative Capacity.
		@param RemainingCumulativeCapacity Remaining Cumulative Capacity	  */
	public void setRemainingCumulativeCapacity (BigDecimal RemainingCumulativeCapacity)
	{
		set_Value (COLUMNNAME_RemainingCumulativeCapacity, RemainingCumulativeCapacity);
	}

	/** Get Remaining Cumulative Capacity.
		@return Remaining Cumulative Capacity	  */
	public BigDecimal getRemainingCumulativeCapacity () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RemainingCumulativeCapacity);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Suggested Qty.
		@param SuggestedQty Suggested Qty	  */
	public void setSuggestedQty (BigDecimal SuggestedQty)
	{
		set_Value (COLUMNNAME_SuggestedQty, SuggestedQty);
	}

	/** Get Suggested Qty.
		@return Suggested Qty	  */
	public BigDecimal getSuggestedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SuggestedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Surplus Shortage Qty.
		@param SurplusShortageQty Surplus Shortage Qty	  */
	public void setSurplusShortageQty (BigDecimal SurplusShortageQty)
	{
		set_Value (COLUMNNAME_SurplusShortageQty, SurplusShortageQty);
	}

	/** Get Surplus Shortage Qty.
		@return Surplus Shortage Qty	  */
	public BigDecimal getSurplusShortageQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_SurplusShortageQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set MRP RUN Capacity Summary ID.
		@param T_MRP_RUN_Capacity_Summary_ID MRP RUN Capacity Summary ID	  */
	public void setT_MRP_RUN_Capacity_Summary_ID (int T_MRP_RUN_Capacity_Summary_ID)
	{
		if (T_MRP_RUN_Capacity_Summary_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_T_MRP_RUN_Capacity_Summary_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_T_MRP_RUN_Capacity_Summary_ID, Integer.valueOf(T_MRP_RUN_Capacity_Summary_ID));
	}

	/** Get MRP RUN Capacity Summary ID.
		@return MRP RUN Capacity Summary ID	  */
	public int getT_MRP_RUN_Capacity_Summary_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_T_MRP_RUN_Capacity_Summary_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total DemandQty.
		@param TotalDemandQty Total DemandQty	  */
	public void setTotalDemandQty (BigDecimal TotalDemandQty)
	{
		set_Value (COLUMNNAME_TotalDemandQty, TotalDemandQty);
	}

	/** Get Total DemandQty.
		@return Total DemandQty	  */
	public BigDecimal getTotalDemandQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalDemandQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Week.
		@param Week Week	  */
	public void setWeek (String Week)
	{
		set_Value (COLUMNNAME_Week, Week);
	}

	/** Get Week.
		@return Week	  */
	public String getWeek () 
	{
		return (String)get_Value(COLUMNNAME_Week);
	}
}