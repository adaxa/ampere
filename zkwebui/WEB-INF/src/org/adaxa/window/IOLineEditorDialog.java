package org.adaxa.window;


import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.Combobox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.NumberBox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WStringEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.MMovement;
import org.compiere.model.MMovementLine;
import org.compiere.model.MProduct;
import org.compiere.model.MWarehouse;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.zkoss.zk.ui.SuspendNotAllowedException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;

/**
 * 
 * @author jobriant
 *
 */
public class IOLineEditorDialog extends Window implements EventListener<Event>{
	
	private static final String FIELD_WIDTH = "100%";
	private static final String FIELD_LABEL_WIDTH = "100px,200px";

	private static final String HEADER_TEXT_STYLE = "color: darkblue; font-size: 16px;";
	
	private static CLogger log = CLogger.getCLogger(IOLineEditorDialog.class);
	
	public enum Mode {ADD, REMOVE, EDIT}
	/**
	 * 
	 */
	private static final long serialVersionUID = -3852236029054284848L;
	private int max;
	private BigDecimal number;
	private boolean cancelled = false;
	private Tabbox tabbox;
	private NumberBox fQty;
	private Label lHeader;
	private Label lWarehouse = new Label("-");
;
	private Label status;
	
	private WStringEditor fProductName = new WStringEditor();
	private WStringEditor fPCode = new WStringEditor();
	private Combobox fLocator = new Combobox();
	private WStringEditor fSerial = new WStringEditor();
	private Textbox fStatus = new Textbox();
	private Textbox fErrorMsg = new Textbox();
	private Textbox fNotes = new Textbox();
	private Checkbox fLimitToProduct = new Checkbox();
	
	private boolean isCrossDock = false;
	private boolean isSerial = false;
	private boolean isQtyUpdatable = false;
	private int m_Locator_ID = 0;
	private int m_PackageLine_ID = 0;
	private String m_notes = null;
	private MProduct m_Product = null;
	private ConfirmPanel confirmPanel = null;
	private boolean isMovement=false;
	private MMovement movement=null;
	private MMovementLine line=null;
	Mode mode=null;
	//Key = Locator + QOH, Value = 
	private HashMap<String, String> mapProductQOH = new HashMap<String, String>();
	private HashMap<String, Integer> mapReceiptLine = new HashMap<String, Integer>();

	//Key = Locator + Product_ID, Value = movement qty
	private HashMap<String, BigDecimal> mapLocatorQty=new HashMap<String, BigDecimal>();
	
	private WListbox table = ListboxFactory.newDataTable();
	private Borderlayout mainPanel = new Borderlayout();
	private Grid northPanel = GridFactory.newGridLayout();

	private boolean isInProcess = false;
	private boolean showASISelection = false;
	private Vector<String> asiColumns = new Vector<String>();
	private boolean asiChanged = false;
	private int m_Warehouse_ID = 0;
	
	private final String sqlASISelection = 
			"SELECT s.m_attributesetinstance_id, COALESCE(asi.serno,asi.lot) AS idno, s.m_locator_id, l.value AS locatorvalue, s.qtyonhand " +
            "       , 1::NUMERIC AS multiplier, 1::NUMERIC AS unit, 1::NUMERIC AS total " +
            "FROM m_storage s " +
            "  INNER JOIN m_attributesetinstance asi ON asi.m_attributesetinstance_id = s.m_attributesetinstance_id " +
            "  INNER JOIN m_attributeset ms ON asi.m_attributeset_id = ms.m_attributeset_id " +
            "  INNER JOIN m_locator l ON s.m_locator_id = l.m_locator_id " +
            "WHERE s.m_attributesetinstance_id <> 0 " +
            "AND   s.qtyonhand <> 0 " +
            "AND   s.M_Product_ID = ? AND ? IN (l.m_warehouse_id, 0) " +
            "ORDER BY 2";
//            "AND   l.M_Warehouse_ID = ? " +
	
	public IOLineEditorDialog(int M_Warehouse_ID, String title, BigDecimal value, boolean isSerial, int maxValue, boolean isQtyUpdatable
			, Mode mode, MProduct product, MMovement movement, MMovementLine line, String notes)
	{
		this(M_Warehouse_ID, title, value, isSerial, maxValue, isQtyUpdatable, mode, product, false, notes);
		isMovement=true;
		this.movement=movement;
		this.line=line;
		this.mode=mode;
		if(mode==Mode.REMOVE)
			fillLocatorRemoveMode();
		else
			fillLocator();
	}
	/**
	 * 
	 * @param title
	 * @param text
	 * @param isSerial
	 * @param maxValue
	 * @param M_Warehouse_ID - must limit Storage and locators to specific warehouse only
	 */
	public IOLineEditorDialog(int M_Warehouse_ID, String title, BigDecimal value, boolean isSerial, int maxValue, boolean isQtyUpdatable
			, Mode mode, MProduct product, boolean isCrossDock, String notes) {
		super();
		setTitle(title);
		this.isSerial = isSerial;
		this.m_Product = product;
		this.max = maxValue;
		this.number = value;
		this.isQtyUpdatable = isQtyUpdatable;
		this.isCrossDock = isCrossDock;
		this.m_notes = notes;
		this.m_Warehouse_ID = M_Warehouse_ID;
		String headerLabel = null;
		switch (mode) {
		case ADD:
			headerLabel = "ITEMS TO BE ADDED";
			break;
		case REMOVE:
			headerLabel = "ITEMS TO BE REMOVED";
			break;
		case EDIT:
			headerLabel = "ITEMS WILL BE UPDATED";
//			if (product.getM_AttributeSet() != null && (
//					product.getM_AttributeSet().isLot() ||  product.getM_AttributeSet().isSerNo())) {
			if (isSerial) {
				showASISelection = true;
				asiColumns.add("");
				asiColumns.add("Att No");
				asiColumns.add("Locator");
				asiColumns.add("QtyOnHand");
				asiColumns.add("Multiplier");
				asiColumns.add("Qty");
				asiColumns.add("Total Qty");
				
				isSerial = true;  // serial and lot should apply, retained to preserve code
				
				
			}
			break;
		}
		this.mode=mode;
		init();
		lHeader.setText(headerLabel);
		fillLocator();
	}
	
	private void fillLocatorRemoveMode()
	{
		fLocator.removeAllItems();
		mapLocatorQty.clear();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuffer sql = new StringBuffer();
		
			sql.append("SELECT l.Value, ml.movementqty FROM M_Locator l ");
			sql.append(" INNER JOIN M_MovementLine ml on l.M_Locator_ID=ml.M_LocatorTo_ID " );
				sql.append("WHERE ml.m_product_id = ?  AND ml.M_Movement_ID=? AND ml.movementqty >0 ");
				if(line!=null)
					sql.append( " AND ml.M_MovementLine_ID=? " );
				
		try {
			pstmt = DB.prepareStatement(sql.toString(), movement.get_TrxName());
			pstmt.setInt(1, m_Product.getM_Product_ID());
			pstmt.setInt(2, movement.getM_Movement_ID());
			if(line!=null)
				pstmt.setInt(3, line.getM_MovementLine_ID());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String locValue= rs.getString(1);
				BigDecimal qoh = rs.getBigDecimal(2);
				if(mapLocatorQty.get(locValue+""+m_Product.getM_Product_ID()) !=null)
				{
					mapLocatorQty.put(locValue+""+m_Product.getM_Product_ID(), mapLocatorQty.get(locValue+""+m_Product.getM_Product_ID()).add(qoh));
					continue;
				}
				else
					mapLocatorQty.put(locValue+""+m_Product.getM_Product_ID(), qoh);
				fLocator.appendItem(locValue);
			}
			if(mapLocatorQty.size() > 0)
			{
				fLocator.setSelectedIndex(0);
				BigDecimal qty=mapLocatorQty.get(fLocator.getValue()+""+m_Product.getM_Product_ID());
				fQty.setValue(qty);
				number=qty;
			}
			
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
	
		
	}

	private void init() {
		//setSizable(true);
		setBorder("normal");
		
		appendChild(mainPanel);
		North north = new North();
		
		ZKUpdateUtil.setWidth(mainPanel, "800px");
		boolean showASISelection = true;
		if (showASISelection) 
			ZKUpdateUtil.setHeight(mainPanel, "750px");
		else 
			ZKUpdateUtil.setHeight(mainPanel, "400px");

		
		Rows rows = northPanel.newRows();
		
		mainPanel.appendChild(north);
		north.appendChild(northPanel);
		lHeader = new Label();
		lHeader.setText("  --  ");
		lHeader.setStyle(HEADER_TEXT_STYLE);
		if (m_Warehouse_ID > 0) 
			lWarehouse.setText(MWarehouse.get(Env.getCtx(), m_Warehouse_ID).getName());
		lWarehouse.setStyle(HEADER_TEXT_STYLE);

		Row row = rows.newRow();
		
		row.appendChild(lHeader);
		row.appendChild(lWarehouse);
		row.setSpans("1,3");
		
		row = rows.newRow();
		row.appendChild(new Label("Status").rightAlign());
		row.appendChild(fStatus);
		ZKUpdateUtil.setWidth(fStatus, FIELD_WIDTH);
		fStatus.setReadonly(true);
		ZKUpdateUtil.setHeight(fStatus, "80px");
		fStatus.setMultiline(true);
		row.setSpans("1,3");
		if (isSerial) {
			row = rows.newRow();
			row.appendChild(new Label("Att No").rightAlign());
//			row.setWidths(FIELD_LABEL_WIDTH);
			row.appendChild(fSerial.getComponent());
			ZKUpdateUtil.setWidth(fSerial.getComponent(), FIELD_WIDTH);
			fSerial.setReadWrite(true);
			fSerial.getComponent().addEventListener(Events.ON_OK, this);
		}
		
//		row = rows.newRow();
		if (isCrossDock) {
			row.appendChild(new Label("Package").rightAlign());
		} else {
			Label labelLocator = new Label("Locator");
			row.appendChild(labelLocator.rightAlign());
			fLocator.setVisible(!isSerial);
			labelLocator.setVisible(!isSerial);

		}
//		row.setWidths(FIELD_LABEL_WIDTH);
		row.appendChild(fLocator);
//		north.appendChild(row);
		ZKUpdateUtil.setWidth(fLocator, FIELD_WIDTH);
		fLocator.setReadonly(false);
		fLocator.addEventListener(Events.ON_OK, this);
		fLocator.addEventListener(Events.ON_FOCUS, this);
		fLocator.addEventListener(Events.ON_CHANGE, this);
		//fLocator.setHeight("100px");

		
		fQty = new NumberBox(false);
		row = rows.newRow();
		row.appendChild(new Label("Qty").rightAlign());
//		row.setWidths(FIELD_LABEL_WIDTH);
		row.appendChild(fQty);
//		north.appendChild(row);
		fQty.setEnabled(isQtyUpdatable);
		ZKUpdateUtil.setWidth(fQty, FIELD_WIDTH);
		fQty.setValue(number);
		ZKUpdateUtil.setWidth(fQty.getDecimalbox(), "100px");
		DecimalFormat format = DisplayType.getNumberFormat(DisplayType.Quantity, AEnv.getLanguage(Env.getCtx()));
		fQty.setFormat(format);
		fQty.addEventListener(Events.ON_OK, this);
	
		
		if (isMovement) {
			fLimitToProduct.setChecked(false);
		} else {
			fLimitToProduct.setChecked(true);
		}
		//row = rows.newRow();
		row.appendChild(new Label(".").rightAlign());
		fLimitToProduct.setLabel("Only Locators Used by Product");
//		row.setWidths(FIELD_LABEL_WIDTH);
		row.appendChild(fLimitToProduct);
//		north.appendChild(row);
		fLimitToProduct.addActionListener(this);
		
		row = rows.newRow();
		row.setSpans("1,3");
		row.appendChild(new Label("Notes").rightAlign());
//		row.setWidths(FIELD_LABEL_WIDTH);
		row.appendChild(fNotes);
//		north.appendChild(row);
		ZKUpdateUtil.setWidth(fNotes, FIELD_WIDTH);
		fNotes.setMultiline(true);
		fNotes.addEventListener(Events.ON_OK, this);
		ZKUpdateUtil.setHeight(fNotes, "50px");
		fNotes.setValue(m_notes);
		row.appendChild(new Separator());

		//hbox = new Hbox();
		Label msgLabel = new Label("System Message");
		msgLabel.setStyle(HEADER_TEXT_STYLE);
		row = rows.newRow();
		row.setSpans("1,3");
		row.appendChild(msgLabel.rightAlign());
		row.appendChild(fErrorMsg);
		
//		north.appendChild(fErrorMsg);
//		vbox.appendChild(hbox);
//		fErrorMsg.setWidth(FIELD_WIDTH);
		fErrorMsg.setReadonly(true);
		fErrorMsg.setMultiline(true);
		ZKUpdateUtil.setWidth(fErrorMsg, "100%");
		//fErrorMsg.addEventListener(Events.ON_OK, this);
		ZKUpdateUtil.setHeight(fErrorMsg, "50px");
		
		// Center
		Center center = new Center();
		center.appendChild(table);
		mainPanel.appendChild(center);

		South south = new South();
		mainPanel.appendChild(south);
		
		confirmPanel = new ConfirmPanel(true);
		south.appendChild(confirmPanel);
		
		confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_RESET));
		if (!isCrossDock) {
			confirmPanel.addButton(confirmPanel.createButton(ConfirmPanel.A_PROCESS));
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setTooltip("Set as Product Default Locator");
			confirmPanel.getButton(ConfirmPanel.A_PROCESS).setTooltiptext("Set as Product Default Locator");
		}
		confirmPanel.getButton(ConfirmPanel.A_RESET).setTooltip("Ignore changes.");
		confirmPanel.getButton(ConfirmPanel.A_RESET).setTooltiptext("Ignore changes.");
		confirmPanel.addActionListener(this);
		

		if (max > 0) {
			status = new Label();			
			appendChild(status);
			//TODO updateStatus(0);
			
			status.setStyle("margin-top:10px;");
			//textBox.addEventListener(Events.ON_CHANGE, this);
			fQty.addEventListener(Events.ON_CHANGE, this);
		}		
		
		ZKUpdateUtil.setWidth(northPanel, "100%");
		ZKUpdateUtil.setWidth(confirmPanel, "100%");

//		tabbox.addEventListener(Events.ON_SELECT, this);
		loadASISelection();
	}

	@Override
	public void doModal() throws SuspendNotAllowedException {

		super.doModal();
	}

	/**
	 * @param event
	 */
	public void onEvent(Event event) throws Exception {
		if (event.getTarget().getId().equals(ConfirmPanel.A_CANCEL)) {
			cancelled = true;
			detach();
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_PROCESS)) {
			if (m_Locator_ID == 0) {
				return;
			}
			if (m_Product == null) {
				return; // avoid NPE
			}
			m_Product.setM_Locator_ID(m_Locator_ID);
			m_Product.save();
			fErrorMsg.setText(m_Product.getValue() + " Default Locator set to " + fLocator.getText() + ". " );
			
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_OK)) {
			if (!isLocatorValid()) {
				return;
			}
			if (!isSerial) {
				number = fQty.getValue();
			}
			
			if (isSerial && fQty.getValue().signum() != 0 && table.getSelectedCount() == 0) {
				setSummary();
				asiChanged = true;
				number = fQty.getValue();
			}
			m_notes = fNotes.getValue();
			detach();
		} else if (event.getTarget().getId().equals(fLimitToProduct)) {
			if(mode==Mode.REMOVE)
				fillLocatorRemoveMode();
			else
				fillLocator();
		}
		else if (Events.ON_FOCUS.equals(event.getName())){
			if (event.getTarget() == fLocator) {
				fLocator.select();
			}
		}
		else if (Events.ON_CHANGE.equals(event.getName())){
			if (event.getTarget() == fLocator) {
				isLocatorValid();
				if(isMovement && mode==Mode.REMOVE)
				{
					String loc=fLocator.getValue();
					BigDecimal qty=mapLocatorQty.get(loc+""+m_Product.getM_Product_ID());
					fQty.setValue(qty);
					number=qty;
				}
				return;
			}
			
		} else if (Events.ON_OK.equals(event.getName())) {
			if (event.getTarget() == fSerial.getComponent()) {
				onSerial();
			} else if (event.getTarget() == fLocator) {
				onLocator();
			}
			else if (event.getTarget() == fQty.getDecimalbox()) {
				onQty();
			}
		} else if (event.getTarget().getId().equals(ConfirmPanel.A_RESET)) {
			fQty.setValue(number);
			fNotes.setValue(m_notes);
		} else if (event.getName().equals(Events.ON_CHANGE)) {
			setSummary();
		} else  if (event.getName().equals(Events.ON_SELECT)) {
			setSummary();
		}
	}

	private void fillLocator()
	{
		mapProductQOH.clear();
		fLocator.removeAllItems();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		StringBuffer sql = new StringBuffer();
		
		if (isCrossDock) {
			//at the moment, Movement scanner is only for Cross Dock Stuff
			//if (isMovement) {
				sql.append("SELECT packageno, movementqty - qtyscanned AS availableqty, m_inoutline_id " 
						+  "  FROM m_inout_line_crossdock_v " 
						+  "WHERE isputaway = 'N' AND m_product_id = ?");
			//}
		} else {
			if (!fLimitToProduct.isChecked()) {
				sql.append(
						"SELECT l.value, SUM(s.qtyonhand) FROM m_locator l " 
					  + "  LEFT JOIN m_storage s ON l.m_locator_id = s.m_locator_id "
					  + "  GROUP BY l.value "
					  + " ORDER BY l.value");			
			}
			else {
				sql.append(
						"SELECT l.value, SUM(s.qtyonhand) FROM m_locator l " 
					  + "  INNER JOIN m_storage s ON l.m_locator_id = s.m_locator_id "
					  + "WHERE s.m_product_id = ?"								
					  + "  GROUP BY l.value "
					  + " ORDER BY l.value");			
			}
			
		}
		
		try {
			pstmt = DB.prepareStatement(sql.toString(), null);
			if(fLimitToProduct.isChecked()) {
				pstmt.setInt(1, m_Product.getM_Product_ID());
			}
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String locValue= rs.getString(1);
				BigDecimal qoh = rs.getBigDecimal(2);
				String s = locValue + (!isMovement ? "(" + qoh +")" : "");
				mapProductQOH.put(s, locValue);
				fLocator.appendItem(s);
				if (isCrossDock) {
					mapReceiptLine.put(s, rs.getInt(3));
				}
			}
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
	}
	
	/**
	 * 
	 */
	private boolean isLocatorValid() {
		String strLocator = (String) fLocator.getValue();
		String sql = "SELECT M_Locator_ID from M_Locator WHERE value = ? AND AD_Client_ID = ?"; 
		m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
		if(!fLimitToProduct.isChecked())
		{
			if (m_Locator_ID <= 0) {
				fLocator.setFocus(true);
				fLocator.select();
				fErrorMsg.setValue("Unknown Locator");
				return false;
			}
			else {
				setM_Locator_ID(m_Locator_ID);
				return true;
			}
		}

		if (isCrossDock) {
			if (mapProductQOH.containsKey(strLocator)) {
				//TODO - check and send some message
				return true;
			}
			fLocator.setFocus(true);
			fLocator.select();
			fErrorMsg.setValue("Invalid PackageNo");
			return false;
		}
		
		if (mapProductQOH.containsKey(strLocator)) 
		{
			strLocator = mapProductQOH.get(strLocator);
		}
		m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
		if (m_Locator_ID <= 0) {
			//fLocator.set highlight text
			fLocator.setFocus(true);
			fLocator.select();
			fErrorMsg.setValue("Unknown Locator");
			return false;
		}
		else {
			updateErrorMessage();
		}
		
		return true;
	}
	
	private void onQty() {
		if (!isSerial) {
			number = fQty.getValue();
		}
		if (!isLocatorValid()) {
			return;
		}
		detach();
	}

	private void onLocator() {
		
		if(isMovement)
			return;

		String strLocator = (String) fLocator.getValue();
		boolean isValid = true;
		if (mapProductQOH.containsKey(strLocator)) 
		{
			strLocator = mapProductQOH.get(strLocator);
		}
		String sql; 
		if (isCrossDock) 	{
			sql = "SELECT COUNT(*) from M_InOutLine_CrossDock_V WHERE PackageNo = ? AND M_Product_ID = ?"; 
			int cnt = DB.getSQLValue(null,sql, strLocator, m_Product.getM_Product_ID());
			if (cnt <= 0) {
				fErrorMsg.setValue("Unknown Package");
				isValid = false;
			}
				
		} else {
			sql = "SELECT M_Locator_ID from M_Locator WHERE value = ? AND AD_Client_ID = ?";
			m_Locator_ID = DB.getSQLValue(null,sql, strLocator, Env.getAD_Client_ID(Env.getCtx()));
			if (m_Locator_ID <= 0) {
				fErrorMsg.setValue("Unknown Locator");
				isValid = false;
			}			
		}

		if (isValid) {
			updateErrorMessage();
		}
		if (isQtyUpdatable)
			fQty.setFocus(true);
		else 
			detach();

		
	}

	/**
	 * 
	 */
	private void updateErrorMessage() {
		String msg = "";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			if (isCrossDock) {
				sql = "SELECT MovementQty FROM M_InOutLine_CrossDock_V WHERE PackageNo = ? AND M_Product_ID = ?"; 
				BigDecimal qty = DB.getSQLValueBD(null,sql, getLocator(), m_Product.getM_Product_ID());
				msg = "Original Qty Received = " + qty.toString() + System.getProperty("line.separator") ;
				sql = "SELECT 'USED IN ' || io.documentno || ' | ' || pl.qty " +
		             "FROM m_inout io  " +
		             "  INNER JOIN m_inoutline iol ON io.m_inout_id = iol.m_inout_id  " +
		             "  INNER JOIN m_packageline pl ON iol.m_inoutline_id = pl.m_inoutline_id  " +
		             "WHERE pl.receiptline_id = ?  ";
				pstmt = DB.prepareStatement(sql, null);
				pstmt.setInt(1, getReceiptLine_ID());
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String res = rs.getString(1);
					msg = msg + System.getProperty("line.separator") + res;
				}
				
			} else {
				sql = "SELECT 'RESERVED IN ' || io.documentno || ' | ' || pl.qty " +
			          "FROM m_inout io " +
			          "  INNER JOIN m_inoutline iol ON io.m_inout_id = iol.m_inout_id " +
			          "  INNER JOIN m_packageline pl ON iol.m_inoutline_id = pl.m_inoutline_id " +
			             //"";
			          "WHERE io.docstatus NOT IN ('CO','CL','RE') " +
			          "AND   iol.m_product_id = ? " +
			          "AND   pl.m_locator_id = ? ";
				if (m_PackageLine_ID > 0) {
					sql = sql + " AND pl.m_packageline_id <> ?";
				}
				pstmt = DB.prepareStatement(sql, null);
				pstmt.setInt(1, m_Product.getM_Product_ID());
				pstmt.setInt(2, m_Locator_ID);
				if (m_PackageLine_ID > 0) {
					pstmt.setInt(3, m_PackageLine_ID);
				}
				rs = pstmt.executeQuery();
				while (rs.next()) {
					String res = rs.getString(1);
					msg = msg + System.getProperty("line.separator") + res;
				}
			}
			
		
		
		}
		catch (Exception e){
			log.severe(e.getMessage());
		}
		finally {
			DB.close(rs, pstmt);
		}
		fErrorMsg.setValue(msg.trim());
	}

	private void onSerial() {
		setSerial((String) fSerial.getValue());
		fLocator.setFocus(true);
		asiChanged = true;
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isCancelled() {
		return cancelled;
	}
	
	/**
	 * 
	 * @return text
	 */
	public BigDecimal getQty() {
		return number;
	}

	public String getProductName() {
		return (String) fProductName.getValue();
	}

	public void setProductName(String productName) {
		this.fProductName.setValue(productName);
	}

	public String getPCode() {
		return (String) fPCode.getValue();
	}

	public void setPCode(String pCode) {
		this.fPCode.setValue(pCode);
	}

	public String getSerial() {
		return (String) fSerial.getValue();
	}

	public void setSerial(String serial) {
		this.fSerial.setValue(serial);
		
		for (int i=0 ; i< table.getRowCount(); i++ ) {
			String attNo = (String) table.getValueAt(i, 1);
			if (attNo.equals(serial)) {
				table.setSelectedIndex(i);
			}
				
		}
	}

	public int getReceiptLine_ID()
	{
		if (isCrossDock) {
			if (mapProductQOH.containsKey(fLocator.getValue())) 
			{
				return mapReceiptLine.get(fLocator.getValue());
			}
		}
			
		return 0;
	}
	public String getLocator() {
		if (mapProductQOH.containsKey(fLocator.getValue())) 
		{
			return mapProductQOH.get(fLocator.getValue());
		}
		return (String) fLocator.getValue();
	}

	public void setLocator(String locator) {
		this.fLocator.setValue(locator);
	}

	public int getM_Locator_ID() {
		return m_Locator_ID;
	}

	public void setM_Locator_ID(int m_Locator_ID) {
		this.m_Locator_ID = m_Locator_ID;
	}

	public void setM_PackageLine_ID(int M_PackageLine_ID) {
		this.m_PackageLine_ID = M_PackageLine_ID;
	}
	public String getNotes() {
		return m_notes;
	}
	public void setNotes(String m_notes) {
		this.m_notes = m_notes;
	}

	
	private void loadASISelection()
	{
		if  (!showASISelection)
			return;
		
		table.setMultiSelection(true);
		table.setKeyColumnIndex(0);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		BigDecimal total = Env.ZERO;
		
		Vector<Object> line = null;
		try {
			pstmt = DB.prepareStatement(sqlASISelection, null);
			//pstmt.setInt(1, warehouseID);
			pstmt.setInt(1, m_Product.getM_Product_ID());
			pstmt.setInt(2, m_Warehouse_ID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				
				line = new Vector<Object>(7);
				line.add(new IDColumn(rs.getInt(1)));  // ID
				line.add(rs.getString(2));  // Att No
				line.add(new KeyNamePair(rs.getInt(3), rs.getString(4)));  // Locator
				line.add(rs.getBigDecimal(5));  // QtyOnHand
				line.add(rs.getBigDecimal(6));  // Multiplier
				line.add(rs.getBigDecimal(7));  // Qty
				line.add(rs.getBigDecimal(8));  // Total Qty
				data.add(line);

				
			}

		} catch (Exception e) {
			log.log(Level.SEVERE, sqlASISelection, e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
//		//Summary / Toal
//		line = new Vector<Object>(7);
//		line.add(null);  // ID
//		line.add(null);  // ID No
//		line.add(null);  // Locator
//		line.add(null);  // QtyOnHand
//		line.add(null);  // Multiplier
//		line.add(null);  // Qty
//		line.add(total);  // TotalQty
//		data.add(line);

		
		ListModelTable model = new ListModelTable(data);
		table.setData(model, asiColumns);
		
		table.setColumnClass(0, IDColumn.class, false);  //M_AttributeSetInstance_ID
		table.setColumnClass(1, String.class, true);  //ID No - Lot or Ser No
		table.setColumnClass(2, KeyNamePair.class, true);  //Locator
		table.setColumnClass(3, BigDecimal.class, true);  //QtyOnHand
		table.setColumnClass(4, BigDecimal.class, true);  //Multiplier
		table.setColumnClass(5, BigDecimal.class, m_Product.getM_AttributeSet().isSerNo());  //Qty  - SerialNo should always be 1
		table.setColumnClass(6, BigDecimal.class, true);  //Total Qty
		
		
		WListItemRenderer renderer = (WListItemRenderer) table.getItemRenderer();
		ZKUpdateUtil.setWidth(renderer.getHeaders().get(0), "50px");
//		renderer.getHeaders().get(3).setWidth("50px");  //line
//		renderer.getHeaders().get(4).setWidth("250px");  //line
		table.autoSize();
		
		
		// Highlight Summary Rows
//		table.getItemAtIndex(table.getRowCount() - 1).setStyle("Background-color: #BBBBBB;");
		
		table.addEventListener(Events.ON_SELECT, this);
		model.addTableModelListener(new WTableModelListener() {
			
			@Override
			public void tableChanged(WTableModelEvent event) {
				setSummary();
				asiChanged = true;
			}
		});
	}

	public void setSummary()
	{
		BigDecimal total = new BigDecimal(0.00);
		int items = table.getSelectedCount();
		
		if (isInProcess)
			return;
		
		isInProcess = true;
		//for ()
		
		for (int n :  table.getSelectedIndices()) {
			BigDecimal qty =  (BigDecimal) table.getValueAt(n, 5);
			BigDecimal multiplier =  (BigDecimal) table.getValueAt(n, 4);
			qty = qty.multiply(multiplier);
			table.setValueAt(qty, n, 6);
			total = total.add(qty);
		}
		
		//receipting process - no att no yet
		if (isSerial && table.getRowCount() == 0 ) {
			total = fQty.getValue();
		}
//		table.setValueAt(items, table.getRowCount() - 1, 1);
//		table.setValueAt(total, table.getRowCount() - 1, table.getColumnCount() -1);
		String msg = "Set Total Qty=" + DisplayType.getNumberFormat(DisplayType.Quantity).format(total) + "\n";
		
		if (items > 1)
			msg = msg + (items - 1) + " additional line(s) will be created.";
		
		fErrorMsg.setText(msg);
		isInProcess = false;
	}
	
	public void setStatus(String status)
	{
		fStatus.setText(status);
	}
	
	public AttSelections[] getSelectedASI()
	{
		if (!showASISelection)
			return null;
		
		ArrayList<AttSelections> list =  new ArrayList<AttSelections>();
		for (int n :  table.getSelectedIndices()) {
			String attNo = (String) table.getValueAt(n, 1);
			KeyNamePair locator = (KeyNamePair) table.getValueAt(n, 2);
			BigDecimal qty =  (BigDecimal) table.getValueAt(n, 6);
			list.add(new AttSelections(table.getRowKey(n), attNo, locator.getKey(), locator.getName(), qty));
		}

		AttSelections[] selections = new AttSelections[list.size()];
		return list.toArray(selections);
	}
	
	public class AttSelections {
		
		public AttSelections(int m_AttributeSetInstance_ID, String attNo, int m_Locator_ID, String locator,
				BigDecimal qty) {
			super();
			M_AttributeSetInstance_ID = m_AttributeSetInstance_ID;
			AttNo = attNo;
			M_Locator_ID = m_Locator_ID;
			Locator = locator;
			this.qty = qty;
		}
		
		int M_AttributeSetInstance_ID;
		String AttNo;
		int M_Locator_ID;
		String Locator;
		BigDecimal qty;
		
	}

	public boolean isAsiChanged() {
		return asiChanged;
	}
	public void setAsiChanged(boolean asiChanged) {
		this.asiChanged = asiChanged;
	}
	public int getM_warehouse_id() {
		return m_Warehouse_ID;
	}
	public void setM_warehouse_id(int m_warehouse_id) {
		this.m_Warehouse_ID = m_warehouse_id;
	}
}
