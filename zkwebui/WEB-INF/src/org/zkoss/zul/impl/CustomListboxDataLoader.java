package org.zkoss.zul.impl;

import org.zkoss.lang.Library;

public class CustomListboxDataLoader extends ListboxDataLoader {
public static final String LISTBOX_DATA_LOADER_LIMIT = "org.zkoss.zul.listbox.DataLoader.limit";
	
	/**
	 * 
	 */
	public CustomListboxDataLoader() {
	}

	/* (non-Javadoc)
	 * @see org.zkoss.zul.impl.ListboxDataLoader#getLimit()
	 */
	@Override
	public int getLimit() {
		String limit = Library.getProperty(LISTBOX_DATA_LOADER_LIMIT);
		if (limit != null) {
			return Integer.parseInt(limit);
		}
		return super.getLimit();
	}
}
