/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * Copyright (C) 2008 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.desktop;

import java.util.List;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.ProcessDialog;
import org.adempiere.webui.component.DesktopTabpanel;
import org.adempiere.webui.component.Tabbox;
import org.adempiere.webui.component.Tabpanel;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.info.InfoWindow;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.InfoGeneralPanel;
import org.adempiere.webui.panel.InfoPanel;
import org.adempiere.webui.part.WindowContainer;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.ADWindow;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MAsset;
import org.compiere.model.MBPartner;
import org.compiere.model.MColumn;
import org.compiere.model.MInOut;
import org.compiere.model.MInfoColumn;
import org.compiere.model.MInfoWindow;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.model.MPayment;
import org.compiere.model.MProduct;
import org.compiere.model.MQuery;
import org.compiere.model.MResourceAssignment;
import org.compiere.model.MTable;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.WebDoc;
import org.compiere.wf.MWorkflow;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanels;

/**
 * A Tabbed MDI implementation
 * @author hengsin
 *
 */
public abstract class TabbedDesktop extends AbstractDesktop {

	protected WindowContainer windowContainer;

	public TabbedDesktop() {
		super();
		windowContainer = new WindowContainer();
	}

	/**
     *
     * @param processId
     * @param soTrx
     * @return ProcessDialog
     */
	public ProcessDialog openProcessDialog(int processId, boolean soTrx) {
		ProcessDialog pd = new ProcessDialog (processId, soTrx);
		if (pd.isValid()) {
			DesktopTabpanel tabPanel = new DesktopTabpanel();
			pd.setParent(tabPanel);
			String title = pd.getTitle();
			pd.setTitle(null);
			preOpenNewTab();
			windowContainer.addWindow(tabPanel, title, true);
		}
		return pd;
	}

    /**
     *
     * @param formId
     * @return ADWindow
     */
	public ADForm openForm(int formId) {
		ADForm form = ADForm.openForm(formId);

		if (Window.Mode.EMBEDDED == form.getWindowMode()) {
			DesktopTabpanel tabPanel = new DesktopTabpanel();
			form.setParent(tabPanel);
			//do not show window title when open as tab
			form.setTitle(null);
			preOpenNewTab();
			windowContainer.addWindow(tabPanel, form.getFormName(), true);
		} else {
			form.setAttribute(Window.MODE_KEY, form.getWindowMode());
			showWindow(form);
		}

		form.setFocus(true);
		
		return form;
	}

	/*
	 * Feature #1449 Opening Info Window (non-Javadoc)
	 * @see org.adempiere.webui.desktop.IDesktop#openInfo(int)
	 */
	public void openInfo(int infoId)
	{
//		MInfoWindow infoWindow = new MInfoWindow(Env.getCtx(), infoId, (String) null);
		MInfoWindow infoWindow = MInfoWindow.get(infoId, null);
		String tableName = infoWindow.getAD_Table().getTableName();
		String keyColumn = tableName + "_ID";
		
		boolean isMulti = infoWindow.isMultiSelection();

		int WindowNo = 0;
		boolean isFoundInfoPanel = false;
		if (tableName.equals(MProduct.Table_Name) && AEnv.canAccessInfo("PRODUCT"))
		{
			InfoPanel.showProduct(WindowNo);
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MBPartner.Table_Name) && AEnv.canAccessInfo("BPARTNER"))
		{
			InfoPanel.showBPartner(WindowNo);
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MAsset.Table_Name) && AEnv.canAccessInfo("ASSET"))
		{
			InfoPanel.showAsset(WindowNo);
			isFoundInfoPanel = true;
		}
//		else if (tableName.equals("InfoAccount") && MRole.getDefault().isShowAcct() && AEnv.canAccessInfo("ACCOUNT"))
//		{
//			new org.adempiere.webui.acct.WAcctViewer();
//		}
//		else if (tableName.equals("InfoSchedule") && AEnv.canAccessInfo("SCHEDULE"))
//		{
//			new InfoSchedule(null, false);
//		}
		else if (tableName.equals(MOrder.Table_Name) && AEnv.canAccessInfo("ORDER"))
		{
			InfoPanel.showOrder(WindowNo, "");
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MInvoice.Table_Name) && AEnv.canAccessInfo("INVOICE"))
		{
			InfoPanel.showInvoice(WindowNo, "");
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MInOut.Table_Name) && AEnv.canAccessInfo("INOUT"))
		{
			InfoPanel.showInOut(WindowNo, "");
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MPayment.Table_Name) && AEnv.canAccessInfo("PAYMENT"))
		{
			InfoPanel.showPayment(WindowNo, "");
			isFoundInfoPanel = true;
		}
		else if (tableName.equals(MResourceAssignment.Table_Name) && AEnv.canAccessInfo("RESOURCE"))
		{
			InfoPanel.showAssignment(WindowNo, "");
			isFoundInfoPanel = true;
		}
		// Check table if view, potentially TableName + _ID will not apply
		else if (MTable.get(Env.getCtx(), tableName).isView()) 
		{
			MColumn column = MColumn.get(Env.getCtx(), tableName, keyColumn);
			if (column == null) {
				MInfoColumn[] infocolumns = infoWindow.getInfoColumns();
				
				for (MInfoColumn infocol : infocolumns ) {
					if (infocol.getAD_Reference_ID() == DisplayType.ID) {
						//assume this is the key
						keyColumn = infocol.getColumnName();
						break;
					}
				}
				//keyColumn = infoWindow.get_KeyColumns()[0];
			}
		}
		
		if (isFoundInfoPanel)
			return;
		
		InfoPanel info = InfoPanel.create(-1, tableName, keyColumn, null, isMulti, null, infoId, false, null);

		if (info != null && (info instanceof InfoWindow || info instanceof InfoGeneralPanel))
		{
			DesktopTabpanel tabPanel = new DesktopTabpanel();
			info.setParent(tabPanel);
			String title = info.getTitle();
			info.setTitle(null);
			preOpenNewTab();
			windowContainer.addWindow(tabPanel, title, true);
			info.focus();
		}
		else
		{
			FDialog.error(0, "NotValid");
		}
	}

	/**
	 *
	 * @param windowId
	 * @return ADWindow
	 */
	public ADWindow openWindow(int windowId) {
		ADWindow adWindow = new ADWindow(Env.getCtx(), windowId);

		DesktopTabpanel tabPanel = new DesktopTabpanel();
		if (adWindow.createPart(tabPanel) != null) {
			preOpenNewTab();
			windowContainer.addWindow(tabPanel, adWindow.getTitle(), true);
			return adWindow;
		} else {
			//user cancel
			return null;
		}
	}

	/**
	 *
	 * @param windowId
     * @param query
	 * @return ADWindow
	 */
	public ADWindow openWindow(int windowId, MQuery query) {
    	ADWindow adWindow = new ADWindow(Env.getCtx(), windowId, query);

		DesktopTabpanel tabPanel = new DesktopTabpanel();
		if (adWindow.createPart(tabPanel) != null) {
			preOpenNewTab();
			windowContainer.addWindow(tabPanel, adWindow.getTitle(), true);
			return adWindow;
		} else {
			//user cancel
			return null;
		}
	}


	/**
	 * @param url
	 */
	public void showURL(String url, boolean closeable)
    {
    	showURL(url, url, closeable);
    }

	/**
	 *
	 * @param url
	 * @param title
	 * @param closeable
	 */
    public void showURL(String url, String title, boolean closeable)
    {
    	Iframe iframe = new Iframe(url);
    	addWin(iframe, title, closeable);
    }

    /**
     * @param webDoc
     * @param title
     * @param closeable
     */
    public void showURL(WebDoc webDoc, String title, boolean closeable)
    {
    	Iframe iframe = new Iframe();

    	AMedia media = new AMedia(title, "html", "text/html", webDoc.toString().getBytes());
    	iframe.setContent(media);

    	addWin(iframe, title, closeable);
    }

    /**
     *
     * @param fr
     * @param title
     * @param closeable
     */
    private void addWin(Iframe fr, String title, boolean closeable)
    {
        ZKUpdateUtil.setWidth(fr, "100%");
        ZKUpdateUtil.setHeight(fr, "100%");
        fr.setStyle("padding: 0; margin: 0; border: none; position: absolute");
        Window window = new Window();
        ZKUpdateUtil.setWidth(window, "100%");
        ZKUpdateUtil.setHeight(window, "100%");
        window.setStyle("padding: 0; margin: 0; border: none");
        window.appendChild(fr);
        window.setStyle("position: absolute");

        Tabpanel tabPanel = new Tabpanel();
    	window.setParent(tabPanel);
    	preOpenNewTab();
    	windowContainer.addWindow(tabPanel, title, closeable);
    }

    /**
     * @param AD_Window_ID
     * @param query
     */
    public void showZoomWindow(int AD_Window_ID, MQuery query)
    {
    	ADWindow wnd = new ADWindow(Env.getCtx(), AD_Window_ID, query);

    	DesktopTabpanel tabPanel = new DesktopTabpanel();
    	if (wnd.createPart(tabPanel) != null)
    	{
    		preOpenNewTab();
    		windowContainer.insertAfter(windowContainer.getSelectedTab(), tabPanel, wnd.getTitle(), true, true);
    	}
	}

	/**
	 *
	 * @param window
	 */
	protected void showEmbedded(Window window)
   	{
		Tabpanel tabPanel = new Tabpanel();
    	window.setParent(tabPanel);
    	String title = window.getTitle();
    	window.setTitle(null);
    	preOpenNewTab();
    	if (Window.INSERT_NEXT.equals(window.getAttribute(Window.INSERT_POSITION_KEY)))
    		windowContainer.insertAfter(windowContainer.getSelectedTab(), tabPanel, title, true, true);
    	else
    		windowContainer.addWindow(tabPanel, title, true);
   	}

	/**
	 * Close active tab
	 * @return boolean
	 */
	public boolean closeActiveWindow()
	{
		if (windowContainer.getSelectedTab() != null)
		{
			Tabpanel panel = (Tabpanel) windowContainer.getSelectedTab().getLinkedPanel();
			Component component = panel.getFirstChild();
			Object att = component.getAttribute(WINDOWNO_ATTRIBUTE);

			if ( windowContainer.closeActiveWindow() )
			{
				if (att != null && (att instanceof Integer))
				{
					unregisterWindow((Integer) att);
				}
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	/**
	 * @return Component
	 */
	public Component getActiveWindow()
	{
		return windowContainer.getSelectedTab().getLinkedPanel().getFirstChild();
	}

	/**
	 *
	 * @param windowNo
	 * @return boolean
	 */
	public boolean closeWindow(int windowNo)
	{
		Tabbox tabbox = windowContainer.getComponent();
		Tabpanels panels = tabbox.getTabpanels();
		List<?> childrens = panels.getChildren();
		for (Object child : childrens)
		{
			Tabpanel panel = (Tabpanel) child;
			Component component = panel.getFirstChild();
			Object att = component.getAttribute(WINDOWNO_ATTRIBUTE);
			if (att != null && (att instanceof Integer))
			{
				if (windowNo == (Integer)att)
				{
					Tab tab = panel.getLinkedTab();
					panel.getLinkedTab().onClose();
					if (tab.getParent() == null)
					{
						unregisterWindow(windowNo);
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * invoke before a new tab is added to the desktop
	 */
	protected void preOpenNewTab()
	{
	}
}
