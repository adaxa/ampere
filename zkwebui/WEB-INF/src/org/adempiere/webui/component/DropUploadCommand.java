/*******************************************************************************
 * Copyright (C) 2017 Adaxa Pty Ltd. All Rights Reserved. This program is free
 * software; you can redistribute it and/or modify it under the terms version 2
 * of the GNU General Public License as published by the Free Software
 * Foundation. This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 ******************************************************************************/
package org.adempiere.webui.component;

import java.util.Map;

import org.adempiere.webui.event.DropUploadEvent;
import org.adempiere.webui.panel.WAttachment;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.au.AuService;
import org.zkoss.zk.mesg.MZk;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.Events;

/**
 * Command class to handle Drop Upload event.
 * 
 * @use {@link WAttachment}
 * @author <a href="mailto:sachin.bhimani89@gmail.com"> Sachin Bhimani </a>
 * @since Jan 22, 2018
 */
public class DropUploadCommand implements AuService
{

	public DropUploadCommand()
	{
	}

	public boolean service(AuRequest request, boolean everError)
	{
		if (!DropUploadEvent.ON_DROP_UPLOAD_EVENT.equals(request.getCommand()))
			return false;

		Map<?, ?> map = request.getData();
		String data = (String) map.get("data");

		final Component comp = request.getComponent();

		if (comp == null)
			throw new UiException(MZk.ILLEGAL_REQUEST_COMPONENT_REQUIRED, this);

		if (data == null || data.trim().length() == 0)
			throw new UiException(MZk.ILLEGAL_REQUEST_WRONG_DATA, new Object[] { data, this });

		Events.postEvent(new DropUploadEvent(comp, data));

		return true;
	}
}
