package org.adempiere.webui.component;

import java.util.Map;

import org.adempiere.webui.event.CaptureEvent;
import org.zkoss.lang.Objects;
import org.zkoss.json.JSONArray;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.au.AuService;
import org.zkoss.zk.mesg.MZk;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.Events;

public class CaptureCommand implements AuService 
{

	public CaptureCommand() {
	}

	public boolean service(AuRequest request, boolean everError) {
		if (!CaptureEvent.ON_CAPTURE_IMAGE.equals(request.getCommand()))
			return false;

		Map<?, ?> map = request.getData();
		JSONArray data = (JSONArray) map.get("data");

		final Component comp = request.getComponent();
		if (comp == null)
			throw new UiException(MZk.ILLEGAL_REQUEST_COMPONENT_REQUIRED, this);
		
		if (data == null || data.size() < 2)
			throw new UiException(MZk.ILLEGAL_REQUEST_WRONG_DATA, new Object[] {
					Objects.toString(data), this });
		
		Events.postEvent(new CaptureEvent(comp, data.get(0)));
		
		return true;
	}

}
