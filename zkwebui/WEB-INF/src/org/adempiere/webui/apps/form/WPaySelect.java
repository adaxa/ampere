/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 * Contributors:                                                              *
 * Colin Rooney (croo) Patch 1605368 Fixed Payment Terms & Only due           *
 *****************************************************************************/
package org.adempiere.webui.apps.form;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.Callback;
import org.adempiere.webui.apps.ProcessModalDialog;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Checkbox;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.editor.WDateEditor;
import org.adempiere.webui.event.WTableModelEvent;
import org.adempiere.webui.event.WTableModelListener;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.CustomForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.apps.form.PaySelect;
import org.compiere.minigrid.IDColumn;
import org.compiere.model.I_C_Bank;
import org.compiere.model.I_C_BankAccount;
import org.compiere.model.MBankAccount;
import org.compiere.model.MPaySelection;
import org.compiere.model.X_C_BankAccountDoc;
import org.compiere.model.X_C_PaySelection;
import org.compiere.process.ProcessInfo;
import org.compiere.util.ASyncProcess;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.compiere.util.ValueNamePair;
import org.zkoss.zk.ui.SuspendNotAllowedException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.North;
import org.zkoss.zul.Separator;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;
import org.zkoss.zul.Vlayout;

/**
 * Create Manual Payments From (AP) Invoices or (AR) Credit Memos. Allows user
 * to select Invoices for payment. When Processed, PaySelection is created and
 * optionally posted/generated and printed
 *
 * @author Jorg Janke
 * @version $Id: VPaySelect.java,v 1.3 2006/07/30 00:51:28 jjanke Exp $
 */
public class WPaySelect extends PaySelect implements IFormController, EventListener<Event>, WTableModelListener, ASyncProcess
{
	private CustomForm		form					= new CustomForm() {
		private static final long serialVersionUID = 7246431147010875583L;

		@Override
		public Mode getWindowMode() {
			return Mode.EMBEDDED;
		};
	};

	//
	private Panel			mainPanel				= new Panel();
	private Vlayout			mainLayout				= new Vlayout();
	private Panel			infoPanel				= new Panel();
	private Panel			parameterPanel			= new Panel();
	private Label 			lPaySelect 				= new Label();
	private Label 			paySelectName 			= new Label();
	private Label 			labelPSTotal 			= new Label();
	private Label 			labelPSInfo 			= new Label();
	private Label			labelBankAccount		= new Label();
	private Label			bankAccountName			= new Label();
	private Grid			infoLayout				= GridFactory.newGridLayout();
	private Grid			parameterLayout			= GridFactory.newGridLayout();
	private Label			labelBankBalance		= new Label();
	private Label			labelCurrency			= new Label();
	private Label			labelBalance			= new Label();
	private Label 			labelDueDate 			= new Label();
	private WDateEditor 	fieldDueFrom 			= new WDateEditor();
	private WDateEditor 	fieldDueTo 				= new WDateEditor();
	private Checkbox 		includeDisputed 		= new Checkbox();
	private Checkbox 		isDiscounted 			= new Checkbox();
	private Label			labelBPartner			= new Label();
	private Listbox			fieldBPartner			= ListboxFactory.newDropdownListbox();
	private Label			labelBPartnerGroup		= new Label();
	private Listbox			fieldBPartnerGroup		= ListboxFactory.newDropdownListbox();
	private Label			dataStatus				= new Label();
	private WListbox		miniTable				= ListboxFactory.newDataTable();
	private ConfirmPanel	commandPanel			= new ConfirmPanel(true, false, false, false, false, false, false);
	private Button			bCancel					= commandPanel.getButton(ConfirmPanel.A_CANCEL);
	private Button 			bSave 					= commandPanel.createButton(ConfirmPanel.A_SAVE);
	private Button			bGenerate				= commandPanel.createButton(ConfirmPanel.A_PROCESS);
	private Button			bRefresh				= commandPanel.createButton(ConfirmPanel.A_REFRESH);
	private Button 			bSelectAll 				= commandPanel.createButton(ConfirmPanel.A_CUSTOMIZE);
	private Label			labelPayDate			= new Label();
	private WDateEditor		fieldPayDate			= new WDateEditor();
	private Label			labelPaymentRule		= new Label();
	private Listbox			fieldPaymentRule		= ListboxFactory.newDropdownListbox();
	private Label			labelDtype				= new Label();
	private Listbox			fieldDtype				= ListboxFactory.newDropdownListbox();
	@SuppressWarnings("unused")
	private ProcessInfo		m_pi;
	private boolean			m_isLock;
	private boolean			isOpenFromPaySelection	= false;

	/**
	 * Initialize Panel
	 */
	public WPaySelect()
	{
		try
		{
			zkInit();
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "error", e);
		}
	} // init

	/**
	 * Static Init
	 * 
	 * @throws Exception
	 */
	private void zkInit() throws Exception
	{
		//
		form.appendChild(mainPanel);
		mainPanel.appendChild(mainLayout);
		mainPanel.setStyle("width: 100%; height: 100%; padding: 0; margin: 0");
		ZKUpdateUtil.setWidth(mainLayout, "99%");
		ZKUpdateUtil.setHeight(mainLayout, "100%");
		

		infoPanel.appendChild(infoLayout);
		ZKUpdateUtil.setHeight(infoLayout, "100%");
		ZKUpdateUtil.setHeight(infoPanel, "100%");
		infoLayout.setStyle("overflow: auto;");
		
		parameterPanel.appendChild(parameterLayout);
		ZKUpdateUtil.setHeight(parameterLayout, "100%");
		ZKUpdateUtil.setHeight(parameterPanel, "100%");
		parameterLayout.setStyle("overflow: auto;");
		//

		lPaySelect.setText(Msg.translate(Env.getCtx(), "C_PaySelection_ID") + ":");
		labelBankAccount.setText(Msg.translate(Env.getCtx(), "C_BankAccount_ID") + ":");
		labelBPartner.setText(Msg.translate(Env.getCtx(), "C_BPartner_ID"));
		labelBPartnerGroup.setText(Msg.translate(Env.getCtx(), "C_BP_Group_ID"));
		fieldBPartner.addActionListener(this);
		fieldBPartnerGroup.addActionListener(this);
		bRefresh.addActionListener(this);
		labelPSTotal.setText("Payment Selection total:");
		labelPSInfo.setText("0" + " -- " + "0" + " lines");
		
		labelPayDate.setText(Msg.translate(Env.getCtx(), "PayDate") + ":");
		labelPaymentRule.setText(Msg.translate(Env.getCtx(), "PaymentRule"));
		fieldPaymentRule.addActionListener(this);
		labelDtype.setText(Msg.translate(Env.getCtx(), "C_DocType_ID"));
		fieldDtype.addActionListener(this);
		//
		labelBankBalance.setText(Msg.translate(Env.getCtx(), "CurrentBalance"));
		labelBalance.setText("0");
		labelDueDate.setText(Msg.translate(Env.getCtx(), "DueDate"));
		includeDisputed.setText(Msg.translate(Env.getCtx(), "IncludeInDispute"));
		isDiscounted.setText(Msg.translate(Env.getCtx(), "OnlyDiscount"));
		dataStatus.setText(" ");
		dataStatus.setPre(true);
		//
		bGenerate.addActionListener(this);
		bSave.addActionListener(this);
		bCancel.addActionListener(this);
		bSelectAll.addActionListener(this);
		bSelectAll.setLabel(Msg.getMsg(Env.getCtx(),"SelectAll"));
		//
		Panel north = new Panel();
		north.setStyle("border: none");
		mainLayout.appendChild(north);
		north.appendChild(infoPanel);
		north.appendChild(parameterPanel);
		

		Rows rows = infoLayout.newRows();

		Row row = rows.newRow();
		row.appendCellChild(lPaySelect.rightAlign());
		row.appendCellChild(paySelectName);
		ZKUpdateUtil.setWidth(paySelectName, "99%");
		row.appendCellChild(labelPayDate.rightAlign());
		row.appendCellChild(fieldPayDate.getComponent());
		ZKUpdateUtil.setWidth(fieldPayDate.getComponent(), "99%");

		row.appendCellChild(labelPSTotal.rightAlign());
		ZKUpdateUtil.setWidth(labelPSTotal, "99%");
		row.appendCellChild(labelPSInfo);
		
		row = rows.newRow();
		row.appendCellChild(labelBankAccount.rightAlign());
		row.appendCellChild(bankAccountName, 2);
		ZKUpdateUtil.setWidth(bankAccountName, "99%");
		row.appendCellChild(labelBankBalance.rightAlign());
		row.appendCellChild(labelCurrency);
		ZKUpdateUtil.setWidth(labelCurrency, "99%");
		row.appendCellChild(labelBalance);
		ZKUpdateUtil.setWidth(labelBalance, "99%");
		
		rows = parameterLayout.newRows();
		
		row = rows.newRow();
		row.appendCellChild(labelBPartner.rightAlign());
		row.appendCellChild(fieldBPartner, 2);
		ZKUpdateUtil.setWidth(fieldBPartner, "99%");
		row.appendCellChild(labelBPartnerGroup.rightAlign());
		row.appendCellChild(fieldBPartnerGroup, 2);
		ZKUpdateUtil.setWidth(fieldBPartnerGroup, "99%");
		row.appendCellChild(labelDueDate.rightAlign());
		row.appendCellChild(fieldDueFrom.getComponent());
		row.appendCellChild(fieldDueTo.getComponent());	
		row.appendCellChild(new Space());

		row = rows.newRow();
		row.appendCellChild(labelDtype.rightAlign());
		row.appendCellChild(fieldDtype, 2);
		ZKUpdateUtil.setWidth(fieldDtype, "99%");
		row.appendCellChild(new Space(), 4);

		row = rows.newRow();
		row.appendCellChild(labelPaymentRule.rightAlign());
		row.appendCellChild(fieldPaymentRule, 2);
		ZKUpdateUtil.setWidth(fieldPaymentRule, "99%");
		row.appendCellChild(includeDisputed);
		row.appendCellChild(isDiscounted);
		row.appendCellChild(bRefresh);

		Panel center = new Panel();
		center.setVflex("1");
		mainLayout.appendChild(center);
		center.appendChild(miniTable);
		

		Panel south = new Panel();
		south.setStyle("border: none");
		mainLayout.appendChild(south);
		Vlayout southPanel = new Vlayout();
		southPanel.appendChild(dataStatus);
		southPanel.appendChild(new Separator());
		southPanel.appendChild(commandPanel);
		ZKUpdateUtil.setWidth(commandPanel, "100%");
		south.appendChild(southPanel);
		//
		commandPanel.addButton(bSave);
		commandPanel.addButton(bGenerate);
		commandPanel.addButton(bSelectAll);
		commandPanel.getButton(ConfirmPanel.A_OK).setVisible(false);
	} // jbInit

	/**
	 *  Dynamic Init.
	 *  - Load Bank Info
	 *  - Load BPartner
	 *  - Init Table
	 */
	private void dynInit()
	{		
		ArrayList<KeyNamePair> bpartnerData = getBPartnerData();
		for (KeyNamePair pp : bpartnerData)
			fieldBPartner.appendItem(pp.getName(), pp);
		fieldBPartner.setSelectedIndex(0);
		

		ArrayList<KeyNamePair> bpartnerGroupData = getBPartnerGroupData();
		for (KeyNamePair pp : bpartnerGroupData)
			fieldBPartnerGroup.appendItem(pp.getName(), pp);
		fieldBPartnerGroup.setSelectedIndex(0);

		ArrayList<KeyNamePair> docTypeData = getDocTypeData();
		for (KeyNamePair pp : docTypeData)
			fieldDtype.appendItem(pp.getName(), pp);

		miniTable.useZkSelection = false;
		prepareTable(miniTable, miniTable.useZkSelection);

		miniTable.getModel().addTableModelListener(this);
		//
		fieldPayDate.setMandatory(true);
		fieldPayDate.setValue(new Timestamp(System.currentTimeMillis()));
	} // dynInit

	/**
	 * Load Bank Info - Load Info from Bank Account and valid Documents
	 * (PaymentRule)
	 */
	private void loadBankInfo()
	{
		if (m_bi == null)
			return;
		
		
		labelCurrency.setText(m_bi.Currency);
		labelBalance.setText(m_format.format(m_bi.Balance));

		// PaymentRule
		fieldPaymentRule.removeAllItems();

		ArrayList<ValueNamePair> paymentRuleData = getPaymentRuleData(m_bi);
		
		int preferredRule = 0;
		
		for(int i = 0; i < paymentRuleData.size(); i++ )
		{
			ValueNamePair vp = paymentRuleData.get(i);
			fieldPaymentRule.appendItem(vp.getName(), vp);
			if ( vp.getValue().equals(X_C_BankAccountDoc.PAYMENTRULE_EFTAP))
				preferredRule = i;
		}
		
		fieldPaymentRule.setSelectedIndex(preferredRule);
		
	} // loadBankInfo

	/**
	 * Query and create TableInfo
	 */
	private void loadTableInfo()
	{
		Timestamp payDate = (Timestamp) fieldPayDate.getValue();
		miniTable.setColorCompare(payDate);
		log.config("PayDate=" + payDate);

		ValueNamePair paymentRule = (ValueNamePair) fieldPaymentRule.getSelectedItem().getValue();
		KeyNamePair bpartner = (KeyNamePair) fieldBPartner.getSelectedItem().getValue();
		KeyNamePair bpartnerGroup = (KeyNamePair) fieldBPartnerGroup.getSelectedItem().getValue();
		KeyNamePair docType = (KeyNamePair) fieldDtype.getSelectedItem().getValue();

		Timestamp dueFrom = (Timestamp) fieldDueFrom.getValue();
		Timestamp dueTo = (Timestamp) fieldDueTo.getValue();

		loadTableInfo(m_bi, payDate, paymentRule, dueFrom, dueTo, includeDisputed.isChecked(), 
				 isDiscounted.isChecked(), bpartner, bpartnerGroup, docType, miniTable);

		calculateSelection();
	} // loadTableInfo

	/**
	 * Dispose
	 */
	public void dispose()
	{
		SessionManager.getAppDesktop().closeActiveWindow();
	} // dispose

	/**************************************************************************
	 * ActionListener
	 * 
	 * @param e event
	 */
	public void onEvent(Event e)
	{
		//  Save PaySelection lines
		if (e.getTarget() == bSave)
		{
			savePaySelect();
		}
		// Generate PaySelection
		else if (e.getTarget() == bGenerate)
		{
			generatePaySelect();
		}

		else if (e.getTarget() == bCancel)
			dispose();

		// Update Open Invoices
		else if (e.getTarget() == fieldBPartner || e.getTarget() == bRefresh || e.getTarget() == fieldDtype)
			loadTableInfo();

		else if (e.getTarget() == bSelectAll)
		{
			int rows = miniTable.getModel().getRowCount();
			for (int i = 0; i < rows; i++)
			{

				Object val = miniTable.getModel().getValueAt(i, 0);
				if ( val instanceof IDColumn )
				{
					IDColumn idColumn = (IDColumn) val;
					idColumn.setSelected(true);
					miniTable.getModel().setValueAt(idColumn, i, 0);
				}
			}
				miniTable.repaint();			
				calculateSelection();
		}

	} // actionPerformed

	private void setPaySelect() {
		
		calculateSelection();
	}

	/**
	 * Table Model Listener
	 * 
	 * @param e event
	 */
	public void tableChanged(WTableModelEvent e)
	{
		if (e.getColumn() == 0)
			calculateSelection();
	} // valueChanged

	/**
	 *  Calculate selected rows.
	 *  - add up selected rows
	 */
	public void calculateSelection()
	{
		dataStatus.setText(calculateSelection(miniTable));
		//
		bSave.setEnabled(m_noSelected != 0 && (m_ps == null || !m_ps.isProcessed()));
		bGenerate.setEnabled(m_ps != null && !m_ps.isProcessed());
		
	} // calculateSelection

	/**
	 *  Save PaySelection lines from table
	 */
	private void savePaySelect()
	{
		if (miniTable.getRowCount() == 0)
			return;
		miniTable.setSelectedIndices(new int[] { 0 });
		calculateSelection();
		if (m_noSelected == 0)
			return;

		String msg = generatePaySelect(miniTable, (ValueNamePair) fieldPaymentRule.getSelectedItem().getValue(),
				new Timestamp(fieldPayDate.getComponent().getValue().getTime()), 
				m_bi);

		if (msg != null && msg.length() > 0)
		{
			FDialog.error(m_WindowNo, form, "SaveError", msg);
			return;
		}
		

		loadBankInfo();  // reloads payment selections
		fieldBPartner.setSelectedIndex(0);
		miniTable.clearTable();

		setPaySelect();
		calculateSelection();
	}   //  generatePaySelect

		/**
		 *  Generate PaySelection
		 */
	private void generatePaySelect()
	{
		calculateSelection();
		if (m_noSelected != 0)
			return;


		if (isOpenFromPaySelection)
		{
			dispose();
			return;
		}

		// Ask to Post it
		FDialog.ask(m_WindowNo, form, "VPaySelectGenerate?", "(" + m_ps.getName() + ")", new Callback<Boolean>() {

			@Override
			public void onCallback(Boolean result) 
			{
				if (result)
				{
				//  Prepare Process 
					int AD_Proces_ID = 155;	//	C_PaySelection_CreatePayment

					//	Execute Process
					ProcessModalDialog dialog = new ProcessModalDialog(WPaySelect.this, m_WindowNo, 
							AD_Proces_ID, X_C_PaySelection.Table_ID, m_ps.getC_PaySelection_ID(), false, null);
					if (dialog.isValid()) {
						try {
							dialog.setWidth("500px");
							dialog.setVisible(true);
							dialog.setPage(form.getPage());
							dialog.doHighlighted();
						} catch (SuspendNotAllowedException e) {
							log.log(Level.SEVERE, e.getLocalizedMessage(), e);
						}
					}
				}
			}
		});	
	} // generatePaySelect

	/**
	 * Lock User Interface Called from the Worker before processing
	 */
	public void lockUI(ProcessInfo pi)
	{
		if (m_isLock)
			return;
		m_isLock = true;
		Clients.showBusy(null);
	} // lockUI

	/**
	 * Unlock User Interface. Called from the Worker when processing is done
	 */
	public void unlockUI(ProcessInfo pi)
	{
		if (!m_isLock)
			return;
		m_isLock = false;
		m_pi = pi;
		Clients.clearBusy();

		this.dispose();
		if (!isOpenFromPaySelection)
		{
			// Start PayPrint
			int AD_Form_ID = 106; // Payment Print/Export
			ADForm form = SessionManager.getAppDesktop().openForm(AD_Form_ID);
			if (m_ps != null)
			{
				WPayPrint pp = (WPayPrint) form.getICustomForm();
				pp.setPaySelection(m_ps.getC_PaySelection_ID());
			}
		}
	}

	public void executeASync(ProcessInfo pi)
	{
	}

	public boolean isUILocked()
	{
		return m_isLock;
	}

	public ADForm getForm()
	{
		return form;
	}

	/**
	 * Configure Form for Payment Selection Window
	 */
	public void setPaymentSelection()
	{


		if (form.getProcessInfo() != null && form.getProcessInfo().getRecord_ID() > 0 )
			m_ps = new MPaySelection(Env.getCtx(), form.getProcessInfo().getRecord_ID(), null);
		else
			throw new AdempiereException("No Payment Selection in context");

		dynInit();
		loadBankInfo();

		ArrayList<BankInfo> bankAccounts = getBankAccountData();
		
		for ( BankInfo bankinfo : bankAccounts )
		{
			if ( bankinfo.C_BankAccount_ID == m_ps.getC_BankAccount_ID() )
				m_bi = bankinfo;
			loadBankInfo();
		}
		
		MBankAccount ba = (MBankAccount) m_ps.getC_BankAccount();
		
		if (m_ps != null)
		{

			fieldPayDate.setValue(m_ps.getPayDate());
			fieldPayDate.setReadWrite(false);
			bankAccountName.setText(ba.getName());
			paySelectName.setText(m_ps.getDocumentInfo());
			labelPSInfo.setText(m_ps.getTotalAmt() + " -- " + m_ps.getLines(true).length + " lines");

		}
		loadTableInfo();
		isOpenFromPaySelection = true;
	}
} // VPaySelect
