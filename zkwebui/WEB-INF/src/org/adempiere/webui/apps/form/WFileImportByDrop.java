package org.adempiere.webui.apps.form;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.editor.WYesNoEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.AbstractADWindowPanel;
import org.adempiere.webui.window.FDialog;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.compiere.impexp.ImpFormat;
import org.compiere.model.MTab;
import org.compiere.util.AdempiereSystemError;
import org.compiere.util.AdempiereUserError;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Center;
import org.zkoss.zul.South;
import org.zkoss.zul.Space;

/**
 * On Toolbar dropped files importing to the Importable window
 * 
 * @author Sachin Bhimani
 */
public class WFileImportByDrop extends ADForm implements EventListener
{

	/**
	 * 
	 */
	private static final long		serialVersionUID	= 1509663574381516560L;

	/** Logger */
	private static CLogger			log					= CLogger.getCLogger(WFileImportByDrop.class);

	//
	private AbstractADWindowPanel	winPanel			= null;
	private ImpFormat				impFormat			= null;
	private File					tmpFile				= null;

	// Layout
	private Borderlayout			mainLayout			= new Borderlayout();

	// Parameters
	private Label					lblLoaderFormat		= new Label(Msg.translate(Env.getCtx(), "AD_ImpFormat_ID"));
	private Listbox 				fLoaderFormat 		= ListboxFactory.newDropdownListbox();

	private WYesNoEditor			chkIncludeHeader	= new WYesNoEditor("IsExcludeHeader", "IsExcludeHeader",
			"Exclude the first row of the file (e.g. if it contains headers)", false, false, true);

	// Buttons
	private ConfirmPanel			confirmPanel		= new ConfirmPanel(true);
	private Button					btn_OK				= confirmPanel.getOKButton();
	private Button					btn_CANCEL			= confirmPanel.getButton(ConfirmPanel.A_CANCEL);
	
	/**
	 * Constructor
	 * 
	 * @param winPanel
	 * @param media
	 */
	public WFileImportByDrop(AbstractADWindowPanel winPanel, Media media)
	{
		this.winPanel = winPanel;

		 //Create temporary file
		if (media.isBinary())
		{
			InputStream iStream = media.getStreamData();
			if (iStream == null)
			{
				FDialog.error(m_WindowNo, "File Content not found");
				return;
			}

			try
			{
				tmpFile = File.createTempFile("tmp_", "_DropToImport");
				tmpFile.deleteOnExit();
				FileOutputStream out = new FileOutputStream(tmpFile);
				IOUtils.copy(iStream, out);
			}
			catch (IOException e)
			{
				throw new AdempiereException(e);
			}
			finally
			{
				if(tmpFile == null)
				{
					throw new AdempiereException("File not found");
				}
			}
		} else {
			try {
				tmpFile = File.createTempFile("tmp_", "_DropToImport");
				tmpFile.deleteOnExit();
				FileUtils.writeStringToFile(tmpFile, media.getStringData());
			} catch (Exception e) {
				
			}
		}

		initForm();
		confirmPanel.addActionListener(this);
	} //

	@Override
	protected void initForm()
	{
		// Layout
		Grid grid = GridFactory.newGridLayout();
		Row row = grid.newRows().newRow();
		row.appendChild(lblLoaderFormat);
		row.appendChild(fLoaderFormat);

		row = ((Rows) row.getParent()).newRow();
		row.appendChild(new Space());
		chkIncludeHeader.setValue(true);
		row.appendChild(chkIncludeHeader.getComponent());

		Center center = new Center();
		center.appendChild(grid);

		South south = new South();
		south.appendChild(confirmPanel);

		mainLayout.appendChild(center);
		mainLayout.appendChild(south);
		mainLayout.setParent(this);
		
		loadCompatibleImportFormat();
	} // initForm

	@Override
	public void onEvent(Event e) throws Exception
	{
		if (e.getTarget() == btn_OK)
		{
			cmd_OK();
			winPanel.onRefresh(false);
			detach();
		}
		else if (e.getTarget() == btn_CANCEL)
		{
			dispose();
			return;
		}
	} // onEvent

	private void cmd_OK() throws AdempiereUserError, AdempiereSystemError
	{
		if (fLoaderFormat.getSelectedCount() == 0)
		{
			FDialog.error(m_WindowNo, fLoaderFormat, "FillMandatory", "Import Format");
			return;
		}

		boolean isExcludeHeader = (boolean) chkIncludeHeader.getValue();
		int AD_ImpFormat_ID = (int) fLoaderFormat.getSelectedItem().getValue();

		if (AD_ImpFormat_ID <= 0)
			throw new AdempiereUserError("Import Format required");

		impFormat = ImpFormat.load(AD_ImpFormat_ID);
		if (impFormat == null)
			throw new AdempiereSystemError("Import Format not loaded");

		//
		int loaded = impFormat.loadFile(Env.getCtx(), null, tmpFile, null, Env.getAD_Client_ID(Env.getCtx()),
				Env.getAD_Org_ID(Env.getCtx()), Env.getAD_User_ID(Env.getCtx()), isExcludeHeader);

		if (winPanel.getActiveGridTab().getParentTab() != null) {
			MTab tab = new MTab(Env.getCtx(), winPanel.getActiveGridTab().getAD_Tab_ID(), null);
			String linkColumn = tab.getAD_Column().getColumnName();
			int parentID = (int) winPanel.getActiveGridTab().getParentTab().getValue(tab.getParent_Column().getColumnName());
					
			log.fine("Tab is a Child Tab. Link Column = " + linkColumn  + " Parent ID=" + parentID);
			impFormat.setLinkColumn(Env.getCtx(), linkColumn, parentID, null);
		}
		FDialog.info(m_WindowNo, this, "Imported Rows: " + loaded);
		log.info("File:" + tmpFile.getName() + ", Loaded Rows: " + loaded);
	} // cmd_OK
	
	private void loadCompatibleImportFormat()
	{
		String sql = "SELECT ad_impformat_id, name " +
	             "FROM ad_impformat " +
	             "WHERE ad_client_id IN (0, ?) " +
	             "AND AD_Table_ID = ? AND IsActive = 'Y' " +
	             "ORDER BY name ASC";

		try
		{
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			pstmt.setInt(1, Env.getAD_Client_ID(Env.getCtx()));
			pstmt.setInt(2, winPanel.getActiveGridTab().getAD_Table_ID());

			ResultSet rs = pstmt.executeQuery();
			while (rs.next())
			{
				fLoaderFormat.addItem(new KeyNamePair(rs.getInt(1), rs.getString(2)));
			}
		}
		catch (Exception e)
		{
			log.severe(e.getMessage());
		}

		if (fLoaderFormat.getItemCount() == 1)
		{
			fLoaderFormat.setSelectedIndex(0);
		}
	} // loadCompatibleImportFormat
}
