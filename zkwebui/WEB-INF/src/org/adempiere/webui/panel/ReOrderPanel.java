/**
 * 
 */
package org.adempiere.webui.panel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.component.Borderlayout;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.GridPanel;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHead;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.SimpleListModel;
import org.adempiere.webui.factory.ButtonFactory;
import org.adempiere.webui.theme.ThemeUtils;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.Env;
import org.compiere.util.NamePair;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Center;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Vbox;

/**
 * @author jtrinidad
 */
public class ReOrderPanel extends Panel
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= -8743156345951054766L;

	static CLogger					log					= CLogger.getCLogger(ReOrderPanel.class);

	/**
	 * Key = ID of Record
	 * Value = Label String
	 */
	private LinkedHashMap<Integer, String>		m_treeItems			= null;
	GridPanel						gridPanel			= null;

	private int						m_WindowNo;
	private int						m_AD_Table_ID;

	// UI variables
	private Label					yesLabel			= new Label();

	private Button					bTop				= ButtonFactory.createButton(null, ThemeUtils.resolveImageURL("First16.png"), null);
	private Button					bUp					= ButtonFactory.createButton(null, ThemeUtils.resolveImageURL("Previous16.png"), null);
	private Button					bDown				= ButtonFactory.createButton(null, ThemeUtils.resolveImageURL("Next16.png"), null);
	private Button					bBottom				= ButtonFactory.createButton(null, ThemeUtils.resolveImageURL("Last16.png"), null);

	private South					south				= new South();
	//
	SimpleListModel					seqModel			= new SimpleListModel();
	Listbox							seqList				= new Listbox();

	private boolean					uiCreated;
	private boolean					m_saved				= false;
	private ConfirmPanel			confirmPanel		= new ConfirmPanel(true, false, false, false, false, false);

	private String m_ParentItem;

	private String m_ColumnSortName;

	/**
	 * Sort Tab Constructor
	 *
	 * @param WindowNo Window No
	 * @param columnsWidth
	 * @param GridTab
	 */
	public ReOrderPanel(int WindowNo, int AD_Table_ID, String columnSortName, String parentItem, LinkedHashMap<Integer, String> treeItems)
	{
		m_WindowNo = WindowNo;

		m_AD_Table_ID = AD_Table_ID;
		m_ColumnSortName = columnSortName;
		m_treeItems = treeItems;
		m_ParentItem = parentItem;
		this.setStyle("position: relative; height: 100%; width: 100%; margin: none; border: none; padding: none;");
	}

	/**
	 * Static Layout
	 * 
	 * @throws Exception
	 */
	private void init() throws Exception
	{
		Borderlayout layout = new Borderlayout();
//		layout.setStyle("height: 100%; width: 100%; border: none; margin: none; padding: 2px;");

		yesLabel.setValue(m_ParentItem);
		ZKUpdateUtil.setHflex(seqList, "1");
		ZKUpdateUtil.setVflex(seqList, "1");
		
		seqList.setStyle("background-color: #f0f0f0;");


		seqList.setSeltype("multiple");
		seqList.setMultiple(true);
		seqList.setCheckmark(true);
		seqModel.setMultiple(true);

		EventListener<Event> crossListMouseListener = new DragListener();
		seqList.addOnDropListener(crossListMouseListener);
		seqList.setItemDraggable(true);

		EventListener<Event> actionListener = new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception
			{
				migrateValueWithinYesList(event);
			}
		};

		bTop.addEventListener(Events.ON_CLICK, actionListener);
		bBottom.addEventListener(Events.ON_CLICK, actionListener);
		bUp.addEventListener(Events.ON_CLICK, actionListener);
		bDown.addEventListener(Events.ON_CLICK, actionListener);

		ListHead listHead = new ListHead();
		listHead.setParent(seqList);
		ListHeader listHeader = new ListHeader();
		listHeader.appendChild(yesLabel);
		listHeader.setParent(listHead);


		Hbox hbox = new Hbox();

		//North
		North north = new North();
		layout.appendChild(north);
		north.appendChild(hbox);

		//Center
		hbox = new Hbox();
		Center center = new Center();
		layout.appendChild(center);
		ZKUpdateUtil.setHflex(hbox, "1");
		ZKUpdateUtil.setVflex(hbox, "1");
		
		Vbox vbox = new Vbox();
		
		hbox.appendChild(seqList);
		vbox = new Vbox();
		vbox.appendChild(bTop);
		vbox.appendChild(bUp);
		vbox.setSpacing("10px");
		vbox.appendChild(bDown);
		vbox.appendChild(bBottom);
		ZKUpdateUtil.setWidth(vbox, "50px");
		
		hbox.appendChild(vbox);
		
		center.appendChild(hbox);
				
		layout.appendChild(south);
		
		ZKUpdateUtil.setHeight(south, "60px");
		

		LayoutUtils.addSclass("dialog-footer", confirmPanel);
		EventListener<Event> onClickListener = new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception
			{
				if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_OK)))
				{
					saveData();
				}
				else if (event.getTarget().equals(confirmPanel.getButton(ConfirmPanel.A_CANCEL)))
				{
					getParent().detach();
				}
			}
		};

		ZKUpdateUtil.setHflex(confirmPanel, "1");
		confirmPanel.addActionListener(onClickListener);
		south.setStyle("padding-top:20px");
		south.appendChild(confirmPanel);
		layout.appendChild(south);

		this.appendChild(layout);

	} // init

	public void loadData()
	{
		int index = 0;
		for (Map.Entry<Integer, String> entry : m_treeItems.entrySet()) {
			ListElement pp = new ListElement(entry.getKey(), entry.getValue());
			seqModel.add(index, pp);
			index++;
			
		}
		

		bTop.setEnabled(true);
		bBottom.setEnabled(true);
		bUp.setEnabled(true);
		bDown.setEnabled(true);
		seqList.setEnabled(true);
		seqList.setItemRenderer(seqModel);
		seqList.setModel(seqModel);

	} // loadData


	/**
	 * Move within Sequence List with Drag Event and Multiple Choice
	 * 
	 * @param event event
	 */
	void moveTo(int endIndex, List<ListElement> selObjects)
	{
		int iniIndex = 0;
		Arrays.sort(selObjects.toArray());
		ListElement endObject = (ListElement) seqModel.getElementAt(endIndex);
		for (ListElement selected : selObjects)
		{
			iniIndex = seqModel.indexOf(selected);
			ListElement selObject = (ListElement) seqModel.getElementAt(iniIndex);
			seqModel.removeElement(selObject);
			endIndex = seqModel.indexOf(endObject) + 1;
			seqModel.add(endIndex, selObject);
		}
	}

	/**
	 * Move within Yes List
	 * 
	 * @param event event
	 */
	void migrateValueWithinYesList(Event event)
	{
		Object[] selObjects = seqList.getSelectedItems().toArray();

		if (selObjects == null)
			return;
		int length = selObjects.length;
		if (length == 0)
			return;
		//
		int[] indices = seqList.getSelectedIndices();
		Arrays.sort(indices);
		//
		boolean change = false;
		//
		Object source = event.getTarget();
		if (source == bUp)
		{
			for (int i = 0; i < length; i++)
			{
				int index = indices[i];
				if (index == 0)
					break;
				ListElement selObject = (ListElement) seqModel.getElementAt(index);
				ListElement newObject = (ListElement) seqModel.getElementAt(index - 1);
				if (!selObject.isUpdateable() || !newObject.isUpdateable())
					break;
				seqModel.setElementAt(newObject, index);
				seqModel.setElementAt(selObject, index - 1);
				indices[i] = index - 1;
				change = true;
			}
		} // up

		else if (source == bDown)
		{
			for (int i = length - 1; i >= 0; i--)
			{
				int index = indices[i];
				if (index >= seqModel.getSize() - 1)
					break;
				ListElement selObject = (ListElement) seqModel.getElementAt(index);
				ListElement newObject = (ListElement) seqModel.getElementAt(index + 1);
				if (!selObject.isUpdateable() || !newObject.isUpdateable())
					break;
				seqModel.setElementAt(newObject, index);
				seqModel.setElementAt(selObject, index + 1);
				seqList.setSelectedIndex(index + 1);
				indices[i] = index + 1;
				change = true;
			}
		} // down
		else if (source == bBottom)
		{
			int listSize = seqModel.getSize();
			for (int i = 0; i < length; i++) {
				int index = indices[i];
				ListElement selObject = (ListElement) seqModel.getElementAt(index-i);  //offset index because of the removeElement 
				int atBottom = listSize - length + i;
				if (!selObject.isUpdateable() )
					break;
				seqModel.removeElement(selObject);
				seqModel.addElement(selObject);
				indices[i] = atBottom ;
				change = true;
			}
		} //bottom
		else if (source == bTop)
		{
//			for (int i = 0; i < length; i++) {
//				int index = indices[i];
//				if (index == 0 || (i - 1 >= 0 && indices[i - 1] == index - 1))
//					continue;
//				ListElement selObject = (ListElement) seqModel.getElementAt(index);
//				ListElement newObject = (ListElement)seqModel.getElementAt(i);
//				if (!selObject.isUpdateable() || !newObject.isUpdateable())
//					break;
//				seqModel.setElementAt(newObject, i + length);
//				seqModel.setElementAt(selObject, i);
//				indices[i] = i;
//				change = true;
//			}
			List<ListElement> selEObjects = new ArrayList<ListElement>();
			for (Object obj : seqList.getSelectedItems())
			{
				ListItem listItem = (ListItem) obj;
				int index = seqList.getIndexOfItem(listItem);
				ListElement selObject = (ListElement) seqModel.getElementAt(index);
				selEObjects.add(selObject);
			}
			moveTo(-1, selEObjects);
		}  //top

		//
		if (change)
		{
			seqList.setSelectedIndices(indices);
			if (seqList.getSelectedItem() != null)
			{
				AuFocus focus = new AuFocus(seqList.getSelectedItem());
				Clients.response(focus);
			}
		}
	} // migrateValueWithinYesList

	public void saveData()
	{
		
		MTable table = MTable.get(Env.getCtx(), m_AD_Table_ID);

		boolean ok = true;
		StringBuilder custom = new StringBuilder();
		int index = 0;

		for (int i = 0; i < seqModel.getSize(); i++)
		{
			ListElement pp = (ListElement) seqModel.getElementAt(i);
			PO po = table.getPO(pp.getKey(), null);
			index += 10;

			po.set_ValueOfColumn(m_ColumnSortName, index);
			
			po.saveEx();

		}
		//
		if (ok)
		{
			m_saved = true;
//			if (gridPanel != null)
//			{
//				Events.postEvent(CustomizeGridPanelDialog.CUSTOMIZE_GRID, gridPanel, null);
//			}
			getParent().detach();
		}
		else
		{
			FDialog.error(m_WindowNo, null, "SaveError", custom.toString());
		}
	} // saveData

	public void activate(boolean b)
	{
		if (b && !uiCreated)
			createUI();
	}

	public void createUI()
	{
		if (uiCreated)
			return;
		try
		{
			init();
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
		uiCreated = true;
	}

	public boolean isSaved()
	{
		return m_saved;
	}

	/**
	 * ListElement Item
	 */
	private static class ListElement extends NamePair
	{
		/**
		 * 
		 */
		private static final long	serialVersionUID	= -1717531470895073281L;

		private int					m_key;

		private boolean				m_updateable;

		public ListElement(int key, String name)
		{
			super(name);
			this.m_key = key;
			this.m_updateable = true;
		}

		public int getKey()
		{
			return m_key;
		}

		public boolean isUpdateable()
		{
			return m_updateable;
		}

		@Override
		public String getID()
		{
			return m_key != -1 ? String.valueOf(m_key) : null;
		}

		@Override
		public int hashCode()
		{
			return m_key;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (obj instanceof ListElement)
			{
				ListElement li = (ListElement) obj;
				return li.getKey() == m_key && li.getName() != null && li.getName().equals(getName());
			}
			return false;
		} // equals

		@Override
		public String toString()
		{
			String s = super.toString();
			if (s == null || s.trim().length() == 0)
				s = "<" + getKey() + ">";
			return s;
		}
	} // ListElement Class

	/**
	 * DragListener Class
	 */
	private class DragListener implements EventListener<Event>
	{

		/**
		 * Creates a ADSortTab.DragListener.
		 */
		public DragListener()
		{
		}

		@Override
		public void onEvent(Event event) throws Exception
		{
			if (event instanceof DropEvent)
			{
				int endIndex = 0;
				DropEvent me = (DropEvent) event;
				ListItem endItem = (ListItem) me.getTarget();
				ListItem startItem = (ListItem) me.getDragged();

				if (!startItem.isSelected())
					startItem.setSelected(true);


				List<ListElement> selObjects = new ArrayList<ListElement>();
				endIndex = seqList.getIndexOfItem(endItem);
				for (Object obj : seqList.getSelectedItems())
				{
					ListItem listItem = (ListItem) obj;
					int index = seqList.getIndexOfItem(listItem);
					ListElement selObject = (ListElement) seqModel.getElementAt(index);
					selObjects.add(selObject);
				}
				moveTo(endIndex, selObjects);
				seqList.clearSelection();
				
			}
		}
	} // DragListener Class

} // CustomizeGridPanel Class
