package org.adempiere.webui.panel;

public class CustomForm extends ADForm
{
	private static final long serialVersionUID = 1L;

	/**
	 * hard coded info ID to enable special forms to use tab customization
	 */
	public static final int	WACCTVIEWER_ID = 100;
	
	public static final int	WFACTERECONCILE_INFO_ID = 1000; 
	public static final int	WTRIALBALANCE_INFO_ID = 1001; 
	public static final int	WPAYSELECT_INFO_ID = 1002; 
	public static final int	WALLOCATION_INVOICE_INFO_ID = 1003; 
	public static final int	WALLOCATION_PAYMENT_INFO_ID = 1004; 

	public static final int	WCREATEFROMBANKSTATEMENT_INFO_ID = 1006; 
	public static final int	WCREATEFROMNVOICE_INFO_ID = 1007; 
	public static final int	WCREATEFROMSHIPMENT_INFO_ID = 1008; 

	
	@Override
	protected void initForm() 
	{
		
	}
}
