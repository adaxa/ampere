/******************************************************************************
 * Copyright (C) 2009 Low Heng Sin                                            *
 * Copyright (C) 2009 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.report;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.theme.ThemeUtils;
import org.apache.ecs.ConcreteElement;
import org.apache.ecs.xhtml.a;
import org.apache.ecs.xhtml.body;
import org.apache.ecs.xhtml.div;
import org.apache.ecs.xhtml.img;
import org.compiere.print.IHTMLExtension;
import org.compiere.print.MPrintFormatItem;
import org.compiere.print.PrintData;
import org.compiere.print.PrintDataElement;
import org.compiere.util.Env;
import org.compiere.util.Msg;

/**
 * 
 * @author hengsin
 *
 */
public class HTMLExtension implements IHTMLExtension {

	private String contextPath;
	private String classPrefix;
	private String componentId;

	public HTMLExtension(String contextPath, String classPrefix, String componentId) {
		this.contextPath = contextPath;
		this.classPrefix = classPrefix;
		this.componentId = componentId;
	}
	
	public void extendIDColumn(int row, ConcreteElement columnElement, a href,
			PrintDataElement dataElement, MPrintFormatItem pfi, int AD_PInstance_ID) {
		String columnName = dataElement.getColumnName();
		String foreignColumnName = dataElement.getForeignColumnName();
		if (columnName.equalsIgnoreCase("Account_ID")) {
			columnName = "C_ElementValue_ID";
		}
		if (foreignColumnName.equalsIgnoreCase("Account_ID")) {
			foreignColumnName = "C_ElementValue_ID";
		}
		
			
		href.addAttribute("onclick", "this.style.color='red'; showColumnMenu(event, '" + columnName + "', " + row + ")");		
		href.addAttribute("onclick", "showColumnMenu(event, '" + columnName + "', " + row + ")");		
		href.addAttribute ("componentId", componentId);
		href.addAttribute ("foreignColumnName", foreignColumnName);
		href.addAttribute ("value", dataElement.getValueAsString());		
	}

	public void extendRowElement(ConcreteElement row, PrintData printData) {
		PrintDataElement pkey = printData.getPKey();
		if (pkey != null)
		{
			row.addAttribute("ondblclick", "parent.drillAcross('" 
					+ componentId + "', '" 
					+ pkey.getColumnName() + "', '" 
					+ pkey.getValueAsString() + "')");
		}
	}

	public String getClassPrefix() {
		return classPrefix;
	}

	public String[] getScriptURL()	{
		String scripts[] = { contextPath	+ "/js/jquery.1.7.0.js", contextPath + "/js/report.js", contextPath + "/js/clusterize.min.js",
								contextPath + "/js/clusterize_support.js" };
		return scripts;
	}

	public String[] getStyleURL() {
		String styles[] = { contextPath + "/css/report.css", contextPath + "/css/clusterize.css" };
		return styles;
	}

	public String getComponentId() {
		return componentId;
	}
	
	public void setWebAttribute (body reportBody){
		// set attribute value for create menu context
		String context = contextPath;
		if (context.length() > 0 && !context.endsWith("/")) {
			context += "/";
		}
		
		reportBody.addAttribute("windowIco", context + ThemeUtils.resolveImageURL(ThemeUtils.MENU_WINDOW_IMAGE));
		reportBody.addAttribute("reportIco", context + ThemeUtils.resolveImageURL(ThemeUtils.MENU_REPORT_IMAGE)) ;
		reportBody.addAttribute ("reportLabel", Msg.getMsg(AEnv.getLanguage(Env.getCtx()), "Report").replace("&", ""));
		reportBody.addAttribute ("windowLabel", Msg.getMsg(AEnv.getLanguage(Env.getCtx()), "Window"));
		
	}
	
}
