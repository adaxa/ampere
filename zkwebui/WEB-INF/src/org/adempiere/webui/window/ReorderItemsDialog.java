package org.adempiere.webui.window;

import java.util.LinkedHashMap;

import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.panel.ReOrderPanel;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.util.Env;
import org.compiere.util.Msg;

public class ReorderItemsDialog extends Window
{
	/**
	 * 
	 */
	private static final long		serialVersionUID	= 9077416157436132855L;

	public static final String		CUSTOMIZE_GRID		= "onCustomizeGrid";

	private ReOrderPanel			reorderPanel;

	/**
	 * Standard Constructor
	 * 
	 * @param WindowNo window no
	 * @param AD_Tab_ID tab
	 * @param AD_User_ID user
	 * @param columnsWidth
	 * @param gridFieldIds
	 * @param isQuickForm
	 */
	public ReorderItemsDialog(int windowNo, int AD_Table_ID, String columnSortName, String parentItem, LinkedHashMap<Integer, String> treeItems)
	{
		setClosable(true);
		setSizable(true);
		setTitle(Msg.getMsg(Env.getCtx(), "Re-arrange Selected Items"));

		initComponent(windowNo, AD_Table_ID, columnSortName, parentItem, treeItems);
	}

	/**
	 * Initialize component of customize panel
	 * 
	 * @param windowNo
	 * @param AD_Table_ID
	 * @param AD_User_ID
	 * @param columnsWidth
	 * @param gridFieldIds
	 */
	private void initComponent(int windowNo, int AD_Table_ID, String columnSortName, String parentItem, LinkedHashMap<Integer, String> treeItems)
	{

		reorderPanel = new ReOrderPanel(windowNo, AD_Table_ID, columnSortName, parentItem, treeItems);
		this.setStyle("position : absolute;");
		this.setBorder("normal");
		this.setSclass("popup-dialog");
		
		ZKUpdateUtil.setWidth(this, "550px");
		ZKUpdateUtil.setHeight(this, "600px");
		reorderPanel.createUI();
		reorderPanel.loadData();
		appendChild(reorderPanel);
	} // initComponent

	/**
	 * @return whether change have been successfully save to DB
	 */
	public boolean isSaved()
	{
		return reorderPanel.isSaved();
	}



	public static boolean showRearrangeTreeItems(int WindowNo, String columnSortName, String parentItem, int AD_Table_ID, LinkedHashMap <Integer, String> treeItems)
	{

		ReorderItemsDialog sequenceWindow = new ReorderItemsDialog(WindowNo, AD_Table_ID, columnSortName,  parentItem, treeItems);
		

			//customizeWindow.setGridPanel(gridPanel);
		AEnv.showWindow(sequenceWindow);

		return sequenceWindow.isSaved();
	} // showCustomize
}
