<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-combobox,
.z-bandbox,
.z-datebox,
.z-timebox,
.z-spinner,
.z-doublespinner {
  display: inline-block;
  min-height: 24px;
  line-height: normal;
  white-space: nowrap;
  clear: both;
  position: relative;
}
.z-combobox-input,
.z-bandbox-input,
.z-datebox-input,
.z-timebox-input,
.z-spinner-input,
.z-doublespinner-input {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  width: 100%;
  height: 24px;
  border: 1px solid #326F70;
  margin: 0;
  padding: 3px 7px;
  padding-right: 31px;
  line-height: 16px;
  background: #FFFFFF;
}
.z-combobox-input:focus,
.z-bandbox-input:focus,
.z-datebox-input:focus,
.z-timebox-input:focus,
.z-spinner-input:focus,
.z-doublespinner-input:focus {
  border-color: rgba(50, 111, 112, 0.9);
  -webkit-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -moz-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -o-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  -ms-box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
  box-shadow: inset -1px 0 0 rgba(50, 111, 112, 0.9);
}
.z-combobox-input[readonly],
.z-bandbox-input[readonly],
.z-datebox-input[readonly],
.z-timebox-input[readonly],
.z-spinner-input[readonly],
.z-doublespinner-input[readonly] {
  border-color: #326F70;
  background: rgba(102, 135, 136, 0.35);
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-combobox-input:focus + .z-combobox-button,
.z-combobox-input:focus + .z-bandbox-button,
.z-combobox-input:focus + .z-datebox-button,
.z-combobox-input:focus + .z-timebox-button,
.z-combobox-input:focus + .z-spinner-button,
.z-combobox-input:focus + .z-doublespinner-button,
.z-bandbox-input:focus + .z-combobox-button,
.z-bandbox-input:focus + .z-bandbox-button,
.z-bandbox-input:focus + .z-datebox-button,
.z-bandbox-input:focus + .z-timebox-button,
.z-bandbox-input:focus + .z-spinner-button,
.z-bandbox-input:focus + .z-doublespinner-button,
.z-datebox-input:focus + .z-combobox-button,
.z-datebox-input:focus + .z-bandbox-button,
.z-datebox-input:focus + .z-datebox-button,
.z-datebox-input:focus + .z-timebox-button,
.z-datebox-input:focus + .z-spinner-button,
.z-datebox-input:focus + .z-doublespinner-button,
.z-timebox-input:focus + .z-combobox-button,
.z-timebox-input:focus + .z-bandbox-button,
.z-timebox-input:focus + .z-datebox-button,
.z-timebox-input:focus + .z-timebox-button,
.z-timebox-input:focus + .z-spinner-button,
.z-timebox-input:focus + .z-doublespinner-button,
.z-spinner-input:focus + .z-combobox-button,
.z-spinner-input:focus + .z-bandbox-button,
.z-spinner-input:focus + .z-datebox-button,
.z-spinner-input:focus + .z-timebox-button,
.z-spinner-input:focus + .z-spinner-button,
.z-spinner-input:focus + .z-doublespinner-button,
.z-doublespinner-input:focus + .z-combobox-button,
.z-doublespinner-input:focus + .z-bandbox-button,
.z-doublespinner-input:focus + .z-datebox-button,
.z-doublespinner-input:focus + .z-timebox-button,
.z-doublespinner-input:focus + .z-spinner-button,
.z-doublespinner-input:focus + .z-doublespinner-button {
  border-left: 1px solid rgba(50, 111, 112, 0.9);
}
.z-combobox-input-full,
.z-bandbox-input-full,
.z-datebox-input-full,
.z-timebox-input-full,
.z-spinner-input-full,
.z-doublespinner-input-full {
  padding-right: 7px;
}
.z-combobox-button,
.z-bandbox-button,
.z-datebox-button,
.z-timebox-button,
.z-spinner-button,
.z-doublespinner-button {
  display: inline-block;
  width: 24px;
  height: 24px;
  position: absolute;
  top: 0;
  right: 0;
  border: 1px solid #326F70;
  background: #FFFFFF;
  vertical-align: middle;
  cursor: pointer;
  overflow: hidden;
}
.z-combobox-button:hover,
.z-bandbox-button:hover,
.z-datebox-button:hover,
.z-timebox-button:hover,
.z-spinner-button:hover,
.z-doublespinner-button:hover {
  border-color: rgba(50, 111, 112, 0.7);
  background: rgba(50, 111, 112, 0.7);
}
.z-combobox-button:hover > i,
.z-bandbox-button:hover > i,
.z-datebox-button:hover > i,
.z-timebox-button:hover > i,
.z-spinner-button:hover > i,
.z-doublespinner-button:hover > i {
  color: #FFFFFF;
}
.z-combobox-button:active,
.z-bandbox-button:active,
.z-datebox-button:active,
.z-timebox-button:active,
.z-spinner-button:active,
.z-doublespinner-button:active {
  border-color: #668788;
  background: #668788;
}
.z-combobox-button:active > i,
.z-bandbox-button:active > i,
.z-datebox-button:active > i,
.z-timebox-button:active > i,
.z-spinner-button:active > i,
.z-doublespinner-button:active > i {
  color: #FFFFFF;
}
.z-combobox-icon,
.z-bandbox-icon,
.z-datebox-icon,
.z-timebox-icon,
.z-spinner-icon,
.z-doublespinner-icon {
  font-size: 100%;
  color: #000000;
  display: block !important;
  width: 22px;
  line-height: 22px;
  text-align: center;
}
.z-combobox-disabled,
.z-bandbox-disabled,
.z-datebox-disabled,
.z-timebox-disabled,
.z-spinner-disabled,
.z-doublespinner-disabled {
  opacity: 1;
  filter: alpha(opacity=100);;
}
.z-combobox-disabled *,
.z-bandbox-disabled *,
.z-datebox-disabled *,
.z-timebox-disabled *,
.z-spinner-disabled *,
.z-doublespinner-disabled * {
  color: #ACACAC !important;
  background: #F2F2F2 !important;
  cursor: default !important;
}
.z-combobox-invalid,
.z-bandbox-invalid,
.z-datebox-invalid,
.z-timebox-invalid,
.z-spinner-invalid,
.z-doublespinner-invalid {
  border: 1px solid #AE312E !important;
  background: #FFFFFF;
  -webkit-box-shadow: inset -1px 0 0 #AE312E;
  -moz-box-shadow: inset -1px 0 0 #AE312E;
  -o-box-shadow: inset -1px 0 0 #AE312E;
  -ms-box-shadow: inset -1px 0 0 #AE312E;
  box-shadow: inset -1px 0 0 #AE312E;
}
.z-combobox-invalid + .z-combobox-button,
.z-combobox-invalid + .z-bandbox-button,
.z-combobox-invalid + .z-datebox-button,
.z-combobox-invalid + .z-timebox-button,
.z-combobox-invalid + .z-spinner-button,
.z-combobox-invalid + .z-doublespinner-button,
.z-bandbox-invalid + .z-combobox-button,
.z-bandbox-invalid + .z-bandbox-button,
.z-bandbox-invalid + .z-datebox-button,
.z-bandbox-invalid + .z-timebox-button,
.z-bandbox-invalid + .z-spinner-button,
.z-bandbox-invalid + .z-doublespinner-button,
.z-datebox-invalid + .z-combobox-button,
.z-datebox-invalid + .z-bandbox-button,
.z-datebox-invalid + .z-datebox-button,
.z-datebox-invalid + .z-timebox-button,
.z-datebox-invalid + .z-spinner-button,
.z-datebox-invalid + .z-doublespinner-button,
.z-timebox-invalid + .z-combobox-button,
.z-timebox-invalid + .z-bandbox-button,
.z-timebox-invalid + .z-datebox-button,
.z-timebox-invalid + .z-timebox-button,
.z-timebox-invalid + .z-spinner-button,
.z-timebox-invalid + .z-doublespinner-button,
.z-spinner-invalid + .z-combobox-button,
.z-spinner-invalid + .z-bandbox-button,
.z-spinner-invalid + .z-datebox-button,
.z-spinner-invalid + .z-timebox-button,
.z-spinner-invalid + .z-spinner-button,
.z-spinner-invalid + .z-doublespinner-button,
.z-doublespinner-invalid + .z-combobox-button,
.z-doublespinner-invalid + .z-bandbox-button,
.z-doublespinner-invalid + .z-datebox-button,
.z-doublespinner-invalid + .z-timebox-button,
.z-doublespinner-invalid + .z-spinner-button,
.z-doublespinner-invalid + .z-doublespinner-button {
  border-left: 1px solid #AE312E !important;
}
.z-combobox-readonly:focus,
.z-bandbox-readonly:focus,
.z-datebox-readonly:focus,
.z-timebox-readonly:focus,
.z-spinner-readonly:focus,
.z-doublespinner-readonly:focus {
  border-right-width: 0;
  border-color: #326F70;
  background: transparent repeat-x 0 0;
  cursor: default;
}
.z-combobox-disabled > .z-combobox-button:hover,
.z-bandbox-disabled > .z-bandbox-button:hover,
.z-datebox-disabled > .z-datebox-button:hover,
.z-timebox-disabled > .z-timebox-button:hover,
.z-spinner-disabled > .z-spinner-button:hover,
.z-doublespinner-disabled > .z-doublespinner-button:hover,
.z-combobox-disabled > .z-combobox-button:active,
.z-bandbox-disabled > .z-bandbox-button:active,
.z-datebox-disabled > .z-datebox-button:active,
.z-timebox-disabled > .z-timebox-button:active,
.z-spinner-disabled > .z-spinner-button:active,
.z-doublespinner-disabled > .z-doublespinner-button:active {
  border-color: #326F70;
  background: 0;
}
.z-combobox-button.z-combobox-disabled,
.z-bandbox-button.z-bandbox-disabled,
.z-datebox-button.z-datebox-disabled,
.z-timebox-button.z-timebox-disabled,
.z-spinner-button.z-spinner-disabled,
.z-doublespinner-button.z-doublespinner-disabled {
  display: none;
}
.z-combobox-button {
  font-size: 114%;
}
.z-timebox-button,
.z-spinner-button,
.z-doublespinner-button {
  padding: 0;
}
.z-timebox-button:hover,
.z-spinner-button:hover,
.z-doublespinner-button:hover,
.z-timebox-button:active,
.z-spinner-button:active,
.z-doublespinner-button:active {
  border-color: #E3E3E3;
  background: 0;
}
.z-timebox-button > a,
.z-spinner-button > a,
.z-doublespinner-button > a {
  line-height: 11px;
  background: #FFFFFF;
}
.z-timebox-button > a:hover,
.z-spinner-button > a:hover,
.z-doublespinner-button > a:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-timebox-button > i,
.z-spinner-button > i,
.z-doublespinner-button > i {
  z-index: 2;
}
.z-timebox-button:hover > i,
.z-spinner-button:hover > i,
.z-doublespinner-button:hover > i {
  width: 23px;
  height: 1px;
  border-top: 1px solid rgba(50, 111, 112, 0.7);
  position: absolute;
  top: 11px;
  left: 0;
}
.z-timebox-disabled a:hover,
.z-spinner-disabled a:hover,
.z-doublespinner-disabled a:hover {
  background: 0;
}
.z-timebox-disabled a + i[class*='-separator'],
.z-spinner-disabled a + i[class*='-separator'],
.z-doublespinner-disabled a + i[class*='-separator'] {
  display: none;
}
.z-timebox-active.z-timebox-icon,
.z-timebox-active.z-timebox-icon:hover {
  color: #FFFFFF;
  background: #668788;
}
.z-spinner-active.z-spinner-icon,
.z-spinner-active.z-spinner-icon:hover {
  color: #FFFFFF;
  background: #668788;
}
.z-doublespinner-active.z-doublespinner-icon,
.z-doublespinner-active.z-doublespinner-icon:hover {
  color: #FFFFFF;
  background: #668788;
}
.z-combobox-inplace .z-combobox-input,
.z-bandbox-inplace .z-bandbox-input,
.z-datebox-inplace .z-datebox-input,
.z-timebox-inplace .z-timebox-input,
.z-spinner-inplace .z-spinner-input,
.z-doublespinner-inplace .z-doublespinner-input {
  padding: 5px;
  border: 0;
  background: none;
}
.z-combobox-inplace .z-combobox-input:focus,
.z-bandbox-inplace .z-bandbox-input:focus,
.z-datebox-inplace .z-datebox-input:focus,
.z-timebox-inplace .z-timebox-input:focus,
.z-spinner-inplace .z-spinner-input:focus,
.z-doublespinner-inplace .z-doublespinner-input:focus {
  background: 0;
}
.z-combobox-inplace .z-combobox-button,
.z-bandbox-inplace .z-bandbox-button,
.z-datebox-inplace .z-datebox-button,
.z-timebox-inplace .z-timebox-button,
.z-spinner-inplace .z-spinner-button,
.z-doublespinner-inplace .z-doublespinner-button {
  display: none;
}
.z-combobox-emptySearchMessage {
  display: block;
  padding: 4px 8px;
  line-height: 16px;
  position: relative;
  white-space: nowrap;
  cursor: pointer;
  min-height: 16px;
  color: #ACACAC;
}
.z-combobox-emptySearchMessage-hidden {
  display: none;
}
.z-comboitem {
  display: block;
  padding: 4px 8px;
  line-height: 16px;
  position: relative;
  white-space: nowrap;
  cursor: pointer;
  min-height: 16px;
}
.z-comboitem-inner,
.z-comboitem-content {
  color: #000000;
  font-size: 80%;
}
.z-comboitem,
.z-comboitem a,
.z-comboitem a:visited {
  color: #000000;
  text-decoration: none;
}
.z-comboitem-selected {
  color: #000000;
  background: rgba(102, 135, 136, 0.2);
}
.z-comboitem-selected .z-comboitem-inner,
.z-comboitem-selected .z-comboitem-content {
  color: #000000;
}
.z-comboitem:hover {
  color: #FFFFFF;
  background: #BD652F;
}
.z-comboitem:hover .z-comboitem-inner,
.z-comboitem:hover .z-comboitem-content {
  color: #FFFFFF;
}
.z-comboitem-selected:hover {
  color: #FFFFFF;
  background: #BD652F;
}
.z-comboitem-selected:hover .z-comboitem-inner,
.z-comboitem-selected:hover .z-comboitem-content {
  color: #FFFFFF;
}
.z-comboitem-image {
  margin-right: 4px;
}
.z-comboitem-image:empty {
  margin-right: 0;
}
.z-comboitem-icon {
  padding: 0 4px 0 3px;
}
.z-combobox-popup,
.z-bandbox-popup,
.z-datebox-popup,
.z-timebox-popup {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: block;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: absolute;
  overflow: hidden;
}
.z-combobox-popup {
  overflow: auto;
  min-height: 10px;
}
.z-bandbox-popup {
  overflow: auto;
  min-height: 10px;
}
.z-combobox-content {
  border: 0;
  margin: 0;
  padding: 0;
  background: transparent none repeat 0 0;
  position: relative;
  list-style-image: none;
  list-style-position: outside;
  list-style-type: none;
  min-width: 100%;
  display: inline-block;
}
