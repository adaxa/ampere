<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-button {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 600;
  font-style: normal;
  color: #FFFFFF;
  min-height: 24px;
  border: 1px solid #7D443E;
  padding: 3px 15px;
  line-height: 15px;
  background: #7D443E;
  cursor: pointer;
  white-space: nowrap;
}
.z-button:hover {
  color: #FFFFFF;
  border-color: rgba(125, 68, 62, 0.7);
  background: rgba(125, 68, 62, 0.7);
  -webkit-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -moz-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -o-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  -ms-box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
  box-shadow: inset 0 -1px 0 rgba(50, 111, 112, 0.7);
}
.z-button:focus {
  color: #FFFFFF;
  border-color: rgba(125, 68, 62, 0.8);
  background: rgba(125, 68, 62, 0.8);
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
}
.z-button:active {
  color: #FFFFFF;
  border-color: rgba(125, 68, 62, 0.9);
  background: rgba(125, 68, 62, 0.9);
  -webkit-box-shadow: inset 0px 1px 0 #326F70;
  -moz-box-shadow: inset 0px 1px 0 #326F70;
  -o-box-shadow: inset 0px 1px 0 #326F70;
  -ms-box-shadow: inset 0px 1px 0 #326F70;
  box-shadow: inset 0px 1px 0 #326F70;
}
.z-button[disabled] {
  color: #ACACAC;
  border: 1px solid #326F70;
  background: rgba(102, 135, 136, 0.35);
  opacity: 1;
  filter: alpha(opacity=100);;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  -o-box-shadow: none;
  -ms-box-shadow: none;
  box-shadow: none;
  cursor: default;
}
