<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-caption {
  font-size: 100%;
  width: 100%;
  height: auto;
  min-height: 24px;
  line-height: 12px;
}
.z-caption > * {
  margin-left: 4px;
}
.z-caption > *:first-child {
  margin-left: 0px;
}
.z-caption-content,
.z-caption .z-label {
  display: inline-block;
  padding: 4px 0;
  line-height: 18px;
}
.z-caption-image {
  vertical-align: middle;
}
.z-caption input {
  font-size: 100%;
}
.z-caption .z-toolbar .z-a,
.z-caption .z-toolbar .z-a:visited,
.z-caption .z-toolbar .z-a:hover {
  color: #FFFFFF;
  border: none;
  background: none;
}
.z-caption .z-a,
.z-caption .z-a:visited {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 80%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  background: none;
  text-decoration: none;
}
.z-caption .z-a:hover {
  text-decoration: underline;
}
