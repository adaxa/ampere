<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-window {
  overflow: hidden;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
}
.z-window-header {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 24px;
  font-weight: 300;
  font-style: normal;
  color: #ACACAC;
  padding: 8px 15px;
  line-height: 20px;
  cursor: default;
  overflow: hidden;
}
.z-window-header-move {
  cursor: move;
}
.z-window-content {
  padding: 7px 15px;
  background: #FFFFFF;
  overflow: hidden;
}
.z-window-icons {
  display: inline-block;
  float: right;
}
.z-window-icon {
  font-size: 12px;
  color: #ACACAC;
  display: inline-block;
  width: 18px;
  height: 18px;
  margin-top: 1px;
  line-height: 18px;
  text-align: center;
  cursor: pointer;
  overflow: hidden;
  border: none;
  background: transparent;
  padding: 0;
}
.z-window-icon:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-window-minimize {
  padding-top: 4px;
}
.z-window-resize-faker {
  border: 1px dashed #1854C2;
  background: #D7E6F7;
  opacity: 0.5;
  filter: alpha(opacity=50);;
  position: absolute;
  left: 0;
  top: 0;
  overflow: hidden;
  z-index: 60000;
}
.z-window-move-ghost {
  border: 1px solid #9F9F9F;
  padding: 0;
  background: #D7E6F7;
  opacity: 0.65;
  filter: alpha(opacity=65);;
  position: absolute;
  cursor: move;
  overflow: hidden;
}
.z-window-move-ghost dl {
  font-size: 0;
  display: block;
  border-top: 1px solid #E3E3E3;
  margin: 0;
  padding: 0;
  line-height: 0;
  overflow: hidden;
}
.z-window-noborder {
  border-width: 0px;
}
.z-messagebox {
  display: inline-block;
  white-space: normal;
  width: 376px;
}
.z-messagebox-window .z-window-content {
  padding: 16px 0px;
  overflow: auto;
}
.z-messagebox-window {
  width: 440px;
}
.z-messagebox .z-label {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 100%;
  font-weight: 400;
  font-style: normal;
  color: #000000;
  display: inline-block;
  padding: 0px 16px 16px;
}
.z-messagebox-buttons {
  text-align: center;
  margin-right: 16px;
  left: 0;
  position: sticky;
}
.z-messagebox-buttons > * {
  margin: 0 4px;
}
.z-messagebox-button {
  width: 100%;
  min-width: 48px;
}
.z-messagebox-icon {
  width: 32px;
  height: 32px;
  background-repeat: no-repeat;
  margin-left: 16px;
  display: inline-block;
  vertical-align: top;
}
.z-messagebox-question {
  background-image: url(${c:encodeThemeURL("~./zul/img/msgbox/question-btn.png")});
}
.z-messagebox-exclamation {
  background-image: url(${c:encodeThemeURL("~./zul/img/msgbox/warning-btn.png")});
}
.z-messagebox-information {
  background-image: url(${c:encodeThemeURL("~./zul/img/msgbox/info-btn.png")});
}
.z-messagebox-error {
  background-image: url(${c:encodeThemeURL("~./zul/img/msgbox/stop-btn.png")});
}
.z-messagebox-viewport {
  overflow: auto;
  white-space: nowrap;
  margin-bottom: 16px;
}
.ie .z-messagebox-icon {
  float: left;
}
