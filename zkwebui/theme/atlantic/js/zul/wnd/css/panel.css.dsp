<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-panel {
  overflow: hidden;
}
.z-panel-shadow {
  -webkit-box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
  -moz-box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
  -o-box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
  -ms-box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.5);
}
.z-panel-collapsed {
  height: auto !important;
}
.z-panel-head {
  border: 1px solid #E3E3E3;
  padding: 7px 15px;
  line-height: 20px;
  background: #FFFFFF;
  overflow: hidden;
}
.z-panel-header {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 24px;
  font-weight: 300;
  font-style: normal;
  color: #ACACAC;
  overflow: hidden;
}
.z-panel-header-move {
  cursor: move;
}
.z-panel-drag-button {
  display: none;
}
.z-panel-drag-button + .z-panel-body {
  height: 100%;
}
.z-panel-body {
  border: 1px solid #E3E3E3;
  border-top-width: 0;
  padding: 8px 15px 7px;
  background: #FFFFFF;
  overflow: hidden;
}
.z-panel-top .z-toolbar {
  border-bottom-width: 0;
  padding: 3px 3px 4px;
}
.z-panel-footer .z-toolbar,
.z-panel-bottom .z-toolbar {
  border-top-width: 0;
  padding: 4px 3px 3px;
}
.z-panel-top .z-toolbar-panel,
.z-panel-footer .z-toolbar-panel {
  border-width: 0px;
  padding: 4px;
}
.z-panel-icons {
  display: inline-block;
  float: right;
}
.z-panel-icon {
  font-size: 12px;
  color: #ACACAC;
  display: inline-block;
  width: 18px;
  height: 18px;
  margin-top: 1px;
  line-height: 18px;
  text-align: center;
  cursor: pointer;
  overflow: hidden;
  border: none;
  background: transparent;
  padding: 0;
}
.z-panel-icon:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-panel-close {
  border: 0;
  background: #FFFFFF;
  line-height: 20px;
}
.z-panel-minimize {
  padding-top: 4px;
}
.z-panel-expand {
  font-size: 14px;
}
.z-panel-resize-faker {
  border: 1px dashed #1854C2;
  background: #D7E6F7;
  opacity: 0.5;
  filter: alpha(opacity=50);;
  position: absolute;
  left: 0;
  top: 0;
  overflow: hidden;
  z-index: 60000;
}
.z-panel-move-ghost {
  border: 1px solid #9F9F9F;
  padding: 0;
  background: #D7E6F7;
  opacity: 0.6;
  filter: alpha(opacity=60);;
  position: absolute;
  cursor: move;
  overflow: hidden;
}
.z-panel-move-ghost .z-panel-body {
  padding: 0;
}
.z-panel-move-ghost dl {
  font-size: 0;
  display: block;
  border: 1px solid #E3E3E3;
  border-top: 0;
  margin: 0;
  padding: 0;
  line-height: 0;
  overflow: hidden;
}
.z-panel-move-block {
  border: 2px dashed #B2CAD6;
}
.z-panel-noframe .z-panel-body {
  border-width: 0;
  padding: 0;
  background: #FFFFFF;
}
.z-panel-noframe .z-panel-body .z-panelchildren {
  border-top-width: 0;
}
.z-panel-noheader .z-panel-body {
  border-top: 1px solid #E3E3E3;
  padding: 15px;
}
.z-panel-noheader.z-panel-noframe .z-panel-body {
  border-width: 0;
  padding: 16px;
}
.z-panel-noheader.z-panel-noframe .z-panelchildren {
  border-top: 1px solid #E3E3E3;
}
.z-panel-noborder.z-panel-noframe .z-panelchildren {
  border-width: 0;
}
.z-panel-noborder .z-panel-bottom .z-toolbar {
  border-width: 0;
  padding: 4px;
}
.z-panelchildren {
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: relative;
  overflow: hidden;
}
