<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-pdfviewer {
  position: relative;
  display: flex;
  flex-direction: column;
}
.z-pdfviewer:-webkit-full-screen {
  width: 100% !important;
  height: 100% !important;
}
.z-pdfviewer:-ms-fullscreen {
  width: 100% !important;
  height: 100% !important;
}
.z-pdfviewer:fullscreen .z-pdfviewer-toolbar-fullscreen > i::before {
  content: '\f066';
}
.z-pdfviewer:-webkit-full-screen .z-pdfviewer-toolbar-fullscreen > i::before {
  content: '\f066';
}
.z-pdfviewer:-ms-fullscreen .z-pdfviewer-toolbar-fullscreen > i::before {
  content: '\f066';
}
.z-pdfviewer:hover .z-pdfviewer-toolbar {
  opacity: 0.5;
}
.z-pdfviewer:hover .z-pdfviewer-toolbar:hover {
  opacity: 1;
}
.z-pdfviewer:focus-within .z-pdfviewer-toolbar {
  opacity: 1;
}
.z-pdfviewer-container {
  overflow: auto;
  height: 100%;
  width: 100%;
  background: gray;
}
.z-pdfviewer-page {
  position: relative;
  margin: 0 auto;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
  overflow: hidden;
}
.z-pdfviewer-text-layer {
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
  overflow: hidden;
  opacity: 0.2;
  line-height: 1;
}
.z-pdfviewer-text-layer ::selection {
  background: blue;
}
.z-pdfviewer-text-layer > span {
  color: transparent;
  position: absolute;
  white-space: pre;
  cursor: text;
  transform-origin: 0 0;
}
.z-pdfviewer-annotation-layer .linkAnnotation {
  position: absolute;
}
.z-pdfviewer-annotation-layer .linkAnnotation > a {
  position: absolute;
  font-size: 1em;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
.z-pdfviewer-toolbar {
  background: #FFFFFF;
  border: 1px solid #E3E3E3;
  border-radius: 0;
  padding: 5px 8px;
  opacity: 0;
  position: absolute;
  bottom: 1em;
  left: 50%;
  transform: translateX(-50%);
  font-size: 100%;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.z-pdfviewer-toolbar-button {
  height: 32px;
  width: 32px;
  padding: 0;
  border-radius: 0;
  border: 0;
  color: rgba(50, 111, 112, 0.8);
  background: transparent;
  margin-left: 8px;
}
.z-pdfviewer-toolbar-button:nth-child(1) {
  margin-left: 0;
}
.z-pdfviewer-toolbar-button > i {
  pointer-events: none;
}
.z-pdfviewer-toolbar-button:hover {
  color: #FFFFFF;
  background: rgba(50, 111, 112, 0.7);
}
.z-pdfviewer-toolbar-button:active {
  color: #FFFFFF;
  background: #668788;
}
.z-pdfviewer-toolbar-button[disabled] {
  opacity: 1;
  color: #ACACAC;
  background: transparent;
}
.z-pdfviewer-toolbar-separator {
  height: 24px;
  width: 1px;
  display: inline-block;
  vertical-align: middle;
  border-left: 1px solid #E3E3E3;
  margin-left: 8px;
}
.z-pdfviewer-toolbar-page {
  vertical-align: middle;
}
.z-pdfviewer-toolbar-page-active {
  -moz-appearance: textfield;
  -webkit-appearance: textfield;
  width: 42px;
  padding-top: 0;
  padding-bottom: 0;
  margin-left: 8px;
}
.z-pdfviewer-toolbar-page-active:focus {
  -moz-appearance: textfield;
  -webkit-appearance: textfield;
}
.z-pdfviewer-toolbar-page-active:invalid {
  border-color: #AE312E;
}
.z-pdfviewer-toolbar-zoom.z-selectbox {
  min-width: auto;
  margin-left: 8px;
}
