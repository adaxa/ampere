<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-columnlayout {
  white-space: nowrap;
}
.z-columnlayout,
.z-columnchildren,
.z-columnchildren-content {
  overflow: hidden;
}
.z-columnchildren {
  height: 100%;
  display: inline-block;
  vertical-align: top;
  white-space: initial;
}
.z-columnchildren-content {
  height: 100%;
  width: 100%;
}
.z-columnlayout,
.z-columnchildren {
  -ms-zoom: 1;
}
