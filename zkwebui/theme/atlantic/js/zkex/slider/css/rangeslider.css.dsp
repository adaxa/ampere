<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
.z-rangeslider {
  white-space: nowrap;
  overflow: visible;
  cursor: pointer;
  font-size: 100%;
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
}
.z-rangeslider-inner {
  position: relative;
}
.z-rangeslider-track {
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: #E3E3E3;
}
.z-rangeslider-marks {
  position: relative;
}
.z-rangeslider-mark {
  position: absolute;
}
.z-rangeslider-mark-dot {
  position: absolute;
  width: 16px;
  height: 16px;
  background-color: #FFFFFF;
  border: 4px solid #E3E3E3;
  border-radius: 50%;
}
.z-rangeslider-mark-active .z-rangeslider-mark-dot {
  border-color: rgba(50, 111, 112, 0.8);
  z-index: 1;
}
.z-rangeslider-mark-label {
  position: absolute;
  color: #000000;
  text-align: center;
}
.z-rangeslider .z-sliderbuttons-button {
  width: 20px;
  height: 20px;
  background-color: #FFFFFF;
  border-width: 1px;
  border-color: #ACACAC;
  border-radius: 50%;
  z-index: 2;
}
.z-rangeslider .z-sliderbuttons-tooltip {
  background: rgba(0, 0, 0, 0.8);
  font-size: 114%;
}
.z-rangeslider .z-sliderbuttons-area {
  background-color: rgba(50, 111, 112, 0.8);
}
.z-rangeslider-horizontal {
  width: 300px;
}
.z-rangeslider-horizontal .z-sliderbuttons-area {
  height: 100%;
}
.z-rangeslider-horizontal .z-sliderbuttons-button {
  margin-top: -6px;
  margin-left: -10px;
  top: 0;
}
.z-rangeslider-horizontal .z-sliderbuttons-tooltip {
  top: -10px;
  left: 50%;
  transform: translateX(-50%) translateY(-100%);
  padding: 4px 6px;
}
.z-rangeslider-horizontal .z-rangeslider-inner {
  margin: 40px 12px 30px;
  height: 8px;
}
.z-rangeslider-horizontal .z-rangeslider-marks {
  width: 100%;
}
.z-rangeslider-horizontal .z-rangeslider-mark-dot {
  top: -4px;
  transform: translateX(-50%);
}
.z-rangeslider-horizontal .z-rangeslider-mark-label {
  top: 22px;
  transform: translateX(-50%);
}
.z-rangeslider-vertical {
  height: 300px;
}
.z-rangeslider-vertical .z-sliderbuttons-area {
  width: 100%;
}
.z-rangeslider-vertical .z-sliderbuttons-button {
  margin-top: -10px;
  margin-left: -6px;
  left: 0;
}
.z-rangeslider-vertical .z-sliderbuttons-tooltip {
  left: 26px;
  top: 50%;
  transform: translateY(-50%);
  padding: 6px 4px;
}
.z-rangeslider-vertical .z-rangeslider-inner {
  margin: 12px 30px 12px 12px;
  width: 8px;
  height: 100%;
}
.z-rangeslider-vertical .z-rangeslider-marks {
  height: 100%;
}
.z-rangeslider-vertical .z-rangeslider-mark-dot {
  left: -4px;
  transform: translateY(-50%);
}
.z-rangeslider-vertical .z-rangeslider-mark-label {
  left: 22px;
  transform: translateY(-50%);
}
.z-rangeslider:not(.z-rangeslider-disabled) .z-sliderbuttons-button:hover {
  background-color: rgba(50, 111, 112, 0.7);
}
.z-rangeslider:not(.z-rangeslider-disabled) .z-sliderbuttons-button:hover .z-sliderbuttons-tooltip {
  display: block;
}
.z-rangeslider:not(.z-rangeslider-disabled) .z-sliderbuttons-button:active {
  background-color: #668788;
}
.z-rangeslider:not(.z-rangeslider-disabled) .z-sliderbuttons-button:focus {
  border-color: rgba(50, 111, 112, 0.9);
}
.z-rangeslider-disabled .z-rangeslider-track {
  background-color: #E5E5E5;
}
.z-rangeslider-disabled {
  cursor: default;
}
.z-rangeslider-disabled .z-sliderbuttons-area {
  background-color: rgba(120, 194, 195, 0.8);
}
.z-rangeslider-disabled .z-sliderbuttons-button {
  background-color: #E3E3E3;
  border-color: #E3E3E3;
}
.z-rangeslider-disabled .z-sliderbuttons-tooltip {
  background-color: rgba(0, 0, 0, 0.5);
}
.z-rangeslider-disabled .z-rangeslider-mark-dot {
  border-color: #E5E5E5;
}
.z-rangeslider-disabled .z-rangeslider-mark-active .z-rangeslider-mark-dot {
  border-color: rgba(120, 194, 195, 0.8);
}
