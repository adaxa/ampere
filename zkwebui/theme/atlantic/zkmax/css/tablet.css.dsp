<%@ taglib uri="http://www.zkoss.org/dsp/web/core" prefix="c" %>
<%@ taglib uri="http://www.zkoss.org/dsp/zk/core" prefix="z" %>
<%@ taglib uri="http://www.zkoss.org/dsp/web/theme" prefix="t" %>
input {
  -webkit-appearance: none;
  -webkit-border-radius: 0;
  border-radius: 0;
}
input[type=checkbox] {
  -webkit-appearance: none;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
  position: relative;
}
input[type=checkbox]:checked {
  font-size: 100%;
  color: #326F70;
  line-height: 18px;
  background: #FFFFFF;
  text-align: center;
}
input[type=radio] {
  -webkit-appearance: none;
  width: 20px;
  height: 20px;
  border: 1px solid #E3E3E3;
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -o-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
  background: #FFFFFF;
  position: relative;
}
input[type=radio]:checked {
  background: #FFFFFF;
}
input[type=radio]:checked:after {
  content: '';
  width: 10px;
  height: 10px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  -o-border-radius: 5px;
  -ms-border-radius: 5px;
  border-radius: 5px;
  margin-top: -5px;
  margin-left: -5px;
  background: #326F70;
  position: absolute;
  top: 50%;
  left: 50%;
}
.z-biglistbox-outer {
  margin: 0 32px 32px 0;
}
.z-biglistbox-verticalbar-tick {
  height: 32px;
  border: 1px solid #E3E3E3;
  .horGradient(#FEFEFE, #EEEEEE);
}
.z-biglistbox-wscroll-vertical {
  width: 32px;
  right: -33px;
  -webkit-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -moz-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -o-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -ms-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag {
  width: 32px;
  height: 176px;
  border: 1px solid #A6A6A6;
  -webkit-border-radius: 2em;
  -moz-border-radius: 2em;
  -o-border-radius: 2em;
  -ms-border-radius: 2em;
  border-radius: 2em;
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")});
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), -moz-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* FF3.6+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FEFEFE),color-stop(100%, #EEEEEE));
  /* Chrome,Safari4+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), -webkit-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* Chrome10+,Safari5.1+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), -o-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* Opera 11.10+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), -ms-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* IE10+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v.png")}), linear-gradient(to bottom, #FEFEFE 0%, #EEEEEE 100%);
  /* W3C */
  background-position: center center;
  background-repeat: no-repeat;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down,
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  width: 100%;
  height: 32px;
  background-position: center center;
  background-repeat: no-repeat;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home {
  border-bottom: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v-home.png")});
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up {
  border-bottom: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v-up.png")});
  top: 32px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down {
  border-top: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v-down.png")});
  bottom: 32px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  border-top: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-v-end.png")});
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-pos {
  width: 24px;
  height: 176px;
  -webkit-border-radius: 2em;
  -moz-border-radius: 2em;
  -o-border-radius: 2em;
  -ms-border-radius: 2em;
  border-radius: 2em;
  left: 4px;
}
.z-biglistbox-wscroll-vertical .z-biglistbox-wscroll-endbar {
  width: 30px;
  background: #FDFDFD;
}
.z-biglistbox-wscroll-horizontal {
  height: 32px;
  bottom: -33px;
  -webkit-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -moz-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -o-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  -ms-box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
  box-shadow: inset 1px 1px 7px rgba(210, 210, 210, 0.75), inset -1px -1px 7px rgba(210, 210, 210, 0.75);
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag {
  width: 176px;
  height: 32px;
  border: 1px solid #A6A6A6;
  -webkit-border-radius: 2em;
  -moz-border-radius: 2em;
  -o-border-radius: 2em;
  -ms-border-radius: 2em;
  border-radius: 2em;
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")});
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), -moz-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* FF3.6+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FEFEFE),color-stop(100%, #EEEEEE));
  /* Chrome,Safari4+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), -webkit-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* Chrome10+,Safari5.1+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), -o-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* Opera 11.10+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), -ms-linear-gradient(top, #FEFEFE 0%, #EEEEEE 100%);
  /* IE10+ */
  background:	url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h.png")}), linear-gradient(to bottom, #FEFEFE 0%, #EEEEEE 100%);
  /* W3C */
  background-position: center center;
  background-repeat: no-repeat;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down,
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  width: 32px;
  height: 100%;
  background-position: center center;
  background-repeat: no-repeat;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-home {
  border-right: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h-home.png")});
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-up {
  border-right: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h-up.png")});
  left: 32px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-down {
  border-left: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h-down.png")});
  right: 32px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-drag .z-biglistbox-wscroll-end {
  border-left: 1px solid #A6A6A6;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/big/drag-h-end.png")});
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-pos {
  width: 176px;
  height: 24px;
  -webkit-border-radius: 2em;
  -moz-border-radius: 2em;
  -o-border-radius: 2em;
  -ms-border-radius: 2em;
  border-radius: 2em;
  top: 4px;
}
.z-biglistbox-wscroll-horizontal .z-biglistbox-wscroll-endbar {
  height: 30px;
  background: #FDFDFD;
}
.z-east-splitter,
.z-west-splitter,
.z-north-splitter,
.z-south-splitter {
  overflow: visible;
}
.z-east-splitter-button,
.z-west-splitter-button,
.z-north-splitter-button,
.z-south-splitter-button {
  border: 1px solid #E3E3E3;
  background: #FFFFFF;
}
.z-east-splitter-button-disabled,
.z-west-splitter-button-disabled,
.z-north-splitter-button-disabled,
.z-south-splitter-button-disabled {
  display: none;
  width: 0;
  height: 0;
}
.z-north-splitter-button,
.z-south-splitter-button {
  width: 32px;
  height: 16px;
  top: -4px;
}
.z-west-splitter-button,
.z-east-splitter-button {
  width: 16px;
  height: 32px;
  left: -4px;
}
.z-north-icon,
.z-south-icon,
.z-west-icon,
.z-east-icon {
  font-size: 114%;
}
.z-west-icon,
.z-east-icon {
  top: 50%;
  margin-top: -6px;
  left: 4px;
}
.z-east-icon {
  left: 5px;
}
.z-north-icon,
.z-south-icon {
  left: 11px;
  top: 50%;
  margin-top: -6px;
}
.z-north-icon {
  margin-top: -7px;
}
.z-splitter-vertical,
.z-splitter-horizontal {
  overflow: visible;
}
.z-splitter-button {
  background: #FFFFFF;
}
.z-splitter-button.z-splitter-button-disabled {
  width: 0;
  height: 0;
}
.z-splitter-icon {
  font-size: 114%;
}
.z-splitter-vertical > .z-splitter-button {
  width: 32px;
  height: 16px;
  top: -4px;
}
.z-splitter-vertical .z-splitter-icon {
  font-size: 16px;
  top: -2px;
}
.z-splitter-vertical.z-splitter-nosplitter .z-splitter-icon {
  top: 0;
}
.z-splitter-horizontal > .z-splitter-button {
  width: 16px;
  height: 32px;
  left: -5px;
}
.z-splitter-horizontal .z-splitter-icon {
  top: 7px;
  left: 4px;
}
.z-splitter-horizontal.z-splitter-nosplitter .z-splitter-icon {
  left: 6px;
}
.z-datebox-popup .z-calendar {
  border: 0px;
  min-width: 100px;
}
.z-timebox-popup .z-timebox-wheel-body {
  margin: 4px 0;
}
.z-calendar-wheel-cave,
.z-timebox-wheel-cave {
  position: relative;
}
.z-calendar-wheel-body,
.z-timebox-wheel-body {
  display: -webkit-box;
  display: -moz-box;
  display: box;
  -webkit-box-orient: horizontal;
  -moz-box-orient: horizontal;
  -o-box-orient: horizontal;
  -ms-box-orient: horizontal;
  box-orient: horizontal;
  width: 100%;
}
.z-calendar-wheel-line,
.z-timebox-wheel-line {
  width: 100%;
  height: 0;
  border-top: 1px solid #333333;
  border-bottom: 1px solid #555555;
  position: absolute;
  top: 50%;
  z-index: 1;
}
.z-calendar-wheel-list,
.z-timebox-wheel-list {
  -webkit-box-flex: 1;
  -moz-box-flex: 1;
  -o-box-flex: 1;
  -ms-box-flex: 1;
  box-flex: 1;
  color: #FFFFFF;
  height: 210px;
  margin: 0 2px;
  .gradient('ver', '#000 0%; #444 45%; #444 55%; #000 100%');
  position: relative;
  overflow: hidden;
}
.z-calendar-wheel-list ul,
.z-timebox-wheel-list ul {
  width: 100%;
  margin: 0;
  padding: 0;
  position: relative;
  list-style: none;
  z-index: 2;
}
.z-calendar-wheel-list li,
.z-timebox-wheel-list li {
  font-size: 40px;
  display: block;
  height: 70px;
  margin: 0;
  padding: 0 5px;
  line-height: 70px;
  opacity: 0.3;
  text-align: center;
  white-space: nowrap;
  text-shadow: 0 1px #FFFFFF;
  list-style: none;
}
.z-calendar-wheel-footer,
.z-timebox-wheel-footer {
  height: 50px;
  padding: 5px 0;
  clear: both;
}
.z-calendar-wheel-button,
.z-timebox-wheel-button {
  font-family: "Source Sans Pro", "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 30px;
  font-weight: bold;
  font-style: normal;
  color: #000000;
  width: 45%;
  border: 1px solid #A6A6A6;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  -o-border-radius: 3px;
  -ms-border-radius: 3px;
  border-radius: 3px;
  margin: 1px 1px 0 0;
  padding: 5px 15px;
  line-height: 28px;
  .verGradient(#FEFEFE, #EEEEEE);
  text-shadow: 0 1px #FFFFFF;
}
.z-calendar-wheel-button:hover,
.z-timebox-wheel-button:hover,
.z-calendar-wheel-button:focus,
.z-timebox-wheel-button:focus {
  border-color: #0CBCFF;
  -webkit-box-shadow: inset 1px 1px 1px #0CBCFF, inset -1px -1px 1px #0CBCFF;
  -moz-box-shadow: inset 1px 1px 1px #0CBCFF, inset -1px -1px 1px #0CBCFF;
  -o-box-shadow: inset 1px 1px 1px #0CBCFF, inset -1px -1px 1px #0CBCFF;
  -ms-box-shadow: inset 1px 1px 1px #0CBCFF, inset -1px -1px 1px #0CBCFF;
  box-shadow: inset 1px 1px 1px #0CBCFF, inset -1px -1px 1px #0CBCFF;
}
.z-calendar-wheel-button:active,
.z-timebox-wheel-button:active {
  border-color: #808080 #B6B6B6 #B6B6B6 #808080;
  .verGradient(#EEEEEE, #FEFEFE);
  -webkit-box-shadow: inset 1px 1px 1px rgba(210, 210, 210, 0.75), 0 0 7px #CFCFCF;
  -moz-box-shadow: inset 1px 1px 1px rgba(210, 210, 210, 0.75), 0 0 7px #CFCFCF;
  -o-box-shadow: inset 1px 1px 1px rgba(210, 210, 210, 0.75), 0 0 7px #CFCFCF;
  -ms-box-shadow: inset 1px 1px 1px rgba(210, 210, 210, 0.75), 0 0 7px #CFCFCF;
  box-shadow: inset 1px 1px 1px rgba(210, 210, 210, 0.75), 0 0 7px #CFCFCF;
}
.z-calendar-wheel-left,
.z-timebox-wheel-left {
  float: left;
}
.z-calendar-wheel-right,
.z-timebox-wheel-right {
  float: right;
}
.z-timebox-wheel {
  padding: 0 2px;
}
.z-timebox-wheel-list {
  margin: 4px 0;
}
.z-calendar-wheel-date,
.z-timebox-wheel-time {
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  -o-border-radius: 3px;
  -ms-border-radius: 3px;
  border-radius: 3px;
  position: relative;
  .gradient('ver', '#000 0%; #333 35%; #888 50%; #333 65%; #000 100%');
}
.z-calendar-wheel-date {
  display: -webkit-box;
  display: -moz-box;
  display: box;
  -webkit-box-orient: horizontal;
  -moz-box-orient: horizontal;
  -o-box-orient: horizontal;
  -ms-box-orient: horizontal;
  box-orient: horizontal;
  -webkit-box-flex: 1;
  -moz-box-flex: 1;
  -o-box-flex: 1;
  -ms-box-flex: 1;
  box-flex: 1;
  width: 100%;
  margin-right: 4px;
}
.z-timebox-wheel-time {
  display: -webkit-box;
  display: -moz-box;
  display: box;
  -webkit-box-orient: horizontal;
  -moz-box-orient: horizontal;
  -o-box-orient: horizontal;
  -ms-box-orient: horizontal;
  box-orient: horizontal;
  width: 50%;
}
li.z-calendar-wheel-list-selected,
li.z-timebox-wheel-list-selected {
  opacity: 1;
}
@media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
  .z-calendar-wheel-date {
    margin-right: 2px;
  }
  .z-calendar-wheel-list,
  .z-timebox-wheel-list {
    height: 120px;
    margin: 0 1px 0 0;
  }
  .z-calendar-wheel-list li,
  .z-timebox-wheel-list li {
    font-size: 20px;
    height: 40px;
    padding: 0;
    line-height: 40px;
  }
  .z-calendar-wheel-button,
  .z-timebox-wheel-button {
    font-size: 22px;
  }
  .z-calendar-wheel-footer,
  .z-timebox-wheel-footer {
    height: 35px;
  }
}
@media only screen and (min-device-width : 480px) and (max-device-width : 720px) {
  .z-calendar-wheel-date {
    margin-right: 2px;
  }
  .z-calendar-wheel-list,
  .z-timebox-wheel-list {
    margin: 0 1px 0 0;
    height: 150px;
  }
  .z-calendar-wheel-list li,
  .z-timebox-wheel-list li {
    font-size: 24px;
    height: 50px;
    padding: 0;
    line-height: 50px;
  }
  .z-calendar-wheel-button,
  .z-timebox-wheel-button {
    font-size: 26px;
  }
  .z-calendar-wheel-footer,
  .z-timebox-wheel-footer {
    height: 40px;
  }
}
.z-colorbox {
  width: 44px;
  height: 32px;
}
.z-colorpalette {
  width: 586px;
  height: 460px;
}
.z-colorpalette-newcolor {
  height: 32px;
  left: 450px;
}
.z-colorpalette-input {
  height: 32px;
  left: auto;
  right: 6px;
}
.z-colorpalette-color {
  width: 32px;
  height: 32px;
}
.z-colorpicker {
  width: 620px;
  height: 430px;
}
.z-colorpicker-gradient,
.z-colorpicker-overlay {
  width: 384px;
  height: 384px;
}
.z-colorpicker-gradient {
  left: 5px;
  top: 40px;
}
.z-colorpicker-overlay {
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/colorbox/colorpicker_gradient.png")});
}
.z-colorpicker-hue {
  width: 32px;
  height: 384px;
  top: 40px;
  left: 400px;
}
.z-colorpicker-bar {
  width: 26px;
  height: 384px;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/colorbox/colorpicker_hue.png")});
}
.z-colorpicker-arrows {
  width: 40px;
  background-image: url(${c:encodeThemeURL("~./zkmax/img/tablet/colorbox/colorpicker_arrows.png")});
}
.z-colorpicker-color {
  top: 40px;
  left: 450px;
}
.z-colorpicker-hex {
  top: 46px;
  left: 510px;
}
.z-colorpicker-hex .z-colorpicker-input {
  margin-left: 2px;
  top: 0;
}
.z-colorpicker-input {
  width: 55px;
  height: 32px;
  padding: 3px;
  position: relative;
  top: -5px;
}
.z-colorpicker-r,
.z-colorpicker-g,
.z-colorpicker-b {
  width: 70px;
  left: 450px;
}
.z-colorpicker-h,
.z-colorpicker-s,
.z-colorpicker-v {
  width: 70px;
  left: 530px;
}
.z-colorpicker-r,
.z-colorpicker-h {
  top: 125px;
}
.z-colorpicker-g,
.z-colorpicker-s {
  top: 160px;
}
.z-colorpicker-b,
.z-colorpicker-v {
  top: 195px;
}
.z-colorpicker-button {
  width: 150px;
  top: 235px;
  left: 450px;
}
.z-colorpicker-icon {
  font-size: 20px;
}
.z-colorbox-paletteicon,
.z-menu-paletteicon,
.z-colorbox-pickericon,
.z-menu-pickericon {
  width: 32px;
  height: 32px;
  background: url(${c:encodeThemeURL("~./zkmax/img/tablet/colorbox/cb-buttons.png")}) ;
  background-position: 0 0;
}
.z-palette-button .z-colorbox-paletteicon,
.z-palette-button .z-menu-paletteicon,
.z-colorpalette-popup .z-menu-paletteicon {
  background-position: -32px 0;
}
.z-colorbox-pickericon,
.z-menu-pickericon,
.z-colorpalette-popup .z-menu-pickericon {
  background-position: 0 -32px;
  left: 40px;
}
.z-picker-button .z-colorbox-pickericon,
.z-picker-button .z-menu-pickericon,
.z-colorpicker-popup .z-menu-pickericon {
  background-position: -32px -32px;
}
.z-combobox-popup {
  font-size: 125%;
  font-weight: 600;
}
.z-comboitem {
  padding: 16px 8px;
  line-height: normal;
}
.z-comboitem-text {
  display: inline-block;
  margin-top: -8px;
}
.z-comboitem-inner {
  font-size: 85%;
  display: block;
}
.z-comboitem-image {
  float: left;
}
.z-spinner-button,
.z-doublespinner-button {
  width: 46px;
  clear: both;
}
.z-spinner-button > a,
.z-doublespinner-button > a {
  line-height: 32px;
  float: right;
}
.z-spinner-up,
.z-doublespinner-up {
  border-left: 1px solid #326F70;
}
.z-spinner-input:not(.z-spinner-input-full),
.z-doublespinner-input:not(.z-doublespinner-input-full) {
  padding-right: 71px;
}
.z-timepicker-popup {
  font-size: 125%;
  font-weight: 600;
}
.z-timepicker-option {
  padding: 16px 8px;
  line-height: normal;
}
.z-timepicker-option-text {
  display: inline-block;
  margin-top: -8px;
}
