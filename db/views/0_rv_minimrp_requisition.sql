CREATE OR REPLACE VIEW rv_minimrp_requisition AS 
 SELECT mrl.ad_client_id,
    mrl.ad_org_id,
    mrl.mrp_runline_id,
    mrl.mrp_run_id,
    r.m_requisition_id,
    r.documentno,
    rl.line,
    p.m_product_id,
    po.vendorproductno,
    rl.c_bpartner_id,
    rl.qty,
    rl.priceactual,
    rl.isactive,
    po.deliverytime_target,
    p.m_product_category_id
   FROM ((((mrp_runline mrl
     JOIN m_requisitionline rl ON (((rl.m_requisition_id = mrl.m_requisition_id) AND (rl.m_product_id = mrl.m_product_id))))
     JOIN m_requisition r ON ((r.m_requisition_id = rl.m_requisition_id)))
     JOIN m_product p ON ((p.m_product_id = rl.m_product_id)))
     LEFT JOIN m_product_po po ON (((po.m_product_id = p.m_product_id) AND (po.c_bpartner_id = rl.c_bpartner_id))))
  ORDER BY r.m_requisition_id, rl.line;