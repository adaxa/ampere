CREATE OR REPLACE VIEW rv_m_forecast_period AS 
 SELECT f.ad_client_id,
    f.ad_org_id,
    f.m_forecast_id,
    max(f.name) AS name,
    f.c_calendar_id,
    f.c_year_id,
    f.c_period_id,
    f.m_product_id,
    f.c_uom_id,
    sum(f.qty) AS qty,
    sum(f.qtycalculated) AS qtycalculated,
    sum(f.totalamt) AS totalamt
   FROM rv_m_forecast f
  GROUP BY f.ad_client_id, f.ad_org_id, f.m_forecast_id, f.m_product_id, f.c_uom_id, f.c_calendar_id, f.c_year_id, f.c_period_id;