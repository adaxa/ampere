CREATE OR REPLACE VIEW rv_fact_acct_proj AS 
 SELECT f.ad_client_id,
    f.ad_org_id,
    f.isactive,
    f.created,
    f.createdby,
    f.updated,
    f.updatedby,
    f.fact_acct_id,
    f.c_acctschema_id,
    f.account_id,
    f.datetrx,
    f.dateacct,
    f.c_period_id,
    f.ad_table_id,
    f.record_id,
    f.line_id,
    f.gl_category_id,
    f.gl_budget_id,
    f.c_tax_id,
    f.m_locator_id,
    f.postingtype,
    f.c_currency_id,
    f.amtsourcedr,
    f.amtsourcecr,
    (f.amtsourcedr - f.amtsourcecr) AS amtsource,
    f.amtacctdr,
    f.amtacctcr,
    (f.amtacctdr - f.amtacctcr) AS amtacct,
        CASE
            WHEN ((f.amtsourcedr - f.amtsourcecr) = (0)::numeric) THEN (0)::numeric
            ELSE ((f.amtacctdr - f.amtacctcr) / (f.amtsourcedr - f.amtsourcecr))
        END AS rate,
    f.c_uom_id,
    f.qty,
    f.m_product_id,
    f.c_bpartner_id,
    f.ad_orgtrx_id,
    f.c_locfrom_id,
    f.c_locto_id,
    f.c_salesregion_id,
    f.c_project_id,
    f.c_campaign_id,
    f.c_activity_id,
    f.user1_id,
    f.user2_id,
    f.a_asset_id,
    f.description,
    o.value AS orgvalue,
    o.name AS orgname,
    ev.value AS accountvalue,
    ev.name,
    ev.accounttype,
    bp.value AS bpartnervalue,
    bp.name AS bpname,
    bp.c_bp_group_id,
    p.value AS productvalue,
    p.name AS productname,
    p.upc,
    p.m_product_category_id,
        CASE
            WHEN ((f.qty <> (0)::numeric) AND (f.qty IS NOT NULL)) THEN f.qty
            WHEN ((mc.currentcostprice <> (0)::numeric) AND ((f.qty IS NULL) OR (f.qty = (0)::numeric))) THEN (1)::numeric
            ELSE NULL::numeric
        END AS sellqty,
    COALESCE(pp.pricelist, (mc.currentcostprice *
        CASE
            WHEN (COALESCE(ta.non_product_markup, (0)::numeric) > (0)::numeric) THEN ta.non_product_markup
            WHEN (COALESCE(ph.non_product_markup, (0)::numeric) > (0)::numeric) THEN ph.non_product_markup
            WHEN (COALESCE(cp.non_product_markup) > (0)::numeric) THEN cp.non_product_markup
            ELSE 1.1
        END)) AS unitsellprice,
    (
        CASE
            WHEN ((f.qty <> (0)::numeric) AND (f.qty IS NOT NULL)) THEN f.qty
            WHEN ((mc.currentcostprice <> (0)::numeric) AND ((f.qty IS NULL) OR (f.qty = (0)::numeric))) THEN (1)::numeric
            ELSE NULL::numeric
        END * COALESCE(pp.pricelist, (mc.currentcostprice *
        CASE
            WHEN (COALESCE(ta.non_product_markup, (0)::numeric) > (0)::numeric) THEN ta.non_product_markup
            WHEN (COALESCE(ph.non_product_markup, (0)::numeric) > (0)::numeric) THEN ph.non_product_markup
            WHEN (COALESCE(cp.non_product_markup) > (0)::numeric) THEN cp.non_product_markup
            ELSE 1.1
        END))) AS extended_sell_price,
        CASE
            WHEN (f.ad_table_id = (53781)::numeric) THEN ((COALESCE(( SELECT s_timesheetentry.comments
               FROM s_timesheetentry
              WHERE (s_timesheetentry.s_timesheetentry_id = f.record_id)), p.name))::character varying)::text
            WHEN (f.ad_table_id = (318)::numeric) THEN COALESCE(( SELECT c_charge.name
               FROM c_charge
              WHERE (c_charge.c_charge_id = ( SELECT il.c_charge_id
                       FROM c_invoiceline il
                      WHERE ((il.c_invoice_id = f.record_id) AND (il.c_invoiceline_id = f.line_id) AND (il.c_charge_id IS NOT NULL))))), ((p.name)::character varying)::text)
            WHEN (f.ad_table_id = (224)::numeric) THEN ( SELECT gl_journal.description
               FROM gl_journal
              WHERE (gl_journal.gl_journal_id = f.record_id))
            WHEN (f.ad_table_id = (321)::numeric) THEN ((COALESCE(( SELECT ((p.name || '+'::text) || il.description)
               FROM m_inventoryline il
              WHERE ((il.m_inventory_id = f.record_id) AND (il.m_inventoryline_id = f.line_id))), p.name))::character varying)::text
            ELSE p.name
        END AS invoicing_desc
   FROM (((((((((((fact_acct f
     JOIN ad_org o ON ((f.ad_org_id = o.ad_org_id)))
     JOIN c_elementvalue ev ON ((f.account_id = ev.c_elementvalue_id)))
     LEFT JOIN c_bpartner bp ON ((f.c_bpartner_id = bp.c_bpartner_id)))
     LEFT JOIN m_product p ON ((f.m_product_id = p.m_product_id)))
     LEFT JOIN c_project cp ON ((cp.c_project_id = f.c_project_id)))
     LEFT JOIN c_projectphase ph ON ((ph.c_projectphase_id = f.c_projectphase_id)))
     LEFT JOIN c_projecttask ta ON ((ta.c_projecttask_id = f.c_projecttask_id)))
     LEFT JOIN m_productprice pp ON (((pp.m_pricelist_version_id = cp.m_pricelist_version_id) AND (pp.m_product_id = f.m_product_id))))
     JOIN c_acctschema asm ON ((asm.c_acctschema_id = f.c_acctschema_id)))
     JOIN m_costelement ce ON (((ce.costingmethod = asm.costingmethod) AND (ce.ad_client_id = f.ad_client_id))))
     LEFT JOIN m_cost mc ON (((mc.m_product_id = f.m_product_id) AND (mc.m_costtype_id = asm.m_costtype_id) AND (mc.c_acctschema_id = f.c_acctschema_id) AND (mc.m_costelement_id = ce.m_costelement_id) AND (mc.ad_org_id = ( SELECT max(m_cost.ad_org_id) AS max
           FROM m_cost
          WHERE ((m_cost.m_product_id = f.m_product_id) AND (m_cost.m_costtype_id = asm.m_costtype_id) AND (m_cost.c_acctschema_id = f.c_acctschema_id) AND (m_cost.m_costelement_id = ce.m_costelement_id) AND (m_cost.ad_org_id = ANY (ARRAY[(0)::numeric, f.ad_org_id]))))))));