CREATE OR REPLACE VIEW ad_user_roles_v AS 
 SELECT u.name,
    r.name AS rolename
   FROM ((ad_user_roles ur
     JOIN ad_user u ON ((ur.ad_user_id = u.ad_user_id)))
     JOIN ad_role r ON ((ur.ad_role_id = r.ad_role_id)));