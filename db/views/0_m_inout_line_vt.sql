CREATE OR REPLACE VIEW m_inout_line_vt AS 
 SELECT iol.ad_client_id,
    iol.ad_org_id,
    iol.isactive,
    iol.created,
    iol.createdby,
    iol.updated,
    iol.updatedby,
    uom.ad_language,
    iol.m_inout_id,
    iol.m_inoutline_id,
    iol.line,
    p.m_product_id,
        CASE
            WHEN ((iol.movementqty <> (0)::numeric) OR (iol.m_product_id IS NOT NULL)) THEN iol.movementqty
            ELSE NULL::numeric
        END AS movementqty,
        CASE
            WHEN ((iol.qtyentered <> (0)::numeric) OR (iol.m_product_id IS NOT NULL)) THEN iol.qtyentered
            ELSE NULL::numeric
        END AS qtyentered,
        CASE
            WHEN ((iol.movementqty <> (0)::numeric) OR (iol.m_product_id IS NOT NULL)) THEN (uom.uomsymbol)::character varying
            ELSE NULL::character varying
        END AS uomsymbol,
    ol.qtyordered,
    ol.qtydelivered,
        CASE
            WHEN ((iol.movementqty <> (0)::numeric) OR (iol.m_product_id IS NOT NULL)) THEN (ol.qtyordered - ol.qtydelivered)
            ELSE NULL::numeric
        END AS qtybackordered,
    COALESCE((COALESCE(pt.name, ((p.name)::character varying)::text) || (productattribute(iol.m_attributesetinstance_id))::text), c.name, iol.description) AS name,
        CASE
            WHEN (COALESCE(pt.name, ((p.name)::character varying)::text, c.name) IS NOT NULL) THEN (iol.description)::character varying
            ELSE NULL::character varying
        END AS description,
    COALESCE(pt.documentnote, ((p.documentnote)::character varying)::text) AS documentnote,
    p.upc,
    p.sku,
    p.value AS productvalue,
    iol.m_locator_id,
    l.m_warehouse_id,
    l.x,
    l.y,
    l.z,
    iol.m_attributesetinstance_id,
    asi.m_attributeset_id,
    asi.serno,
    asi.lot,
    asi.m_lot_id,
    asi.guaranteedate,
    pt.description AS productdescription,
    p.imageurl,
    iol.c_campaign_id,
    iol.c_project_id,
    iol.c_activity_id,
    iol.c_projectphase_id,
    iol.c_projecttask_id
   FROM (((((((m_inoutline iol
     JOIN c_uom_trl uom ON ((iol.c_uom_id = uom.c_uom_id)))
     LEFT JOIN m_product p ON ((iol.m_product_id = p.m_product_id)))
     LEFT JOIN m_product_trl pt ON (((iol.m_product_id = pt.m_product_id) AND ((uom.ad_language)::text = (pt.ad_language)::text))))
     LEFT JOIN m_attributesetinstance asi ON ((iol.m_attributesetinstance_id = asi.m_attributesetinstance_id)))
     LEFT JOIN m_locator l ON ((iol.m_locator_id = l.m_locator_id)))
     LEFT JOIN c_orderline ol ON ((iol.c_orderline_id = ol.c_orderline_id)))
     LEFT JOIN c_charge_trl c ON ((iol.c_charge_id = c.c_charge_id)));