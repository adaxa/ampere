CREATE OR REPLACE VIEW c_payselection_check_vt AS 
 SELECT psc.ad_client_id,
    psc.ad_org_id,
    l.ad_language,
    psc.c_payselection_id,
    psc.c_payselectioncheck_id,
    oi.c_location_id AS org_location_id,
    oi.taxid,
    p.c_doctype_id,
    bp.c_bpartner_id,
    bp.value AS bpvalue,
    bp.taxid AS bptaxid,
    bp.naics,
    bp.duns,
    bpg.greeting AS bpgreeting,
    bp.name,
    bp.name2,
    bpartnerremitlocation(bp.c_bpartner_id) AS c_location_id,
    bp.referenceno,
    bp.poreference,
    ps.paydate,
    psc.payamt,
    psc.payamt AS amtinwords,
    psc.qty,
    psc.paymentrule,
    psc.documentno,
    COALESCE(oi.logo_id, ci.logo_id) AS logo_id,
    dt.printname AS documenttype,
    dt.documentnote AS documenttypenote,
    p.description,
    ba.description AS bankaccountdescription
   FROM (((((((((c_payselectioncheck psc
     JOIN c_payselection ps ON ((psc.c_payselection_id = ps.c_payselection_id)))
     LEFT JOIN c_payment p ON ((psc.c_payment_id = p.c_payment_id)))
     LEFT JOIN c_bankaccount ba ON ((p.c_bankaccount_id = ba.c_bankaccount_id)))
     LEFT JOIN c_doctype_trl dt ON ((p.c_doctype_id = dt.c_doctype_id)))
     JOIN c_bpartner bp ON ((psc.c_bpartner_id = bp.c_bpartner_id)))
     JOIN ad_orginfo oi ON ((psc.ad_org_id = oi.ad_org_id)))
     JOIN ad_clientinfo ci ON ((psc.ad_client_id = ci.ad_client_id)))
     LEFT JOIN ad_language l ON ((l.issystemlanguage = 'Y'::bpchar)))
     LEFT JOIN c_greeting_trl bpg ON (((bp.c_greeting_id = bpg.c_greeting_id) AND ((bpg.ad_language)::text = l.ad_language))));