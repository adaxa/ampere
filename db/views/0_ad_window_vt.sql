CREATE OR REPLACE VIEW ad_window_vt AS 
 SELECT trl.ad_language,
    bt.ad_window_id,
    trl.name,
    trl.description,
    trl.help,
    bt.windowtype,
    bt.ad_color_id,
    bt.ad_image_id,
    bt.isactive,
    bt.winwidth,
    bt.winheight,
    bt.issotrx
   FROM (ad_window bt
     JOIN ad_window_trl trl ON ((bt.ad_window_id = trl.ad_window_id)))
  WHERE (bt.isactive = 'Y'::bpchar);