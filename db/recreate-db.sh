#!/bin/sh

DB_CONTAINER=${1:-ampere_postgres_1}
DB_USER=${2:-ampere}
DB_NAME=${3:-ampere}

docker exec -i $DB_CONTAINER dropdb -U $DB_USER $DB_NAME

docker exec -i $DB_CONTAINER createdb -U $DB_USER $DB_NAME

docker exec -i $DB_CONTAINER psql -U $DB_USER -d $DB_NAME < init-db.sql
