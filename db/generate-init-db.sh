#!/bin/sh

cat pre-data.sql > init-db.sql

echo "SET search_path TO 'ampere';" >> init-db.sql

find data -name "*.sql" -type f -exec cat {} \+ >> init-db.sql

echo "" >> init-db.sql

cat post-data.sql >> init-db.sql
