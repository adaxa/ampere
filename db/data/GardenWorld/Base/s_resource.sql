copy "s_resource" ("s_resource_id", "name", "ad_client_id", "ad_org_id", "ad_user_id", "chargeableqty", "created", "createdby", "dailycapacity", "description", "isactive", "isavailable", "ismanufacturingresource", "manufacturingresourcetype", "m_warehouse_id", "percentutilization", "planninghorizon", "queuingtime", "s_resourcetype_id", "updated", "updatedby", "value", "waitingtime") from STDIN;
100	Mary Consultant	11	0	101	0	2022-06-22 21:28:27	100	\N	\N	Y	Y	N	\N	103	100	0	\N	100	2022-11-20 12:47:06	100	Mary	\N
50000	Fertilizer Plant	11	0	\N	0	2022-09-22 19:21:29	100	0	\N	Y	Y	Y	PT	50002	100	120	0	50000	2022-09-24 18:06:03	100	Fertilizer Plant	0
50001	Assembly Area	11	0	\N	0	2022-09-22 19:26:01	100	8.000000000000	\N	Y	Y	Y	WC	50001	100	0	0	50001	2022-09-25 17:56:04	100	Assembly Area	0
50002	Paint Area	11	0	\N	0	2022-09-22 19:27:08	100	8.000000000000	\N	Y	Y	Y	WC	50001	100	0	0	50001	2022-09-25 17:56:42	100	Paint Area	0
50003	Chrome Subcontract Area	11	0	\N	0	2022-09-22 19:29:08	100	8.000000000000	\N	Y	Y	Y	WC	50001	100	0	0	50001	2022-09-25 17:56:10	100	Chrome Subcontract Area	0
50004	Inspection Area	11	0	\N	0	2022-09-22 19:30:01	100	8.000000000000	\N	Y	Y	Y	WC	50001	100	0	0	50001	2022-09-25 17:56:29	100	Inspection Area	0
50005	Furniture Plant	11	0	\N	0	2022-09-22 19:31:59	100	0	\N	Y	Y	Y	PT	50001	100	120	0	50000	2022-09-24 18:05:55	100	Furniture Plant	0
50006	Packing Production Line	11	0	\N	0	2022-09-22 19:32:43	100	8.000000000000	\N	Y	Y	Y	PL	50002	100	0	0	50001	2022-09-25 17:56:37	100	Packing Production Line	0
50007	Mixed Area	11	0	\N	0	2022-09-22 19:33:31	100	8.000000000000	\N	Y	Y	Y	WC	50002	100	0	0	50001	2022-09-25 17:56:33	100	Mixed Area	0
50008	Dry Area	11	0	\N	0	2022-09-22 19:34:25	100	8.000000000000	\N	Y	Y	Y	WC	50002	100	0	0	50001	2022-09-25 17:56:15	100	Dry Area	0
50009	Fertilizer Inspection Area	11	0	\N	0	2022-09-22 19:35:32	100	8.000000000000	\N	Y	Y	Y	WC	50002	100	0	0	50001	2022-09-25 17:56:20	100	Fertilizer Inspection Area	0
\.
