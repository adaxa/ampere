copy "c_location" ("c_location_id", "city", "ad_client_id", "address1", "address2", "address3", "address4", "ad_org_id", "c_city_id", "c_country_id", "created", "createdby", "c_region_id", "isactive", "latitude", "longitude", "postal", "postal_add", "regionname", "updated", "updatedby") from STDIN;
114	Portland	11	2828 SW Corbett Ave	Suite 130	\N	\N	0	\N	100	2021-03-27 15:44:24	0	142	Y	\N	\N	97201	\N	\N	2022-09-22 13:20:22	100
115	Monroe	11	40 Old Tannery Rd	\N	\N	\N	0	\N	100	2021-03-27 15:44:25	0	102	Y	\N	\N	\N	\N	\N	2021-07-27 00:00:00	0
116	NJ Town	11	1 Main St	\N	\N	\N	11	\N	100	2021-04-11 00:00:00	100	109	Y	\N	\N	00011	\N	\N	2021-05-21 00:00:00	0
119	Stamford	11	100 Elm St	\N	\N	\N	11	\N	100	2021-04-11 00:00:00	100	102	Y	\N	\N	03323	\N	\N	2021-04-11 00:00:00	0
120	Hartford	11	123 Oak St	\N	\N	\N	11	\N	100	2021-04-11 00:00:00	100	102	Y	\N	\N	04460	\N	\N	2021-04-11 00:00:00	0
121	Westport	11	1000 Main Street	\N	\N	\N	12	\N	100	2021-05-11 00:00:00	100	102	Y	\N	\N	06555	\N	\N	2021-05-11 00:00:00	0
122	Small Village	11	Mud Rd	\N	\N	\N	11	\N	100	2021-05-21 00:00:00	100	102	Y	\N	\N	06455	\N	\N	2021-05-21 00:00:00	0
123	New York	11	1 Furniture Avenue	\N	\N	\N	11	\N	100	2021-01-13 00:00:00	100	108	Y	\N	\N	10009	\N	\N	2021-01-13 00:00:00	0
124	Old York	11	33 Main Street	\N	\N	\N	11	\N	100	2021-02-21 00:00:00	100	108	Y	\N	\N	10002	\N	\N	2021-02-21 00:00:00	0
125	Near Village	11	26 Home Str	\N	\N	\N	11	\N	100	2021-03-18 00:00:00	100	102	Y	\N	\N	06488	\N	\N	2021-03-18 00:00:00	0
126	Tangelwood	11	\N	\N	\N	\N	11	\N	100	2021-08-09 22:02:47	100	104	Y	\N	\N	\N	\N	\N	2021-08-09 22:03:45	100
127	33	11	33	\N	\N	\N	11	\N	100	2022-03-30 23:47:13	100	102	Y	\N	\N	33	\N	\N	2022-03-30 23:47:13	100
50000	\N	11	2828 SW Corbett Ave	\N	\N	\N	0	\N	100	2022-09-22 13:17:38	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:17:38	100
50001	\N	11	2828 SW Corbett Ave	\N	\N	\N	0	\N	100	2022-09-22 13:18:50	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:18:50	100
50002	Portland	11	2828 SW Corbett Ave	Suite 130	\N	\N	0	\N	100	2022-09-22 13:19:44	100	142	Y	\N	\N	97201	\N	\N	2022-09-22 13:20:34	100
50003	Pachuca	11	Carretera Mexico Pachuca Km 80	\N	\N	\N	0	\N	247	2022-09-22 13:21:48	100	188	Y	\N	\N	43000	\N	\N	2022-09-22 13:21:48	100
50004	Munich	11	Schillerstrabe 18d	\N	\N	\N	0	\N	101	2022-09-22 13:23:56	100	\N	Y	\N	\N	47445	\N	\N	2022-09-22 13:23:56	100
50005	\N	11	Store North	\N	\N	\N	0	\N	100	2022-09-22 13:36:11	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:36:11	100
50006	\N	11	Store South	\N	\N	\N	0	\N	100	2022-09-22 13:37:27	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:37:27	100
50007	\N	11	Store East	\N	\N	\N	0	\N	100	2022-09-22 13:38:41	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:38:41	100
50008	\N	11	Store West	\N	\N	\N	0	\N	100	2022-09-22 13:39:26	100	142	Y	\N	\N	\N	\N	\N	2022-09-22 13:39:26	100
50009	\N	11	Store East	\N	\N	\N	0	\N	100	2022-09-24 14:46:10	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 14:46:10	100
50010	\N	11	Store Central	\N	\N	\N	0	\N	100	2022-09-24 14:47:01	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 14:47:01	100
50011	\N	11	Store North	\N	\N	\N	0	\N	100	2022-09-24 14:48:43	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 14:48:43	100
50012	\N	11	Store South	\N	\N	\N	0	\N	100	2022-09-24 14:49:49	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 14:49:49	100
50013	\N	11	Store West	\N	\N	\N	0	\N	100	2022-09-24 14:50:50	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 14:50:50	100
50014	\N	11	Fertilizer Transit	\N	\N	\N	0	\N	100	2022-09-24 15:07:16	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 15:07:16	100
50015	\N	11	Furniture Transit	\N	\N	\N	0	\N	100	2022-09-24 15:10:12	100	142	Y	\N	\N	\N	\N	\N	2022-09-24 15:10:12	100
\.
