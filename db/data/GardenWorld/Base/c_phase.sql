copy "c_phase" ("c_phase_id", "name", "c_projecttype_id", "ad_client_id", "ad_org_id", "created", "createdby", "description", "help", "isactive", "m_product_id", "seqno", "standardqty", "updated", "updatedby") from STDIN;
100	Initial Need Evaluation	100	11	11	2021-08-09 22:05:37	100	Evaluate the need and priorities	\N	Y	\N	10	1	2021-08-09 22:29:25	100
101	CRP - Critical Operation	100	11	11	2021-08-09 22:07:05	100	Conference Room Pilot  - Critical Operations	This phase conentrates on critical operation of the business	Y	\N	20	0	2021-08-18 13:36:33	100
102	Initial Scope Definition	101	11	11	2021-08-18 13:39:22	100	\N	\N	Y	\N	10	1	2021-08-18 13:40:38	100
103	Design	101	11	11	2021-08-18 13:41:07	100	\N	\N	Y	\N	20	1	2021-08-18 13:41:07	100
104	Implementation	101	11	11	2021-08-18 13:41:17	100	\N	\N	Y	\N	30	1	2021-08-18 13:41:17	100
105	Initial Contact	102	11	11	2021-08-18 14:56:57	100	Initial Contact	\N	Y	\N	10	1	2021-08-18 14:56:57	100
106	Initial Qualification	102	11	11	2021-08-18 14:59:04	100	How likely is the suspect to go ahead ?\n- Real pressing need with timeline (vs. looking around)\n- Approved Budget in line with targeted project scope	\N	Y	\N	20	1	2021-08-18 15:01:05	100
107	Initial Sales Presentation	102	11	11	2021-08-18 15:00:49	100	- What difference does our product / service make\n- Capabilities & Competencies	\N	Y	\N	30	1	2021-08-18 15:00:49	100
108	Initial Scope	102	11	11	2021-08-18 15:03:12	100	Rough estimate and Budget check	\N	Y	\N	40	1	2021-08-18 15:03:12	100
109	First Product Presentation	102	11	11	2021-08-18 15:04:43	100	Based on pressing needs and info gathered do tailored overview presentation of the product	\N	Y	\N	50	1	2021-08-18 15:04:43	100
110	Paid Scope Study	102	11	11	2021-08-18 15:05:38	100	Define Scope, Phases and Timelines	\N	Y	\N	60	1	2021-08-18 15:05:38	100
111	Contract Negotiation	102	11	11	2021-08-18 15:06:37	100	Based on Scope Study, negotiation of terms	\N	Y	\N	70	1	2021-08-18 15:06:37	100
112	Closed	102	11	11	2021-08-18 15:07:05	100	Contract Signed.	\N	Y	\N	80	1	2021-08-18 15:07:05	100
\.
