copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50057	a_asset_retirement_pkey	0	0	540	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (a_asset_retirement_id)	CP	\N	2022-11-02 05:57:56.041854	100
50058	cinvoiceline_aassetretirement	0	0	540	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_invoiceline_id) REFERENCES c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50059	aasset_aassetretirement	0	0	540	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50091	ad_archive_pkey	0	0	754	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_archive_id)	CP	\N	2022-11-02 05:57:56.041854	100
50092	adprocess_adarchive	0	0	754	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_process_id) REFERENCES ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50093	adtable_adarchive	0	0	754	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50094	cbpartner_adarchive	0	0	754	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50108	ad_attribute_value_pkey	0	0	406	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_attribute_id, record_id)	CP	\N	2022-11-02 05:57:56.041854	100
50109	adattribute_adattributevalue	0	0	406	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_attribute_id) REFERENCES ad_attribute(ad_attribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50417	ad_private_access_pkey	0	0	627	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_user_id, ad_table_id, record_id)	CP	\N	2022-11-02 05:57:56.041854	100
50418	adtable_adprivateaccess	0	0	627	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50419	aduser_adprivateaccess	0	0	627	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50443	ad_recentitem_key	0	0	200000	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_recentitem_id)	CP	\N	2022-11-02 05:57:56.041854	100
50522	ad_sequence_audit_pkey	0	0	121	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_sequence_id, documentno)	CP	\N	2022-11-02 05:57:56.041854	100
50523	sequence_auditclient	0	0	121	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50524	adtable_adsequenceaudit	0	0	121	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50525	ad_sequence_sequenceaudit	0	0	121	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_sequence_id) REFERENCES ad_sequence(ad_sequence_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50526	sequence_auditorg	0	0	121	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50527	ad_sequence_no_pkey	0	0	122	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_sequence_id, calendaryear)	CP	\N	2022-11-02 05:57:56.041854	100
50528	sequence_noorg	0	0	122	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50529	ad_sequence_sequenceno	0	0	122	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_sequence_id) REFERENCES ad_sequence(ad_sequence_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50530	sequence_noclient	0	0	122	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50549	ad_tab_customization_key	0	0	53894	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tab_customization_id)	CP	\N	2022-11-02 05:57:56.041854	100
50567	ad_tableselection_column_t_key	0	0	53404	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tableselection_column_id)	CP	\N	2022-11-02 05:57:56.041854	100
50576	ad_treebar_pkey	0	0	456	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, ad_user_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50577	adtree_adtreebar	0	0	456	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50578	aduser_adtreebar	0	0	456	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50579	ad_tree_favorite_key	0	0	53869	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_favorite_id)	CP	\N	2022-11-02 05:57:56.041854	100
50580	ad_tree_favorite_node_key	0	0	53870	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_favorite_node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50581	ad_treenode_pkey	0	0	289	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50582	adtree_adtreenode	0	0	289	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50583	ad_treenodebp_pkey	0	0	451	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50584	adtree_adtreenodebp	0	0	451	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50585	ad_treenodemm_pkey	0	0	452	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50586	adtree_adtreenodemm	0	0	452	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50587	ad_treenodepr_pkey	0	0	453	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50588	adtree_adtreenodepr	0	0	453	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50589	ad_treenodeu1_pkey	0	0	852	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50590	adtree_adtreenodeu1	0	0	852	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50591	ad_treenodeu2_pkey	0	0	851	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50592	adtree_adtreenodeu2	0	0	851	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50593	ad_treenodeu3_pkey	0	0	850	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50594	adtree_adtreenodeu3	0	0	850	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50595	ad_treenodeu4_pkey	0	0	849	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_tree_id, node_id)	CP	\N	2022-11-02 05:57:56.041854	100
50596	adtree_adtreenodeu4	0	0	849	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tree_id) REFERENCES ad_tree(ad_tree_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50624	ad_user_orgaccess_pkey	0	0	769	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_user_id, ad_org_id)	CP	\N	2022-11-02 05:57:56.041854	100
50625	adorg_aduserorgaccess	0	0	769	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50626	aduser_aduserorgaccess	0	0	769	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50627	ad_userquery_pkey	0	0	814	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_userquery_id)	CP	\N	2022-11-02 05:57:56.041854	100
50628	adtab_aduserquery	0	0	814	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_tab_id) REFERENCES ad_tab(ad_tab_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50629	adtable_aduserquery	0	0	814	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50630	aduser_aduserquery	0	0	814	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50636	ad_user_substitute_pkey	0	0	642	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_user_substitute_id)	CP	\N	2022-11-02 05:57:56.041854	100
50637	aduser_adusersub	0	0	642	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50638	adusersub_ad_usersub	0	0	642	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (substitute_id) REFERENCES ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51414	c_projectissuema_pkey	0	0	761	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (c_projectissue_id, m_attributesetinstance_id)	CP	\N	2022-11-02 05:57:56.041854	100
51415	masi_cprojectissuema	0	0	761	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attributesetinstance_id) REFERENCES m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51416	cprojectissue_cprojectissuema	0	0	761	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_projectissue_id) REFERENCES c_projectissue(c_projectissue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51660	cbpartner_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51661	cactivity_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_activity_id) REFERENCES c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51662	account_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (account_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51663	user1_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (user1_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51664	glbudget_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_budget_id) REFERENCES gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51665	csubacct_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_subacct_id) REFERENCES c_subacct(c_subacct_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51666	cacctschema_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51667	ccampaign_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_campaign_id) REFERENCES c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51668	cprojecttask_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_projecttask_id) REFERENCES c_projecttask(c_projecttask_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51669	pareportcube_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (pa_reportcube_id) REFERENCES pa_reportcube(pa_reportcube_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51670	cprojectphase_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_projectphase_id) REFERENCES c_projectphase(c_projectphase_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51671	mproduct_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51672	cperiod_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51673	user2_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (user2_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51674	cproject_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_project_id) REFERENCES c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51675	csalesregion_factacctsummary	0	0	53203	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_salesregion_id) REFERENCES c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51676	fact_reconciliation_key	0	0	53286	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (fact_reconciliation_id)	CP	\N	2022-11-02 05:57:56.041854	100
51765	i_attributesetinstance_key	0	0	54030	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (i_attributesetinstance_id)	CP	\N	2022-11-02 05:57:56.041854	100
51796	i_fajournal_key	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (i_fajournal_id)	CP	\N	2022-11-02 05:57:56.041854	100
51797	cacctschema_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51798	cbpartner_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51799	ccurrency_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51800	user1_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (user1_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51801	glbudget_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_budget_id) REFERENCES gl_budget(gl_budget_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51802	glcategory_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_category_id) REFERENCES gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51803	user2_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (user2_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51804	cperiod_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51805	csalesregion_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_salesregion_id) REFERENCES c_salesregion(c_salesregion_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51806	adorgdoc_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_orgdoc_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51807	cuom_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_uom_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51808	cvalidcombination_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_validcombination_id) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51809	gljournalbatch_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_journalbatch_id) REFERENCES gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51810	cdoctype_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_doctype_id) REFERENCES c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51811	cactivity_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_activity_id) REFERENCES c_activity(c_activity_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51812	ccampaign_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_campaign_id) REFERENCES c_campaign(c_campaign_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51813	adorgtrx_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_orgtrx_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51814	mproduct_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51815	cproject_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_project_id) REFERENCES c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51816	gljournalline_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_journalline_id) REFERENCES gl_journalline(gl_journalline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51817	account_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (account_id) REFERENCES c_elementvalue(c_elementvalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51818	gljournal_ifajournal	0	0	53117	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (gl_journal_id) REFERENCES gl_journal(gl_journal_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51819	i_freightregion_key	0	0	53375	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (i_freightregion_id)	CP	\N	2022-11-02 05:57:56.041854	100
51926	i_product_bom_key	0	0	54029	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (i_product_bom_id)	CP	\N	2022-11-02 05:57:56.041854	100
51935	m_attributeinstance_pkey	0	0	561	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (m_attributesetinstance_id, m_attribute_id)	CP	\N	2022-11-02 05:57:56.041854	100
51936	mattrsetinst__mattrinst	0	0	561	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attributesetinstance_id) REFERENCES m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51937	mattributevalue_mattrinst	0	0	561	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attributevalue_id) REFERENCES m_attributevalue(m_attributevalue_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51938	mattribute_mattributeinst	0	0	561	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attribute_id) REFERENCES m_attribute(m_attribute_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52188	m_pbatch_line_key	0	0	54066	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (m_pbatch_line_id)	CP	\N	2022-11-02 05:57:56.041854	100
52296	m_productionlinema_pkey	0	0	765	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (m_productionline_id, m_attributesetinstance_id)	CP	\N	2022-11-02 05:57:56.041854	100
52297	masi_mproductionlinema	0	0	765	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attributesetinstance_id) REFERENCES m_attributesetinstance(m_attributesetinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52298	mproductionline_mplinema	0	0	765	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_productionline_id) REFERENCES m_productionline(m_productionline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52415	m_transactionallocation_pkey	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (m_transaction_id, allocationstrategytype)	CP	\N	2022-11-02 05:57:56.041854	100
52416	mproductionlineout_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (out_m_productionline_id) REFERENCES m_productionline(m_productionline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52417	mttransaction_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_transaction_id) REFERENCES m_transaction(m_transaction_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52418	minoutlineout_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (out_m_inoutline_id) REFERENCES m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52419	mproductionline_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_productionline_id) REFERENCES m_productionline(m_productionline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52420	minventoryline_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_inventoryline_id) REFERENCES m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52421	mtransactionout_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (out_m_transaction_id) REFERENCES m_transaction(m_transaction_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52422	minoutline_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_inoutline_id) REFERENCES m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52423	mattributesetinst_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_attributesetinstance_id) REFERENCES m_attributesetinstance(m_attributesetinstance_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52424	minventorylineout_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (out_m_inventoryline_id) REFERENCES m_inventoryline(m_inventoryline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52425	mproduct_mtrxalloc	0	0	636	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52719	t_distributionrundetail_pkey	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (m_distributionrun_id, m_distributionrunline_id, m_distributionlist_id, m_distributionlistline_id)	CP	\N	2022-11-02 05:57:56.041854	100
52720	cbpartner_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52721	mproduct_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52722	cbpartnerlocation_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_bpartner_location_id) REFERENCES c_bpartner_location(c_bpartner_location_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52723	mdistributionrun_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_distributionrun_id) REFERENCES m_distributionrun(m_distributionrun_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52724	mdistributionrline_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_distributionrunline_id) REFERENCES m_distributionrunline(m_distributionrunline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52725	mdistributionlline_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_distributionlistline_id) REFERENCES m_distributionlistline(m_distributionlistline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52726	mdistributionlist_tdrdetail	0	0	714	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (m_distributionlist_id) REFERENCES m_distributionlist(m_distributionlist_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52737	t_invoicegl_pkey	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (ad_pinstance_id, c_invoice_id, fact_acct_id)	CP	\N	2022-11-02 05:57:56.041854	100
52738	factacct_tinvoicegl	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (fact_acct_id) REFERENCES fact_acct(fact_acct_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52739	cconversiontype_tinvoicegl	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_conversiontypereval_id) REFERENCES c_conversiontype(c_conversiontype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52740	cinvoice_tinvoicegl	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_invoice_id) REFERENCES c_invoice(c_invoice_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52741	adpinstance_tinvoicegl	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (ad_pinstance_id) REFERENCES ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52742	cdoctype_tinvoicegl	0	0	803	2022-11-02 05:57:56.041854	100	D	\N	Y	FOREIGN KEY (c_doctypereval_id) REFERENCES c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52743	t_minimrp_key	0	0	53868	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (t_minimrp_id)	CP	\N	2022-11-02 05:57:56.041854	100
52744	t_mrp_run_capacity_key	0	0	54110	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (t_mrp_run_capacity_id)	CP	\N	2022-11-02 05:57:56.041854	100
52745	t_mrp_run_capacity_summary_key	0	0	54226	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (t_mrp_run_capacity_summary_id)	CP	\N	2022-11-02 05:57:56.041854	100
52746	t_product_bomline_key	0	0	54222	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (t_product_bomline_id)	CP	\N	2022-11-02 05:57:56.041854	100
52753	t_replenish_po_key	0	0	53949	2022-11-02 05:57:56.041854	100	D	\N	Y	PRIMARY KEY (t_replenish_po_id)	CP	\N	2022-11-02 05:57:56.041854	100
52885	ad_treenode_parentid	0	0	289	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (parent_id)	I	\N	2022-11-02 06:15:05.061147	100
52886	ad_treenodebp_parent	0	0	451	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (parent_id)	I	\N	2022-11-02 06:15:05.061147	100
52887	ad_treenodemm_parent	0	0	452	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (parent_id)	I	\N	2022-11-02 06:15:05.061147	100
52888	ad_treenodepr_parent	0	0	453	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (parent_id)	I	\N	2022-11-02 06:15:05.061147	100
52889	m_transactionallocation_prd	0	0	636	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (m_product_id, m_attributesetinstance_id, isallocated)	I	\N	2022-11-02 06:15:05.061147	100
52890	fas_account	0	0	53203	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (ad_client_id, ad_org_id, c_acctschema_id, account_id)	I	\N	2022-11-02 06:15:05.061147	100
52891	fas_dateacct	0	0	53203	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (dateacct)	I	\N	2022-11-02 06:15:05.061147	100
52892	fas_period	0	0	53203	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (c_period_id)	I	\N	2022-11-02 06:15:05.061147	100
52893	fas_reportcube	0	0	53203	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (pa_reportcube_id)	I	\N	2022-11-02 06:15:05.061147	100
52894	fact_matchcode	0	0	53286	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (matchcode)	I	\N	2022-11-02 06:15:05.061147	100
52895	trec_matchcode	0	0	53287	2022-11-02 06:15:05.061147	100	D	\N	Y	 USING btree (matchcode)	I	\N	2022-11-02 06:15:05.061147	100
52965	a_asset_retirement_isactive_check	0	0	540	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52987	ad_archive_isactive_check	0	0	754	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53170	ad_private_access_isactive_check	0	0	627	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53184	ad_recentitem_isactive_check	0	0	200000	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53224	ad_sequence_audit_isactive_check	0	0	121	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53225	ad_sequence_no_isactive_check	0	0	122	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53237	ad_tab_customization_isrefreshgridtimer_check	0	0	53894	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isrefreshgridtimer = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53238	ad_tab_customization_isactive_check	0	0	53894	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53239	ad_tab_customization_isquickentry_check	0	0	53894	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isquickentry = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53262	ad_tableselection_column_trl_isactive_check	0	0	53404	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53263	ad_tableselection_column_trl_istranslated_check	0	0	53404	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53264	ad_tableselection_trl_isactive_check	0	0	53403	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53265	ad_tableselection_trl_istranslated_check	0	0	53403	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53272	ad_treebar_isactive_check	0	0	456	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53273	ad_tree_favorite_isactive_check	0	0	53869	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53274	ad_tree_favorite_node_issummary_check	0	0	53870	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((issummary = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53275	ad_tree_favorite_node_iscollapsible_check	0	0	53870	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((iscollapsible = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53276	ad_tree_favorite_node_isactive_check	0	0	53870	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53277	ad_treenode_isactive_check	0	0	289	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53278	ad_treenodebp_isactive_check	0	0	451	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53279	ad_treenodemm_isactive_check	0	0	452	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53280	ad_treenodepr_isactive_check	0	0	453	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53281	ad_treenodeu1_isactive_check	0	0	852	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53282	ad_treenodeu2_isactive_check	0	0	851	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53283	ad_treenodeu3_isactive_check	0	0	850	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53284	ad_treenodeu4_isactive_check	0	0	849	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53306	ad_user_orgaccess_isactive_check	0	0	769	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53307	ad_userquery_isshowinlist_check	0	0	814	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isshowinlist = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53308	ad_userquery_isactive_check	0	0	814	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53310	ad_user_substitute_isactive_check	0	0	642	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53642	c_projectissuema_isactive_check	0	0	761	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53728	fact_acct_summary_isactive_check	0	0	53203	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53729	fact_reconciliation_isactive_check	0	0	53286	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53798	i_attributesetinstance_i_isimported_check	0	0	54030	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53799	i_attributesetinstance_isactive_check	0	0	54030	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53836	i_fajournal_i_isimported_check	0	0	53117	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53837	i_fajournal_isactive_check	0	0	53117	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53838	i_fajournal_processed_check	0	0	53117	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53839	i_freightregion_isactive_check	0	0	53375	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53840	i_freightregion_i_isimported_check	0	0	53375	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53841	i_freightregion_processed_check	0	0	53375	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53879	i_product_bom_isactive_check	0	0	54029	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53880	i_product_bom_i_isimported_check	0	0	54029	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53893	m_attributeinstance_isactive_check	0	0	561	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53989	m_pbatch_line_isactive_check	0	0	54066	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53990	m_pbatch_line_isendproduct_check	0	0	54066	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isendproduct = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54033	m_productionlinema_isactive_check	0	0	765	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54082	m_transactionallocation_isactive_check	0	0	636	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54083	m_transactionallocation_isallocated_check	0	0	636	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isallocated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54084	m_transactionallocation_ismanual_check	0	0	636	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((ismanual = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54254	t_distributionrundetail_isactive_check	0	0	714	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54257	t_invoicegl_isactive_check	0	0	803	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54258	t_minimrp_isactive_check	0	0	53868	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54259	t_mrp_run_capacity_isactive_check	0	0	54110	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54260	t_mrp_run_capacity_summary_isactive_check	0	0	54226	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54261	t_product_bomline_isactive_check	0	0	54222	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54262	t_reconciliation_isactive_check	0	0	53287	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54263	t_replenish_po_iscomplete_check	0	0	53949	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((iscomplete = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54264	t_replenish_po_isactive_check	0	0	53949	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54265	t_replenish_po_processed_check	0	0	53949	2022-11-02 12:40:39.392787	100	D	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
