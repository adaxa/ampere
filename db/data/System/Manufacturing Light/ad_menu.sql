copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
53296	Manufacturing Light	\N	0	\N	\N	0	\N	\N	2011-07-27 16:24:37	100	\N	Manufacturing Light	Y	Y	N	N	Y	2011-07-27 16:24:37	100
53297	Production Order Batch	W	0	\N	\N	0	\N	53127	2011-07-27 16:24:37	100	\N	Manufacturing Light	N	Y	N	Y	N	2016-04-12 09:51:54	0
53298	Parts and BOMs	W	0	\N	\N	0	\N	53128	2011-07-27 16:24:40	100	Maintain Bill of Materials	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:40	100
53299	Validate BOM Flags	P	0	\N	\N	0	53228	\N	2011-07-27 16:24:46	100	Validate BOM Flags	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:46	100
53300	Verify BOM Structure	P	0	\N	\N	0	53229	\N	2011-07-27 16:24:41	100	Verify BOM for correctness	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:41	100
53301	Rollup BOM Cost	P	0	\N	\N	0	53230	\N	2011-07-27 16:24:41	100	Rollup BOM cost	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:41	100
53302	BOM Viewer	X	0	53017	\N	0	\N	\N	2011-07-27 16:24:46	100	Shows the parent-component relationship for the product entered in the Product field.	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:46	100
53350	Quality Test	W	0	\N	\N	0	\N	53149	2011-07-27 16:24:38	100	\N	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:38	100
53351	Part Type	W	0	\N	\N	0	\N	53150	2011-07-27 16:24:39	100	Manufacturing Part Types	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:39	100
53352	Universal substitution	P	0	\N	\N	0	53265	\N	2011-07-27 16:24:42	100	Substitute one product for another in all BOMs	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:42	100
53353	Indented Bill of Material	R	0	\N	\N	0	53266	\N	2011-07-27 16:24:43	100	Indented BOM report	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:43	100
53354	Replenish Report incl. Production	R	0	\N	\N	0	53267	\N	2011-07-27 16:24:44	100	Inventory Replenish Report	Manufacturing Light	N	Y	N	N	N	2011-07-27 16:24:44	100
53478	Rollup BOM Future Cost	P	0	\N	\N	0	53348	\N	2013-01-03 21:57:42	100	Rollup BOM future cost	Manufacturing Light	N	Y	N	N	N	2013-01-03 21:57:42	100
53481	BOM Copy	P	0	\N	\N	0	53355	\N	2013-02-08 06:51:39	100	\N	Manufacturing Light	N	Y	N	N	N	2013-02-08 06:51:39	100
53883	Delete Production	P	0	\N	\N	0	53707	\N	2014-10-21 20:25:23	100	Delete open production orders	Manufacturing Light	N	N	N	N	N	2014-10-21 20:25:23	100
54041	Production Recording	W	0	\N	\N	0	\N	53466	2016-04-12 18:14:21	100	\N	Manufacturing Light	N	Y	N	N	N	2016-04-12 18:14:21	100
54049	Work Centre	W	0	\N	\N	0	\N	53469	2016-05-12 16:26:43	100	\N	Manufacturing Light	N	Y	N	N	N	2016-05-12 16:26:43	100
54099	Production Batch migration with older DB	P	0	\N	\N	0	53888	\N	2016-08-22 17:14:09	100	\N	Manufacturing Light	N	Y	N	N	N	2016-08-22 17:14:09	100
54123	Production Scanner	X	0	53069	\N	0	\N	\N	2016-11-25 12:11:39	100	Production Processing using Barcode Scanner	Manufacturing Light	N	Y	N	N	N	2016-11-25 12:11:39	100
54161	Production Test - Kit Pull	R	0	\N	\N	0	53946	\N	2017-04-18 12:43:24	100	\N	Manufacturing Light	N	Y	N	N	N	2017-04-18 12:43:24	100
\.
