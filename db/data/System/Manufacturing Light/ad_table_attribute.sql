copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
52187	m_parttype_key	0	0	53334	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (m_parttype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52319	m_product_qualitytest_key	0	0	53333	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (m_product_qualitytest_id)	CP	\N	2022-11-02 05:57:56.041854	100
52347	m_qualitytest_key	0	0	53332	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (m_qualitytest_id)	CP	\N	2022-11-02 05:57:56.041854	100
52439	m_workcentre_key	0	0	54094	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (m_workcentre_id)	CP	\N	2022-11-02 05:57:56.041854	100
52440	m_workcentretype_key	0	0	54095	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (m_workcentretype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52716	t_bom_indented_key	0	0	53335	2022-11-02 05:57:56.041854	100	Manufacturing Light	\N	Y	PRIMARY KEY (t_bom_indented_id)	CP	\N	2022-11-02 05:57:56.041854	100
53988	m_parttype_isactive_check	0	0	53334	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54041	m_product_qualitytest_isactive_check	0	0	53333	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54054	m_qualitytest_isactive_check	0	0	53332	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54091	m_workcentre_isactive_check	0	0	54094	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54092	m_workcentretype_isactive_check	0	0	54095	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54242	t_bom_indented_isactive_check	0	0	53335	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54243	t_bom_indented_ispurchased_check	0	0	53335	2022-11-02 12:40:39.392787	100	Manufacturing Light	\N	Y	CHECK ((ispurchased = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
