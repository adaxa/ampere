copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
51053	c_cycle_name	0	0	432	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
51054	c_cycle_pkey	0	0	432	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_cycle_id)	CP	\N	2022-11-02 05:57:56.041854	100
51055	ccurrency_ccycle	0	0	432	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51056	c_cyclephase_pkey	0	0	433	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_cyclestep_id, c_phase_id)	CP	\N	2022-11-02 05:57:56.041854	100
51057	cphase_ccyclephase	0	0	433	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_phase_id) REFERENCES c_phase(c_phase_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51058	ccyclestep_ccyclephase	0	0	433	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_cyclestep_id) REFERENCES c_cyclestep(c_cyclestep_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51059	c_cyclestep_pkey	0	0	590	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_cyclestep_id)	CP	\N	2022-11-02 05:57:56.041854	100
51060	ccycle_ccyclestep	0	0	590	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_cycle_id) REFERENCES c_cycle(c_cycle_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51383	c_phase_pkey	0	0	577	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_phase_id)	CP	\N	2022-11-02 05:57:56.041854	100
51384	cprojecttype_cphase	0	0	577	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_projecttype_id) REFERENCES c_projecttype(c_projecttype_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51385	mproduct_cphase	0	0	577	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51436	c_projecttype_pkey	0	0	575	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_projecttype_id)	CP	\N	2022-11-02 05:57:56.041854	100
51526	c_task_pkey	0	0	583	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	PRIMARY KEY (c_task_id)	CP	\N	2022-11-02 05:57:56.041854	100
51527	cphase_ctask	0	0	583	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (c_phase_id) REFERENCES c_phase(c_phase_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51528	mproduct_ctask	0	0	583	2022-11-02 05:57:56.041854	100	Project Management	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
53461	c_cycle_isactive_check	0	0	432	2022-11-02 12:40:39.392787	100	Project Management	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53462	c_cyclestep_isactive_check	0	0	590	2022-11-02 12:40:39.392787	100	Project Management	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
