copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
53255	A_Table_Rate_Type	L	0	0	2008-05-30 16:35:32	100	List that contains type of rate	Assets	\N	Y	N	2008-05-30 16:35:32	100	\N
53256	A_Depreciation_Manual_Period	L	0	0	2008-05-30 16:35:44	100	List available periods for calculating manual depreciation expense	Assets	\N	Y	N	2008-05-30 16:35:44	100	\N
53257	A_Entry_Type	L	0	0	2008-05-30 16:36:02	100	Entry Types for Fixed Assets	Assets	\N	Y	N	2008-05-30 16:36:02	100	\N
53259	A_Reval_Method_Type	L	0	0	2008-05-30 16:38:47	100	List of Revaluation Types	Assets	\N	Y	N	2008-05-30 16:38:47	100	\N
53260	A_Reval_Multiplier	L	0	0	2008-05-30 16:38:53	100	Revaluation Multiplier Type	Assets	\N	Y	N	2008-05-30 16:38:53	100	\N
53261	A_Reval_Effectivity	L	0	0	2008-05-30 16:39:12	100	Sets the date as to when the revaluation will be based on	Assets	\N	Y	N	2008-05-30 16:39:12	100	\N
53262	A_Reval_Code	L	0	0	2008-05-30 16:39:15	100	Lookup Valid Revaluation Codes	Assets	\N	Y	N	2008-05-30 16:39:15	100	\N
53263	A_Split_Type	L	0	0	2008-05-30 16:42:03	100	Split Type	Assets	\N	Y	N	2008-05-30 16:42:03	100	\N
53264	A_Depreciation_Type	T	0	0	2008-05-30 16:43:56	100	Depreciation Type selection	Assets	\N	Y	N	2008-05-30 16:43:56	100	\N
53265	A_Depreciation_Table_Codes	T	0	0	2008-05-30 16:43:59	100	List of depreciation table codes	Assets	\N	Y	N	2008-05-30 16:43:59	100	\N
53266	A_Depreciation_Calc_Type	T	0	0	2008-05-30 16:44:27	100	Depreciation Calculation Type selection	Assets	\N	Y	N	2008-05-30 16:44:27	100	\N
53267	A_ConventionType	T	0	0	2008-05-30 16:44:32	100	Display mid year convention types	Assets	\N	Y	N	2008-05-30 16:44:32	100	\N
53268	A_Asset_Spread	T	0	0	2008-05-30 16:44:38	100	Depreciation Type Spread	Assets	\N	Y	N	2008-05-30 16:44:38	100	\N
53269	A_Disposed_Reason	L	0	0	2008-05-30 16:45:40	100	Reason for Disposal	Assets	\N	Y	N	2008-05-30 16:45:40	100	\N
53270	A_Disposal_Method	L	0	0	2008-05-30 16:45:58	100	Method of Disposals	Assets	\N	Y	N	2008-05-30 16:45:58	100	\N
53273	A_Update_Type	L	0	0	2008-05-30 16:56:35	100	Update Type	Assets	\N	Y	N	2008-05-30 16:56:35	100	\N
53274	DocumentNo Invoice Vendor	T	0	0	2008-05-30 16:59:56	100	\N	Assets	\N	Y	N	2008-05-30 16:59:56	100	\N
53275	C_Invoice (Vendor)	T	0	0	2008-05-30 16:59:59	100	Vendor Invoices	Assets	\N	Y	N	2008-05-30 16:59:59	100	\N
53276	A_SourceType	L	0	0	2008-05-30 17:00:01	100	Entry Source Types	Assets	Listing of entry source types	Y	N	2008-05-30 17:00:01	100	\N
53278	C_ValidCombination (Assets)	T	0	0	2008-05-30 17:01:52	100	Valid Account combinations	Assets	\N	Y	N	2008-05-30 17:01:52	100	\N
\.
