copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50015	a_asset_acct_pkey	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_acct_id)	CP	\N	2022-11-02 05:57:56.041854	100
50016	adepreciationconv_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_conv_id) REFERENCES a_depreciation_convention(a_depreciation_convention_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50017	adepreciationmethod_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_method_id) REFERENCES a_depreciation_method(a_depreciation_method_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50018	cacctschema_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50019	adepreciation_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_id) REFERENCES a_depreciation(a_depreciation_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50020	aasset_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50021	adepreciationtableheader_aasse	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_table_header_id) REFERENCES a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50022	aassetspread_aassetacct	0	0	53123	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_spread_id) REFERENCES a_asset_spread(a_asset_spread_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50023	a_asset_addition_pkey	0	0	53137	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_addition_id)	CP	\N	2022-11-02 05:57:56.041854	100
50024	cinvoiceline_aassetaddition	0	0	53137	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_invoiceline_id) REFERENCES c_invoiceline(c_invoiceline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50025	cinvoice_aassetaddition	0	0	53137	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_invoice_id) REFERENCES c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50026	gljournalbatch_aassetaddition	0	0	53137	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (gl_journalbatch_id) REFERENCES gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50027	aasset_aassetaddition	0	0	53137	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50028	a_asset_change_pkey	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_change_id)	CP	\N	2022-11-02 05:57:56.041854	100
50029	cacctschema_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50030	adepreciationtableheader_aass2	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_table_header_id) REFERENCES a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50031	cbpartnerlocation_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_bpartner_location_id) REFERENCES c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50032	aparentasset_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_parent_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50033	clocation_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_location_id) REFERENCES c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50034	aaaddition_aachange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_addition_id) REFERENCES a_asset_addition(a_asset_addition_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50035	aaretirement_aachange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_retirement_id) REFERENCES a_asset_retirement(a_asset_retirement_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50036	aduser_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50037	aasset_aassetchange	0	0	53133	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50038	a_asset_delivery_pkey	0	0	541	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_delivery_id)	CP	\N	2022-11-02 05:57:56.041854	100
50039	moutline_aassetdelivery	0	0	541	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_inoutline_id) REFERENCES m_inoutline(m_inoutline_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50040	mproductdl_aassetdelivery	0	0	541	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_productdownload_id) REFERENCES m_productdownload(m_productdownload_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50041	aasset_aassetdelivery	0	0	541	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50042	aduser_aassetdelivery	0	0	541	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50043	a_asset_disposed_key	0	0	53127	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_disposed_id)	CP	\N	2022-11-02 05:57:56.041854	100
50044	cperiod_aassetdisposed	0	0	53127	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50045	aassettrade_aassetdisposed	0	0	53127	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_trade_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50047	a_asset_group_acct_key	0	0	53130	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_group_acct_id)	CP	\N	2022-11-02 05:57:56.041854	100
50048	adepreciationtableheader_aass3	0	0	53130	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_table_header_id) REFERENCES a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50049	adepreciation_aassetgroupacct	0	0	53130	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_id) REFERENCES a_depreciation(a_depreciation_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50050	cacctschema_aassetgroupacct	0	0	53130	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50051	aassetgroup_aassetgroupacct	0	0	53130	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_group_id) REFERENCES a_asset_group(a_asset_group_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50060	a_asset_reval_entry_key	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_reval_entry_id)	CP	\N	2022-11-02 05:57:56.041854	100
50061	cdoctype_aassetrevalentry	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_doctype_id) REFERENCES c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50062	glcategory_aassetrevalentry	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (gl_category_id) REFERENCES gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50063	ccurrency_aassetrevalentry	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50064	cacctschema_aassetrevalentry	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50065	cperiod_aassetrevalentry	0	0	53119	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50066	a_asset_reval_index_key	0	0	53120	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_reval_index_id)	CP	\N	2022-11-02 05:57:56.041854	100
50067	a_asset_split_key	0	0	53122	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_split_id)	CP	\N	2022-11-02 05:57:56.041854	100
50068	cperiod_aassetsplit	0	0	53122	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50069	a_asset_spread_key	0	0	53126	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_spread_id)	CP	\N	2022-11-02 05:57:56.041854	100
50070	a_asset_transfer_key	0	0	53128	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_transfer_id)	CP	\N	2022-11-02 05:57:56.041854	100
50071	cperiod_aassettransfer	0	0	53128	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50072	cacctschema_aassettransfer	0	0	53128	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50073	a_asset_use_pkey	0	0	53138	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_asset_use_id)	CP	\N	2022-11-02 05:57:56.041854	100
50074	aasset_aassetuse	0	0	53138	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50173	a_depreciation_pkey	0	0	53112	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_id)	CP	\N	2022-11-02 05:57:56.041854	100
50174	a_depreciation_build_key	0	0	53129	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_build_id)	CP	\N	2022-11-02 05:57:56.041854	100
50175	astartasset_adepreciationbuild	0	0	53129	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_start_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50176	aendasset_adepreciationbuild	0	0	53129	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_end_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50177	cperiod_adepreciationbuild	0	0	53129	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50178	a_depreciation_convention_key	0	0	53125	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_convention_id)	CP	\N	2022-11-02 05:57:56.041854	100
50179	a_depreciation_entry_key	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_entry_id)	CP	\N	2022-11-02 05:57:56.041854	100
50180	glcategory_adepreciationentry	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (gl_category_id) REFERENCES gl_category(gl_category_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50181	cperiod_adepreciationentry	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50182	cdoctype_adepreciationentry	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_doctype_id) REFERENCES c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50183	cacctschema_adepreciationentry	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50184	ccurrency_adepreciationentry	0	0	53121	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50185	a_depreciation_exp_key	0	0	53115	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_exp_id)	CP	\N	2022-11-02 05:57:56.041854	100
50189	a_depreciation_method_key	0	0	53124	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_method_id)	CP	\N	2022-11-02 05:57:56.041854	100
50190	a_depreciation_table_detai_key	0	0	53113	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_table_detail_id)	CP	\N	2022-11-02 05:57:56.041854	100
50191	a_depreciation_table_heade_key	0	0	53114	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_table_header_id)	CP	\N	2022-11-02 05:57:56.041854	100
50192	a_depreciation_workfile_key	0	0	53116	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_depreciation_workfile_id)	CP	\N	2022-11-02 05:57:56.041854	100
50680	a_registration_pkey	0	0	651	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_registration_id)	CP	\N	2022-11-02 05:57:56.041854	100
50681	cbpartner_aregistration	0	0	651	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50682	aasset_aregistration	0	0	651	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50683	mproduct_aregistration	0	0	651	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50684	aduser_aregistration	0	0	651	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50685	a_registrationattribute_pkey	0	0	652	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_registrationattribute_id)	CP	\N	2022-11-02 05:57:56.041854	100
50686	adreference_aregattribute	0	0	652	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (ad_reference_id) REFERENCES ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50687	adreferencevalue_aregattribute	0	0	652	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (ad_reference_value_id) REFERENCES ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50688	a_registrationproduct_pkey	0	0	715	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_registrationattribute_id, m_product_id)	CP	\N	2022-11-02 05:57:56.041854	100
50689	aregattribute_aregproduct	0	0	715	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_registrationattribute_id) REFERENCES a_registrationattribute(a_registrationattribute_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50690	mproduct_aregproduct	0	0	715	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50691	a_registrationvalue_pkey	0	0	653	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (a_registration_id, a_registrationattribute_id)	CP	\N	2022-11-02 05:57:56.041854	100
50692	aregattribute_aregvalue	0	0	653	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_registrationattribute_id) REFERENCES a_registrationattribute(a_registrationattribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50693	aregistration_aregvalue	0	0	653	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_registration_id) REFERENCES a_registration(a_registration_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51756	i_asset_key	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	PRIMARY KEY (i_asset_id)	CP	\N	2022-11-02 05:57:56.041854	100
51757	mlocator_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_locator_id) REFERENCES m_locator(m_locator_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51758	cacctschema_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51759	mproduct_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51760	adepreciationtableheader_iasse	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_depreciation_table_header_id) REFERENCES a_depreciation_table_header(a_depreciation_table_header_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51761	aassetgroup_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (a_asset_group_id) REFERENCES a_asset_group(a_asset_group_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51762	clocation_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_location_id) REFERENCES c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51763	cbpartnerlocation_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_bpartner_location_id) REFERENCES c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51764	cbpartner_iasset	0	0	53139	2022-11-02 05:57:56.041854	100	Assets	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52939	a_asset_acct_isactive_check	0	0	53123	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52940	a_asset_acct_processing_check	0	0	53123	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52941	a_asset_addition_isactive_check	0	0	53137	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52942	a_asset_change_isactive_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52943	a_asset_change_isdepreciated_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52944	a_asset_change_isdisposed_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdisposed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52945	a_asset_change_isfullydepreciated_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isfullydepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52946	a_asset_change_isinposession_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isinposession = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52947	a_asset_change_isowned_check	0	0	53133	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isowned = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52948	a_asset_delivery_isactive_check	0	0	541	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52949	a_asset_disposed_isactive_check	0	0	53127	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52950	a_asset_disposed_processed_check	0	0	53127	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52956	a_asset_group_acct_isactive_check	0	0	53130	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52957	a_asset_group_acct_processing_check	0	0	53130	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52966	a_asset_reval_entry_isactive_check	0	0	53119	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52967	a_asset_reval_entry_processed_check	0	0	53119	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52968	a_asset_reval_index_isactive_check	0	0	53120	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52969	a_asset_split_a_transfer_balance_is_check	0	0	53122	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((a_transfer_balance_is = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52970	a_asset_split_isactive_check	0	0	53122	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52971	a_asset_split_processed_check	0	0	53122	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52972	a_asset_spread_isactive_check	0	0	53126	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52973	a_asset_transfer_a_transfer_balance_check	0	0	53128	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((a_transfer_balance = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52974	a_asset_transfer_a_transfer_balance_is_check	0	0	53128	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((a_transfer_balance_is = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52975	a_asset_transfer_isactive_check	0	0	53128	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52976	a_asset_transfer_processed_check	0	0	53128	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52977	a_asset_use_isactive_check	0	0	53138	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53035	a_depreciation_isactive_check	0	0	53112	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53036	a_depreciation_build_processing_check	0	0	53129	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53037	a_depreciation_build_isactive_check	0	0	53129	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53038	a_depreciation_convention_isactive_check	0	0	53125	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53039	a_depreciation_entry_isactive_check	0	0	53121	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53040	a_depreciation_exp_isdepreciated_check	0	0	53115	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53041	a_depreciation_exp_isactive_check	0	0	53115	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53042	a_depreciation_exp_processed_check	0	0	53115	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53045	a_depreciation_method_isactive_check	0	0	53124	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53046	a_depreciation_table_detail_isactive_check	0	0	53113	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53047	a_depreciation_table_detail_processed_check	0	0	53113	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53048	a_depreciation_table_header_processed_check	0	0	53114	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53049	a_depreciation_table_header_isactive_check	0	0	53114	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53050	a_depreciation_workfile_processing_check	0	0	53116	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53051	a_depreciation_workfile_isactive_check	0	0	53116	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53052	a_depreciation_workfile_isdepreciated_check	0	0	53116	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53327	a_registration_isallowpublish_check	0	0	651	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isallowpublish = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53328	a_registration_isactive_check	0	0	651	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53329	a_registration_isregistered_check	0	0	651	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isregistered = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53330	a_registration_isinproduction_check	0	0	651	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isinproduction = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53331	a_registrationattribute_isactive_check	0	0	652	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53332	a_registrationproduct_isactive_check	0	0	715	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53333	a_registrationvalue_isactive_check	0	0	653	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53790	i_asset_isinposession_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isinposession = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53791	i_asset_isfullydepreciated_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isfullydepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53792	i_asset_isdisposed_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdisposed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53793	i_asset_isdepreciated_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isdepreciated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53794	i_asset_processed_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53795	i_asset_i_isimported_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((i_isimported = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53796	i_asset_isowned_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isowned = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53797	i_asset_isactive_check	0	0	53139	2022-11-02 12:40:39.392787	100	Assets	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
