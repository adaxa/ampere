copy "ad_chart" ("ad_chart_id", "name", "ad_client_id", "ad_org_id", "chartbetweenbar", "chartbetweencategory", "chartbgcolor", "chartgridcolor", "chartleftmargin", "chartorientation", "chartrightmargin", "charttype", "created", "createdby", "description", "domainlabel", "isactive", "isdisplaylegend", "istimeseries", "rangelabel", "timescope", "timeunit", "updated", "updatedby", "winheight") from STDIN;
50000	My Sales Pipeline	0	0	\N	\N	\N	\N	\N	V	\N	BS	2011-09-02 12:16:26	100	\N	Sales Stage	Y	Y	N	Value	0	\N	2012-06-08 15:16:37	0	300
50001	My Opportunities by Campaign	0	0	\N	\N	\N	\N	\N	H	\N	P3	2011-09-02 17:01:39	100	\N	Opportunity	Y	Y	N	Campaign	0	\N	2012-06-08 15:16:37	0	300
50002	Sales YTD	0	0	\N	\N	\N	\N	\N	V	\N	LC	2011-11-03 18:13:39	100	\N	Month	Y	N	Y	Value	12	M	2012-06-08 15:16:37	0	300
50003	My Open Requests	0	0	\N	\N	\N	\N	\N	V	\N	BS	2011-11-04 11:29:41	100	\N	\N	Y	Y	N	\N	0	\N	2011-11-04 11:29:41	100	300
50004	Work Centre Capacity	0	0	\N	\N	\N	\N	\N	V	\N	BS	2016-05-16 16:55:03	100	\N	Week	Y	Y	N	Name	12	W	2019-02-04 13:39:28	0	350
50005	Work Centre Cumulated Capacity	0	0	\N	\N	\N	\N	\N	V	\N	BS	2017-03-15 16:47:44	100	\N	Week	Y	Y	N	Name	12	W	2019-02-04 13:39:28	0	350
\.
