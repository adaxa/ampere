copy "ad_entitytype" ("ad_entitytype_id", "name", "ad_client_id", "ad_org_id", "classpath", "created", "createdby", "description", "entitytype", "help", "isactive", "modelpackage", "processing", "updated", "updatedby", "version") from STDIN;
10	Dictionary	0	0	\N	2006-06-11 12:11:19	100	Application Dictionary Ownership ** System Maintained **	D	The entity is owned by the Application Dictionary. You should NOT use Disctionary as all modifications are likely to be reversed, to maintain customizations, copy the record. (Change Log & Reapply Customization allows you to maintain minor modifications)	Y	\N	N	2006-06-11 12:12:04	100	\N
100	User maintained	0	0	\N	2006-06-11 12:16:15	100	User maintained modifications	U	The default entity type for your Customizations and custom Extensions.  Will be preserved during version migration.	Y	\N	N	2006-06-11 12:16:15	100	\N
50058	System Admin	0	0	\N	2019-11-25 15:43:37	100	\N	System Admin	\N	Y	\N	N	2019-11-25 15:43:37	100	\N
50059	Application Dictionary	0	0	\N	2019-11-25 15:43:48	100	\N	Application Dictionary	\N	Y	\N	N	2019-11-25 15:43:48	100	\N
50060	Partner Relations	0	0	\N	2019-11-25 15:43:58	100	\N	Partner Relations	\N	Y	\N	N	2019-11-25 15:43:58	100	\N
50061	Quote-to-Invoice	0	0	\N	2019-11-25 15:44:07	100	\N	Quote-to-Invoice	\N	Y	\N	N	2019-11-25 15:44:07	100	\N
50062	Requisition-to-Invoice	0	0	\N	2019-11-25 15:44:16	100	\N	Requisition-to-Invoice	\N	Y	\N	N	2019-11-25 15:44:16	100	\N
50063	Returns	0	0	\N	2019-11-25 15:44:28	100	\N	Returns	\N	Y	\N	N	2019-11-25 15:44:28	100	\N
50064	Open Items	0	0	\N	2019-11-25 15:44:36	100	\N	Open Items	\N	Y	\N	N	2019-11-25 15:44:36	100	\N
50065	Material Management	0	0	\N	2019-11-25 15:44:43	100	\N	Material Management	\N	Y	\N	N	2019-11-25 15:44:43	100	\N
50066	Sales Management	0	0	\N	2019-11-25 15:44:51	100	\N	Sales Management	\N	Y	\N	N	2019-11-25 15:44:51	100	\N
50067	Performance Analysis	0	0	\N	2019-11-25 15:44:58	100	\N	Performance Analysis	\N	Y	\N	N	2019-11-25 15:44:58	100	\N
50068	Project Management	0	0	\N	2019-11-25 15:45:07	100	\N	Project Management	\N	Y	\N	N	2019-11-25 15:45:07	100	\N
50069	Assets	0	0	\N	2019-11-25 15:45:14	100	\N	Assets	\N	Y	\N	N	2019-11-25 15:45:14	100	\N
50070	Asset Management	0	0	\N	2019-11-25 15:45:20	100	\N	Asset Management	\N	Y	\N	N	2019-11-25 15:45:20	100	\N
50071	Manufacturing Management	0	0	\N	2019-11-25 15:45:27	100	\N	Manufacturing Management	\N	Y	\N	N	2019-11-25 15:45:27	100	\N
50072	Human Resource & Payroll	0	0	\N	2019-11-25 15:45:34	100	\N	Human Resource & Payroll	\N	Y	\N	N	2019-11-25 15:45:34	100	\N
50073	Collaboration	0	0	\N	2019-11-25 15:45:51	100	\N	Collaboration	\N	Y	\N	N	2019-11-25 15:45:51	100	\N
50074	Knowledge Base	0	0	\N	2019-11-25 15:45:59	100	\N	Knowledge Base	\N	Y	\N	N	2019-11-25 15:45:59	100	\N
50075	Replication Data	0	0	\N	2019-11-25 15:46:06	100	\N	Replication Data	\N	Y	\N	N	2019-11-25 15:46:06	100	\N
50076	EDI	0	0	\N	2019-11-25 15:46:14	100	\N	EDI	\N	Y	\N	N	2019-11-25 15:46:14	100	\N
50077	Web	0	0	\N	2019-11-25 15:46:21	100	\N	Web	\N	Y	\N	N	2019-11-25 15:46:21	100	\N
50078	Sales and Marketing	0	0	\N	2019-11-25 15:46:28	100	\N	Sales and Marketing	\N	Y	\N	N	2019-11-25 15:46:28	100	\N
50079	Web POS	0	0	\N	2019-11-25 15:46:35	100	\N	Web POS	\N	Y	\N	N	2019-11-25 15:46:35	100	\N
50080	Market Place	0	0	\N	2019-11-25 15:46:47	100	\N	Market Place	\N	Y	\N	N	2019-11-25 15:46:47	100	\N
50081	Point of Sales	0	0	\N	2019-11-25 15:46:54	100	\N	Point of Sales	\N	Y	\N	N	2019-11-25 15:46:54	100	\N
50082	Application Packaging	0	0	\N	2019-11-25 22:42:19	100	\N	Application Packaging	\N	Y	\N	N	2019-11-25 22:42:19	100	\N
50083	Request	0	0	\N	2019-11-25 22:42:44	100	\N	Request	\N	Y	\N	N	2019-11-25 22:42:44	100	\N
50084	Service	0	0	\N	2019-11-25 22:43:17	100	\N	Service	\N	Y	\N	N	2019-11-25 22:43:17	100	\N
50085	Product Attributes	0	0	\N	2019-11-25 22:44:06	100	\N	Product Attributes	\N	Y	\N	N	2019-11-25 22:44:06	100	\N
50086	Manufacturing Processes	0	0	\N	2019-11-25 22:44:40	100	\N	Manufacturing Processes	\N	Y	\N	N	2019-11-25 22:44:40	100	\N
50087	Manufacturing Light	0	0	\N	2019-11-25 22:45:01	100	\N	Manufacturing Light	\N	Y	\N	N	2019-11-25 22:45:01	100	\N
50088	Accounting Rules	0	0	\N	2019-11-25 22:45:38	100	\N	Accounting Rules	\N	Y	\N	N	2019-11-25 22:45:38	100	\N
50089	Financial Reporting	0	0	\N	2019-11-25 22:46:02	100	\N	Financial Reporting	\N	Y	\N	N	2019-11-25 22:46:02	100	\N
50090	Performance Measurement	0	0	\N	2019-11-25 22:46:20	100	\N	Performance Measurement	\N	Y	\N	N	2019-11-25 22:46:20	100	\N
50091	Costing	0	0	\N	2019-11-25 22:46:39	100	\N	Costing	\N	Y	\N	N	2019-11-25 22:46:39	100	\N
50093	Manual	0	0	\N	2019-11-29 22:51:59	100	\N	Manual	\N	Y	\N	N	2019-11-29 22:51:59	100	\N
50094	Data Import	0	0	\N	2019-11-29 23:52:02	100	\N	Data Import	\N	Y	\N	N	2019-11-29 23:52:02	100	\N
50095	Deprecated	0	0	\N	2019-12-05 15:51:04	100	\N	Deprecated	\N	Y	\N	N	2019-12-05 15:51:04	100	\N
50096	Workflow	0	0	\N	2019-11-29 01:14:12	100	\N	Workflow	\N	Y	\N	N	2019-11-29 01:14:12	100	\N
50097	Server	0	0	\N	2019-11-29 01:14:38	100	\N	Server	\N	Y	\N	N	2019-11-29 01:14:38	100	\N
50098	Material Management Rules	0	0	\N	2019-12-01 00:20:24	100	\N	Material Management Rules	\N	Y	\N	N	2019-12-01 00:20:24	100	\N
\.
