copy "ad_reference" ("ad_reference_id", "name", "validationtype", "ad_client_id", "ad_org_id", "created", "createdby", "description", "entitytype", "help", "isactive", "isorderbyvalue", "updated", "updatedby", "vformat") from STDIN;
10	String	D	0	0	1999-05-21 00:00:00	0	Character String	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
11	Integer	D	0	0	1999-05-21 00:00:00	0	10 Digit numeric	Manual	\N	Y	N	2019-02-04 13:38:36	0	\N
12	Amount	D	0	0	1999-05-21 00:00:00	0	Number with 4 decimals	Manual	\N	Y	N	2019-02-04 13:38:33	0	\N
13	ID	D	0	0	1999-05-21 00:00:00	0	10 Digit Identifier	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
14	Text	D	0	0	1999-05-21 00:00:00	0	Character String up to 2000 characters	Manual	\N	Y	N	2019-02-04 13:38:37	0	\N
15	Date	D	0	0	1999-05-21 00:00:00	0	Date mm/dd/yyyy	Manual	\N	Y	N	2019-02-04 13:38:33	0	\N
16	Date+Time	D	0	0	1999-05-21 00:00:00	0	Date with time	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
17	List	D	0	0	1999-05-21 00:00:00	0	Reference List	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
18	Table	D	0	0	1999-05-21 00:00:00	0	Table List	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
19	Table Direct	D	0	0	1999-05-21 00:00:00	0	Direct Table Access	Manual	\N	Y	N	2019-02-04 13:38:33	0	\N
20	Yes-No	D	0	0	1999-05-21 00:00:00	0	CheckBox	Manual	\N	Y	N	2019-02-04 13:38:33	0	\N
21	Location (Address)	D	0	0	1999-05-24 00:00:00	0	Location/Address	Manual	\N	Y	N	2008-03-23 20:51:40	100	\N
22	Number	D	0	0	1999-06-21 00:00:00	0	Float Number	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
23	Binary	D	0	0	1999-06-29 00:00:00	0	Binary Data	Manual	\N	Y	N	2008-03-23 20:45:38	100	\N
24	Time	D	0	0	1999-06-29 00:00:00	0	Time	Manual	\N	Y	N	2007-12-17 01:44:49	0	\N
25	Account	D	0	0	1999-07-10 00:00:00	0	Account Element	Manual	\N	Y	N	2016-04-11 16:32:42	0	\N
26	RowID	D	0	0	1999-07-10 00:00:00	0	Row ID Data Type	Manual	\N	N	N	2000-01-02 00:00:00	0	\N
27	Color	D	0	0	1999-09-09 00:00:00	0	Color element	Manual	\N	Y	N	2000-01-02 00:00:00	0	\N
28	Button	D	0	0	1999-09-09 00:00:00	0	Command Button - starts a process	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
29	Quantity	D	0	0	1999-12-13 10:31:53	0	Quantity data type	Manual	\N	Y	N	2016-04-11 16:47:03	0	\N
30	Search	D	0	0	2000-01-16 16:25:58	0	Search Field	Manual	\N	Y	N	2019-02-04 13:38:35	0	\N
31	Locator (WH)	D	0	0	2000-10-12 15:42:51	0	Warehouse Locator Data type	Manual	\N	Y	N	2016-04-11 16:47:13	0	\N
32	Image	D	0	0	2001-09-13 22:53:23	0	Binary Image Data	Manual	\N	Y	N	2000-01-02 00:00:00	0	\N
33	Assignment	D	0	0	2002-06-20 04:39:03	0	Resource Assignment	Manual	\N	Y	N	2000-01-02 00:00:00	0	\N
34	Memo	D	0	0	2003-04-29 22:49:53	0	Large Text Editor - Character String up to 2000 characters	Manual	\N	Y	N	2016-04-11 16:33:59	0	\N
35	Product Attribute	D	0	0	2003-06-13 00:31:04	0	Product Attribute	Manual	\N	Y	N	2016-04-11 16:47:13	0	\N
36	Text Long	D	0	0	2003-10-07 22:31:42	0	Text (Long) - Text > 2000 characters	Manual	\N	Y	N	2007-12-16 22:27:44	0	\N
37	Costs+Prices	D	0	0	2004-07-21 01:33:15	0	Costs + Prices (minimum currency precision but if exists more)	Manual	\N	Y	N	2016-04-11 16:47:08	0	\N
38	FilePath	D	0	0	2006-02-14 13:52:31	100	Local File Path	Manual	\N	Y	N	2006-02-14 13:53:28	100	\N
39	FileName	D	0	0	2006-02-14 13:53:22	100	Local File	Manual	\N	Y	N	2006-02-14 13:53:22	100	\N
40	URL	D	0	0	2006-02-14 13:53:55	100	URL	Manual	\N	Y	N	2016-04-11 16:47:12	0	\N
42	Printer Name	D	0	0	2006-02-14 14:12:23	100	\N	Manual	\N	Y	N	2006-02-14 14:12:23	100	\N
99	JSON	D	0	0	2021-09-06 17:27:12	100	JSON object	Manual	\N	Y	N	2021-09-06 17:27:12	100	\N
53370	Chart	D	0	0	2010-08-23 15:00:59	100	Chart	Manual	A chart provides a graphical display of information.	Y	N	2010-08-23 15:00:59	100	\N
\.
