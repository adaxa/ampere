copy "ad_form" ("ad_form_id", "name", "accesslevel", "ad_client_id", "ad_org_id", "classname", "created", "createdby", "description", "entitytype", "help", "isactive", "isbetafunctionality", "jspurl", "updated", "updatedby") from STDIN;
104	Payment Allocation	1	0	0	org.compiere.apps.form.VAllocation	2001-01-09 16:47:06	0	Allocate invoices and payments	Open Items	\N	Y	N	\N	2000-01-02 00:00:00	0
106	Payment Print/Export	1	0	0	org.compiere.apps.form.VPayPrint	2001-07-26 18:04:58	0	Print or export your payments	Open Items	\N	Y	N	\N	2000-01-02 00:00:00	0
107	Payment Selection (manual)	1	0	0	org.compiere.apps.form.VPaySelect	2001-07-26 18:27:19	0	Manual Payment Selection	Open Items	Select vendor invoices for payment. If you don't see an invoice, make sure that it is not included in a different (nprocessed) Payment Selection.	Y	N	\N	2005-02-24 11:08:12	100
\.
