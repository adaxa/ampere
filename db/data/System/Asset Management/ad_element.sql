copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1931	AssetDepreciationDate	0	0	15	\N	2003-01-23 00:10:32	0	Date of last depreciation	Asset Management	7	Date of the last deprecation, if the asset is used internally and depreciated.	Y	Asset Depreciation Date	\N	\N	\N	\N	Asset depreciation date	2016-04-11 16:36:02	0
1932	AssetDisposalDate	0	0	15	\N	2003-01-23 00:10:32	0	Date when the asset is/was disposed	Asset Management	7	\N	Y	Asset Disposal Date	\N	\N	\N	\N	Asset disposal date	2016-04-11 16:36:02	0
1934	AssetServiceDate	0	0	15	\N	2003-01-23 00:10:32	0	Date when Asset was put into service	Asset Management	7	The date when the asset was put into service - usually used as start date for depreciation.	Y	In Service Date	\N	\N	\N	\N	In Service date	2016-04-11 16:36:02	0
1938	IsDepreciated	0	0	20	\N	2003-01-23 00:10:32	0	The asset will be depreciated	Asset Management	1	The asset is used internally and will be depreciated	Y	Depreciable Asset	\N	\N	\N	\N	Depreciable Asset	2016-04-11 16:36:04	0
1939	IsDisposed	0	0	20	\N	2003-01-23 00:10:32	0	The asset is disposed	Asset Management	1	The asset is no longer used and disposed	Y	Disposed	\N	\N	\N	\N	Disposed	2016-04-11 16:36:04	0
1940	IsInPosession	0	0	20	\N	2003-01-23 00:10:32	0	The asset is in the possession of the organization	Asset Management	1	Assets which are not in possession are e.g. at Customer site and may or may not be owned by the company.	Y	In Possession	\N	\N	\N	\N	In Possession	2016-04-11 16:36:04	0
1941	IsOwned	0	0	20	\N	2003-01-23 00:10:32	0	The asset is owned by the organization	Asset Management	1	The asset may not be in possession, but the asset is legally owned by the organization	Y	Owned	\N	\N	\N	\N	Owned	2016-04-11 16:36:04	0
1942	LifeUseUnits	0	0	11	\N	2003-01-23 00:10:32	0	Units of use until the asset is not usable anymore	Asset Management	22	Life use and the actual use may be used to calculate the depreciation	Y	Life use	\N	\N	\N	\N	Life use	2016-04-11 16:36:05	0
1943	LocationComment	0	0	10	\N	2003-01-23 00:10:32	0	Additional comments or remarks concerning the location	Asset Management	255	\N	Y	Location comment	\N	\N	\N	\N	Location comment	2016-04-11 16:36:05	0
1946	UseLifeMonths	0	0	11	\N	2003-01-23 00:10:32	0	Months of the usable life of the asset	Asset Management	22	\N	Y	Usable Life - Months	\N	\N	\N	\N	Usable life months	2016-04-11 16:36:06	0
1947	UseLifeYears	0	0	11	\N	2003-01-23 00:10:32	0	Years of the usable life of the asset	Asset Management	22	\N	Y	Usable Life - Years	\N	\N	\N	\N	Usable life years	2016-04-11 16:36:06	0
1948	UseUnits	0	0	11	\N	2003-01-23 00:10:32	0	Currently used units of the assets	Asset Management	22	\N	Y	Use units	\N	\N	\N	\N	Use units	2016-04-11 16:36:07	0
1951	IsFullyDepreciated	0	0	20	\N	2003-01-29 20:43:53	0	The asset is fully depreciated	Asset Management	1	The asset costs are fully amortized.	Y	Fully depreciated	\N	\N	\N	\N	Fully depreciated	2016-04-11 16:36:04	0
2727	IsOneAssetPerUOM	0	0	20	\N	2005-04-30 01:14:47	100	Create one asset per UOM	Asset Management	1	If selected, one asset per UOM is created, otherwise one asset with the quantity received/shipped.  If you have multiple lines, one asset is created per line.	Y	One Asset Per UOM	\N	\N	\N	\N	One Asset Per UOM	2016-04-11 16:32:37	0
2754	IsCreateAsActive	0	0	20	\N	2005-05-11 00:14:11	100	Create Asset and activate it	Asset Management	1	You may want to consider not to automatically make the asset active if you need to get some additional information	Y	Create As Active	\N	\N	\N	\N	Create Active	2016-04-11 16:32:37	0
2931	LastMaintenanceDate	0	0	16	\N	2005-12-29 21:45:24	100	Last Maintenance Date	Asset Management	7	\N	Y	Last Maintenance	\N	\N	\N	\N	Last Maintenance	2016-04-11 16:36:04	0
2932	NextMaintenenceDate	0	0	16	\N	2005-12-29 21:45:24	100	Next Maintenance Date	Asset Management	7	\N	Y	Next Maintenance	\N	\N	\N	\N	Next Maintenance	2016-04-11 16:36:05	0
2933	LastMaintenanceUnit	0	0	11	\N	2005-12-29 21:45:25	100	Last Maintenance Unit	Asset Management	10	\N	Y	Last Unit	\N	\N	\N	\N	Last Unit	2016-04-11 16:36:04	0
2934	NextMaintenenceUnit	0	0	11	\N	2005-12-29 21:45:25	100	Next Maintenance Unit	Asset Management	10	\N	Y	Next Unit	\N	\N	\N	\N	Next Unit	2016-04-11 16:36:05	0
2935	LeaseTerminationDate	0	0	15	\N	2005-12-29 21:45:25	100	Lease Termination Date	Asset Management	7	Last Date of Lease	Y	Lease Termination	\N	\N	\N	\N	Lease Termination	2016-04-11 16:36:05	0
2936	Lease_BPartner_ID	0	0	30	192	2005-12-29 21:45:25	100	The Business Partner who rents or leases	Asset Management	10	\N	Y	Lessor	\N	\N	\N	\N	Lessor	2016-04-11 16:36:05	0
2937	LastMaintenanceNote	0	0	10	\N	2005-12-29 22:02:10	100	Last Maintenance Note	Asset Management	60	\N	Y	Last Note	\N	\N	\N	\N	Last Note	2016-04-11 16:36:04	0
2956	IsTrackIssues	0	0	20	\N	2005-12-31 20:53:57	100	Enable tracking issues for this asset	Asset Management	1	Issues created by automatic Error Reporting	Y	Track Issues	\N	\N	\N	\N	Track Issues	2016-04-11 16:32:37	0
53473	A_Depreciation_ID	0	0	13	\N	2008-05-30 16:35:08	100	\N	Asset Management	22	\N	Y	Depreciation Type	\N	\N	\N	\N	Depreciation Type	2016-04-11 16:32:51	0
53494	A_QTY_Current	0	0	22	\N	2008-05-30 16:36:46	100	\N	Asset Management	22	\N	Y	Quantity	\N	\N	\N	\N	Quantity	2016-04-11 16:36:02	0
53498	A_Base_Amount	0	0	12	\N	2008-05-30 16:36:56	100	\N	Asset Management	22	\N	Y	Base Amount	\N	\N	\N	\N	Base Amount	2016-04-11 16:33:02	0
53526	A_Asset_CreateDate	0	0	15	\N	2008-05-30 16:42:52	100	\N	Asset Management	7	\N	Y	Asset Creation Date	\N	\N	\N	\N	Asset Creation Date	2016-04-11 16:36:01	0
53527	A_Asset_RevalDate	0	0	15	\N	2008-05-30 16:42:53	100	\N	Asset Management	7	\N	Y	Asset Reval. Date	\N	\N	\N	\N	Asset Reval. Date	2016-04-11 16:36:01	0
53528	A_Parent_Asset_ID	0	0	30	53258	2008-05-30 16:42:55	100	\N	Asset Management	22	\N	Y	Asset ID	\N	\N	\N	\N	Asset ID	2016-04-11 16:36:02	0
53529	A_QTY_Original	0	0	22	\N	2008-05-30 16:42:59	100	\N	Asset Management	22	\N	Y	Original Qty	\N	\N	\N	\N	Original Qty	2016-04-11 16:36:02	0
53589	A_Asset_Info_Tax_ID	0	0	13	\N	2008-05-30 16:54:24	100	\N	Asset Management	22	\N	Y	Asset Info Tax	\N	\N	\N	\N	Asset Info Tax	2016-04-11 16:33:54	0
53590	A_Investment_CR	0	0	11	\N	2008-05-30 16:54:29	100	\N	Asset Management	22	\N	Y	Investment Credit	\N	\N	\N	\N	Investment Credit	2016-04-11 16:33:55	0
53591	A_Tax_Entity	0	0	10	\N	2008-05-30 16:54:35	100	\N	Asset Management	22	\N	Y	Tax Entity	\N	\N	\N	\N	Tax Entity	2016-04-11 16:33:55	0
53592	A_New_Used	0	0	20	\N	2008-05-30 16:54:36	100	\N	Asset Management	1	\N	Y	Purchased New?	\N	\N	\N	\N	Purchased New?	2016-04-11 16:33:55	0
53593	A_Finance_Meth	0	0	17	53271	2008-05-30 16:54:41	100	\N	Asset Management	2	\N	Y	Finance Method	\N	\N	\N	\N	Finance Method	2016-04-11 16:33:44	0
53594	A_Asset_Info_Fin_ID	0	0	13	\N	2008-05-30 16:54:54	100	\N	Asset Management	22	\N	Y	Asset Info Fin.	\N	\N	\N	\N	Asset Info Fin.	2016-04-11 16:33:44	0
53595	A_Due_On	0	0	17	53272	2008-05-30 16:55:01	100	\N	Asset Management	22	\N	Y	Payment Due Date	\N	\N	\N	\N	Payment Due Date	2016-04-11 16:33:44	0
53596	A_Purchase_Option	0	0	20	\N	2008-05-30 16:55:03	100	\N	Asset Management	2	\N	Y	Purchase Option	\N	\N	\N	\N	Purchase Option	2016-04-11 16:33:45	0
53597	A_Purchase_Option_Credit_Per	0	0	22	\N	2008-05-30 16:55:04	100	\N	Asset Management	22	\N	Y	Purchase Option Credit %	\N	\N	\N	\N	Purchase Option Credit %	2016-04-11 16:33:45	0
53598	A_Purchase_Price	0	0	12	\N	2008-05-30 16:55:12	100	\N	Asset Management	22	\N	Y	Option Purchase Price	\N	\N	\N	\N	Option Purchase Price	2016-04-11 16:33:45	0
53599	A_Purchase_Option_Credit	0	0	11	\N	2008-05-30 16:55:13	100	\N	Asset Management	22	\N	Y	Purchase Option Credit	\N	\N	\N	\N	Purchase Option Credit	2016-04-11 16:33:46	0
53600	A_Monthly_Payment	0	0	12	\N	2008-05-30 16:55:14	100	\N	Asset Management	22	\N	Y	Monthly Payment	\N	\N	\N	\N	Monthly Payment	2016-04-11 16:33:46	0
53601	A_Expired_Date	0	0	15	\N	2008-05-30 16:55:15	100	\N	Asset Management	7	\N	Y	Contract Expiration Date	\N	\N	\N	\N	Contract Expiration Date	2016-04-11 16:33:46	0
53602	A_Contract_Date	0	0	15	\N	2008-05-30 16:55:17	100	\N	Asset Management	7	\N	Y	Contract Date	\N	\N	\N	\N	Contract Date	2016-04-11 16:33:46	0
53613	A_Asset_Info_Lic_ID	0	0	13	\N	2008-05-30 16:57:52	100	\N	Asset Management	22	\N	Y	Asset Info Lic.	\N	\N	\N	\N	Asset Info Lic.	2016-04-11 16:33:49	0
53614	A_License_Fee	0	0	12	\N	2008-05-30 16:57:55	100	\N	Asset Management	22	\N	Y	License Fee	\N	\N	\N	\N	License Fee	2016-04-11 16:33:50	0
53615	A_Renewal_Date	0	0	15	\N	2008-05-30 16:57:56	100	\N	Asset Management	7	\N	Y	Policy Renewal Date	\N	\N	\N	\N	Policy Renewal Date	2016-04-11 16:33:50	0
53616	A_License_No	0	0	10	\N	2008-05-30 16:58:03	100	\N	Asset Management	120	\N	Y	License No	\N	\N	\N	\N	License No	2016-04-11 16:33:50	0
53617	A_Issuing_Agency	0	0	10	\N	2008-05-30 16:58:04	100	\N	Asset Management	22	\N	Y	Issuing Agency	\N	\N	\N	\N	Issuing Agency	2016-04-11 16:33:50	0
53618	A_Asset_Info_Ins_ID	0	0	13	\N	2008-05-30 16:58:17	100	\N	Asset Management	22	\N	Y	Asset Info Ins.	\N	\N	\N	\N	Asset Info Ins.	2016-04-11 16:33:52	0
53619	A_Ins_Value	0	0	12	\N	2008-05-30 16:58:21	100	\N	Asset Management	22	\N	Y	Insured Value	\N	\N	\N	\N	Insured Value	2016-04-11 16:36:09	0
53620	A_Policy_No	0	0	10	\N	2008-05-30 16:58:22	100	\N	Asset Management	100	\N	Y	Policy Number	\N	\N	\N	\N	Policy Number	2016-04-11 16:33:52	0
53621	A_Replace_Cost	0	0	12	\N	2008-05-30 16:58:24	100	\N	Asset Management	22	\N	Y	Replacement Costs	\N	\N	\N	\N	Replacement Costs	2016-04-11 16:33:52	0
53622	A_Insurance_Co	0	0	10	\N	2008-05-30 16:58:31	100	\N	Asset Management	22	\N	Y	Insurance Company	\N	\N	\N	\N	Insurance Company	2016-04-11 16:33:53	0
53623	A_Ins_Premium	0	0	12	\N	2008-05-30 16:58:33	100	\N	Asset Management	22	\N	Y	Insurance Premium	\N	\N	\N	\N	Insurance Premium	2016-04-11 16:33:53	0
53624	A_Asset_Info_Oth_ID	0	0	13	\N	2008-05-30 16:58:46	100	\N	Asset Management	22	\N	Y	Asset Info Oth.	\N	\N	\N	\N	Asset Info Oth.	2016-04-11 16:33:57	0
53625	A_User1	0	0	10	\N	2008-05-30 16:58:47	100	\N	Asset Management	3	\N	Y	User 1	\N	\N	\N	\N	User 1	2016-04-11 16:33:57	0
53626	A_User10	0	0	10	\N	2008-05-30 16:58:48	100	\N	Asset Management	3	\N	Y	User 10	\N	\N	\N	\N	User 10	2016-04-11 16:33:57	0
53627	A_User11	0	0	10	\N	2008-05-30 16:58:51	100	\N	Asset Management	10	\N	Y	User 11	\N	\N	\N	\N	User 11	2016-04-11 16:33:57	0
53628	A_User12	0	0	10	\N	2008-05-30 16:58:52	100	\N	Asset Management	10	\N	Y	User 12	\N	\N	\N	\N	User 12	2016-04-11 16:33:57	0
53629	A_User13	0	0	10	\N	2008-05-30 16:58:53	100	\N	Asset Management	10	\N	Y	User 13	\N	\N	\N	\N	User 13	2016-04-11 16:33:57	0
53630	A_User14	0	0	10	\N	2008-05-30 16:58:55	100	\N	Asset Management	10	\N	Y	User 14	\N	\N	\N	\N	User 14	2016-04-11 16:33:58	0
53631	A_User15	0	0	10	\N	2008-05-30 16:58:57	100	\N	Asset Management	10	\N	Y	User 15	\N	\N	\N	\N	User 15	2016-04-11 16:33:58	0
53632	A_User2	0	0	10	\N	2008-05-30 16:58:59	100	\N	Asset Management	3	\N	Y	User 2	\N	\N	\N	\N	User 2	2016-04-11 16:33:58	0
53633	A_User3	0	0	10	\N	2008-05-30 16:59:00	100	\N	Asset Management	3	\N	Y	User 3	\N	\N	\N	\N	User 3	2016-04-11 16:33:58	0
53634	A_User4	0	0	10	\N	2008-05-30 16:59:01	100	\N	Asset Management	3	\N	Y	User 4	\N	\N	\N	\N	User 4	2016-04-11 16:33:58	0
53635	A_User5	0	0	10	\N	2008-05-30 16:59:03	100	\N	Asset Management	3	\N	Y	User 5	\N	\N	\N	\N	User 5	2016-04-11 16:33:58	0
53636	A_User6	0	0	10	\N	2008-05-30 16:59:04	100	\N	Asset Management	3	\N	Y	User 6	\N	\N	\N	\N	User 6	2016-04-11 16:33:58	0
53637	A_User7	0	0	10	\N	2008-05-30 16:59:05	100	\N	Asset Management	3	\N	Y	User 7	\N	\N	\N	\N	User 7	2016-04-11 16:33:59	0
53638	A_User8	0	0	10	\N	2008-05-30 16:59:06	100	\N	Asset Management	3	\N	Y	User 8	\N	\N	\N	\N	User 8	2016-04-11 16:33:59	0
53639	A_User9	0	0	10	\N	2008-05-30 16:59:07	100	\N	Asset Management	3	\N	Y	User 9	\N	\N	\N	\N	User 9	2016-04-11 16:33:59	0
56259	C_ElementValue_FA_ID	0	0	\N	\N	2014-07-14 15:12:55	100	\N	Asset Management	\N	\N	Y	Account Elements for Fixed Assets	\N	\N	\N	\N	Account Elements for Fixed Assets	2014-07-14 15:12:55	100
56260	FAAccountType	0	0	\N	\N	2014-07-14 15:11:15	100	\N	Asset Management	\N	\N	Y	FA Account Type	\N	\N	\N	\N	FA Account Type	2016-04-11 16:36:24	0
56261	GAAS_Asset_Type	0	0	\N	\N	2014-07-14 15:07:19	100	\N	Asset Management	\N	\N	Y	Type	\N	\N	\N	\N	Type	2016-04-11 16:36:03	0
56262	GAAS_DepreciateFrom	0	0	\N	\N	2014-07-14 15:08:11	100	\N	Asset Management	\N	\N	Y	Depreciate From	\N	\N	\N	\N	Depreciate From	2014-07-14 15:08:11	100
56263	GAAS_DepreciateFromAddition	0	0	\N	\N	2014-07-14 15:08:11	100	\N	Asset Management	\N	\N	Y	Depreciate from date of addition	\N	\N	\N	\N	Depreciate from date of addition	2014-07-14 15:08:11	100
56264	GAAS_SubAssetsAllowed	0	0	\N	\N	2014-07-14 15:08:12	100	\N	Asset Management	\N	\N	Y	Sub Assets Allowed	\N	\N	\N	\N	Sub Assets Allowed	2014-07-14 15:08:12	100
56265	GAAS_Asset_Group_Acct_ID	0	0	\N	\N	2014-07-14 15:08:18	100	\N	Asset Management	\N	\N	Y	Asset Group Accounting	\N	\N	\N	\N	Asset Group Accounting	2014-07-14 15:08:18	100
56266	DepreciationRate	0	0	\N	\N	2014-07-14 15:07:44	100	\N	Asset Management	\N	\N	Y	Depreciation Rate	\N	\N	\N	\N	Depreciation Rate	2016-04-11 16:36:09	0
56281	Default_Depn_Periods	0	0	\N	\N	2014-07-14 15:08:31	100	Default Depn Periods	Asset Management	\N	Standard Number of Depreciation Periods we wish to add	Y	Default_Depn_Periods	\N	\N	\N	\N	Default Depn Periods	2014-07-14 15:08:31	100
56286	GAAS_BuildDepreciation	0	0	\N	\N	2014-07-14 15:09:24	100	\N	Asset Management	\N	\N	Y	Build Depreciation Expense	\N	\N	\N	\N	Build Depreciation Expense	2014-07-14 15:09:24	100
56287	GAAS_AssetJournal_ID	0	0	\N	\N	2014-07-14 15:05:46	100	\N	Asset Management	\N	\N	Y	Asset Journal	\N	\N	\N	\N	Asset Journal	2014-07-14 15:05:46	100
56288	EntryType	0	0	\N	\N	2014-07-14 15:06:01	100	\N	Asset Management	\N	\N	Y	Entry Type	\N	\N	\N	\N	Entry Type	2016-04-11 16:36:26	0
56289	GenDepreciationEntries	0	0	\N	\N	2014-07-14 15:06:02	100	\N	Asset Management	\N	\N	Y	Generate Depreciation Entries	\N	\N	\N	\N	Generate Depreciation Entries	2014-07-14 15:06:02	100
56290	GAAS_AssetJournalLine_ID	0	0	\N	\N	2014-07-14 15:06:24	100	\N	Asset Management	\N	\N	Y	Asset Journal Line	\N	\N	\N	\N	Asset Journal Line	2016-04-11 16:36:10	0
56291	AccumulatedDepreciationAmt	0	0	\N	\N	2014-07-14 15:06:28	100	\N	Asset Management	\N	\N	Y	Accumulated Depreciation Amount	\N	\N	\N	\N	Accumulated Depreciation Amount	2014-07-14 15:06:28	100
56292	AssetCost	0	0	\N	\N	2014-07-14 15:06:28	100	\N	Asset Management	\N	\N	Y	Asset Cost	\N	\N	\N	\N	Asset Cost	2016-04-11 16:36:07	0
56293	BookAmt	0	0	\N	\N	2014-07-14 15:06:29	100	\N	Asset Management	\N	\N	Y	Book Amount	\N	\N	\N	\N	Book Amount	2014-07-14 15:06:29	100
56294	ClosingBalance	0	0	\N	\N	2014-07-14 15:06:30	100	\N	Asset Management	\N	\N	Y	Closing Balance	\N	\N	\N	\N	Closing Balance	2014-07-14 15:06:30	100
56295	DepreciationExpenseAmt	0	0	\N	\N	2014-07-14 15:06:33	100	\N	Asset Management	\N	\N	Y	Depreciation Expense Amount	\N	\N	\N	\N	Depreciation Expense Amount	2014-07-14 15:06:33	100
56296	GAAS_Depreciation_Expense_ID	0	0	\N	\N	2014-07-14 15:06:34	100	\N	Asset Management	\N	\N	Y	Depreciation Expense	\N	\N	\N	\N	Depreciation Expense	2014-07-14 15:06:34	100
56297	OpeningBalance	0	0	\N	\N	2014-07-14 15:06:35	100	\N	Asset Management	\N	\N	Y	Opening Balance	\N	\N	\N	\N	Opening Balance	2014-07-14 15:06:35	100
56298	ParentAsset_ID	0	0	\N	\N	2014-07-14 15:06:35	100	\N	Asset Management	\N	\N	Y	Parent Asset	\N	\N	\N	\N	Parent Asset	2014-07-14 15:06:35	100
56299	PostedAmt	0	0	\N	\N	2014-07-14 15:06:37	100	\N	Asset Management	\N	\N	Y	Posted Amount	\N	\N	\N	\N	Posted Amount	2014-07-14 15:06:37	100
56300	InitialCost	0	0	\N	\N	2014-07-14 15:06:38	100	\N	Asset Management	\N	\N	Y	Initial Cost	\N	\N	\N	\N	Initial Cost	2014-07-14 15:06:38	100
56301	Old_Asset_Group_ID	0	0	\N	\N	2014-07-14 15:06:39	100	\N	Asset Management	\N	\N	Y	Old Asset Group	\N	\N	\N	\N	Old Asset Group	2014-07-14 15:06:39	100
56302	New_Asset_Group_ID	0	0	\N	\N	2014-07-14 15:06:39	100	\N	Asset Management	\N	\N	Y	New Asset Group	\N	\N	\N	\N	New Asset Group	2014-07-14 15:06:39	100
56303	Old_Asset_Acct_ID	0	0	\N	\N	2014-07-14 15:06:40	100	\N	Asset Management	\N	\N	Y	Old Asset Acct	\N	\N	\N	\N	Old Asset Acct	2014-07-14 15:06:40	100
56304	New_Asset_Acct_ID	0	0	\N	\N	2014-07-14 15:06:43	100	\N	Asset Management	\N	\N	Y	New Asset Acct	\N	\N	\N	\N	New Asset Acct	2014-07-14 15:06:43	100
56305	DisposalAmt	0	0	\N	\N	2014-07-14 15:06:43	100	\N	Asset Management	\N	\N	Y	Disposal Amount	\N	\N	\N	\N	Disposal Amount	2014-07-14 15:06:43	100
56306	GAAS_AssetJournalLine_Det_ID	0	0	\N	\N	2014-07-14 15:06:59	100	\N	Asset Management	\N	\N	Y	Asset Journal Line Detail	\N	\N	\N	\N	Asset Journal Line Detail	2014-07-14 15:06:59	100
56307	GAAS_Depreciation_Workfile_ID	0	0	\N	\N	2014-07-14 15:09:12	100	\N	Asset Management	\N	\N	Y	Depreciation Workfile	\N	\N	\N	\N	Depreciation Workfile	2014-07-14 15:09:12	100
56308	AccumulatedDepr	0	0	\N	\N	2014-07-14 15:09:13	100	\N	Asset Management	\N	\N	Y	Accumulated Depreciation	\N	\N	\N	\N	Accumulated Depreciation	2014-07-14 15:09:13	100
56309	CurrentDeprExpense	0	0	\N	\N	2014-07-14 15:09:17	100	\N	Asset Management	\N	\N	Y	Current Depreciation Expense	\N	\N	\N	\N	Current Depreciation Expense	2014-07-14 15:09:17	100
56310	Expense_SL	0	0	\N	\N	2014-07-14 15:09:18	100	\N	Asset Management	\N	\N	Y	SL Expense/Period	\N	\N	\N	\N	SL Expense/Period	2014-07-14 15:09:18	100
56311	CurrentLifeYear	0	0	\N	\N	2014-07-14 15:09:19	100	\N	Asset Management	\N	\N	Y	Current Life Year	\N	\N	\N	\N	Current Life Year	2014-07-14 15:09:19	100
56312	CurrentPeriod	0	0	\N	\N	2014-07-14 15:09:19	100	\N	Asset Management	\N	\N	Y	Current Period	\N	\N	\N	\N	Current Period	2014-07-14 15:09:19	100
56313	SalvageValue	0	0	\N	\N	2014-07-14 15:09:20	100	\N	Asset Management	\N	\N	Y	Salvage Value	\N	\N	\N	\N	Salvage Value	2014-07-14 15:09:20	100
56314	StartPeriod	0	0	\N	\N	2014-07-14 15:07:37	100	\N	Asset Management	\N	\N	Y	Start Period	\N	\N	\N	\N	Start Period	2016-04-11 16:36:07	0
56315	UseLifePeriods	0	0	\N	\N	2014-07-14 15:09:21	100	\N	Asset Management	\N	\N	Y	UseLife Periods	\N	\N	\N	\N	UseLife Periods	2014-07-14 15:09:21	100
56316	LifeYears	0	0	\N	\N	2014-07-14 15:09:21	100	\N	Asset Management	\N	\N	Y	Life Years	\N	\N	\N	\N	Life Years	2014-07-14 15:09:21	100
56317	InitialAccumulatedDepr	0	0	\N	\N	2014-07-14 15:07:37	100	\N	Asset Management	\N	\N	Y	Initial Accumulated Depreciation	\N	\N	\N	\N	Initial Accumulated Depreciation	2016-04-11 16:36:07	0
56318	IsDepreciationProcessed	0	0	\N	\N	2014-07-14 15:09:25	100	\N	Asset Management	\N	\N	Y	Depreciation Processed	\N	\N	\N	\N	Depreciation Processed	2014-07-14 15:09:25	100
56319	GAAS_Asset_Acct_ID	0	0	\N	\N	2014-07-14 15:09:36	100	\N	Asset Management	\N	\N	Y	Asset Acct	\N	\N	\N	\N	Asset Acct	2014-07-14 15:09:36	100
56320	BookAmtEnd	0	0	\N	\N	2014-07-14 15:10:00	100	\N	Asset Management	\N	\N	Y	Book Amount at End	\N	\N	\N	\N	Book Amount at End	2014-07-14 15:10:00	100
56321	Processed_Flag	0	0	\N	\N	2014-07-14 15:10:02	100	\N	Asset Management	\N	\N	Y	Processed_Flag	\N	\N	\N	\N	Processed_Flag	2014-07-14 15:10:02	100
56322	InvoiceNo	0	0	\N	\N	2014-07-14 15:07:42	100	\N	Asset Management	\N	\N	Y	Invoice No	\N	\N	\N	\N	Invoice No	2016-04-11 16:36:09	0
56323	I_GAAS_Asset_ID	0	0	\N	\N	2014-07-14 15:07:09	100	\N	Asset Management	\N	\N	Y	Import Asset FA	\N	\N	\N	\N	Import Asset FA	2016-04-11 16:36:01	0
56324	A_Asset_Group_Value	0	0	\N	\N	2014-07-14 15:07:36	100	\N	Asset Management	\N	\N	Y	Asset Group Key	\N	\N	\N	\N	Asset Group Key	2016-04-11 16:36:07	0
56325	DepreciatedPeriods	0	0	\N	\N	2014-07-14 15:07:38	100	\N	Asset Management	\N	\N	Y	Depreciated Periods	\N	\N	\N	\N	Depreciated Periods	2016-04-11 16:36:08	0
56326	DepreciationPeriods	0	0	\N	\N	2014-07-14 15:07:38	100	\N	Asset Management	\N	\N	Y	Depreciation Periods	\N	\N	\N	\N	Depreciation Periods	2016-04-11 16:36:08	0
56327	LastDepreciationDate	0	0	\N	\N	2014-07-14 15:07:39	100	\N	Asset Management	\N	\N	Y	Last Depreciation Date	\N	\N	\N	\N	Last Depreciation Date	2016-04-11 16:36:08	0
56328	GAAS_Department_ID	0	0	\N	\N	2014-07-14 15:07:41	100	\N	Asset Management	\N	\N	Y	Department	\N	\N	\N	\N	Department	2016-04-11 16:36:08	0
56329	GAAS_Department_Key	0	0	\N	\N	2014-07-14 15:07:41	100	\N	Asset Management	\N	\N	Y	Department Value	\N	\N	\N	\N	Department Value	2016-04-11 16:36:08	0
56330	Fin_Comment	0	0	\N	\N	2014-07-14 15:07:42	100	\N	Asset Management	\N	\N	Y	Info Finance Comment	\N	\N	\N	\N	Info Finance Comment	2016-04-11 16:36:09	0
56336	GAAS_Depreciation_Type	0	0	\N	\N	2014-07-14 15:07:44	100	\N	Asset Management	\N	\N	Y	Depreciation Method	\N	\N	\N	\N	Depreciation Method	2016-04-11 16:36:09	0
56375	AmtAsset	0	0	\N	\N	2014-07-14 15:11:25	100	\N	Asset Management	\N	\N	Y	Asset Amount	\N	\N	\N	\N	Asset Amount	2016-04-11 16:36:20	0
56376	AmtDepreciation	0	0	\N	\N	2014-07-14 15:11:26	100	\N	Asset Management	\N	\N	Y	Depreciation Amount	\N	\N	\N	\N	Depreciation Amount	2016-04-11 16:36:21	0
57256	lastmaintenencedate	0	0	\N	\N	2014-07-14 15:08:39	100	\N	Asset Management	\N	\N	Y	lastmaintenencedate	\N	\N	\N	\N	lastmaintenencedate	2014-07-14 15:08:39	100
57257	lastmaintenanceuseunit	0	0	\N	\N	2014-07-14 15:08:39	100	\N	Asset Management	\N	\N	Y	lastmaintenanceuseunit	\N	\N	\N	\N	lastmaintenanceuseunit	2014-07-14 15:08:39	100
57258	nextmaintenanceuseunit	0	0	\N	\N	2014-07-14 15:08:40	100	\N	Asset Management	\N	\N	Y	nextmaintenanceuseunit	\N	\N	\N	\N	nextmaintenanceuseunit	2014-07-14 15:08:40	100
57263	AssetNameAndStartDate	0	0	\N	\N	2014-07-14 15:11:29	100	\N	Asset Management	\N	\N	Y	AssetNameAndStartDate	\N	\N	\N	\N	AssetNameAndStartDate	2014-07-14 15:11:29	100
57264	DepreciationStartDate	0	0	\N	\N	2014-07-14 15:11:30	100	\N	Asset Management	\N	\N	Y	Depreciation Start Date	\N	\N	\N	\N	Depreciation Start Date	2014-07-14 15:11:30	100
\.
