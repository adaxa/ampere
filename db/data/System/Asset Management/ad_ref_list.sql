copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
53390	Capitalized Lease	0	0	53271	2008-05-30 16:54:38	100	Asset is on a capitalized lease	Asset Management	Y	2008-05-30 16:54:38	100	\N	\N	CL
53391	Non-Capitalized Lease	0	0	53271	2008-05-30 16:54:39	100	Asset is on a non-capitalized lease	Asset Management	Y	2008-05-30 16:54:39	100	\N	\N	NL
53392	Owned	0	0	53271	2008-05-30 16:54:39	100	Asset owned outright	Asset Management	Y	2008-05-30 16:54:39	100	\N	\N	OW
53393	Rented	0	0	53271	2008-05-30 16:54:40	100	Asset is rented	Asset Management	Y	2008-05-30 16:54:40	100	\N	\N	RE
53394	15th of every month	0	0	53272	2008-05-30 16:54:57	100	\N	Asset Management	Y	2016-04-11 16:33:44	0	\N	\N	15T
53395	1st of every month	0	0	53272	2008-05-30 16:54:59	100	\N	Asset Management	Y	2016-04-11 16:33:44	0	\N	\N	1st
53396	Beginning of every month	0	0	53272	2008-05-30 16:55:00	100	\N	Asset Management	Y	2016-04-11 16:33:44	0	\N	\N	BEG
53397	Yearly on or before contract date	0	0	53272	2008-05-30 16:55:00	100	\N	Asset Management	Y	2016-04-11 16:33:44	0	\N	\N	YER
54037	1 Fixed Asset	0	0	53540	2014-07-14 15:11:16	100	1 Fixed Asset	Asset Management	Y	2014-07-14 15:11:16	100	\N	\N	1
54038	2 Cum Depreciation	0	0	53540	2014-07-14 15:11:16	100	2 Cum Depreciation	Asset Management	Y	2014-07-14 15:11:16	100	\N	\N	2
54039	Fixed Asset	0	0	53541	2014-07-14 15:07:20	100	\N	Asset Management	Y	2016-04-11 16:36:03	0	\N	\N	FA
54040	Investments	0	0	53541	2014-07-14 15:07:20	100	\N	Asset Management	Y	2016-04-11 16:36:03	0	\N	\N	IN
54047	Depreciation	0	0	53543	2014-07-14 15:06:02	100	Depreciation Entries	Asset Management	Y	2014-07-14 15:06:02	100	\N	\N	DEP
54048	Disposal	0	0	53543	2014-07-14 15:06:02	100	Disposal Entries	Asset Management	Y	2014-07-14 15:06:02	100	\N	\N	DIS
54049	New	0	0	53543	2014-07-14 15:06:02	100	New Assets	Asset Management	Y	2014-07-14 15:06:02	100	\N	\N	NEW
54050	Revaluation	0	0	53543	2014-07-14 15:06:02	100	Revaluation Entries for Fixed Assets	Asset Management	N	2014-07-14 15:06:02	100	\N	\N	REV
54051	Transfers	0	0	53543	2014-07-14 15:06:02	100	Transfer Entries for Fixed Assets	Asset Management	Y	2014-07-14 15:06:02	100	\N	\N	TRN
54056	Straight Line	0	0	53551	2014-07-14 15:07:45	100	\N	Asset Management	Y	2016-04-11 16:36:09	0	\N	\N	SL
54057	Reducing Balance	0	0	53551	2014-07-14 15:07:44	100	\N	Asset Management	Y	2016-04-11 16:36:09	0	\N	\N	RB
\.
