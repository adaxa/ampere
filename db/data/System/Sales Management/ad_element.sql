copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1502	DateLastAction	0	0	16	\N	2001-01-11 17:05:05	0	Date this request was last acted on	Sales Management	7	The Date Last Action indicates that last time that the request was acted on.	Y	Date last action	\N	\N	\N	\N	Date last action	2000-01-02 00:00:00	0
1503	DateNextAction	0	0	16	\N	2001-01-11 17:05:05	0	Date that this request should be acted on	Sales Management	7	The Date Next Action indicates the next scheduled date for an action to occur for this request.	Y	Date next action	\N	\N	\N	\N	Date next action	2000-01-02 00:00:00	0
1504	DueType	0	0	17	222	2001-01-11 17:05:05	0	Status of the next action for this Request	Sales Management	1	The Due Type indicates if this request is Due, Overdue or Scheduled.	Y	Due type	\N	\N	\N	\N	Due type	2000-01-02 00:00:00	0
1509	IsEscalated	0	0	20	\N	2001-01-11 17:05:05	0	This request has been escalated	Sales Management	1	The Escalated checkbox indicates that this request has been escalated or raised in importance.	Y	Escalated	\N	\N	\N	\N	Escalated	2000-01-02 00:00:00	0
1513	NextAction	0	0	17	219	2001-01-11 17:05:05	0	Next Action to be taken	Sales Management	1	The Next Action indicates the next action to be taken on this request.	Y	Next action	\N	\N	\N	\N	Next action	2000-01-02 00:00:00	0
1517	R_RequestAction_ID	0	0	13	\N	2001-01-11 17:05:05	0	Request has been changed	Sales Management	22	Old values	Y	Request History	\N	\N	\N	\N	Request History	2005-05-17 12:26:24	100
1520	RequestAmt	0	0	12	\N	2001-01-11 17:05:05	0	Amount associated with this request	Sales Management	22	The Request Amount indicates any amount that is associated with this request.  For example, a warranty amount or refund amount.	Y	Request Amount	\N	\N	\N	\N	Request Amt	2000-01-02 00:00:00	0
2704	R_Group_ID	0	0	19	\N	2005-04-26 20:15:49	100	Request Group	Sales Management	10	Group of requests (e.g. version numbers, responsibility, ...)	Y	Group	\N	\N	\N	\N	Group	2005-04-26 21:20:57	100
2705	R_Category_ID	0	0	19	\N	2005-04-26 20:15:49	100	Request Category	Sales Management	10	Category or Topic of the Request 	Y	Category	\N	\N	\N	\N	Category	2005-04-26 21:30:11	100
2706	R_Status_ID	0	0	19	\N	2005-04-26 20:15:49	100	Request Status	Sales Management	10	Status if the request (open, closed, investigating, ..)	Y	Status	\N	\N	\N	\N	Status	2005-04-26 21:06:21	100
2707	R_Resolution_ID	0	0	19	\N	2005-04-26 20:15:50	100	Request Resolution	Sales Management	10	Resolution status (e.g. Fixed, Rejected, ..)	Y	Resolution	\N	\N	\N	\N	Resolution	2005-04-26 21:14:11	100
2708	PriorityUser	0	0	17	154	2005-04-26 20:15:50	100	Priority of the issue for the User	Sales Management	1	\N	Y	User Importance	\N	\N	\N	\N	User Importance	2005-04-26 21:55:12	100
2709	ConfidentialType	0	0	17	340	2005-04-26 20:15:50	100	Type of Confidentiality	Sales Management	1	\N	Y	Confidentiality	\N	\N	\N	\N	Confidentiality	2005-04-26 21:45:30	100
2710	R_RequestRelated_ID	0	0	30	341	2005-04-26 20:15:50	100	Related Request (Master Issue, ..)	Sales Management	10	Request related to this request	Y	Related Request	\N	\N	\N	\N	Related Request	2005-04-26 21:58:32	100
2711	ConfidentialTypeEntry	0	0	17	340	2005-04-26 20:15:50	100	Confidentiality of the individual entry	Sales Management	1	\N	Y	Entry Confidentiality	\N	\N	\N	\N	Entry Confidentiality	2005-04-26 22:05:44	100
2712	R_StandardResponse_ID	0	0	19	\N	2005-04-26 20:15:50	100	Request Standard Response 	Sales Management	10	Text blocks to be copied into request response text	Y	Standard Response	\N	\N	\N	\N	Std Response	2005-04-26 21:05:45	100
2713	StartTime	0	0	16	\N	2005-04-26 20:15:50	100	Time started	Sales Management	7	\N	Y	Start Time	\N	\N	\N	\N	Start Time	2005-04-26 22:02:04	100
2714	EndTime	0	0	16	\N	2005-04-26 20:15:50	100	End of the time span	Sales Management	7	\N	Y	End Time	\N	\N	\N	\N	End Time	2005-04-26 21:50:40	100
2715	QtySpent	0	0	29	\N	2005-04-26 20:15:50	100	Quantity used for this event	Sales Management	22	\N	Y	Quantity Used	\N	\N	\N	\N	Qty Used	2005-04-26 21:56:30	100
2716	M_ProductSpent_ID	0	0	30	162	2005-04-26 20:15:50	100	Product/Resource/Service used in Request	Sales Management	10	Invoicing uses the Product used.	Y	Product Used	\N	\N	\N	\N	Product Used	2005-04-26 21:52:59	100
2717	C_InvoiceRequest_ID	0	0	30	336	2005-04-26 20:15:51	100	The generated invoice for this request	Sales Management	10	The optionally generated invoice for the request	Y	Request Invoice	\N	\N	\N	\N	Request Invoice	2005-05-19 19:33:42	100
2723	IsClosed	0	0	20	\N	2005-04-27 11:01:27	100	The status is closed	Sales Management	1	This allows to have multiple closed status	Y	Closed Status	\N	\N	\N	\N	Closed	2010-01-13 10:38:15	100
2785	M_ChangeRequest_ID	0	0	13	\N	2005-05-15 14:58:21	100	BOM (Engineering) Change Request	Sales Management	10	Change requests for a Bill of Materials. They can be automatically created from Requests, if enabled in the Request Type and the Request Group refers to a Bill of Materials	Y	Change Request	\N	\N	\N	\N	Change Request	2010-01-13 10:38:15	100
2791	R_RequestUpdate_ID	0	0	13	\N	2005-05-17 12:21:27	100	Request Updates	Sales Management	10	\N	Y	Request Update	\N	\N	\N	\N	Request Update	2005-05-17 12:23:27	100
2796	NullColumns	0	0	10	\N	2005-05-20 00:49:05	100	Columns with NULL value	Sales Management	255	Null values are used for showing "no change"	Y	Null Columns	\N	\N	\N	\N	Null Columns	2005-05-20 00:51:00	100
2797	M_FixChangeNotice_ID	0	0	18	351	2005-05-20 22:39:56	100	Fixed in Change Notice	Sales Management	10	\N	Y	Fixed in	\N	\N	\N	\N	Fixed in	2005-05-20 22:40:52	100
2898	TaskStatus	0	0	17	366	2005-12-21 15:57:01	100	Status of the Task	Sales Management	1	Completion Rate and Status of the Task	Y	Task Status	\N	\N	\N	\N	Task Status	2005-12-21 16:19:14	100
2899	DateCompletePlan	0	0	15	\N	2005-12-21 15:57:01	100	Planned Completion Date	Sales Management	7	Date when the task is planned to be complete	Y	Complete Plan	\N	\N	\N	\N	Complete Plan	2005-12-21 16:16:31	100
2900	QtyPlan	0	0	29	\N	2005-12-21 15:57:01	100	Planned Quantity	Sales Management	22	Planned Quantity	Y	Quantity Plan	\N	\N	\N	\N	Quantity Plan	2005-12-21 16:18:19	100
2901	DateStartPlan	0	0	15	\N	2005-12-21 17:29:47	100	Planned Start Date	Sales Management	7	Date when you plan to start	Y	Start Plan	\N	\N	\N	\N	Start Plan	2005-12-21 17:35:09	100
55290	IsWon	0	0	20	\N	2011-09-01 22:20:32	100	The opportunity was won	Sales Management	1	\N	Y	Won	\N	\N	\N	\N	Won	2012-06-08 15:15:24	0
55292	SalesPipeline	0	0	53370	\N	2011-09-02 12:29:18	100	\N	Sales Management	10	\N	Y	Sales Pipeline	\N	\N	\N	\N	Sales Pipeline	2011-09-02 12:29:18	100
55294	OpportunityCampaign	0	0	53370	\N	2011-09-02 16:59:53	100	\N	Sales Management	1	\N	Y	Opportunity by Campaign	\N	\N	\N	\N	Opportunity by Campaign	2011-09-02 16:59:53	100
55363	OpenRequests	0	0	53370	\N	2011-11-04 11:38:11	100	\N	Sales Management	1	\N	Y	Open Requests	\N	\N	\N	\N	Open Requests	2011-11-04 11:38:11	100
\.
