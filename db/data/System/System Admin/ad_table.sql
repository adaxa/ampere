copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "istscapable", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
111	AD_Language	Language ID	4	0	0	\N	106	\N	1999-05-21 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	50	\N	L	\N	2000-01-02 00:00:00	0
112	AD_Client	Client	6	0	0	\N	109	\N	1999-05-21 00:00:00	0	Client/Tenant for this installation.	System Admin	\N	\N	Y	Y	N	N	N	Y	N	N	40	\N	L	\N	2000-01-02 00:00:00	0
114	AD_User	User/Contact	7	0	0	\N	108	\N	1999-05-21 00:00:00	0	User within the system - Internal or Business Partner Contact	System Admin	\N	\N	Y	Y	N	Y	Y	N	N	N	80	\N	L	\N	2008-03-23 20:52:14	100
116	AD_Menu	Menu	6	0	0	\N	105	\N	1999-05-21 00:00:00	0	Identifies a Menu	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2009-09-01 21:40:05	0
120	AD_Menu_Trl	Menu Trl	6	0	0	\N	105	\N	1999-05-21 00:00:00	0	Identifies a Menu	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2009-09-01 21:40:28	0
133	AD_Workflow_Trl	Workflow Trl	6	0	0	\N	113	\N	1999-05-21 00:00:00	0	Workflow or combination of tasks	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 02:00:44	0
135	Test	Test ID	4	0	0	\N	127	\N	1999-05-21 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
139	C_Calendar	Calendar	2	0	0	\N	117	\N	1999-05-21 00:00:00	0	Accounting Calendar Name	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	110	\N	L	\N	2000-01-02 00:00:00	0
145	C_Period	Period	2	0	0	\N	117	\N	1999-05-21 00:00:00	0	Period of the Calendar	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	120	\N	L	\N	2000-01-02 00:00:00	0
155	AD_Org	Organisation	7	0	0	\N	110	\N	1999-05-21 00:00:00	0	Organisational entity within client	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	45	\N	L	\N	2000-01-02 00:00:00	0
162	C_Location	Address	7	0	0	\N	121	\N	1999-06-03 00:00:00	0	Location or Address	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	70	\N	L	\N	2000-01-02 00:00:00	0
163	C_NonBusinessDay	Non Business Day	6	0	0	\N	117	\N	1999-06-03 00:00:00	0	Day on which business is not transacted	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
164	C_Region	Region	6	0	0	\N	122	\N	1999-06-03 00:00:00	0	Identifies a geographical Region	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	60	\N	L	\N	2000-01-02 00:00:00	0
170	C_Country	Country	6	0	0	\N	122	\N	1999-06-03 00:00:00	0	Country 	System Admin	\N	\N	Y	Y	N	Y	Y	N	N	N	55	\N	L	\N	2000-01-02 00:00:00	0
177	C_Year	Year	2	0	0	\N	117	\N	1999-06-03 00:00:00	0	Calendar Year	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	115	\N	L	\N	2000-01-02 00:00:00	0
186	C_City	City	6	0	0	\N	122	\N	1999-06-03 00:00:00	0	City	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
195	AD_Preference	Preference	7	0	0	\N	129	\N	1999-06-14 00:00:00	0	Personal Preference	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
227	AD_ClientInfo	AD_ClientInfo	6	0	0	\N	109	\N	1999-07-04 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
228	AD_OrgInfo	Organization Info	7	0	0	\N	110	\N	1999-07-04 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-12-23 14:24:17	100
229	C_PeriodControl	Period Control	2	0	0	\N	117	\N	1999-07-04 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
254	AD_Attachment	Attachment	6	0	0	\N	128	\N	1999-08-08 00:00:00	0	Attachment for the document	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
282	AD_PInstance	Process Instance	6	0	0	\N	332	\N	1999-11-10 00:00:00	0	Instance of the process	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
283	AD_PInstance_Para	AD_PInstance_Para	6	0	0	\N	332	\N	1999-11-10 00:00:00	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 02:15:00	0
288	AD_Tree	Tree	6	0	0	\N	163	\N	1999-11-10 00:00:00	0	Identifies a Tree	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	90	\N	L	\N	2000-01-02 00:00:00	0
296	C_Bank	Bank	3	0	0	\N	158	\N	1999-12-04 19:50:17	0	Bank	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	85	\N	L	\N	2000-01-02 00:00:00	0
297	C_BankAccount	Bank Account	3	0	0	\N	158	\N	1999-12-04 19:50:17	0	Account at the Bank	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	90	\N	L	\N	2000-01-02 00:00:00	0
380	AD_Error	Error	6	0	0	\N	188	\N	2000-09-15 14:45:03	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
389	AD_Note	Notice	7	0	0	\N	193	\N	2000-12-17 16:19:52	0	System Notice	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 04:46:40	0
391	C_BankAccount_Acct	C_BankAccount_Acct	3	0	0	\N	158	\N	2000-12-17 16:19:52	0	\N	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:24:45	100
397	C_InterOrg_Acct	C_InterOrg_Acct	3	0	0	\N	110	\N	2000-12-17 16:19:52	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
398	C_PaymentProcessor	Payment Processor	3	0	0	\N	158	\N	2000-12-17 16:19:52	0	Payment processor for electronic payments	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	95	\N	L	\N	2000-01-02 00:00:00	0
404	AD_Find	Find	7	0	0	\N	\N	\N	2000-12-17 17:35:08	0	\N	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
454	AD_PrintForm	Print Form	6	0	0	\N	224	\N	2001-07-28 19:43:54	0	Form	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
455	C_BankAccountDoc	Account Payment Methods	3	0	0	\N	158	\N	2001-07-28 19:43:54	0	Checks, Transfers, etc.	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2024-06-01 12:32:57	100
457	AD_Color	System Color	4	0	0	\N	225	\N	2001-09-05 20:55:17	0	Color for backgrounds or indicators	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	45	\N	L	\N	2000-01-02 00:00:00	0
461	AD_Image	Image	6	0	0	\N	227	\N	2001-09-05 20:55:18	0	System Image or Icon	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	40	\N	L	\N	2010-04-15 18:49:15	100
489	AD_PrintFormatItem	Print Format Item	7	0	0	\N	240	\N	2002-07-11 18:33:16	0	Item/Column in the Print format	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
490	AD_PrintColor	Print Color	6	0	0	\N	238	\N	2002-07-11 18:33:17	0	Color used for printing and display	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	55	\N	L	\N	2000-01-02 00:00:00	0
491	AD_PrintFont	Print Font	6	0	0	\N	239	\N	2002-07-11 18:33:17	0	Maintain Print Font	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	55	\N	L	\N	2000-01-02 00:00:00	0
492	AD_PrintPaper	Print Paper	6	0	0	\N	241	\N	2002-07-11 18:33:17	0	Printer paper definition	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	60	\N	L	\N	2000-01-02 00:00:00	0
493	AD_PrintFormat	Print Format	7	0	0	\N	240	\N	2002-07-11 18:33:17	0	Data Print Format	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	65	\N	L	\N	2000-01-02 00:00:00	0
521	AD_PrintGraph	Graph	7	0	0	\N	240	\N	2002-08-24 14:28:09	0	Graph included in Reports	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
522	AD_PrintFormatItem_Trl	Print Format Item Trl	7	0	0	\N	240	\N	2002-08-24 14:28:09	0	Item/Column in the Print format	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
523	AD_PrintTableFormat	Print Table Format	6	0	0	\N	243	\N	2002-08-24 14:28:09	0	Table Format in Reports	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	60	\N	L	\N	2000-01-02 00:00:00	0
531	AD_System	System	4	0	0	\N	246	\N	2002-11-01 20:51:37	0	System Definition	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
534	I_ElementValue	Import Account	6	0	0	\N	248	\N	2003-01-11 14:57:27	0	Import Account Value	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
566	AD_Session	Session	6	0	0	\N	264	\N	2003-05-28 21:35:14	0	User Session Online or Web	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
573	C_Recurring_Run	Recurring Run	3	0	0	\N	266	\N	2003-05-28 21:35:14	0	Recurring Document Run	System Admin	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-09-03 12:10:32	100
574	C_Recurring	Recurring	3	0	0	\N	266	\N	2003-05-28 21:35:14	0	Recurring Document	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
578	AD_PInstance_Log	AD_PInstance_Log	6	0	0	\N	332	\N	2003-05-29 21:57:02	0	(Non Standard Table)	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
585	C_OrgAssignment	Org Assignment	2	0	0	\N	108	\N	2003-06-01 23:14:26	0	Assigment to (transaction) Organization	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-05-15 01:11:32	100
616	C_Country_Trl	Country Trl	6	0	0	\N	122	\N	2003-07-25 18:10:34	0	Country 	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
625	AD_Registration	System Registration	4	0	0	\N	340	\N	2003-10-07 14:38:03	0	System Registration	System Admin	System Registration - Only one Record - Do not add additional records.	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
640	C_BankStatementLoader	Bank Statement Loader	3	0	0	\N	158	\N	2003-12-25 14:02:23	0	Definition of Bank Statement Loader (SWIFT, OFX)	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
658	C_BankStatementMatcher	Bank Statement Matcher	2	0	0	\N	302	\N	2004-01-24 19:22:32	0	Algorithm to match Bank Statement Info to Business Partners, Invoices and Payments	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
689	AD_OrgType	Organisation Type	6	0	0	\N	304	\N	2004-02-19 10:29:53	0	Organisation Type	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
705	AD_AttachmentNote	Attachment Note	6	0	0	\N	128	\N	2004-03-16 00:47:46	0	Personal Attachment Note	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
717	AD_AccessLog	Access Log	6	0	0	\N	326	\N	2004-04-13 13:50:38	0	Log of Access to the System	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-04-26 20:31:21	100
739	RV_PrintFormatDetail	PrintFormat Detail	6	0	0	\N	\N	\N	2004-06-18 14:14:20	0	\N	System Admin	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:57:14	100
782	AD_UserMail	User Mail	7	0	0	\N	108	\N	2005-05-02 19:10:21	100	Mail sent to the user	System Admin	\N	N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-11-09 14:23:34	100
792	C_Remuneration	Remuneration	2	0	0	\N	353	\N	2005-05-15 01:20:52	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2005-09-03 12:10:09	100
793	C_JobRemuneration	Position Remuneration	2	0	0	\N	353	\N	2005-05-15 01:41:10	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:10:57	100
794	C_UserRemuneration	Employee Remuneration	2	0	0	\N	353	\N	2005-05-15 01:43:25	100	Employee Wage or Salary Overwrite	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:10:06	100
827	AD_ClientShare	Client Share	2	0	0	\N	109	\N	2005-11-20 15:59:50	100	\N	System Admin	\N	N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-11-20 15:59:50	100
828	AD_Issue	System Issue	6	0	0	\N	363	\N	2005-12-12 16:38:08	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-12-30 14:51:17	100
50009	AD_SysConfig	System Configurator	6	0	0	\N	50006	N	2007-02-28 01:41:08	100	\N	System Admin	\N	N	Y	Y	N	Y	Y	N	N	N	0	\N	L	\N	2007-02-28 01:41:08	100
50010	PA_DashboardContent	Dashboard Content	6	0	0	\N	50007	\N	2007-07-09 00:00:00	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	\N	\N	L	\N	2010-12-05 20:03:35	100
53118	A_Depreciation_Forecast	Depreciation Forecast	7	0	0	\N	\N	\N	2008-05-30 16:38:11	100	\N	System Admin	\N	N	Y	Y	N	N	N	N	N	N	\N	\N	L	\N	2008-05-30 16:38:11	100
53221	RV_Unprocessed	Not Processed	3	0	0	\N	53086	\N	2009-07-24 12:44:54	100	\N	System Admin	\N	\N	Y	Y	N	N	N	N	N	Y	\N	\N	L	\N	2009-07-24 12:44:54	100
53246	AD_RelationType	Relation Type	4	0	0	\N	53102	N	2009-11-13 14:59:55	100	Defines the sets of record pairs (and the conditions a given pair must fulfill to be part of one)	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2020-10-16 23:34:41	100
53295	PA_DashboardContent_Trl	Dashboard Content Trl	6	0	0	\N	50007	\N	2010-12-05 20:03:49	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	\N	\N	L	\N	2010-12-05 20:03:49	100
53330	C_Recurring_Document	Recurring Document	3	0	0	\N	266	\N	2011-07-19 14:47:14	100	Recurring Document	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2011-07-19 14:47:14	100
53898	U_PA_DocumentStatus	Document Status Indicator	7	0	0	\N	53386	\N	2014-12-15 17:36:22	100	\N	System Admin	\N	N	Y	N	N	Y	N	N	N	N	\N	\N	L	\N	2016-04-11 16:54:18	0
53935	X_AuthorityType	AuthorityType	3	0	0	\N	53401	N	2015-01-29 13:51:54	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2016-04-11 16:38:36	0
53961	AD_PrintConfig	Print Config ID	3	0	0	\N	\N	N	2015-04-08 11:44:12	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2015-04-08 11:44:12	100
54050	ETL_Setup	ETL Setup ID	3	0	0	\N	\N	N	2015-10-27 16:29:15	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2015-10-27 16:29:15	100
54051	ETL_SetupLine	ETL Setup Line ID	3	0	0	\N	\N	N	2015-10-27 16:32:14	100	\N	System Admin	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2015-10-27 16:32:14	100
400001	AD_AttachmentEntry	Attachment Entry	6	0	0	\N	128	\N	2022-03-02 13:58:32	100	Attachment File Entry	System Admin	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2022-03-02 13:58:32	100
400002	AD_EventLog	EventLog	6	0	0	\N	\N	\N	2022-03-07 17:39:49	100	Log of events	System Admin	\N	\N	Y	Y	N	Y	Y	N	N	N	145	\N	L	\N	2022-03-07 17:39:49	100
\.
