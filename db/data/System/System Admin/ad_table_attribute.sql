copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50075	ad_accesslog_pkey	0	0	717	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_accesslog_id)	CP	\N	2022-11-02 05:57:56.041854	100
50076	adcolumn_adaccesslog	0	0	717	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_column_id) REFERENCES ad_column(ad_column_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50077	adtable_adacceslog	0	0	717	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50095	ad_attachment_record	0	0	254	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_table_id, record_id)	CU	\N	2022-11-02 05:57:56.041854	100
50096	ad_attachment_pkey	0	0	254	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_attachment_id)	CP	\N	2022-11-02 05:57:56.041854	100
50097	adtable_adattachment	0	0	254	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50098	ad_attachmententry_key	0	0	400001	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_attachmententry_id)	CP	\N	2022-11-02 05:57:56.041854	100
50099	adattachment_entry	0	0	400001	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_attachment_id) REFERENCES ad_attachment(ad_attachment_id)	CF	\N	2022-11-02 05:57:56.041854	100
50100	ad_attachmentnote_pkey	0	0	705	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_attachmentnote_id)	CP	\N	2022-11-02 05:57:56.041854	100
50101	adattachment_note	0	0	705	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_attachment_id) REFERENCES ad_attachment(ad_attachment_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50102	aduser_adattachmentnote	0	0	705	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50112	ad_client_name	0	0	112	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (name)	CU	\N	2022-11-02 05:57:56.041854	100
50113	ad_client_pkey	0	0	112	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_client_id)	CP	\N	2022-11-02 05:57:56.041854	100
50114	adlangu_adclient	0	0	112	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50115	ad_clientinfo_pkey	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_client_id)	CP	\N	2022-11-02 05:57:56.041854	100
50116	c_uom_volume_ad_clientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_uom_volume_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50117	adtreemenu_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_menu_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50118	logoreport_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (logoreport_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50119	adtreesalesreg_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_salesregion_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50120	adtreecampaign_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_campaign_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50121	mproductfreight_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (m_productfreight_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50122	adtreebpartner_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_bpartner_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50123	c_uom_weight_ad_clientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_uom_weight_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50124	cbpartnercashtrx_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bpartnercashtrx_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50125	adtreeactivity_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_activity_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50126	cacctschema1_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_acctschema1_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50127	logoweb_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (logoweb_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50128	adtreeorg_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_org_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50129	adtreeproduct_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_product_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50130	ccalendar_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_calendar_id) REFERENCES c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50131	adtreeproject_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_tree_project_id) REFERENCES ad_tree(ad_tree_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50132	logo_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (logo_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50133	c_uom_length_ad_clientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_uom_length_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50134	c_uom_time_ad_clientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_uom_time_id) REFERENCES c_uom(c_uom_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50135	adclient_adclientinfo	0	0	227	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50136	ad_clientshare_table	0	0	827	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, ad_table_id)	CU	\N	2022-11-02 05:57:56.041854	100
50137	ad_clientshare_pkey	0	0	827	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_clientshare_id)	CP	\N	2022-11-02 05:57:56.041854	100
50138	adclient_adclientshare	0	0	827	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50139	adtable_adclientshare	0	0	827	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50140	adorg_adclientshare	0	0	827	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50141	ad_color_pkey	0	0	457	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_color_id)	CP	\N	2022-11-02 05:57:56.041854	100
50142	adimage_adcolor	0	0	457	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_image_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50186	a_depreciation_forecast_key	0	0	53118	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (a_depreciation_forecast_id)	CP	\N	2022-11-02 05:57:56.041854	100
50187	aendasset_adepreciationforecas	0	0	53118	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (a_end_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50188	astartasset_adepreciationforec	0	0	53118	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (a_start_asset_id) REFERENCES a_asset(a_asset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50193	ad_error_pkey	0	0	380	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_error_id)	CP	\N	2022-11-02 05:57:56.041854	100
50194	adlangu_aderror	0	0	380	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50195	ad_eventlog_key	0	0	400002	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_eventlog_id)	CP	\N	2022-11-02 05:57:56.041854	100
50216	ad_find_pkey	0	0	404	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_find_id)	CP	\N	2022-11-02 05:57:56.041854	100
50217	adcolumn_adfind	0	0	404	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_column_id) REFERENCES ad_column(ad_column_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50228	ad_image_pkey	0	0	461	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_image_id)	CP	\N	2022-11-02 05:57:56.041854	100
50229	entityt_adimage	0	0	461	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (entitytype) REFERENCES ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50254	ad_issue_pkey	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_issue_id)	CP	\N	2022-11-02 05:57:56.041854	100
50255	rknownissue_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_issueknown_id) REFERENCES r_issueknown(r_issueknown_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50256	aasset_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (a_asset_id) REFERENCES a_asset(a_asset_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50257	adprocess_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_process_id) REFERENCES ad_process(ad_process_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50258	rissuesystem_ad_issue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_issuesystem_id) REFERENCES r_issuesystem(r_issuesystem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50259	rrequest_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_request_id) REFERENCES r_request(r_request_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50260	adform_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_form_id) REFERENCES ad_form(ad_form_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50261	rissueproject_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_issueproject_id) REFERENCES r_issueproject(r_issueproject_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50262	adwindow_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_window_id) REFERENCES ad_window(ad_window_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50263	rissueuser_adissue	0	0	828	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_issueuser_id) REFERENCES r_issueuser(r_issueuser_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50264	ad_language_pkey	0	0	111	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_language)	CP	\N	2022-11-02 05:57:56.041854	100
50265	languageclient	0	0	111	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50266	languageorg	0	0	111	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50268	ad_menu_pkey	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_menu_id)	CP	\N	2022-11-02 05:57:56.041854	100
50269	adwindow_admenu	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_window_id) REFERENCES ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50270	adclient_admenu	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50271	adform_admenu	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_form_id) REFERENCES ad_form(ad_form_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50272	adprocess_admenu	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_process_id) REFERENCES ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50273	entityt_admenu	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (entitytype) REFERENCES ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50274	ad_menu_org	0	0	116	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50275	ad_menu_trl_pkey	0	0	120	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_menu_id, ad_language)	CP	\N	2022-11-02 05:57:56.041854	100
50276	ad_menutrl	0	0	120	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_menu_id) REFERENCES ad_menu(ad_menu_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50277	ad_language_menutrl	0	0	120	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50290	ad_note_pkey	0	0	389	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_note_id)	CP	\N	2022-11-02 05:57:56.041854	100
50291	admessage_adnote	0	0	389	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_message_id) REFERENCES ad_message(ad_message_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50292	adtable_adnote	0	0	389	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50293	aduser_adnote	0	0	389	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50294	ad_org_value	0	0	155	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, value)	CU	\N	2022-11-02 05:57:56.041854	100
50295	ad_org_pkey	0	0	155	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_org_id)	CP	\N	2022-11-02 05:57:56.041854	100
50296	adclient_adorg	0	0	155	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50297	ad_orginfo_pkey	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_org_id)	CP	\N	2022-11-02 05:57:56.041854	100
50298	dropshipwarehouse_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (dropship_warehouse_id) REFERENCES m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50299	adorgtype_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_orgtype_id) REFERENCES ad_orgtype(ad_orgtype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50300	aduser_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50301	cbank_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (transferbank_id) REFERENCES c_bank(c_bank_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50302	adorgparent_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (parent_org_id) REFERENCES ad_org(ad_org_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50303	logo_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (logo_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50304	ccalendar_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_calendar_id) REFERENCES c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50305	adorg_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50306	c_location_ad_orginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_location_id) REFERENCES c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50307	mwarehouse_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (m_warehouse_id) REFERENCES m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50308	ccashbook_adorginfo	0	0	228	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (transfercashbook_id) REFERENCES c_cashbook(c_cashbook_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50309	ad_orgtype_pkey	0	0	689	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_orgtype_id)	CP	\N	2022-11-02 05:57:56.041854	100
50310	adprintcolor_adorgtype	0	0	689	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50343	ad_pinstance_pkey	0	0	282	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_pinstance_id)	CP	\N	2022-11-02 05:57:56.041854	100
50344	aduser_pinstance	0	0	282	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50345	adprocess_adpinstance	0	0	282	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_process_id) REFERENCES ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50346	ad_pinstance_log_pkey	0	0	578	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_pinstance_id, log_id)	CP	\N	2022-11-02 05:57:56.041854	100
50347	adpinstance_pilog	0	0	578	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_pinstance_id) REFERENCES ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50348	ad_pinstance_para_pkey	0	0	283	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_pinstance_id, seqno)	CP	\N	2022-11-02 05:57:56.041854	100
50349	adpinstance_adpinstancepara	0	0	283	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_pinstance_id) REFERENCES ad_pinstance(ad_pinstance_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50350	ad_preference_attribute	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, ad_org_id, ad_window_id, ad_user_id, attribute)	CU	\N	2022-11-02 05:57:56.041854	100
50351	ad_preference_pkey	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_preference_id)	CP	\N	2022-11-02 05:57:56.041854	100
50352	ad_preference_client	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50353	ad_user_preference	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50354	ad_window_preference	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_window_id) REFERENCES ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50355	ad_preference_org	0	0	195	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50356	ad_printcolor_name	0	0	490	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50357	ad_printcolor_pkey	0	0	490	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printcolor_id)	CP	\N	2022-11-02 05:57:56.041854	100
50358	ad_printconfig_key	0	0	53961	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printconfig_id)	CP	\N	2022-11-02 05:57:56.041854	100
50359	ad_printfont_name	0	0	491	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50360	ad_printfont_pkey	0	0	491	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printfont_id)	CP	\N	2022-11-02 05:57:56.041854	100
50361	ad_printform_client	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, ad_org_id)	CU	\N	2022-11-02 05:57:56.041854	100
50362	ad_printform_pkey	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printform_id)	CP	\N	2022-11-02 05:57:56.041854	100
50363	adprintformat_formremittance	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (remittance_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50364	adprintformat_formproject	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (project_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50365	adprintformat_formshipment	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (shipment_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50366	adprintformat_formorder	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (order_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50367	adprintformat_forminvoice	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (invoice_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50368	rmailtext_shipadprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (shipment_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50369	adclient_adprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50370	manufordermailtext_adprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (manuf_order_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50371	manuforderprintformat_adprintf	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (manuf_order_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50372	distriborderprintformat_adprin	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (distrib_order_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50373	distribordermailtext_adprintfo	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (distrib_order_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50374	rmailtext_invoiceadprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (invoice_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50375	rmailtext_orderadprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (order_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50376	rmailtext_projectadprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (project_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50377	rmailtext_remitadprintform	0	0	454	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (remittance_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50378	ad_printformat_name	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, ad_table_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50379	ad_printformat_pkey	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printformat_id)	CP	\N	2022-11-02 05:57:56.041854	100
50380	adprintview_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_reportview_id) REFERENCES ad_reportview(ad_reportview_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50381	adtable_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50382	adprintpaper_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printpaper_id) REFERENCES ad_printpaper(ad_printpaper_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50383	jasperprocess_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (jasperprocess_id) REFERENCES ad_process(ad_process_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50384	adprintformattable_format	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printtableformat_id) REFERENCES ad_printtableformat(ad_printtableformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50385	adprintcolor_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50386	ad_printfont_adprintformat	0	0	493	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printfont_id) REFERENCES ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50387	ad_printformatitem_pkey	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printformatitem_id)	CP	\N	2022-11-02 05:57:56.041854	100
50388	adprintgraph_printformatitem	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printgraph_id) REFERENCES ad_printgraph(ad_printgraph_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50389	adprintcolor_adprintformatitem	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50390	adprintformat_printformatchild	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printformatchild_id) REFERENCES ad_printformat(ad_printformat_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50391	adprintformat_printformatitem	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printformat_id) REFERENCES ad_printformat(ad_printformat_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50392	adcolumn_adprintformatitem	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_column_id) REFERENCES ad_column(ad_column_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50393	adprintfont_adprintformatitem	0	0	489	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printfont_id) REFERENCES ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50394	ad_printformatitem_trl_pkey	0	0	522	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printformatitem_id, ad_language)	CP	\N	2022-11-02 05:57:56.041854	100
50395	adprintformatitem_trl	0	0	522	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50396	adlanguage_adprintformitemtrl	0	0	522	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50397	ad_printgraph_pkey	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printgraph_id)	CP	\N	2022-11-02 05:57:56.041854	100
50398	adprintformatitem_graphdescr	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (description_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50399	adprintformatitem_graphdata4	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (data4_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50400	adprintformatitem_graphdata	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (data_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50401	adprintformatitem_graphdata1	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (data1_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50402	adprintformat_adprintgraph	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50403	adprintformatitem_graphdata3	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (data3_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50404	adprintformatitem_graphdata2	0	0	521	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (data2_printformatitem_id) REFERENCES ad_printformatitem(ad_printformatitem_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50405	ad_printpaper_name	0	0	492	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50406	ad_printpaper_pkey	0	0	492	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printpaper_id)	CP	\N	2022-11-02 05:57:56.041854	100
50407	ad_printtableformat_pkey	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_printtableformat_id)	CP	\N	2022-11-02 05:57:56.041854	100
50408	adimage_adprinttableformat	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_image_id) REFERENCES ad_image(ad_image_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50409	adprintfont_tableformatfunc	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (funct_printfont_id) REFERENCES ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50410	adprintfont_tablehdr	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (hdr_printfont_id) REFERENCES ad_printfont(ad_printfont_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50411	adprintcolor_tableline	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (line_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50412	adprintcolor_tablehdrtextfg	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (hdrtextfg_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50413	adprintcolor_tablehdrtextbg	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (hdrtextbg_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50414	adprintcolor_tablehdrline	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (hdrline_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50415	adprintcolor_tablefunctfg	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (functfg_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50416	adprintcolor_tablefunctbg	0	0	523	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (functbg_printcolor_id) REFERENCES ad_printcolor(ad_printcolor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50473	ad_registration_pkey	0	0	625	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_registration_id, ad_client_id, ad_system_id)	CP	\N	2022-11-02 05:57:56.041854	100
50474	clocation_adregistration	0	0	625	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_location_id) REFERENCES c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50475	ccurrency_adregistration	0	0	625	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50476	adsystem_adregistration	0	0	625	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_system_id, ad_client_id) REFERENCES ad_system(ad_system_id, ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50477	ad_relationtype_key	0	0	53246	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_relationtype_id)	CP	\N	2022-11-02 05:57:56.041854	100
50478	adreferencesource_adrelationty	0	0	53246	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_reference_source_id) REFERENCES ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50479	adreferencetarget_adrelationty	0	0	53246	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_reference_target_id) REFERENCES ad_reference(ad_reference_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50531	ad_session_pkey	0	0	566	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_session_id)	CP	\N	2022-11-02 05:57:56.041854	100
50532	adrole_adsession	0	0	566	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_role_id) REFERENCES ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50533	ad_sysconfig_pkey	0	0	50009	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_sysconfig_id)	CP	\N	2022-11-02 05:57:56.041854	100
50534	entityt_adsysconfig	0	0	50009	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (entitytype) REFERENCES ad_entitytype(entitytype) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50535	ad_system_pkey	0	0	531	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_system_id, ad_client_id)	CP	\N	2022-11-02 05:57:56.041854	100
50574	ad_tree_name	0	0	288	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50575	ad_tree_pkey	0	0	288	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_tree_id)	CP	\N	2022-11-02 05:57:56.041854	100
50598	ad_user_pkey	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_user_id)	CP	\N	2022-11-02 05:57:56.041854	100
50599	aduser_supervisor	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50600	ad_user_org	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50601	cbpartner_aduser	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50602	cgreeting_aduser	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_greeting_id) REFERENCES c_greeting(c_greeting_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50603	cbplocation_aduser	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bpartner_location_id) REFERENCES c_bpartner_location(c_bpartner_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50604	adorgtrx_aduser	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_orgtrx_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50605	cjob_aduser	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_job_id) REFERENCES c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50606	ad_user_client	0	0	114	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50621	ad_usermail_pkey	0	0	782	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_usermail_id)	CP	\N	2022-11-02 05:57:56.041854	100
50622	aduser_adusermail	0	0	782	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50623	rmailtext_adusermail	0	0	782	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (r_mailtext_id) REFERENCES r_mailtext(r_mailtext_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50676	ad_workflow_trl_pkey	0	0	133	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_workflow_id, ad_language)	CP	\N	2022-11-02 05:57:56.041854	100
50677	ad_workflowtrl	0	0	133	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_workflow_id) REFERENCES ad_workflow(ad_workflow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50678	ad_language_workflowtrl	0	0	133	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50822	c_bank_pkey	0	0	296	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bank_id)	CP	\N	2022-11-02 05:57:56.041854	100
50823	clocation_cbank	0	0	296	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_location_id) REFERENCES c_location(c_location_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50824	c_bankaccount_pkey	0	0	297	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bankaccount_id)	CP	\N	2022-11-02 05:57:56.041854	100
50825	cbank_cbankaccount	0	0	297	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bank_id) REFERENCES c_bank(c_bank_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50826	ccurrency_cbankaccount	0	0	297	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50827	c_bankaccount_acct_pkey	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bankaccount_id, c_acctschema_id)	CP	\N	2022-11-02 05:57:56.041854	100
50828	vc_bpaymentselect_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_paymentselect_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50829	vc_brevaluationgain_cbankaccou	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_revaluationgain_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50830	cbankaccount_cbankacctacct	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bankaccount_id) REFERENCES c_bankaccount(c_bankaccount_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50831	vc_bexpense_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_expense_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50832	vc_basset_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_asset_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50833	vc_binterestrev_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_interestrev_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50834	vc_bunidentified_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_unidentified_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50835	vc_bsettlementgain_cbankaccoun	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_settlementgain_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50836	cacctschema_cbankaccountacct	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50837	vc_brevaluationloss_cbankaccou	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_revaluationloss_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50838	vc_bsettlementloss_cbankaccoun	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_settlementloss_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50839	vc_bunallocatedcash_cbankaccou	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_unallocatedcash_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50840	vc_bintransit_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_intransit_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50841	vc_binterestexp_cbankaccount	0	0	391	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (b_interestexp_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50842	c_bankaccountdoc_pkey	0	0	455	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bankaccountdoc_id)	CP	\N	2022-11-02 05:57:56.041854	100
50843	cbankaccount_cbadoc	0	0	455	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bankaccount_id) REFERENCES c_bankaccount(c_bankaccount_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50844	adprintformat_cbankaccountdoc	0	0	455	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (check_printformat_id) REFERENCES ad_printformat(ad_printformat_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50854	c_bankstatementloader_pkey	0	0	640	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bankstatementloader_id)	CP	\N	2022-11-02 05:57:56.041854	100
50855	cbankacct_cbankstmtloader	0	0	640	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bankaccount_id) REFERENCES c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50856	c_bankstatementmatcher_pkey	0	0	658	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_bankstatementmatcher_id)	CP	\N	2022-11-02 05:57:56.041854	100
50945	c_calendar_name	0	0	139	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (ad_client_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
50946	c_calendar_pkey	0	0	139	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_calendar_id)	CP	\N	2022-11-02 05:57:56.041854	100
50947	c_calendarclient	0	0	139	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50948	c_calendarorg	0	0	139	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50993	c_city_pkey	0	0	186	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_city_id)	CP	\N	2022-11-02 05:57:56.041854	100
50994	ccountry_ccity	0	0	186	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_country_id) REFERENCES c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50995	c_cityclient	0	0	186	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50996	c_cityorg	0	0	186	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50997	cregion_ccity	0	0	186	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_region_id) REFERENCES c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51029	c_countrycode	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (countrycode)	CU	\N	2022-11-02 05:57:56.041854	100
51030	c_country_pkey	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_country_id)	CP	\N	2022-11-02 05:57:56.041854	100
51031	ccurrency_ccountry	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51032	c_countryorg	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51033	adlanguage_ccountry	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51034	ccountry_ccountry	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_country_id) REFERENCES c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51035	c_countryclient	0	0	170	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51036	c_country_trl_pkey	0	0	616	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_country_id, ad_language)	CP	\N	2022-11-02 05:57:56.041854	100
51037	adlanguage_ccountrytrl	0	0	616	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51038	ccountry_ccountrytrl	0	0	616	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_country_id) REFERENCES c_country(c_country_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51134	c_interorg_acct_pkey	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_acctschema_id, ad_org_id, ad_orgto_id)	CP	\N	2022-11-02 05:57:56.041854	100
51135	adorgto_cinterorgacct	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_orgto_id) REFERENCES ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51136	vc_intercompanyduefrom_cintero	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (intercompanyduefrom_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51137	cacctschema_cinterorgacct	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51138	vc_intercompanydueto_cinterorg	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (intercompanydueto_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51139	adorg_cinterorgacct	0	0	397	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51225	c_jobremuneration_pkey	0	0	793	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_jobremuneration_id)	CP	\N	2022-11-02 05:57:56.041854	100
51226	cremuneration_cjobrem	0	0	793	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_remuneration_id) REFERENCES c_remuneration(c_remuneration_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51227	cjob_cjobremuneration	0	0	793	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_job_id) REFERENCES c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51239	c_location_pkey	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_location_id)	CP	\N	2022-11-02 05:57:56.041854	100
51240	adclient_clocation	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51241	adorg_clocation	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51242	ccity_clocation	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_city_id) REFERENCES c_city(c_city_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51243	c_region_location	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_region_id) REFERENCES c_region(c_region_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51244	c_country_location	0	0	162	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_country_id) REFERENCES c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51245	c_nonbusinessday_pkey	0	0	163	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_nonbusinessday_id)	CP	\N	2022-11-02 05:57:56.041854	100
51246	c_nonbusinesdaysorg	0	0	163	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51247	c_nonbusinesdaysclient	0	0	163	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51248	c_calendarnonbusinessday	0	0	163	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_calendar_id) REFERENCES c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51324	c_orgassignment_pkey	0	0	585	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_orgassignment_id)	CP	\N	2022-11-02 05:57:56.041854	100
51325	adorg_corgassignment	0	0	585	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51326	aduser_corgassignment	0	0	585	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51354	c_paymentprocessor_pkey	0	0	398	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_paymentprocessor_id)	CP	\N	2022-11-02 05:57:56.041854	100
51355	cbankaccount_cpaymtprocessor	0	0	398	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_bankaccount_id) REFERENCES c_bankaccount(c_bankaccount_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51356	ccurrency_cpaymentprocessor	0	0	398	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51357	adsequence_cpaymentprocessor	0	0	398	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_sequence_id) REFERENCES ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51376	c_period_nounique	0	0	145	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (c_year_id, periodno)	CU	\N	2022-11-02 05:57:56.041854	100
51377	c_period_pkey	0	0	145	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_period_id)	CP	\N	2022-11-02 05:57:56.041854	100
51378	c_year_period	0	0	145	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_year_id) REFERENCES c_year(c_year_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51379	c_periodclient	0	0	145	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51380	c_periodorg	0	0	145	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51381	c_periodcontrol_pkey	0	0	229	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_periodcontrol_id)	CP	\N	2022-11-02 05:57:56.041854	100
51382	c_period_periodcontrol	0	0	229	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_period_id) REFERENCES c_period(c_period_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51437	c_recurring_pkey	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_recurring_id)	CP	\N	2022-11-02 05:57:56.041854	100
51438	cinvoice_crecurring	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_invoice_id) REFERENCES c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51439	cproject_crecurring	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_project_id) REFERENCES c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51440	cpayment_crecurring	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_payment_id) REFERENCES c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51441	gljournalbatch_crecurring	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (gl_journalbatch_id) REFERENCES gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51442	corder_crecurring	0	0	574	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_order_id) REFERENCES c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51443	c_recurring_document_key	0	0	53330	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_recurring_document_id)	CP	\N	2022-11-02 05:57:56.041854	100
51444	c_recurring_run_pkey	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_recurring_run_id)	CP	\N	2022-11-02 05:57:56.041854	100
51445	cinvoice_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_invoice_id) REFERENCES c_invoice(c_invoice_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51446	crecurring_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_recurring_id) REFERENCES c_recurring(c_recurring_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51447	corder_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_order_id) REFERENCES c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51448	cpayment_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_payment_id) REFERENCES c_payment(c_payment_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51449	cproject_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_project_id) REFERENCES c_project(c_project_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51450	gljournalbatch_crecurringrun	0	0	573	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (gl_journalbatch_id) REFERENCES gl_journalbatch(gl_journalbatch_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51451	c_region_name	0	0	164	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (c_country_id, name)	CU	\N	2022-11-02 05:57:56.041854	100
51452	c_region_pkey	0	0	164	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_region_id)	CP	\N	2022-11-02 05:57:56.041854	100
51453	c_regionorg	0	0	164	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51454	c_regionclient	0	0	164	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51455	ccountry_cregion	0	0	164	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_country_id) REFERENCES c_country(c_country_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51456	c_remuneration_pkey	0	0	792	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_remuneration_id)	CP	\N	2022-11-02 05:57:56.041854	100
51597	c_userremuneration_pkey	0	0	794	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_userremuneration_id)	CP	\N	2022-11-02 05:57:56.041854	100
51598	aduser_cuserremuneration	0	0	794	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51599	cremuneration_cuserrem	0	0	794	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_remuneration_id) REFERENCES c_remuneration(c_remuneration_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51625	c_year_name	0	0	177	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	UNIQUE (c_calendar_id, fiscalyear)	CU	\N	2022-11-02 05:57:56.041854	100
51626	c_year_pkey	0	0	177	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (c_year_id)	CP	\N	2022-11-02 05:57:56.041854	100
51627	c_yearorg	0	0	177	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51628	c_yearclient	0	0	177	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51629	c_calendar_year	0	0	177	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_calendar_id) REFERENCES c_calendar(c_calendar_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51630	etl_setup_key	0	0	54050	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (etl_setup_id)	CP	\N	2022-11-02 05:57:56.041854	100
51631	etl_setupline_key	0	0	54051	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (etl_setupline_id)	CP	\N	2022-11-02 05:57:56.041854	100
51791	i_elementvalue_pkey	0	0	534	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (i_elementvalue_id)	CP	\N	2022-11-02 05:57:56.041854	100
51792	adcolumn_ielementvalue	0	0	534	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_column_id) REFERENCES ad_column(ad_column_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51793	celementvalue_ielementvalue	0	0	534	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_elementvalue_id) REFERENCES c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51794	celement_ielementvalue	0	0	534	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (c_element_id) REFERENCES c_element(c_element_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51795	cevalueparent_ielementvalue	0	0	534	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (parentelementvalue_id) REFERENCES c_elementvalue(c_elementvalue_id) ON DELETE SET NULL DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52452	pa_dashboardcontent_key	0	0	50010	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (pa_dashboardcontent_id)	CP	\N	2022-11-02 05:57:56.041854	100
52453	adwindow_padashboardcontent	0	0	50010	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_window_id) REFERENCES ad_window(ad_window_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52454	pagoal_padashboardcontent	0	0	50010	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (pa_goal_id) REFERENCES pa_goal(pa_goal_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52455	pa_dashboardcontent_trl_key	0	0	53295	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (ad_language, pa_dashboardcontent_id)	CP	\N	2022-11-02 05:57:56.041854	100
52456	adlangu_padashboardcontenttrl	0	0	53295	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (ad_language) REFERENCES ad_language(ad_language) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52457	padashboardcontent_padashboard	0	0	53295	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (pa_dashboardcontent_id) REFERENCES pa_dashboardcontent(pa_dashboardcontent_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52727	test_pkey	0	0	135	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (test_id)	CP	\N	2022-11-02 05:57:56.041854	100
52728	vc_account_test	0	0	135	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	FOREIGN KEY (account_acct) REFERENCES c_validcombination(c_validcombination_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52772	u_pa_documentstatus_key	0	0	53898	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (u_pa_documentstatus_id)	CP	\N	2022-11-02 05:57:56.041854	100
52773	x_authoritytype_key	0	0	53935	2022-11-02 05:57:56.041854	100	System Admin	\N	Y	PRIMARY KEY (x_authoritytype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52878	ad_user_email	0	0	114	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (email)	I	\N	2022-11-02 06:15:05.061147	100
52879	ad_pinstance_record	0	0	282	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (ad_process_id, record_id)	I	\N	2022-11-02 06:15:05.061147	100
52880	c_bankacct_bank	0	0	297	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (c_bank_id)	I	\N	2022-11-02 06:15:05.061147	100
52881	ad_printformatitem_format	0	0	489	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (ad_printformat_id)	I	\N	2022-11-02 06:15:05.061147	100
52882	ad_printformat_table	0	0	493	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (ad_table_id)	I	\N	2022-11-02 06:15:05.061147	100
52883	fki_adattachment_entry	0	0	400001	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (ad_attachment_id)	I	\N	2022-11-02 06:15:05.061147	100
52884	ad_eventlog_record	0	0	400002	2022-11-02 06:15:05.061147	100	System Admin	\N	Y	 USING btree (ad_table_id, record_id)	I	\N	2022-11-02 06:15:05.061147	100
52978	ad_accesslog_isactive_check	0	0	717	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52988	ad_attachment_isactive_check	0	0	254	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52989	ad_attachmententry_isactive_check	0	0	400001	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52990	ad_attachmentnote_isactive_check	0	0	705	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53003	ad_client_iserrorshowdialog_check	0	0	112	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iserrorshowdialog = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53004	ad_client_issmtptls_check	0	0	112	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issmtptls = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53005	ad_client_iswarningshowdialog_check	0	0	112	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iswarningshowdialog = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53006	ad_client_postautoconsolelims_check	0	0	112	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((postautoconsolelims = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53007	ad_client_isactive_check	0	0	112	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53008	ad_clientinfo_isdiscountlineamt_check	0	0	227	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdiscountlineamt = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53009	ad_clientinfo_isactive_check	0	0	227	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53010	ad_clientshare_isactive_check	0	0	827	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53011	ad_color_isactive_check	0	0	457	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53043	a_depreciation_forecast_isactive_check	0	0	53118	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53044	a_depreciation_forecast_processed_check	0	0	53118	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53053	ad_error_isactive_check	0	0	380	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53054	ad_eventlog_isactive_check	0	0	400002	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53072	ad_find_isactive_check	0	0	404	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53081	ad_image_isactive_check	0	0	461	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53110	ad_issue_processed_check	0	0	828	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53111	ad_issue_isactive_check	0	0	828	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53112	ad_issue_isreproducible_check	0	0	828	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isreproducible = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53113	ad_issue_isvanillasystem_check	0	0	828	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isvanillasystem = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53114	ad_language_issystemlanguage_check	0	0	111	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issystemlanguage = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53115	ad_language_isactive_check	0	0	111	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53116	ad_language_isbaselanguage_check	0	0	111	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isbaselanguage = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53119	ad_menu_issotrx_check	0	0	116	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issotrx = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53120	ad_menu_isactive_check	0	0	116	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53121	ad_menu_iscentrallymaintained_check	0	0	116	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iscentrallymaintained = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53122	ad_menu_isreadonly_check	0	0	116	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isreadonly = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53123	ad_menu_trl_isactive_check	0	0	120	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53124	ad_menu_trl_istranslated_check	0	0	120	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53130	ad_note_isactive_check	0	0	389	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53131	ad_note_processed_check	0	0	389	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53132	ad_org_isactive_check	0	0	155	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53133	ad_org_isconsoladjustorg_check	0	0	155	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isconsoladjustorg = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53134	ad_orginfo_isactive_check	0	0	228	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53135	ad_pinstance_isprocessing_check	0	0	282	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isprocessing = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53136	ad_preference_isactive_check	0	0	195	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53137	ad_printcolor_isactive_check	0	0	490	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53138	ad_printconfig_isvisible_check	0	0	53961	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isvisible = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53139	ad_printconfig_isactive_check	0	0	53961	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53140	ad_printconfig_isdirectprint_check	0	0	53961	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdirectprint = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53141	ad_printfont_isembeddedfont_check	0	0	491	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isembeddedfont = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53142	ad_printfont_isactive_check	0	0	491	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53143	ad_printform_isactive_check	0	0	454	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53144	ad_printformat_isform_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isform = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53145	ad_printformat_istablebased_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istablebased = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53146	ad_printformat_isactive_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53147	ad_printformat_isprintparameters_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isprintparameters = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53148	ad_printformat_isnewpageafterrh_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isnewpageafterrh = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53149	ad_printformat_isstandardheaderfooter_check	0	0	493	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isstandardheaderfooter = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53150	ad_printformatitem_iscalculated_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iscalculated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53151	ad_printformatitem_isactive_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53152	ad_printformatitem_isdesc_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdesc = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53153	ad_printformatitem_isgroupby_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isgroupby = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53154	ad_printformatitem_isheightoneline_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isheightoneline = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53155	ad_printformatitem_isnextline_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isnextline = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53156	ad_printformatitem_isorderby_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isorderby = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53157	ad_printformatitem_ispagebreak_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((ispagebreak = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53158	ad_printformatitem_isprintbarcodetext_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isprintbarcodetext = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53159	ad_printformatitem_isprinted_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isprinted = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53160	ad_printformatitem_isrelativeposition_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isrelativeposition = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53161	ad_printformatitem_issummarized_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issummarized = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53162	ad_printformatitem_issuppressrepeats_check	0	0	489	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issuppressrepeats = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53163	ad_printformatitem_trl_isactive_check	0	0	522	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53164	ad_printformatitem_trl_istranslated_check	0	0	522	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53165	ad_printgraph_isactive_check	0	0	521	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53166	ad_printpaper_isactive_check	0	0	492	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53167	ad_printpaper_islandscape_check	0	0	492	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((islandscape = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53168	ad_printtableformat_isactive_check	0	0	523	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53169	ad_printtableformat_ismultilineheader_check	0	0	523	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((ismultilineheader = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53196	ad_registration_isallowpublish_check	0	0	625	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isallowpublish = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53197	ad_registration_isactive_check	0	0	625	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53198	ad_registration_isallowstatistics_check	0	0	625	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isallowstatistics = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53199	ad_registration_isregistered_check	0	0	625	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isregistered = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53200	ad_registration_isinproduction_check	0	0	625	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isinproduction = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53201	ad_relationtype_isdirected_check	0	0	53246	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdirected = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53202	ad_relationtype_isactive_check	0	0	53246	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53226	ad_sysconfig_isactive_check	0	0	50009	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53227	ad_system_isfailonmissingmodelvalidator_check	0	0	531	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isfailonmissingmodelvalidator = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53228	ad_system_isfailonbuilddiffer_check	0	0	531	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isfailonbuilddiffer = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53229	ad_system_isactive_check	0	0	531	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53270	ad_tree_isactive_check	0	0	288	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53271	ad_tree_isallnodes_check	0	0	288	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isallnodes = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53286	ad_user_isactive_check	0	0	114	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53287	ad_user_iswebstoreuser_check	0	0	114	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iswebstoreuser = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53288	ad_user_issaleslead_check	0	0	114	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((issaleslead = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53289	ad_user_isinpayroll_check	0	0	114	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isinpayroll = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53290	ad_user_isdefaultcontact_check	0	0	114	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdefaultcontact = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53305	ad_usermail_isactive_check	0	0	782	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53324	ad_workflow_trl_isactive_check	0	0	133	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53325	ad_workflow_trl_istranslated_check	0	0	133	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53359	c_bank_isactive_check	0	0	296	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53360	c_bank_isownbank_check	0	0	296	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isownbank = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53361	c_bankaccount_isactive_check	0	0	297	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53362	c_bankaccount_acct_isactive_check	0	0	391	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53363	c_bankaccountdoc_isactive_check	0	0	455	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53370	c_bankstatementloader_isactive_check	0	0	640	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53371	c_bankstatementmatcher_isactive_check	0	0	658	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53412	c_calendar_isactive_check	0	0	139	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53433	c_city_isactive_check	0	0	186	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53448	c_country_ispostcodelookup_check	0	0	170	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((ispostcodelookup = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53449	c_country_hasregion_check	0	0	170	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((hasregion = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53450	c_country_isactive_check	0	0	170	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53451	c_country_haspostal_add_check	0	0	170	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((haspostal_add = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53452	c_country_allowcitiesoutoflist_check	0	0	170	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((allowcitiesoutoflist = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53453	c_country_trl_istranslated_check	0	0	616	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53454	c_country_trl_isactive_check	0	0	616	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53540	c_interorg_acct_isactive_check	0	0	397	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53571	c_jobremuneration_isactive_check	0	0	793	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53572	c_location_isactive_check	0	0	162	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53573	c_nonbusinessday_isactive_check	0	0	163	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53607	c_paymentprocessor_acceptdiscover_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptdiscover = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53608	c_paymentprocessor_acceptmc_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptmc = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53609	c_paymentprocessor_acceptvisa_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptvisa = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53610	c_paymentprocessor_requirevv_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((requirevv = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53611	c_paymentprocessor_acceptatm_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptatm = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53612	c_paymentprocessor_acceptamex_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptamex = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53613	c_paymentprocessor_acceptcorporate_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptcorporate = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53614	c_paymentprocessor_acceptdiners_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptdiners = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53615	c_paymentprocessor_isactive_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53616	c_paymentprocessor_acceptcheck_check	0	0	398	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((acceptcheck = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53634	c_period_isactive_check	0	0	145	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53635	c_periodcontrol_isactive_check	0	0	229	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53646	c_recurring_isactive_check	0	0	574	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53647	c_recurring_document_isoverriderunlimit_check	0	0	53330	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isoverriderunlimit = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53648	c_recurring_document_isactive_check	0	0	53330	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53649	c_recurring_run_isactive_check	0	0	573	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53650	c_region_isdefault_check	0	0	164	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdefault = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53651	c_region_isactive_check	0	0	164	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53652	c_remuneration_isactive_check	0	0	792	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53714	c_userremuneration_isactive_check	0	0	794	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53723	c_year_isactive_check	0	0	177	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53724	etl_setup_isactive_check	0	0	54050	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53725	etl_setupline_isactive_check	0	0	54051	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53829	i_elementvalue_postbudget_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((postbudget = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53830	i_elementvalue_postactual_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((postactual = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53831	i_elementvalue_isdoccontrolled_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isdoccontrolled = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53832	i_elementvalue_poststatistical_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((poststatistical = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53833	i_elementvalue_processed_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53834	i_elementvalue_isactive_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53835	i_elementvalue_postencumbrance_check	0	0	534	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((postencumbrance = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54098	pa_dashboardcontent_iscollapsible_check	0	0	50010	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((iscollapsible = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54099	pa_dashboardcontent_isactive_check	0	0	50010	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54100	pa_dashboardcontent_trl_isactive_check	0	0	53295	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54101	pa_dashboardcontent_trl_istranslated_check	0	0	53295	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((istranslated = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54255	test_processed_check	0	0	135	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54256	test_isactive_check	0	0	135	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54267	u_pa_documentstatus_isactive_check	0	0	53898	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54268	x_authoritytype_isactive_check	0	0	53935	2022-11-02 12:40:39.392787	100	System Admin	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
