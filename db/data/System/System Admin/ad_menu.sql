copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
108	Location	W	0	\N	\N	0	\N	121	1999-06-11 00:00:00	0	Maintain Location Address	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
109	Country Region and City	W	0	\N	\N	0	\N	122	1999-06-11 00:00:00	0	Maintain Countries Regions and Cities	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
113	Test	W	0	\N	\N	0	\N	127	1999-06-21 00:00:00	0	Test Screen	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
114	Attachment	W	0	\N	\N	0	\N	128	1999-06-29 00:00:00	0	Maintain Attachments	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
115	Preference	W	0	\N	\N	0	\N	129	1999-06-29 00:00:00	0	Maintain System Client Org and User Preferences	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
144	Menu	W	0	\N	\N	0	\N	105	1999-05-21 00:00:00	0	Maintain Menu	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
145	Language	W	0	\N	\N	0	\N	106	1999-05-21 00:00:00	0	Maintain Languages	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
147	User	W	0	\N	\N	0	\N	108	1999-06-07 00:00:00	0	Maintain Users of the system	System Admin	Y	Y	N	Y	N	2005-11-22 11:37:02	100
148	Client	W	0	\N	\N	0	\N	109	1999-06-11 00:00:00	0	Maintain Clients/Tenants	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
149	Organisation	W	0	\N	\N	0	\N	110	1999-06-11 00:00:00	0	Maintain Organisations	System Admin	Y	Y	N	Y	N	2016-04-11 16:07:05	0
150	Role	W	0	\N	\N	0	\N	111	1999-06-11 00:00:00	0	Maintain User Responsibilities	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
156	Client Rules	\N	0	\N	\N	0	\N	\N	1999-11-27 20:32:33	0	Maintain Client Rules	System Admin	Y	N	N	Y	Y	2000-01-02 00:00:00	0
159	Utility	\N	0	\N	\N	0	\N	\N	1999-11-27 21:17:25	0	\N	System Admin	Y	N	N	Y	Y	2000-01-02 00:00:00	0
161	System Rules	\N	0	\N	\N	0	\N	\N	1999-12-02 13:01:22	0	General System Rules	System Admin	Y	N	N	Y	Y	2000-01-02 00:00:00	0
170	Tree	W	0	\N	\N	0	\N	163	1999-12-09 09:29:18	0	Maintain Tree definition	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
171	Bank	W	0	\N	\N	0	\N	158	1999-12-04 21:30:21	0	Maintain Bank	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
175	Organisation Rules	\N	0	\N	\N	0	\N	\N	1999-12-09 10:12:39	0	\N	System Admin	Y	N	N	Y	Y	2016-04-11 16:07:05	0
218	System Admin	\N	0	\N	\N	0	\N	\N	2000-09-04 14:06:15	0	\N	System Admin	Y	N	N	N	Y	2000-01-02 00:00:00	0
221	Error Message	W	0	\N	\N	0	\N	188	2000-09-15 14:51:56	0	Display Error Messages	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
225	Initial Client Setup	X	0	102	\N	0	\N	\N	2000-09-25 11:11:29	0	Initial new Client/Tenant Setup	System Admin	N	Y	N	N	N	2009-09-22 14:02:30	100
233	Notice	W	0	\N	\N	0	\N	193	2000-12-18 22:11:17	0	View System Notices	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
290	Find (indirect use)	W	0	\N	\N	0	\N	221	2001-07-22 11:16:07	0	Find Dialog (indirect use)	System Admin	N	Y	N	Y	N	2000-01-02 00:00:00	0
293	Print Form	W	0	\N	\N	0	\N	224	2001-07-28 19:50:25	0	Maintain Print Forms (Invoices, Checks, ..) used	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
294	System Color	W	0	\N	\N	0	\N	225	2001-09-05 21:23:37	0	Maintain System Colors	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
296	System Image	W	0	\N	\N	0	\N	227	2001-09-05 21:50:55	0	Maintain Images and Icons	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
303	Synchronise Terminology	P	0	\N	\N	0	172	\N	2001-05-13 12:17:26	0	Synchronise the terminology within the system.	System Admin	Y	Y	N	Y	N	2016-04-11 16:07:05	0
322	Print Format	W	0	\N	\N	0	\N	240	2002-07-11 19:01:44	0	Maintain Print Format	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
323	Print Font	W	0	\N	\N	0	\N	239	2002-07-11 18:57:07	0	Maintain Print Font	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
324	Print Color	W	0	\N	\N	0	\N	238	2002-07-11 18:51:42	0	Maintain Print Color	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
325	Print Paper	W	0	\N	\N	0	\N	241	2002-07-11 19:15:33	0	Maintain Print Paper	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
326	Printing	\N	0	\N	\N	0	\N	\N	2002-07-11 22:00:40	0	Print Definition	System Admin	Y	N	N	N	Y	2000-01-02 00:00:00	0
331	Print Table Format	W	0	\N	\N	0	\N	243	2002-08-24 16:57:43	0	Define Report Table Format	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
334	System	W	0	\N	\N	0	\N	246	2002-11-01 21:02:17	0	System Definition	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
336	Translation Import/Export	X	0	109	\N	0	\N	\N	2003-01-04 15:22:01	0	Import or Export Language Translation	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
341	Translation Check	W	0	\N	\N	0	\N	250	2003-01-15 16:04:50	0	Check System Language Translations	System Admin	Y	Y	N	N	N	2022-02-17 22:55:49	100
361	Recurring	W	0	\N	\N	0	\N	266	2003-05-28 22:25:58	0	Recurring Document	System Admin	N	Y	N	Y	N	2022-02-17 23:00:12	100
362	Role Data Access	W	0	\N	\N	0	\N	268	2003-05-28 22:59:19	0	Maintain Data Access Rules	System Admin	N	Y	N	Y	N	2022-02-17 22:56:57	100
366	Session Audit	W	0	\N	\N	0	\N	264	2003-05-28 21:55:18	0	Audit of User Sessions	System Admin	Y	Y	N	Y	N	2000-01-02 00:00:00	0
367	Security	\N	0	\N	\N	0	\N	\N	2003-05-29 00:11:06	0	\N	System Admin	Y	N	N	N	Y	2000-01-02 00:00:00	0
383	Cache Reset	P	0	\N	\N	0	205	\N	2003-06-16 00:31:55	0	Reset Cache of the System ** Close all Windows before proceeding **	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
397	Merge Entities	X	0	112	\N	0	\N	\N	2003-08-09 13:11:49	0	Merge From Entity to To Entity - Delete From	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
437	Bank Statement Matcher	W	0	\N	\N	0	\N	302	2004-01-25 13:59:06	0	Algorithm to match Bank Statement Info to Business Partners, Invoices and Payments	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
441	Organisation Type	W	0	\N	\N	0	\N	304	2004-02-19 10:49:23	0	Maintain Organisation Types	System Admin	Y	Y	N	Y	N	2016-04-11 16:07:05	0
461	Sequence Check	P	0	\N	\N	0	258	\N	2004-03-05 23:41:29	0	Check System and Document Sequences	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
465	Tree Maintenance	X	0	115	\N	0	\N	\N	2004-03-19 17:14:47	0	Maintain Trees	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
475	Access Audit	W	0	\N	\N	0	\N	326	2004-04-13 14:05:05	0	Audit of Access to data or resources	System Admin	Y	Y	N	N	N	2008-05-30 17:40:14	100
483	Process Audit	W	0	\N	\N	0	\N	332	2004-06-15 09:43:13	0	Audit process use	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
485	Print Format Detail	R	0	\N	\N	0	287	\N	2004-06-18 16:04:07	0	Print Format Detail Report	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
487	Reset Password	P	0	\N	\N	0	288	\N	2004-07-04 12:16:20	0	Reset Passwords for User	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
495	Role Access Update	P	0	\N	\N	0	295	\N	2004-07-21 23:22:43	0	Update the access rights of a role or roles of a client	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
498	System Registration	W	0	\N	\N	0	\N	340	2004-09-24 20:00:48	0	Register your System	System Admin	Y	Y	N	N	N	2000-01-02 00:00:00	0
508	Archive Viewer	X	0	118	\N	0	\N	\N	2005-01-10 20:55:59	100	View automatically archived Documents	System Admin	Y	Y	N	Y	N	2005-01-10 20:55:59	100
514	Sync Translation	P	0	\N	\N	0	321	\N	2005-03-10 22:06:12	100	Synchronise Document Translation	System Admin	Y	Y	N	N	N	2022-02-17 22:55:39	100
532	Remuneration	W	0	\N	\N	0	\N	353	2005-05-15 02:33:44	100	Maintain Remuneration	System Admin	Y	Y	N	N	N	2005-05-15 02:33:44	100
552	System Issue Report	W	0	\N	\N	0	\N	363	2005-12-12 17:26:31	100	Automatically created or manually entered System Issue Reports	System Admin	N	Y	N	N	N	2020-10-16 23:34:57	100
50007	Copy Role	P	0	\N	\N	0	50010	\N	2006-12-12 00:17:45	0	Copy Role	System Admin	Y	Y	N	N	N	2009-08-11 12:43:24	100
50008	System Configurator	W	0	\N	\N	0	\N	50006	2007-02-28 01:46:54	100	\N	System Admin	Y	Y	N	N	N	2007-02-28 01:46:54	100
50010	Dashboard Content Edit	W	0	\N	\N	0	\N	50007	2007-07-09 00:00:00	100	\N	System Admin	Y	Y	N	N	N	2007-07-09 00:00:00	100
53193	Enable Native Sequence	P	0	\N	\N	0	53156	\N	2008-09-08 22:24:03	0	Enable Native Sequence	System Admin	Y	Y	N	N	N	2008-09-08 22:24:03	0
53202	Initial Client Setup Process	P	0	\N	\N	0	53161	\N	2009-02-14 10:27:03	100	\N	System Admin	Y	Y	N	N	N	2009-02-14 10:27:03	100
53225	My Unprocessed Documents	W	0	\N	\N	0	\N	53086	2009-07-24 12:49:37	100	My Unprocessed Documents	System Admin	Y	Y	N	N	N	2009-07-24 13:07:49	100
53226	Unprocessed Documents (All)	W	0	\N	\N	0	\N	53087	2009-07-24 13:09:26	100	Unprocessed Documents (All)	System Admin	Y	Y	N	N	N	2009-07-24 13:09:26	100
53246	My Profile	W	0	\N	\N	0	\N	53100	2009-09-12 14:31:37	100	My user information	System Admin	Y	Y	N	Y	N	2009-09-12 14:31:37	100
53251	Relation Type	W	0	\N	\N	0	\N	53102	2009-11-13 15:22:45	100	\N	System Admin	Y	Y	N	N	N	2009-11-13 15:22:45	100
53348	Convert passwords to hashes	P	0	\N	\N	0	53259	\N	2011-06-29 17:00:27	100	Convert existing plain text/encrypted user passwords to one way hash	System Admin	N	Y	N	N	N	2022-02-17 22:56:29	100
53355	Generate Test Data	P	0	\N	\N	0	53268	\N	2011-08-04 17:21:46	100	Generate random documents for test/demonstration	System Admin	Y	Y	N	N	N	2011-08-04 17:21:46	100
53357	Delete Entities	X	0	53024	\N	0	\N	\N	2011-08-25 15:22:39	100	Delete From Entity to To Entity - Delete From	System Admin	Y	Y	N	N	N	2011-08-25 15:22:39	100
53752	Clean Up Garden World	P	0	\N	\N	0	53559	\N	2014-01-20 16:19:59	100	\N	System Admin	Y	Y	N	N	N	2014-01-20 16:19:59	100
53897	Document Status Indicator	W	0	\N	\N	0	\N	53386	2014-12-15 17:37:48	100	\N	System Admin	Y	N	N	N	N	2016-04-11 16:44:32	0
53933	Authority Type	W	0	\N	\N	0	\N	53401	2015-01-29 14:16:50	100	Authority Type	System Admin	N	Y	N	N	N	2022-02-17 22:56:52	100
53976	Print Config	W	0	\N	\N	0	\N	53409	2015-04-08 11:52:46	100	\N	System Admin	Y	Y	N	N	N	2015-04-08 11:52:46	100
54021	ETL Setup	W	0	\N	\N	0	\N	53462	2015-10-27 16:42:34	100	\N	System Admin	Y	Y	N	N	N	2015-10-27 16:42:34	100
54142	Copy Binary Objects	P	0	\N	\N	0	53929	\N	2017-03-03 15:29:56	100	Copy binary objects from current storage to specified destination	System Admin	Y	Y	N	N	N	2017-03-03 15:29:56	100
54150	Convert Column to Case Insensitive	P	0	\N	\N	0	53937	\N	2017-03-28 18:16:21	100	Make Column case insensitive	System Admin	Y	Y	N	N	N	2017-03-28 18:16:21	100
\.
