copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "istscapable", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
381	AD_ImpFormat	Import Format	6	0	0	\N	189	\N	2000-09-15 14:45:03	0	\N	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
382	AD_ImpFormat_Row	Format Field	6	0	0	\N	189	\N	2000-09-15 14:45:03	0	\N	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-11-23 09:28:21	100
532	I_Product	Import Product	2	0	0	\N	247	\N	2003-01-11 14:57:26	0	Import Item or Service	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
535	I_ReportLine	Import Report Line Set	6	0	0	\N	249	\N	2003-01-11 14:57:27	0	Import Report Line Set values	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
572	I_Inventory	Import Inventory	2	0	0	\N	267	\N	2003-05-28 21:35:14	0	Import Inventory Transactions	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
591	I_Order	Import Order	2	0	0	\N	281	\N	2003-06-07 19:48:39	0	Import Orders	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
597	I_Payment	Import Payment	2	0	0	\N	280	\N	2003-06-07 19:48:39	0	Import Payment	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
598	I_Invoice	Import Invoice	2	0	0	\N	279	\N	2003-06-07 19:48:39	0	Import Invoice	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
599	I_GLJournal	Import GL Journal	2	0	0	\N	278	\N	2003-06-07 19:48:39	0	Import General Ledger Journal	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
600	I_BankStatement	Import Bank Statement	2	0	0	\N	277	\N	2003-06-07 19:48:39	0	Import of the Bank Statement	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
641	I_Conversion_Rate	Import Conversion Rate	6	0	0	\N	296	\N	2003-12-29 18:34:20	0	Import Currency Conversion Rate	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
740	I_InOutLineConfirm	Ship/Receipt Confirmation Import Line	2	0	0	\N	334	\N	2004-07-02 14:14:36	0	Material Shipment or Receipt Confirmation Import Line	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
53173	I_PriceList	Import Price List	2	0	0	\N	53071	\N	2009-03-17 23:16:52	100	Import Price List and Versions	Data Import	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2009-03-17 23:16:52	100
53367	AD_Import	Data Import	4	0	0	\N	\N	N	2012-02-28 16:23:41	100	Data Import definition	Data Import	Defines process to load data from staging table into database.	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2012-02-28 16:23:41	100
53368	AD_ImportValidation	Import Validation	4	0	0	\N	\N	N	2012-02-28 16:38:16	100	Import Validation SQL	Data Import	SQL to validate the source table data prior to loading into the destination table.	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2012-02-28 16:38:16	100
53369	AD_ImportColumn	Import Column Mapping	4	0	0	\N	53172	N	2012-02-28 16:55:29	100	Import Column Mapping	Data Import	Mapping of source column to target column.	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2012-06-08 15:18:13	0
53372	I_User	User/Contact import	3	0	0	\N	\N	N	2012-03-13 15:31:53	100	\N	Data Import	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2012-03-13 15:31:53	100
53618	I_Budget	I_Budget_ID	3	0	0	\N	\N	N	2013-10-11 16:23:56	100	\N	Data Import	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2013-10-11 16:23:56	100
53818	I_Imp_Invoice_Payment_V	I_Imp_Invoice_Payment_V	4	0	0	\N	\N	N	2014-07-04 14:08:51	100	\N	Data Import	\N	N	Y	Y	N	N	N	N	N	Y	0	\N	L	\N	2016-04-11 16:38:03	0
54176	I_Column	Import Column ID	4	0	0	\N	53499	N	2016-12-02 14:24:36	100	\N	Data Import	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2019-02-04 13:39:07	0
\.
