copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50564	ad_table_edi_key	0	0	53420	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (ad_table_edi_id)	CP	\N	2022-11-02 05:57:56.041854	100
50878	c_bpartner_edi_key	0	0	53421	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_bpartner_edi_id)	CP	\N	2022-11-02 05:57:56.041854	100
50898	c_bp_edi_pkey	0	0	366	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_bp_edi_id)	CP	\N	2022-11-02 05:57:56.041854	100
50899	mwarehouse_cbpedi	0	0	366	2022-11-02 05:57:56.041854	100	EDI	\N	Y	FOREIGN KEY (m_warehouse_id) REFERENCES m_warehouse(m_warehouse_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50900	c_bpartner_cbpedi	0	0	366	2022-11-02 05:57:56.041854	100	EDI	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50901	adsequence_cbpedi	0	0	366	2022-11-02 05:57:56.041854	100	EDI	\N	Y	FOREIGN KEY (ad_sequence_id) REFERENCES ad_sequence(ad_sequence_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51101	c_edi_ack_key	0	0	53428	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_edi_ack_id)	CP	\N	2022-11-02 05:57:56.041854	100
51102	c_edi_ackline_key	0	0	53429	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_edi_ackline_id)	CP	\N	2022-11-02 05:57:56.041854	100
51103	c_edi_doctype_key	0	0	53422	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_edi_doctype_id)	CP	\N	2022-11-02 05:57:56.041854	100
51104	c_ediformat_key	0	0	53423	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediformat_id)	CP	\N	2022-11-02 05:57:56.041854	100
51105	c_ediformat_line_key	0	0	53424	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediformat_line_id)	CP	\N	2022-11-02 05:57:56.041854	100
51106	c_ediformat_lineelement_key	0	0	53425	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediformat_lineelement_id)	CP	\N	2022-11-02 05:57:56.041854	100
51107	c_ediformat_lineelementcom_key	0	0	53426	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediformat_lineelementcomp_id)	CP	\N	2022-11-02 05:57:56.041854	100
51108	c_edi_interchange_key	0	0	53430	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_edi_interchange_id)	CP	\N	2022-11-02 05:57:56.041854	100
51109	c_edi_message_key	0	0	53431	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_edi_message_id)	CP	\N	2022-11-02 05:57:56.041854	100
51110	c_ediprocessor_key	0	0	53418	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediprocessor_id)	CP	\N	2022-11-02 05:57:56.041854	100
51111	c_ediprocessor_parameter_key	0	0	53419	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediprocessor_parameter_id)	CP	\N	2022-11-02 05:57:56.041854	100
51112	c_ediprocessortype_key	0	0	53427	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (c_ediprocessortype_id)	CP	\N	2022-11-02 05:57:56.041854	100
52017	m_edi_key	0	0	367	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (m_edi_id)	CP	\N	2022-11-02 05:57:56.041854	100
52018	m_edi_info_key	0	0	368	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (m_edi_info_id)	CP	\N	2022-11-02 05:57:56.041854	100
52776	x_inoutcons_key	0	0	53432	2022-11-02 05:57:56.041854	100	EDI	\N	Y	PRIMARY KEY (x_inoutcons_id)	CP	\N	2022-11-02 05:57:56.041854	100
53252	ad_table_edi_inbound_check	0	0	53420	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((inbound = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53253	ad_table_edi_isactive_check	0	0	53420	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53385	c_bpartner_edi_isactive_check	0	0	53421	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53386	c_bpartner_edi_inbound_check	0	0	53421	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((inbound = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53398	c_bp_edi_isactive_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53399	c_bp_edi_sendorder_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((sendorder = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53400	c_bp_edi_sendinquiry_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((sendinquiry = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53401	c_bp_edi_receiveorderreply_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((receiveorderreply = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53402	c_bp_edi_receiveinquiryreply_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((receiveinquiryreply = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53403	c_bp_edi_isinfosent_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isinfosent = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53404	c_bp_edi_isaudited_check	0	0	366	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isaudited = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53498	c_edi_ack_issubmitted_check	0	0	53428	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((issubmitted = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53499	c_edi_ack_isactive_check	0	0	53428	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53500	c_edi_ackline_isactive_check	0	0	53429	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53501	c_edi_doctype_isactive_check	0	0	53422	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53502	c_ediformat_isactive_check	0	0	53423	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53503	c_ediformat_isacknowledgement_check	0	0	53423	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isacknowledgement = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53504	c_ediformat_line_isactive_check	0	0	53424	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53505	c_ediformat_line_isackline_check	0	0	53424	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isackline = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53506	c_ediformat_line_isonetomany_check	0	0	53424	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isonetomany = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53507	c_ediformat_line_isreset_check	0	0	53424	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isreset = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53508	c_ediformat_line_ismandatory_check	0	0	53424	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((ismandatory = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53509	c_ediformat_lineelement_ismandatory_check	0	0	53425	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((ismandatory = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53510	c_ediformat_lineelement_isactive_check	0	0	53425	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53511	c_ediformat_lineelementcomp_isactive_check	0	0	53426	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53512	c_ediformat_lineelementcomp_ismandatory_check	0	0	53426	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((ismandatory = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53513	c_edi_interchange_inbound_check	0	0	53430	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((inbound = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53514	c_edi_interchange_isactive_check	0	0	53430	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53515	c_edi_message_isactive_check	0	0	53431	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53516	c_ediprocessor_isactive_check	0	0	53418	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53517	c_ediprocessor_parameter_isactive_check	0	0	53419	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53518	c_ediprocessortype_isactive_check	0	0	53427	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53931	m_edi_processed_check	0	0	367	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53932	m_edi_isactive_check	0	0	367	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53933	m_edi_info_isactive_check	0	0	368	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54271	x_inoutcons_ediacknowledged_check	0	0	53432	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((ediacknowledged = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54272	x_inoutcons_isedisubmitted_check	0	0	53432	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isedisubmitted = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54273	x_inoutcons_isactive_check	0	0	53432	2022-11-02 12:40:39.392787	100	EDI	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
