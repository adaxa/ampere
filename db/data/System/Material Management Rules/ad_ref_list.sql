copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
232	Whole Number .00	0	0	155	1999-08-06 00:00:00	0	Number w/o decimals	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	0
233	No Rounding	0	0	155	1999-08-06 00:00:00	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	N
234	Quarter .25 .50 .75	0	0	155	1999-08-06 00:00:00	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	Q
235	Dime .10, .20, .30, ...	0	0	155	1999-08-06 00:00:00	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	D
236	Nickel .05, .10, .15, ...	0	0	155	1999-08-06 00:00:00	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	5
237	Ten 10.00, 20.00, ..	0	0	155	1999-08-06 00:00:00	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	T
240	Maintain Maximum Level	0	0	164	1999-08-10 00:00:00	0	\N	Material Management Rules	Y	2016-04-11 16:47:03	0	\N	\N	2
242	Manual	0	0	164	1999-08-10 00:00:00	0	\N	Material Management Rules	Y	2016-04-11 16:47:03	0	\N	\N	0
244	Reorder below Minimum Level	0	0	164	1999-08-10 00:00:00	0	\N	Material Management Rules	Y	2016-04-11 16:47:03	0	\N	\N	1
312	Customer Shipment	0	0	189	1999-12-15 19:07:27	0	\N	Material Management Rules	Y	2007-12-17 04:17:28	0	\N	\N	C-
313	Customer Returns	0	0	189	1999-12-15 19:07:43	0	\N	Material Management Rules	Y	2007-12-17 04:17:37	0	\N	\N	C+
314	Vendor Receipts	0	0	189	1999-12-15 19:07:59	0	\N	Material Management Rules	Y	2007-12-17 04:17:47	0	\N	\N	V+
315	Vendor Returns	0	0	189	1999-12-15 19:08:23	0	\N	Material Management Rules	Y	2007-12-17 04:17:59	0	\N	\N	V-
316	Inventory Out	0	0	189	1999-12-15 19:09:57	0	\N	Material Management Rules	Y	2007-12-17 04:18:07	0	\N	\N	I-
317	Inventory In	0	0	189	1999-12-15 19:10:24	0	\N	Material Management Rules	Y	2007-12-17 04:18:15	0	\N	\N	I+
325	List Price	0	0	194	2000-01-28 09:36:46	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	L
326	Standard Price	0	0	194	2000-01-28 09:37:17	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	S
327	Limit (PO) Price	0	0	194	2000-01-28 09:37:50	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	X
349	Movement From	0	0	189	2000-05-24 21:21:54	0	\N	Material Management Rules	Y	2007-12-17 04:18:22	0	\N	\N	M-
350	Movement To	0	0	189	2000-05-24 21:22:46	0	\N	Material Management Rules	Y	2007-12-17 04:18:29	0	\N	\N	M+
391	Production +	0	0	189	2000-10-15 22:32:09	0	\N	Material Management Rules	Y	2007-12-17 04:17:12	0	\N	\N	P+
392	Production -	0	0	189	2000-10-15 22:34:13	0	\N	Material Management Rules	Y	2007-12-17 04:17:20	0	\N	\N	P-
496	Line	0	0	246	2001-12-28 20:07:27	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	L
497	Flat Percent	0	0	247	2001-12-28 20:09:44	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	F
498	Formula	0	0	247	2001-12-28 20:11:11	0	\N	Material Management Rules	N	2000-01-02 00:00:00	0	\N	\N	S
499	Breaks	0	0	247	2001-12-28 20:11:57	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	B
500	Pricelist	0	0	247	2001-12-28 20:12:54	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	P
516	Fixed Price	0	0	194	2002-02-21 16:33:15	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	F
539	Item	0	0	270	2002-10-25 22:46:38	0	\N	Material Management Rules	Y	2016-04-11 16:47:12	0	\N	\N	I
540	Service	0	0	270	2002-10-25 22:47:35	0	\N	Material Management Rules	Y	2016-04-11 16:47:12	0	\N	\N	S
541	Resource	0	0	270	2002-10-25 22:48:11	0	\N	Material Management Rules	Y	2016-04-11 16:47:12	0	\N	\N	R
542	Expense type	0	0	270	2002-10-25 22:48:32	0	\N	Material Management Rules	Y	2016-04-11 16:47:12	0	\N	\N	E
543	Online	0	0	270	2002-10-25 22:48:54	0	\N	Material Management Rules	N	2016-04-11 16:47:12	0	\N	\N	O
544	Currency Precision	0	0	155	2002-12-04 11:56:11	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	C
551	Standard Part	0	0	279	2003-05-21 17:50:24	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	P
552	Optional Part	0	0	279	2003-05-21 17:50:34	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	O
553	In alternative Group 1	0	0	279	2003-05-21 17:51:04	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	1
554	In alternative Group 2	0	0	279	2003-05-21 17:51:52	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	2
555	In alternaltve Group 3	0	0	279	2003-05-21 17:52:16	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	3
556	In alternative Group 4	0	0	279	2003-05-21 17:52:25	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	4
571	General	0	0	288	2003-09-02 18:12:51	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	N
572	Asset Project	0	0	288	2003-09-02 18:13:08	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	A
573	Work Order (Job)	0	0	288	2003-09-02 18:13:35	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	W
574	Service (Charge) Project	0	0	288	2003-09-02 18:14:24	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	S
582	Work Order +	0	0	189	2003-09-03 14:14:36	0	\N	Material Management Rules	Y	2007-12-17 04:18:37	0	\N	\N	W+
583	Work Order -	0	0	189	2003-09-03 14:14:54	0	\N	Material Management Rules	Y	2007-12-17 04:18:44	0	\N	\N	W-
639	In alternative Group 5	0	0	279	2004-01-07 12:20:38	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	5
640	In alternative Group 6	0	0	279	2004-01-07 12:20:49	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	6
641	In alternative Group 7	0	0	279	2004-01-07 12:21:01	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	7
642	In alternative Group 8	0	0	279	2004-01-07 12:21:11	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	8
643	In alternative Group 9	0	0	279	2004-01-07 12:21:21	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	9
657	Web Promotion	0	0	313	2004-02-07 17:50:52	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	P
679	Alternative	0	0	313	2004-05-16 14:14:59	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	A
680	Supplemental	0	0	313	2004-05-16 14:16:14	0	\N	Material Management Rules	Y	2000-01-02 00:00:00	0	\N	\N	S
742	Current Active	0	0	347	2005-05-15 14:29:42	100	\N	Material Management Rules	Y	2007-12-17 01:10:44	0	\N	\N	A
743	Make-To-Order	0	0	347	2005-05-15 14:35:24	100	\N	Material Management Rules	Y	2007-12-17 01:10:52	0	\N	\N	O
744	Previous	0	0	347	2005-05-15 14:35:41	100	\N	Material Management Rules	N	2009-01-12 00:09:17	100	\N	\N	P
745	Previous, Spare	0	0	347	2005-05-15 14:36:04	100	\N	Material Management Rules	N	2009-01-12 00:09:21	100	\N	\N	S
746	Future	0	0	347	2005-05-15 14:36:14	100	\N	Material Management Rules	N	2009-01-12 00:09:09	100	\N	\N	F
747	Maintenance	0	0	347	2005-05-15 14:36:25	100	\N	Material Management Rules	N	2009-01-12 00:09:12	100	\N	\N	M
748	Repair	0	0	347	2005-05-15 14:36:36	100	\N	Material Management Rules	Y	2007-12-17 01:11:48	0	\N	\N	R
749	Master	0	0	348	2005-05-15 14:37:12	100	\N	Material Management Rules	Y	2007-12-17 01:12:33	0	\N	\N	A
750	Engineering	0	0	348	2005-05-15 14:38:06	100	\N	Material Management Rules	Y	2007-12-17 01:12:41	0	\N	\N	E
751	Manufacturing	0	0	348	2005-05-15 14:38:18	100	\N	Material Management Rules	Y	2007-12-17 01:12:55	0	\N	\N	M
752	Planning	0	0	348	2005-05-15 14:38:31	100	\N	Material Management Rules	Y	2007-12-17 01:13:04	0	\N	\N	P
753	Standard Product	0	0	349	2005-05-15 15:54:43	100	\N	Material Management Rules	Y	2005-05-15 15:54:43	100	\N	\N	S
754	Optional Product	0	0	349	2005-05-15 15:54:57	100	\N	Material Management Rules	Y	2005-05-15 16:42:01	100	\N	\N	O
755	Alternative	0	0	349	2005-05-15 15:55:25	100	\N	Material Management Rules	Y	2005-05-15 15:55:25	100	\N	\N	A
756	Alternative (Default)	0	0	349	2005-05-15 15:55:41	100	\N	Material Management Rules	Y	2005-05-15 15:55:41	100	\N	\N	D
757	Outside Processing	0	0	349	2005-05-15 15:55:55	100	\N	Material Management Rules	Y	2005-05-15 16:42:01	100	\N	\N	X
772	Custom	0	0	164	2005-07-21 13:58:04	100	\N	Material Management Rules	Y	2016-04-11 16:47:03	0	\N	\N	9
892	None	0	0	383	2006-03-26 18:40:36	100	\N	Material Management Rules	Y	2006-03-26 18:40:36	100	\N	\N	-
893	Committed Amount	0	0	383	2006-03-26 18:41:10	100	\N	Material Management Rules	Y	2006-03-26 18:41:10	100	\N	\N	C
894	Time&Material max Comitted	0	0	383	2006-03-26 18:41:44	100	\N	Material Management Rules	Y	2006-03-26 18:42:18	100	\N	\N	c
895	Time&Material	0	0	383	2006-03-26 18:42:07	100	\N	Material Management Rules	Y	2006-03-26 18:42:24	100	\N	\N	T
896	Product  Quantity	0	0	383	2006-03-26 19:01:30	100	\N	Material Management Rules	Y	2006-03-26 19:01:30	100	\N	\N	P
897	Project	0	0	384	2006-03-26 20:31:46	100	\N	Material Management Rules	Y	2006-03-26 20:31:46	100	\N	\N	P
898	Phase	0	0	384	2006-03-26 20:32:12	100	\N	Material Management Rules	Y	2006-03-26 20:32:12	100	\N	\N	A
899	Task	0	0	384	2006-03-26 20:32:25	100	\N	Material Management Rules	Y	2006-03-26 20:32:25	100	\N	\N	T
50042	Ending in 9/5	0	0	155	2007-04-26 00:00:00	100	The price ends in either a 5 or 9 whole unit	Material Management Rules	Y	2007-04-26 00:00:00	100	\N	\N	9
53242	Product Configure	0	0	347	2007-12-17 01:11:49	0	\N	Material Management Rules	Y	2007-12-17 01:11:49	0	\N	\N	C
53243	Quality	0	0	348	2007-12-17 01:13:05	0	\N	Material Management Rules	Y	2007-12-17 01:13:05	0	\N	\N	Q
53257	By-Product	0	0	53225	2007-12-17 03:26:22	0	\N	Material Management Rules	Y	2009-06-11 12:06:16	0	\N	\N	BY
53258	Component	0	0	53225	2007-12-17 03:26:22	0	\N	Material Management Rules	Y	2008-02-12 13:45:33	0	\N	\N	CO
53259	Phantom	0	0	53225	2007-12-17 03:26:23	0	\N	Material Management Rules	Y	2008-02-12 13:45:33	0	\N	\N	PH
53260	Packing	0	0	53225	2007-12-17 03:26:24	0	\N	Material Management Rules	Y	2008-02-12 13:45:33	0	\N	\N	PK
53261	Planning	0	0	53225	2007-12-17 03:26:25	0	\N	Material Management Rules	Y	2008-02-12 13:45:34	0	\N	\N	PL
53262	Tools	0	0	53225	2007-12-17 03:26:26	0	\N	Material Management Rules	Y	2008-02-12 13:45:34	0	\N	\N	TL
53263	Option	0	0	53225	2007-12-17 03:26:27	0	\N	Material Management Rules	Y	2008-02-12 13:45:34	0	\N	\N	OP
53264	Variant	0	0	53225	2007-12-17 03:26:28	0	\N	Material Management Rules	Y	2008-02-12 13:45:34	0	\N	\N	VA
53265	Issue	0	0	53226	2007-12-17 03:26:46	0	Components issue manuality	Material Management Rules	Y	2009-01-02 02:32:21	0	\N	\N	0
53266	Backflush	0	0	53226	2007-12-17 03:26:47	0	Components issue in Backflush	Material Management Rules	Y	2009-01-02 02:31:53	0	\N	\N	1
53449	Floor Stock	0	0	53226	2009-01-02 02:31:23	0	Components issue in Floor Stock	Material Management Rules	Y	2009-01-02 02:31:23	0	\N	\N	2
53450	Make-To-Kit	0	0	347	2009-01-12 00:08:45	100	Create a Manufacturing Order, Receipt the finish product and issue the Components automaticaly 	Material Management Rules	Y	2009-01-12 00:35:52	0	\N	\N	K
53462	>=	0	0	53294	2009-04-07 16:55:33	100	\N	Material Management Rules	Y	2009-04-07 16:55:33	100	\N	\N	>=
53463	<=	0	0	53294	2009-04-07 16:55:54	100	\N	Material Management Rules	Y	2009-04-07 16:55:54	100	\N	\N	<=
53464	Min	0	0	53295	2009-04-07 17:15:53	100	\N	Material Management Rules	Y	2009-04-07 17:15:53	100	\N	\N	I
53465	Max	0	0	53295	2009-04-07 17:16:07	100	\N	Material Management Rules	Y	2009-04-07 17:16:07	100	\N	\N	X
53466	Minus	0	0	53295	2009-04-07 17:16:18	100	\N	Material Management Rules	Y	2009-04-07 17:16:18	100	\N	\N	N
53467	Ascending	0	0	53296	2009-04-07 17:20:38	100	\N	Material Management Rules	Y	2009-04-07 17:20:38	100	\N	\N	A
53468	Descending	0	0	53296	2009-04-07 17:20:51	100	\N	Material Management Rules	Y	2009-04-07 17:20:51	100	\N	\N	D
53469	Percentage	0	0	53298	2009-04-09 16:26:32	100	\N	Material Management Rules	Y	2009-04-09 16:26:32	100	\N	\N	P
53470	Flat Discount	0	0	53298	2009-04-09 16:27:13	100	\N	Material Management Rules	Y	2009-04-09 16:27:13	100	\N	\N	F
53471	Absolute Amount	0	0	53298	2009-04-09 16:27:32	100	\N	Material Management Rules	Y	2009-04-09 16:27:32	100	\N	\N	A
53481	Co-Product	0	0	53225	2009-06-11 12:06:05	0	\N	Material Management Rules	Y	2009-06-11 12:06:05	0	\N	\N	CP
53514	Angle	0	0	53323	2009-09-12 15:00:35	100	\N	Material Management Rules	Y	2009-09-12 15:00:35	100	\N	\N	AN
53515	Area	0	0	53323	2009-09-12 15:00:43	100	\N	Material Management Rules	Y	2009-09-12 15:00:43	100	\N	\N	AR
53516	Data Storage	0	0	53323	2009-09-12 15:01:27	100	\N	Material Management Rules	Y	2009-09-12 15:01:27	100	\N	\N	DS
53517	Density	0	0	53323	2009-09-12 15:01:41	100	\N	Material Management Rules	Y	2009-09-12 15:01:41	100	\N	\N	DE
53518	Energy	0	0	53323	2009-09-12 15:01:50	100	\N	Material Management Rules	Y	2009-09-12 15:01:50	100	\N	\N	EN
53519	Force	0	0	53323	2009-09-12 15:02:03	100	\N	Material Management Rules	Y	2009-09-12 15:02:03	100	\N	\N	FO
53520	Kitchen Measures	0	0	53323	2009-09-12 15:02:14	100	\N	Material Management Rules	Y	2009-09-12 15:02:14	100	\N	\N	KI
53521	Length	0	0	53323	2009-09-12 15:02:24	100	\N	Material Management Rules	Y	2009-09-12 15:02:24	100	\N	\N	LE
53522	Power	0	0	53323	2009-09-12 15:02:37	100	\N	Material Management Rules	Y	2009-09-12 15:02:37	100	\N	\N	PO
53523	Pressure	0	0	53323	2009-09-12 15:02:51	100	\N	Material Management Rules	Y	2009-09-12 15:02:51	100	\N	\N	PR
53524	Temperature	0	0	53323	2009-09-12 15:02:58	100	\N	Material Management Rules	Y	2009-09-12 15:02:58	100	\N	\N	TE
53525	Time	0	0	53323	2009-09-12 15:03:05	100	\N	Material Management Rules	Y	2009-09-12 15:03:05	100	\N	\N	TM
53526	Torque	0	0	53323	2009-09-12 15:03:11	100	\N	Material Management Rules	Y	2009-09-12 15:03:11	100	\N	\N	TO
53527	Velocity	0	0	53323	2009-09-12 15:03:17	100	\N	Material Management Rules	Y	2009-09-12 15:03:17	100	\N	\N	VE
53528	Volume Liquid	0	0	53323	2009-09-12 15:03:24	100	\N	Material Management Rules	Y	2009-09-12 15:03:24	100	\N	\N	VL
53529	Volume Dry	0	0	53323	2009-09-12 15:03:31	100	\N	Material Management Rules	Y	2009-09-12 15:03:31	100	\N	\N	VD
53530	Weigth	0	0	53323	2009-09-12 15:03:42	100	\N	Material Management Rules	Y	2009-09-12 15:03:42	100	\N	\N	WE
53531	Currency	0	0	53323	2009-09-12 15:03:46	100	\N	Material Management Rules	Y	2009-09-12 15:03:46	100	\N	\N	CU
53532	Data Speed	0	0	53323	2009-09-12 15:04:02	100	\N	Material Management Rules	Y	2009-09-12 15:04:02	100	\N	\N	DV
53533	Frequency	0	0	53323	2009-09-12 15:04:08	100	\N	Material Management Rules	Y	2009-09-12 15:04:08	100	\N	\N	FR
53534	Other	0	0	53323	2009-09-12 15:04:16	100	\N	Material Management Rules	Y	2009-09-12 15:04:16	100	\N	\N	OT
54559	Maintain Minimum Level	0	0	164	2015-03-23 13:09:44	100	\N	Material Management Rules	Y	2016-04-11 16:47:03	0	\N	\N	3
54627	MRP Calculated Replenish	0	0	164	2016-01-29 10:28:11	100	Replenish Type is used in MiniMRP Calculations	Material Management Rules	Y	2016-01-29 10:28:11	100	\N	\N	4
\.
