copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "istscapable", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
146	C_UOM	UOM	6	0	0	\N	120	\N	1999-05-21 00:00:00	0	Unit of Measure	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	75	\N	L	\N	2000-01-02 00:00:00	0
175	C_UOM_Conversion	UOM Conversion	6	0	0	\N	120	\N	1999-06-03 00:00:00	0	Unit of Measure Conversion	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
190	M_Warehouse	Warehouse	3	0	0	\N	139	\N	1999-06-03 00:00:00	0	Storage Warehouse and Service Point	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	80	\N	L	\N	2000-01-02 00:00:00	0
191	M_Warehouse_Acct	M_Warehouse_Acct	3	0	0	\N	139	\N	1999-06-03 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
203	C_Project	Project	3	0	0	\N	130	\N	1999-06-14 00:00:00	0	Financial Project	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	100	\N	L	\N	2000-01-02 00:00:00	0
204	C_Project_Acct	C_Project_Acct	3	0	0	\N	286	\N	1999-06-14 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
207	M_Locator	Locator	7	0	0	\N	139	\N	1999-06-14 00:00:00	0	Warehouse Locator	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	85	\N	L	\N	2000-01-02 00:00:00	0
208	M_Product	Product	3	0	0	\N	140	\N	1999-06-14 00:00:00	0	Product Service Item	Material Management Rules	\N	\N	Y	Y	Y	Y	Y	N	N	N	90	\N	L	\N	2016-04-11 16:47:11	0
209	M_Product_Category	Product Category	3	0	0	\N	144	\N	1999-06-14 00:00:00	0	Category of a Product	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	75	\N	L	\N	2000-01-02 00:00:00	0
210	M_Product_PO	Product Purchasing	3	0	0	\N	140	\N	1999-06-14 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2016-04-11 16:47:08	0
213	M_Substitute	M_Substitute	3	0	0	\N	140	\N	1999-06-14 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
249	M_Replenish	M_Replenish	3	0	0	\N	140	\N	1999-07-04 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2016-04-11 16:47:03	0
250	M_Storage	M_Storage	3	0	0	\N	139	\N	1999-07-04 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
251	M_ProductPrice	Product Price	3	0	0	\N	146	\N	1999-08-08 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-05-15 13:22:20	100
253	M_Shipper	Shipper	3	0	0	\N	142	\N	1999-08-08 00:00:00	0	Method or manner of product delivery	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	105	\N	L	\N	2007-12-17 06:57:27	0
255	M_PriceList	Price List	3	0	0	\N	146	\N	1999-08-08 00:00:00	0	Unique identifier of a Price List	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	60	\N	L	\N	2000-01-02 00:00:00	0
273	M_Product_Acct	Product Acct	3	0	0	\N	140	\N	1999-09-21 00:00:00	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:25:16	100
295	M_PriceList_Version	Price List Version	3	0	0	\N	146	\N	1999-11-10 00:00:00	0	Identifies a unique instance of a Price List	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	95	\N	L	\N	2000-01-02 00:00:00	0
312	M_Product_Trl	Product Trl	3	0	0	\N	140	\N	1999-12-04 19:50:17	0	Product, Service, Item	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 06:26:58	0
329	M_Transaction	Inventory Transaction	1	0	0	\N	130	\N	1999-12-19 20:39:32	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	Y	N	N	N	140	\N	L	\N	2007-12-17 04:14:54	0
339	C_UOM_Trl	UOM Trl	6	0	0	\N	120	\N	2000-01-24 17:03:25	0	Unit of Measure	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
342	M_PerpetualInv	Perpetual Inventory	3	0	0	\N	175	\N	2000-01-24 17:03:25	0	Rules for generating physical inventory	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
383	M_Product_BOM	BOM Line	3	0	0	\N	140	\N	2000-10-11 21:46:44	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
401	M_Product_Category_Acct	Product Category Acct	3	0	0	\N	144	\N	2000-12-17 16:19:52	0	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-09-24 09:25:20	100
434	C_ProjectLine	Project Line	3	0	0	\N	286	\N	2001-03-11 17:34:41	0	Task or step in a project	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
475	M_DiscountSchema	Discount Schema	3	0	0	\N	233	\N	2001-12-28 19:43:25	0	Schema to calculate the trade discount percentage	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	60	337	L	\N	2000-01-02 00:00:00	0
476	M_DiscountSchemaBreak	Discount Schema Break	3	0	0	\N	233	\N	2001-12-28 19:43:25	0	Trade Discount Break	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
477	M_DiscountSchemaLine	Discount Pricelist	3	0	0	\N	337	\N	2001-12-28 19:43:25	0	Line of the pricelist trade discount schema	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
576	C_ProjectPhase	Project Phase	3	0	0	\N	130	\N	2003-05-28 21:35:14	0	Phase of a Project	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	120	\N	L	\N	2000-01-02 00:00:00	0
584	C_ProjectTask	Project Task	3	0	0	\N	130	\N	2003-06-01 23:14:26	0	Actual Project Task in a Phase	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
595	M_FreightCategory	Freight Category	3	0	0	\N	282	\N	2003-06-07 19:48:39	0	Category of the Freight	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	85	\N	L	\N	2007-12-17 06:37:33	0
596	M_Freight	Freight	3	0	0	\N	142	\N	2003-06-07 19:48:39	0	Freight Rate	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 06:45:43	0
623	C_ProjectIssue	Project Issue	3	0	0	\N	286	\N	2003-09-02 18:00:11	0	Project Issues (Material, Labor)	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
632	C_BPartner_Product	Product Business Partner	3	0	0	\N	\N	\N	2003-12-07 12:43:52	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 06:20:34	0
662	M_RelatedProduct	Product Related	3	0	0	\N	140	\N	2004-02-19 10:29:53	0	\N	Material Management Rules	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-05-15 13:23:44	100
771	M_Cost	Product Cost	3	0	0	\N	344	\N	2005-04-24 22:23:14	100	\N	Material Management Rules	\N	N	Y	Y	N	N	N	N	N	N	145	\N	L	\N	2005-07-26 14:12:09	0
777	M_ProductDownload	Product Download	3	0	0	\N	\N	\N	2005-04-29 20:20:05	100	Product downloads	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2005-09-03 12:08:11	100
795	M_BOMAlternative	Alternative Group	3	0	0	\N	\N	\N	2005-05-15 13:12:58	100	Product BOM Alternative Group	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2005-09-03 12:09:31	100
796	M_ProductOperation	Product Operation	3	0	0	\N	354	\N	2005-05-15 13:21:20	100	\N	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2005-09-03 12:08:08	100
797	M_OperationResource	Operation Resource	3	0	0	\N	354	\N	2005-05-15 14:14:21	100	Product Operation Resource	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:08:26	100
798	M_BOM	BOM	3	0	0	\N	354	\N	2005-05-15 14:26:20	100	Bill of Material	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2007-12-17 01:08:32	0
801	M_BOMProduct	BOM Component	3	0	0	\N	354	\N	2005-05-15 15:51:17	100	Bill of Material Component (Product)	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:09:28	100
53172	M_ProductPriceVendorBreak	Product Price Break	3	0	0	\N	146	N	2009-03-17 22:34:19	100	\N	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-10-01 20:36:58	100
53176	M_PromotionGroup	Promotion Group	3	0	0	\N	\N	N	2009-04-07 11:51:03	100	Grouping of product for promotion setup	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 11:51:03	100
53177	M_PromotionGroupLine	Promotion Group Line	3	0	0	\N	\N	N	2009-04-07 12:02:19	100	Line items of a promotion group	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 12:02:19	100
53178	M_Promotion	Promotion	3	0	0	\N	\N	N	2009-04-07 12:16:39	100	Promotion setup	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 12:16:39	100
53179	M_PromotionLine	Promotion Line	3	0	0	\N	\N	N	2009-04-07 13:19:33	100	Promotion line items	Material Management Rules	The promotion line items include the combination of product that make up this promotion	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 13:19:33	100
53180	M_PromotionPreCondition	Promotion Pre Condition	3	0	0	\N	\N	N	2009-04-07 13:59:58	100	Pre condition for promotion	Material Management Rules	Promotion is applicable to order that satisfy the pre condition	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 13:59:58	100
53181	M_PromotionDistribution	Promotion Distribution	3	0	0	\N	\N	N	2009-04-07 16:47:54	100	Distribute order line items into multiple bucket for reward calculation	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-07 16:47:54	100
53182	M_PromotionReward	Promotion Reward	3	0	0	\N	\N	N	2009-04-09 15:41:55	100	Setup reward for the promotion	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-04-09 15:41:55	100
53373	M_FreightCharge	Freight Charge	3	0	0	\N	142	\N	2012-03-26 15:17:10	100	Freight Charge Calculation	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	\N	\N	L	\N	2012-03-26 15:17:10	100
53374	M_FreightRegion	Freight Region	3	0	0	\N	\N	\N	2012-03-26 15:18:49	100	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	\N	\N	L	\N	2012-03-26 15:18:49	100
53377	M_FreightRegion_Location	Freight Region Location	3	0	0	\N	\N	\N	2012-03-26 16:56:52	100	\N	Material Management Rules	\N	\N	Y	Y	N	N	N	N	N	N	\N	\N	L	\N	2012-03-26 16:56:52	100
53783	C_ProjectResource	Project Resource	2	0	0	\N	130	N	2014-03-19 13:45:16	100	\N	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2014-03-19 13:45:16	100
54161	M_Product_Document	Product Document ID	3	0	0	\N	140	N	2016-07-25 12:58:10	100	\N	Material Management Rules	\N	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2016-07-25 12:58:10	100
\.
