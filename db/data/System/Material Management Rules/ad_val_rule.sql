copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
132	M_PriceList Base	0	0	M_PriceList.C_Currency_ID=@C_Currency_ID@	2000-05-22 15:28:02	0	Base Pricelists need to have same currency	Material Management Rules	Y	S	2000-01-02 00:00:00	0
171	C_Phase of Project Type	0	0	C_Phase.C_ProjectType_ID=@C_ProjectType_ID@	2003-08-09 22:57:44	0	\N	Material Management Rules	Y	S	2000-01-02 00:00:00	0
205	M_DiscountSchema Type	0	0	(('@IsSOTrx@'='N' AND Value='P') OR ('@IsSOTrx@'='Y' AND Value<>'P'))	2004-07-07 22:16:46	0	\N	Material Management Rules	Y	S	2000-01-02 00:00:00	0
223	M_ProductOperation of Product	0	0	M_ProductOperation.M_Product_ID=@M_Product_ID@	2005-05-15 16:15:58	100	\N	Material Management Rules	Y	S	2005-05-15 16:15:58	100
224	M_BOMAlternative of Product	0	0	M_BOMAlternative.M_Product_ID=@M_Product_ID@	2005-05-15 16:29:19	100	\N	Material Management Rules	Y	S	2005-05-15 16:29:19	100
225	M_ChangeNotice of BOM Component	0	0	EXISTS (SELECT * FROM M_BOM b WHERE b.M_Product_ID=@M_ProductBOM_ID@ AND b.M_ChangeNotice_ID=M_ChangeNotice.M_ChangeNotice_ID)	2005-05-15 16:33:20	100	\N	Material Management Rules	Y	S	2005-05-15 16:33:20	100
241	M_CostElements Manual	0	0	(CostingMethod IS NULL OR CostingMethod='S')	2005-09-13 21:25:05	100	\N	Material Management Rules	Y	S	2005-09-13 21:25:05	100
262	C_ProjectPhase (nq) of Project	0	0	C_Project_ID=@C_Project_ID@	2006-03-26 18:46:44	100	Not Qualified	Material Management Rules	Y	S	2006-03-26 18:48:29	100
263	C_ProjectTask (nq) of ProjectPhase	0	0	C_ProjectPhase_ID=@C_ProjectPhase_ID@	2006-03-26 18:47:31	100	Not qualified	Material Management Rules	Y	S	2006-03-26 18:47:48	100
52048	M_PromotionDistribution of M_Promotion	0	0	M_PromotionDistribution.M_Promotion_ID = @M_Promotion_ID@	2009-04-09 16:14:00	100	\N	Material Management Rules	Y	S	2009-04-09 16:14:00	100
52049	M_PromotionLine of M_Promotion	0	0	M_PromotionLine.M_Promotion_ID = @M_Promotion_ID@	2009-04-09 16:36:31	100	\N	Material Management Rules	Y	S	2009-04-09 16:36:31	100
52117	M_FreightRegion of Shipper	0	0	M_FreightRegion.M_Shipper_ID=@M_Shipper_ID@	2012-03-26 15:18:37	100	\N	Material Management Rules	Y	S	2012-03-26 15:18:37	100
52434	C_BPartner (Trx) Excluding prospects	0	0	C_BPartner.IsActive='Y' AND C_BPartner.IsSummary='N' AND C_BPartner.IsProspect='N'	2015-08-03 17:34:27	100	\N	Material Management Rules	Y	S	2015-08-03 17:34:27	100
\.
