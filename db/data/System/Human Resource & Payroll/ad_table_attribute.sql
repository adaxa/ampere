copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
51219	c_job_pkey	0	0	789	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	PRIMARY KEY (c_job_id)	CP	\N	2022-11-02 05:57:56.041854	100
51220	cjobcategory_cjob	0	0	789	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	FOREIGN KEY (c_jobcategory_id) REFERENCES c_jobcategory(c_jobcategory_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51221	c_jobassignment_pkey	0	0	791	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	PRIMARY KEY (c_jobassignment_id)	CP	\N	2022-11-02 05:57:56.041854	100
51222	cjob_cjobassignment	0	0	791	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	FOREIGN KEY (c_job_id) REFERENCES c_job(c_job_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51223	aduser_cjobassignment	0	0	791	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51224	c_jobcategory_pkey	0	0	790	2022-11-02 05:57:56.041854	100	Human Resource & Payroll	\N	Y	PRIMARY KEY (c_jobcategory_id)	CP	\N	2022-11-02 05:57:56.041854	100
53568	c_job_isactive_check	0	0	789	2022-11-02 12:40:39.392787	100	Human Resource & Payroll	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53569	c_jobassignment_isactive_check	0	0	791	2022-11-02 12:40:39.392787	100	Human Resource & Payroll	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53570	c_jobcategory_isactive_check	0	0	790	2022-11-02 12:40:39.392787	100	Human Resource & Payroll	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
