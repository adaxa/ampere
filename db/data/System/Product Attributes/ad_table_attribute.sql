copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
51933	m_attribute_pkey	0	0	562	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attribute_id)	CP	\N	2022-11-02 05:57:56.041854	100
51934	mattributesearch_mattribute	0	0	562	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attributesearch_id) REFERENCES m_attributesearch(m_attributesearch_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51939	m_attributesearch_pkey	0	0	564	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attributesearch_id)	CP	\N	2022-11-02 05:57:56.041854	100
51940	m_attributeset_pkey	0	0	560	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attributeset_id)	CP	\N	2022-11-02 05:57:56.041854	100
51941	mlotctl_mattributeset	0	0	560	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_lotctl_id) REFERENCES m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51942	msernoctl_attributeset	0	0	560	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_sernoctl_id) REFERENCES m_sernoctl(m_sernoctl_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51943	m_attributesetexclude_pkey	0	0	809	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attributesetexclude_id)	CP	\N	2022-11-02 05:57:56.041854	100
51944	adtable_mattributesetexclude	0	0	809	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51945	mattributeset_masexclude	0	0	809	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attributeset_id) REFERENCES m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51946	m_attributesetinstance_pkey	0	0	559	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attributesetinstance_id)	CP	\N	2022-11-02 05:57:56.041854	100
51947	mattributeset_mattribsetinst	0	0	559	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attributeset_id) REFERENCES m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51948	mlot_mattributesetinstance	0	0	559	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_lot_id) REFERENCES m_lot(m_lot_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51949	m_attributeuse_pkey	0	0	563	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attribute_id, m_attributeset_id)	CP	\N	2022-11-02 05:57:56.041854	100
51950	mattributeset_mattributeuse	0	0	563	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attributeset_id) REFERENCES m_attributeset(m_attributeset_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51951	mattribute_mattributeuse	0	0	563	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attribute_id) REFERENCES m_attribute(m_attribute_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
51952	m_attributevalue_pkey	0	0	558	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_attributevalue_id)	CP	\N	2022-11-02 05:57:56.041854	100
51953	mattribute_mattributevalue	0	0	558	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_attribute_id) REFERENCES m_attribute(m_attribute_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52125	m_lot_pkey	0	0	557	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_lot_id)	CP	\N	2022-11-02 05:57:56.041854	100
52126	mlotctl_mlot	0	0	557	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_lotctl_id) REFERENCES m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52127	mproduct_mlot	0	0	557	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_product_id) REFERENCES m_product(m_product_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52128	m_lotctl_pkey	0	0	556	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_lotctl_id)	CP	\N	2022-11-02 05:57:56.041854	100
52129	m_lotctlexclude_pkey	0	0	810	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_lotctlexclude_id)	CP	\N	2022-11-02 05:57:56.041854	100
52130	mlotctl_mlotctlexclude	0	0	810	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_lotctl_id) REFERENCES m_lotctl(m_lotctl_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52131	adtable_mlotctlexclude	0	0	810	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52389	m_sernoctl_pkey	0	0	555	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_sernoctl_id)	CP	\N	2022-11-02 05:57:56.041854	100
52390	m_sernoctlexclude_pkey	0	0	811	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	PRIMARY KEY (m_sernoctlexclude_id)	CP	\N	2022-11-02 05:57:56.041854	100
52391	msernoctl_msernoctlexclude	0	0	811	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (m_sernoctl_id) REFERENCES m_sernoctl(m_sernoctl_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52392	adtable_msernoctlexclude	0	0	811	2022-11-02 05:57:56.041854	100	Product Attributes	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
53890	m_attribute_isactive_check	0	0	562	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53891	m_attribute_isinstanceattribute_check	0	0	562	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isinstanceattribute = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53892	m_attribute_ismandatory_check	0	0	562	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((ismandatory = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53894	m_attributesearch_isactive_check	0	0	564	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53895	m_attributeset_islot_check	0	0	560	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((islot = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53896	m_attributeset_linktoasset_check	0	0	560	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((linktoasset = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53897	m_attributeset_isactive_check	0	0	560	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53898	m_attributeset_isguaranteedate_check	0	0	560	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isguaranteedate = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53899	m_attributeset_isserno_check	0	0	560	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isserno = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53900	m_attributesetexclude_issotrx_check	0	0	809	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((issotrx = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53901	m_attributesetexclude_isactive_check	0	0	809	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53902	m_attributesetinstance_isactive_check	0	0	559	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53903	m_attributeuse_isactive_check	0	0	563	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53904	m_attributevalue_isactive_check	0	0	558	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53971	m_lot_isactive_check	0	0	557	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53972	m_lotctl_isactive_check	0	0	556	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53973	m_lotctlexclude_issotrx_check	0	0	810	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((issotrx = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53974	m_lotctlexclude_isactive_check	0	0	810	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54073	m_sernoctl_isactive_check	0	0	555	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54074	m_sernoctl_isunique_check	0	0	555	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isunique = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54075	m_sernoctlexclude_isactive_check	0	0	811	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54076	m_sernoctlexclude_issotrx_check	0	0	811	2022-11-02 12:40:39.392787	100	Product Attributes	\N	Y	CHECK ((issotrx = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
