copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
52370	m_rma_pkey	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	PRIMARY KEY (m_rma_id)	CP	\N	2022-11-02 05:57:56.041854	100
52371	salesrep_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (salesrep_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52372	ccurrency_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (c_currency_id) REFERENCES c_currency(c_currency_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52373	refrma_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (ref_rma_id) REFERENCES m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52374	corder_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (c_order_id) REFERENCES c_order(c_order_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52375	cbpartner_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (c_bpartner_id) REFERENCES c_bpartner(c_bpartner_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52376	minout_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (inout_id) REFERENCES m_inout(m_inout_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52377	cdoctype_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (c_doctype_id) REFERENCES c_doctype(c_doctype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52378	mrmatype_mrma	0	0	661	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (m_rmatype_id) REFERENCES m_rmatype(m_rmatype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52379	m_rmaline_pkey	0	0	660	2022-11-02 05:57:56.041854	100	Returns	\N	Y	PRIMARY KEY (m_rmaline_id)	CP	\N	2022-11-02 05:57:56.041854	100
52380	minoutline_mrmaline	0	0	660	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (m_inoutline_id) REFERENCES m_inoutline(m_inoutline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52381	refrmaline_mrmaline	0	0	660	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (ref_rmaline_id) REFERENCES m_rmaline(m_rmaline_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52382	ccharge_mrmaline	0	0	660	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (c_charge_id) REFERENCES c_charge(c_charge_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52383	mrma_mrmaline	0	0	660	2022-11-02 05:57:56.041854	100	Returns	\N	Y	FOREIGN KEY (m_rma_id) REFERENCES m_rma(m_rma_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52384	m_rmatype_pkey	0	0	729	2022-11-02 05:57:56.041854	100	Returns	\N	Y	PRIMARY KEY (m_rmatype_id)	CP	\N	2022-11-02 05:57:56.041854	100
54064	m_rma_issotrx_check	0	0	661	2022-11-02 12:40:39.392787	100	Returns	\N	Y	CHECK ((issotrx = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54065	m_rmatype_isactive_check	0	0	729	2022-11-02 12:40:39.392787	100	Returns	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
