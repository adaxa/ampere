copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
102	Currency	W	0	\N	\N	0	\N	115	1999-06-11 00:00:00	0	Maintain Currencies	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
103	Currency Rate	W	0	\N	\N	0	\N	116	1999-06-11 00:00:00	0	Maintain Currency Conversion Rates	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
104	Calendar Year and Period	W	0	\N	\N	0	\N	117	1999-06-11 00:00:00	0	Maintain Calendars Years Periods	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
105	Account Element	W	0	\N	\N	0	\N	118	1999-06-11 00:00:00	0	Maintain Account Elements	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
106	Account Combination	W	0	\N	\N	0	\N	153	1999-09-26 00:00:00	0	Maintain Valid Account Combinations 	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
111	Accounting Schema	W	0	\N	\N	0	\N	125	1999-06-11 00:00:00	0	Maintain Accounting Schema - For changes to become effective you must re-login	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
117	GL Category	W	0	\N	\N	0	\N	131	1999-06-29 00:00:00	0	Maintain General Ledger Categories	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
120	Activity (ABC)	W	0	\N	\N	0	\N	134	1999-06-29 00:00:00	0	Maintain Activities for Activity Based Costing	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
121	Document Type	W	0	\N	\N	0	\N	135	1999-06-29 00:00:00	0	Maintain Document Types	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
123	Tax Rate	W	0	\N	\N	0	\N	137	1999-08-09 00:00:00	0	Maintain Taxes and their Rates	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
124	Tax Category	W	0	\N	\N	0	\N	138	1999-08-09 00:00:00	0	Maintain Tax Categories	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
135	GL Budget	W	0	\N	\N	0	\N	154	1999-09-26 00:00:00	0	Maintain General Ledger Budgets	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
151	Document Sequence	W	0	\N	\N	0	\N	112	1999-06-11 00:00:00	0	Maintain System and Document Sequences	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
164	Accounting Setup	\N	0	\N	\N	0	\N	\N	1999-12-02 13:14:25	0	\N	Accounting Rules	Y	N	N	Y	Y	2022-02-28 00:21:05	100
174	Charge	W	0	\N	\N	0	\N	161	1999-12-04 21:41:28	0	Maintain Charges	Accounting Rules	Y	Y	N	Y	N	2000-01-02 00:00:00	0
254	Generate Charges	X	0	105	\N	0	\N	\N	2001-02-06 18:55:00	0	Generate Charges from natural accounts	Accounting Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
306	Resubmit Posting	P	0	\N	\N	0	175	\N	2001-12-02 12:35:57	0	Resubmit posting of documents with posting errors or locked documents	Accounting Rules	Y	Y	N	N	N	2006-06-06 15:49:33	100
307	Reset Accounting	P	0	\N	\N	0	176	\N	2001-12-02 12:36:30	0	Reset Accounting Entries ** Stop Accounting Server before starting **	Accounting Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
393	Update Accounting Balance	P	0	\N	\N	0	203	\N	2003-07-21 21:17:17	0	Update Daily Accounting Balances	Accounting Rules	N	Y	N	N	N	2008-07-15 01:27:48	100
409	Verify Document Types	P	0	\N	\N	0	233	\N	2003-10-11 00:15:50	0	Verify Document Types and Period Controls	Accounting Rules	Y	Y	N	N	N	2006-03-14 17:46:19	100
417	UnPosted Documents	W	0	\N	\N	0	\N	294	2003-12-14 01:46:10	0	Unposted Documents	Accounting Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
418	Currency Type	W	0	\N	\N	0	\N	295	2003-12-21 00:40:05	0	Maintain Currency Conversion Rate Types	Accounting Rules	Y	Y	N	N	N	2000-01-02 00:00:00	0
464	GL Distribution	W	0	\N	\N	0	\N	323	2004-03-19 12:50:50	0	General Ledger Distribution	Accounting Rules	N	Y	N	Y	N	2019-12-05 14:13:33	100
476	Counter Document	W	0	\N	\N	0	\N	327	2004-04-14 13:05:13	0	Maintain Counter Document Types	Accounting Rules	N	Y	N	N	N	2022-02-17 22:50:16	100
547	Tax Declaration	W	0	\N	\N	0	\N	359	2005-10-18 12:05:09	100	Define the declaration to the tax authorities	Accounting Rules	N	Y	N	N	N	2022-02-17 22:50:31	100
550	Budget Control	W	0	\N	\N	0	\N	361	2005-10-25 10:31:33	100	Maintain Budget Controls	Accounting Rules	N	Y	N	N	N	2019-12-05 14:13:42	100
551	GL Fund (Alpha)	W	0	\N	\N	0	\N	362	2005-10-25 10:59:59	100	Maintain Fund Controls	Accounting Rules	N	Y	N	N	N	2019-12-05 14:13:45	100
53087	Update Sequence No	P	0	\N	\N	0	53068	\N	2008-02-02 12:43:56	100	\N	Accounting Rules	Y	Y	N	N	N	2008-02-02 12:43:56	100
53091	Global Tax Management	\N	0	\N	\N	0	\N	\N	2008-03-03 22:17:09	0	\N	Accounting Rules	Y	N	Y	N	Y	2008-03-03 22:17:09	0
53092	Tax Group	W	0	\N	\N	0	\N	53020	2008-03-03 22:17:10	0	Tax Groups let you group the business partner with a reference tax.	Accounting Rules	N	Y	N	N	N	2019-12-05 14:13:51	100
53093	C_Invoce Calculate Tax	P	0	\N	\N	0	53072	\N	2008-03-03 22:17:11	0	\N	Accounting Rules	N	Y	N	N	N	2019-12-05 14:14:09	100
53094	Tax Definition	W	0	\N	\N	0	\N	53021	2008-03-03 22:17:13	0	Lets you define different tax combinations.	Accounting Rules	N	Y	N	N	N	2019-12-05 14:14:05	100
53095	Tax Rate Parent	W	0	\N	\N	0	\N	53022	2008-03-03 22:17:14	0	Maintain Taxes and their Rates	Accounting Rules	N	Y	N	N	N	2019-12-05 14:14:02	100
53096	Tax Type	W	0	\N	\N	0	\N	53023	2008-03-03 22:17:14	0	Tax Types let you group taxes together.	Accounting Rules	N	Y	N	N	N	2019-12-05 14:13:54	100
53097	Tax Base	W	0	\N	\N	0	\N	53024	2008-03-03 22:17:15	0	Defines tax base for a tax	Accounting Rules	N	Y	N	N	N	2019-12-05 14:13:58	100
53189	Charge Type	W	0	\N	\N	0	\N	53062	2008-08-26 23:08:56	100	\N	Accounting Rules	Y	Y	N	N	N	2008-08-26 23:08:56	100
53248	Post accounting	P	0	\N	\N	0	53187	\N	2009-09-13 17:59:58	100	Client Accounting Processor	Accounting Rules	Y	Y	N	N	N	2022-02-17 22:52:02	100
53990	Currency Convert	W	0	\N	\N	0	\N	53422	2015-07-25 10:53:55	0	\N	Accounting Rules	Y	Y	N	N	N	2016-04-11 16:52:41	0
\.
