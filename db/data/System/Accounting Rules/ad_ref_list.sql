copy "ad_ref_list" ("ad_ref_list_id", "name", "ad_client_id", "ad_org_id", "ad_reference_id", "created", "createdby", "description", "entitytype", "isactive", "updated", "updatedby", "validfrom", "validto", "value") from STDIN;
118	Account	0	0	116	1999-06-23 00:00:00	0	Account Element	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	A
119	User defined	0	0	116	1999-06-23 00:00:00	0	User defined element	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	U
146	Standard Costing	0	0	122	1999-06-23 00:00:00	0	Standard Purchase Order Price Costing	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	S
147	Average PO	0	0	122	1999-06-23 00:00:00	0	Weighted Average Purchase Order Price Costing	Accounting Rules	N	2020-10-29 12:27:41	100	\N	\N	A
148	Lifo	0	0	122	1999-06-23 00:00:00	0	Last In First Out Costing	Accounting Rules	N	2020-10-29 12:27:49	100	\N	\N	L
149	Fifo	0	0	122	1999-06-23 00:00:00	0	First In First Out Costing	Accounting Rules	N	2020-10-29 12:27:43	100	\N	\N	F
150	International GAAP	0	0	123	1999-06-23 00:00:00	0	International GAAP	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	UN
151	US GAAP	0	0	123	1999-06-23 00:00:00	0	United States GAAP	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	US
152	German HGB	0	0	123	1999-06-23 00:00:00	0	German HGB	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	DE
204	On Credit Order	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	WI
205	POS Order	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	WR
206	Warehouse Order	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	WP
207	Standard Order	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	SO
208	Proposal	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	ON
209	Quotation	0	0	148	1999-08-05 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	OB
239	Last PO Price	0	0	122	1999-08-06 00:00:00	0	\N	Accounting Rules	N	2020-10-29 12:27:51	100	\N	\N	p
266	Draft	0	0	178	1999-09-27 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	D
267	Approved	0	0	178	1999-09-27 00:00:00	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	A
272	Organization	0	0	181	1999-10-04 00:00:00	0	Owning Organization	Accounting Rules	Y	2005-11-01 00:56:43	100	\N	\N	OO
273	Account	0	0	181	1999-10-04 00:00:00	0	Natural Account	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	AC
274	Product	0	0	181	1999-10-04 00:00:00	0	Product	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	PR
275	BPartner	0	0	181	1999-10-04 00:00:00	0	Business Partner	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	BP
276	Org Trx	0	0	181	1999-10-04 00:00:00	0	Transaction Organization	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	OT
277	Location From	0	0	181	1999-10-04 00:00:00	0	Location From	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	LF
278	Location To	0	0	181	1999-10-04 00:00:00	0	Location To	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	LT
279	Sales Region	0	0	181	1999-10-04 00:00:00	0	Sales Region	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	SR
280	Project	0	0	181	1999-10-04 00:00:00	0	Project	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	PJ
281	Campaign	0	0	181	1999-10-04 00:00:00	0	Marketing Campaign	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	MC
282	User List 1	0	0	181	1999-10-04 00:00:00	0	User 1	Accounting Rules	Y	2005-11-01 01:00:10	100	\N	\N	U1
283	User List 2	0	0	181	1999-10-04 00:00:00	0	User 2	Accounting Rules	Y	2005-11-01 01:00:24	100	\N	\N	U2
318	Activity	0	0	181	1999-12-22 10:21:51	0	Business Activity	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	AY
320	Return Material	0	0	148	2000-01-21 15:25:04	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	RM
331	Prepay Order	0	0	148	2000-01-29 09:55:15	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	PR
374	Manual	0	0	207	2000-08-19 14:08:17	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	M
375	Import	0	0	207	2000-08-19 14:08:44	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	I
376	Document	0	0	207	2000-08-19 14:09:26	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	D
568	Both	0	0	287	2003-08-19 23:21:37	0	\N	Accounting Rules	Y	2008-03-03 22:12:56	0	\N	\N	B
569	Sales Tax	0	0	287	2003-08-19 23:21:59	0	\N	Accounting Rules	Y	2008-03-03 22:12:57	0	\N	\N	S
570	Purchase Tax	0	0	287	2003-08-19 23:22:23	0	\N	Accounting Rules	Y	2008-03-03 22:12:57	0	\N	\N	P
575	French Accounting Standard	0	0	123	2003-09-03 10:25:06	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	FR
576	Custom Accounting Rules	0	0	123	2003-09-03 10:25:28	0	\N	Accounting Rules	Y	2000-01-02 00:00:00	0	\N	\N	XX
773	Client	0	0	355	2005-07-26 16:31:39	0	\N	Accounting Rules	Y	2005-07-26 16:31:39	0	\N	\N	C
774	Organization	0	0	355	2005-07-26 16:31:52	0	\N	Accounting Rules	Y	2005-07-26 16:31:52	0	\N	\N	O
775	Batch/Lot	0	0	355	2005-07-26 16:32:09	0	\N	Accounting Rules	Y	2005-07-26 16:32:09	0	\N	\N	B
776	Average Invoice	0	0	122	2005-07-31 09:15:01	100	Weighted Average Purchase Invoice Price Costing	Accounting Rules	N	2020-10-29 12:27:47	100	\N	\N	I
777	Last Invoice	0	0	122	2005-07-31 09:18:01	100	\N	Accounting Rules	N	2020-10-29 12:27:45	100	\N	\N	i
778	User Defined	0	0	122	2005-07-31 09:19:06	100	\N	Accounting Rules	N	2007-12-17 05:04:57	0	\N	\N	U
782	_	0	0	122	2005-09-13 21:14:16	100	\N	Accounting Rules	Y	2007-12-17 05:04:30	0	\N	\N	x
784	PO Commitment only	0	0	359	2005-10-11 17:11:38	100	\N	Accounting Rules	Y	2007-12-01 01:52:54	100	\N	\N	C
785	PO Commitment & Reservation	0	0	359	2005-10-11 17:14:59	100	\N	Accounting Rules	Y	2007-12-01 01:51:24	100	\N	\N	B
786	None	0	0	359	2005-10-11 17:17:14	100	\N	Accounting Rules	Y	2005-10-11 17:17:14	100	\N	\N	N
792	Period only	0	0	361	2005-10-25 09:58:51	100	\N	Accounting Rules	Y	2005-10-25 09:59:00	100	\N	\N	P
793	Year To Date	0	0	361	2005-10-25 09:59:20	100	\N	Accounting Rules	Y	2005-10-25 09:59:20	100	\N	\N	Y
794	Total	0	0	361	2005-10-25 09:59:34	100	\N	Accounting Rules	Y	2005-10-25 09:59:34	100	\N	\N	T
795	Sub Account	0	0	181	2005-11-01 00:57:26	100	Sub Account for Element Value	Accounting Rules	Y	2005-11-01 00:57:26	100	\N	\N	SA
796	User Element 1	0	0	181	2005-11-01 00:58:12	100	\N	Accounting Rules	Y	2005-11-01 00:58:12	100	\N	\N	X1
797	User Element 2	0	0	181	2005-11-01 00:58:34	100	\N	Accounting Rules	Y	2005-11-01 00:58:34	100	\N	\N	X2
814	System generated	0	0	207	2005-12-18 14:07:36	100	\N	Accounting Rules	Y	2005-12-18 14:07:36	100	\N	\N	S
915	None	0	0	392	2006-07-23 17:17:06	100	\N	Accounting Rules	Y	2006-07-23 17:17:06	100	\N	\N	N
916	Write-off only	0	0	392	2006-07-23 17:17:34	100	\N	Accounting Rules	Y	2006-07-23 17:17:34	100	\N	\N	W
917	Discount only	0	0	392	2006-07-23 17:18:05	100	\N	Accounting Rules	Y	2006-07-23 17:18:54	100	\N	\N	D
918	Write-off and Discount	0	0	392	2006-07-23 17:18:29	100	\N	Accounting Rules	Y	2006-07-23 17:18:45	100	\N	\N	B
53223	PO/SO Commitment & Reservation	0	0	359	2007-12-01 01:53:45	100	\N	Accounting Rules	Y	2007-12-01 01:53:45	100	\N	\N	A
53224	SO Commitment only	0	0	359	2007-12-01 01:54:31	100	\N	Accounting Rules	Y	2007-12-01 01:54:31	100	\N	\N	S
53225	PO/SO Commitment	0	0	359	2007-12-01 01:55:35	100	\N	Accounting Rules	Y	2007-12-01 01:55:35	100	\N	\N	O
53330	Cost	0	0	53240	2008-03-03 22:16:47	0	\N	Accounting Rules	Y	2008-03-03 22:16:47	0	\N	\N	C
53331	Price	0	0	53240	2008-03-03 22:16:48	0	\N	Accounting Rules	Y	2008-03-03 22:16:48	0	\N	\N	P
53332	Quantity	0	0	53240	2008-03-03 22:16:48	0	\N	Accounting Rules	Y	2008-03-03 22:16:48	0	\N	\N	Q
53559	Weight	0	0	53240	2009-12-01 08:20:20	100	\N	Accounting Rules	Y	2009-12-01 08:20:20	100	\N	\N	W
53749	Order	0	0	53425	2012-01-13 14:09:47	100	\N	Accounting Rules	Y	2012-01-13 14:09:47	100	\N	\N	0
53750	Shipment	0	0	53425	2012-01-13 14:09:55	100	\N	Accounting Rules	Y	2012-01-13 14:09:55	100	\N	\N	1
53751	Invoice	0	0	53425	2012-01-13 14:10:04	100	\N	Accounting Rules	Y	2012-01-13 14:10:04	100	\N	\N	2
54557	Include	0	0	53773	2015-02-05 10:06:13	100	\N	Accounting Rules	Y	2015-02-05 10:06:13	100	\N	\N	Include
54558	Exclude	0	0	53773	2015-02-05 10:06:20	100	\N	Accounting Rules	Y	2015-02-05 10:06:20	100	\N	\N	Exclude
\.
