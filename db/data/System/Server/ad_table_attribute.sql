copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50080	ad_alertprocessor_pkey	0	0	700	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_alertprocessor_id)	CP	\N	2022-11-02 05:57:56.041854	100
50081	aduser_calertprocessor	0	0	700	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50082	ad_alertprocessorlog_pkey	0	0	699	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_alertprocessor_id, ad_alertprocessorlog_id)	CP	\N	2022-11-02 05:57:56.041854	100
50083	calertprocessor_log	0	0	699	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_alertprocessor_id) REFERENCES ad_alertprocessor(ad_alertprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50226	ad_housekeeping_key	0	0	53147	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_housekeeping_id)	CP	\N	2022-11-02 05:57:56.041854	100
50227	adtable_adhousekeeping	0	0	53147	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50501	ad_scheduler_pkey	0	0	688	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_scheduler_id)	CP	\N	2022-11-02 05:57:56.041854	100
50502	aduser_adscheduler	0	0	688	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50503	adprocess_adscheduler	0	0	688	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_process_id) REFERENCES ad_process(ad_process_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50504	ad_schedulerlog_pkey	0	0	687	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_scheduler_id, ad_schedulerlog_id)	CP	\N	2022-11-02 05:57:56.041854	100
50505	adscheduler_log	0	0	687	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_scheduler_id) REFERENCES ad_scheduler(ad_scheduler_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50506	ad_scheduler_para_pkey	0	0	698	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_scheduler_id, ad_process_para_id)	CP	\N	2022-11-02 05:57:56.041854	100
50507	adscheduler_adschedulerpara	0	0	698	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_scheduler_id) REFERENCES ad_scheduler(ad_scheduler_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50508	adprocesspara_adschedulerpara	0	0	698	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_process_para_id) REFERENCES ad_process_para(ad_process_para_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50509	ad_schedulerrecipient_pkey	0	0	704	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (ad_schedulerrecipient_id)	CP	\N	2022-11-02 05:57:56.041854	100
50510	adrole_adschedulerrecipient	0	0	704	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_role_id) REFERENCES ad_role(ad_role_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50511	adscheduler_recipient	0	0	704	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_scheduler_id) REFERENCES ad_scheduler(ad_scheduler_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50512	aduser_adschedulerrecipient	0	0	704	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50695	c_acctprocessor_pkey	0	0	695	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (c_acctprocessor_id)	CP	\N	2022-11-02 05:57:56.041854	100
50696	aduser_cacctprocessor	0	0	695	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50697	cacctschema_cacctprocessor	0	0	695	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (c_acctschema_id) REFERENCES c_acctschema(c_acctschema_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50698	adtable_cacctprocessor	0	0	695	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50699	c_acctprocessorlog_pkey	0	0	694	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (c_acctprocessor_id, c_acctprocessorlog_id)	CP	\N	2022-11-02 05:57:56.041854	100
50700	cacctprocessor_log	0	0	694	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (c_acctprocessor_id) REFERENCES c_acctprocessor(c_acctprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52637	r_requestprocessor_pkey	0	0	420	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (r_requestprocessor_id)	CP	\N	2022-11-02 05:57:56.041854	100
52638	rrequesttype_rrequestprocessor	0	0	420	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (r_requesttype_id) REFERENCES r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52639	aduser_rrequestprocessor	0	0	420	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (supervisor_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52640	r_requestprocessorlog_pkey	0	0	659	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (r_requestprocessor_id, r_requestprocessorlog_id)	CP	\N	2022-11-02 05:57:56.041854	100
52641	rrequestprocessor_log	0	0	659	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (r_requestprocessor_id) REFERENCES r_requestprocessor(r_requestprocessor_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52642	r_requestprocessor_route_pkey	0	0	474	2022-11-02 05:57:56.041854	100	Server	\N	Y	PRIMARY KEY (r_requestprocessor_route_id)	CP	\N	2022-11-02 05:57:56.041854	100
52643	rrequesttype_rprocessorrule	0	0	474	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (r_requesttype_id) REFERENCES r_requesttype(r_requesttype_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52644	rrequestprocessor_route	0	0	474	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (r_requestprocessor_id) REFERENCES r_requestprocessor(r_requestprocessor_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52645	aduser_rrequestprocessorroute	0	0	474	2022-11-02 05:57:56.041854	100	Server	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52982	ad_alertprocessor_isactive_check	0	0	700	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52983	ad_alertprocessorlog_iserror_check	0	0	699	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((iserror = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
52984	ad_alertprocessorlog_isactive_check	0	0	699	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53078	ad_housekeeping_isactive_check	0	0	53147	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53079	ad_housekeeping_isexportxmlbackup_check	0	0	53147	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isexportxmlbackup = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53080	ad_housekeeping_issaveinhistoric_check	0	0	53147	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((issaveinhistoric = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53215	ad_scheduler_isignoreprocessingtime_check	0	0	688	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isignoreprocessingtime = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53216	ad_schedulerrecipient_isactive_check	0	0	704	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54193	r_requestprocessor_isactive_check	0	0	420	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
54194	r_requestprocessor_route_isactive_check	0	0	474	2022-11-02 12:40:39.392787	100	Server	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
