copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "istscapable", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
445	PA_Report	Financial Report	3	0	0	\N	216	\N	2001-05-09 21:18:37	0	Financial Report	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
446	PA_ReportColumn	Report Column	7	0	0	\N	217	\N	2001-05-09 21:18:37	0	Column in Report	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2000-01-02 00:00:00	0
447	PA_ReportColumnSet	Report Column Set	7	0	0	\N	217	\N	2001-05-09 21:18:37	0	Collection of Columns for Report	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
448	PA_ReportLine	Report Line	3	0	0	\N	218	\N	2001-05-09 21:18:37	0	\N	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
449	PA_ReportLineSet	Report Line Set	3	0	0	\N	218	\N	2001-05-09 21:18:37	0	\N	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
450	PA_ReportSource	Report Source	3	0	0	\N	218	\N	2001-05-09 21:18:37	0	Restriction of what will be shown in Report Line	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
544	T_Report	T_Report	4	0	0	\N	\N	\N	2003-02-05 14:39:17	0	Temporary Reporting Table	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:07:05	100
545	T_ReportStatement	T_ReportStatement	4	0	0	\N	\N	\N	2003-02-15 00:42:07	0	\N	Financial Reporting	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:07:08	100
821	PA_Hierarchy	Reporting Hierarchy	2	0	0	\N	360	\N	2005-10-23 18:37:22	100	Reporting Hierarchy	Financial Reporting	\N	N	Y	Y	N	N	N	N	N	N	130	\N	L	\N	2005-10-25 09:20:37	100
53202	PA_ReportCube	Report Cube	3	0	0	\N	53078	N	2009-05-14 11:43:30	100	Define reporting cube for pre-calculation of summary accounting data.	Financial Reporting	Summary data will be generated for each period of the selected calendar, grouped by the selected dimensions..	N	Y	Y	N	Y	N	N	N	N	0	\N	L	\N	2009-05-14 11:43:43	100
54020	RV_Fact_Acct_Proj	Project Accounting Fact ID	3	0	0	\N	\N	N	2015-08-03 12:35:26	100	\N	Financial Reporting	\N	N	Y	Y	N	N	N	N	N	Y	0	\N	L	\N	2015-08-03 12:35:26	100
54042	RV_Fact_Acct_Summary	Accounting Fact summary	3	0	0	\N	\N	N	2015-08-30 19:54:17	0	\N	Financial Reporting	\N	N	Y	Y	N	N	N	N	N	Y	0	\N	L	\N	2015-08-30 19:54:17	0
\.
