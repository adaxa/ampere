copy "ad_menu" ("ad_menu_id", "name", "action", "ad_client_id", "ad_form_id", "ad_infowindow_id", "ad_org_id", "ad_process_id", "ad_window_id", "created", "createdby", "description", "entitytype", "isactive", "iscentrallymaintained", "isreadonly", "issotrx", "issummary", "updated", "updatedby") from STDIN;
280	Financial Reporting	\N	0	\N	\N	0	\N	\N	2001-05-13 10:58:17	0	\N	Financial Reporting	Y	N	N	N	Y	2000-01-02 00:00:00	0
281	Financial Report	W	0	\N	\N	0	\N	216	2001-05-13 10:59:40	0	Maintain Financial Reports	Financial Reporting	Y	Y	N	N	N	2000-01-02 00:00:00	0
282	Report Column Set	W	0	\N	\N	0	\N	217	2001-05-13 10:59:59	0	Maintain Financial Report Column Sets	Financial Reporting	Y	Y	N	N	N	2000-01-02 00:00:00	0
283	Report Line Set	W	0	\N	\N	0	\N	218	2001-05-13 11:00:18	0	Maintain Financial Report Line Sets	Financial Reporting	Y	Y	N	N	N	2000-01-02 00:00:00	0
350	Statement of Accounts	R	0	\N	\N	0	204	\N	2003-02-14 15:53:41	0	Report Account Statement Beginning Balance and Transactions	Financial Reporting	Y	Y	N	Y	N	2005-10-24 08:12:22	100
502	Trial Balance	R	0	\N	\N	0	310	\N	2004-10-08 02:03:18	0	Trial Balance for a period or date range	Financial Reporting	Y	Y	N	Y	N	2005-10-24 08:03:21	100
548	Reporting Hierarchy	W	0	\N	\N	0	\N	360	2005-10-23 19:03:51	100	Define Reporting Hierarchy	Financial Reporting	Y	Y	N	N	N	2005-10-23 19:03:51	100
53214	Report Cube	W	0	\N	\N	0	\N	53078	2009-05-14 12:15:40	100	Define reporting cube for pre-calculation of summary accounting data.	Financial Reporting	Y	Y	N	N	N	2009-05-14 12:15:40	100
53215	Recalculate Cube	P	0	\N	\N	0	53166	\N	2009-05-15 00:44:20	100	Recalculate summary facts based on report cube definitions.	Financial Reporting	Y	Y	N	N	N	2009-12-10 20:24:39	100
53356	Accounting Fact Summary	R	0	\N	\N	0	53270	\N	2011-08-11 16:06:11	100	Accounting Fact Summary Report	Financial Reporting	Y	Y	N	N	N	2011-08-11 16:06:11	100
53935	Trial Balance Analysis	X	0	53051	\N	0	\N	\N	2015-01-31 16:27:54	100	Trial Balance Drillable Report	Financial Reporting	Y	Y	N	N	N	2022-02-17 22:52:45	100
53991	Accounting Fact Project	R	0	\N	\N	0	53785	\N	2015-08-03 14:13:45	100	Project Accounting Fact Details	Financial Reporting	Y	Y	N	N	N	2015-08-03 14:13:45	100
\.
