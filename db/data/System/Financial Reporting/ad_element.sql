copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1601	PA_ReportColumn_ID	0	0	13	\N	2001-05-09 21:20:17	0	Column in Report	Financial Reporting	22	\N	Y	Report Column	\N	\N	\N	\N	Report Column	2000-01-02 00:00:00	0
1602	AmountType	0	0	17	235	2001-05-09 21:20:17	0	Type of amount to report	Financial Reporting	2	You can choose between the total and period amounts as well as the balance or just the debit/credit amounts.	Y	Amount Type	\N	\N	\N	\N	Amt Type	2000-01-02 00:00:00	0
1605	CalculationType	0	0	17	236	2001-05-09 21:20:17	0	\N	Financial Reporting	1	\N	Y	Calculation	\N	\N	\N	\N	Calculation	2000-01-02 00:00:00	0
1606	ColumnType	0	0	17	237	2001-05-09 21:20:17	0	\N	Financial Reporting	1	\N	Y	Column Type	\N	\N	\N	\N	Column Type	2008-03-23 20:46:19	100
1607	CurrencyType	0	0	17	238	2001-05-09 21:20:17	0	\N	Financial Reporting	1	\N	Y	Currency Type	\N	\N	\N	\N	Currency Type	2000-01-02 00:00:00	0
1608	IsAdhocConversion	0	0	20	\N	2001-05-09 21:20:17	0	Perform conversion for all amounts to currency	Financial Reporting	1	If a currency is selected, only this currency will be reported. If adhoc conversion is selected, all currencies are converted to the defined currency	Y	Adhoc Conversion	\N	\N	\N	\N	Adhoc Conversion	2000-01-02 00:00:00	0
1609	LineType	0	0	17	241	2001-05-09 21:20:17	0	\N	Financial Reporting	1	\N	Y	Line Type	\N	\N	\N	\N	Line Type	2000-01-02 00:00:00	0
1610	Oper_1_ID	0	0	18	239	2001-05-09 21:20:17	0	First operand for calculation	Financial Reporting	22	\N	Y	Operand 1	\N	\N	\N	\N	Operand 1	2000-01-02 00:00:00	0
1611	Oper_2_ID	0	0	18	239	2001-05-09 21:20:17	0	Second operand for calculation	Financial Reporting	22	\N	Y	Operand 2	\N	\N	\N	\N	Operand 2	2000-01-02 00:00:00	0
1612	PA_Report_ID	0	0	13	\N	2001-05-09 21:20:17	0	Financial Report	Financial Reporting	22	\N	Y	Financial Report	\N	\N	\N	\N	Financial Report	2000-01-02 00:00:00	0
1613	PA_ReportColumnSet_ID	0	0	19	\N	2001-05-09 21:20:17	0	Collection of Columns for Report	Financial Reporting	22	The Report Column Set identifies the columns used in a Report.	Y	Report Column Set	\N	\N	\N	\N	Report Column Set	2000-01-02 00:00:00	0
1614	PA_ReportLine_ID	0	0	13	\N	2001-05-09 21:20:17	0	\N	Financial Reporting	22	\N	Y	Report Line	\N	\N	\N	\N	Report Line	2000-01-02 00:00:00	0
1615	PA_ReportLineSet_ID	0	0	19	\N	2001-05-09 21:20:17	0	\N	Financial Reporting	22	\N	Y	Report Line Set	\N	\N	\N	\N	Report Line Set	2000-01-02 00:00:00	0
1616	PA_ReportSource_ID	0	0	13	\N	2001-05-09 21:20:17	0	Restriction of what will be shown in Report Line	Financial Reporting	22	\N	Y	Report Source	\N	\N	\N	\N	Report Source	2000-01-02 00:00:00	0
1619	RelativePeriod	0	0	22	\N	2001-05-09 21:20:17	0	Period offset (0 is current)	Financial Reporting	22	\N	Y	Relative Period	\N	\N	\N	\N	Relative Period	2000-01-02 00:00:00	0
1954	Col_1	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_1	\N	\N	\N	\N	Col_1	2008-03-23 21:03:47	100
1955	Col_2	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_2	\N	\N	\N	\N	Col_2	2008-03-23 21:03:49	100
1956	Col_3	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_3	\N	\N	\N	\N	Col_3	2008-03-23 21:03:50	100
1957	Col_4	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_4	\N	\N	\N	\N	Col_4	2008-03-23 21:03:51	100
1958	Col_5	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_5	\N	\N	\N	\N	Col_5	2008-03-23 21:03:52	100
1959	Col_6	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_6	\N	\N	\N	\N	Col_6	2008-03-23 21:03:54	100
1960	Col_7	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_7	\N	\N	\N	\N	Col_7	2008-03-23 21:03:55	100
1961	Col_8	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_8	\N	\N	\N	\N	Col_8	2008-03-23 21:03:57	100
1962	Col_9	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_9	\N	\N	\N	\N	Col_9	2000-01-02 00:00:00	0
1963	Col_10	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_10	\N	\N	\N	\N	Col_10	2000-01-02 00:00:00	0
1964	Col_11	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_11	\N	\N	\N	\N	Col_11	2000-01-02 00:00:00	0
1965	Col_12	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_12	\N	\N	\N	\N	Col_12	2000-01-02 00:00:00	0
1966	Col_13	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_13	\N	\N	\N	\N	Col_13	2000-01-02 00:00:00	0
1967	Col_14	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_14	\N	\N	\N	\N	Col_14	2000-01-02 00:00:00	0
1968	Col_15	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_15	\N	\N	\N	\N	Col_15	2000-01-02 00:00:00	0
1969	Col_16	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_16	\N	\N	\N	\N	Col_16	2000-01-02 00:00:00	0
1970	Col_17	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_17	\N	\N	\N	\N	Col_17	2000-01-02 00:00:00	0
1971	Col_18	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_18	\N	\N	\N	\N	Col_18	2000-01-02 00:00:00	0
1972	Col_19	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_19	\N	\N	\N	\N	Col_19	2000-01-02 00:00:00	0
1973	Col_20	0	0	12	\N	2003-02-05 14:40:34	0	\N	Financial Reporting	22	\N	Y	Col_20	\N	\N	\N	\N	Col_20	2000-01-02 00:00:00	0
1982	LevelNo	0	0	11	\N	2003-02-12 00:40:19	0	\N	Financial Reporting	22	\N	Y	Level no	\N	\N	\N	\N	Level no	2000-01-02 00:00:00	0
1983	ListSources	0	0	20	\N	2003-02-12 00:40:19	0	List Report Line Sources	Financial Reporting	1	List the Source Accounts for Summary Accounts selected	Y	List Sources	\N	\N	\N	\N	List Sources	2000-01-02 00:00:00	0
1984	ListTrx	0	0	20	\N	2003-02-12 00:40:19	0	List the report transactions	Financial Reporting	1	List the transactions of the report source lines	Y	List Transactions	\N	\N	\N	\N	List Trx	2000-01-02 00:00:00	0
1985	Col_0	0	0	12	\N	2003-02-14 14:30:39	0	\N	Financial Reporting	22	\N	Y	Col_0	\N	\N	\N	\N	Col_0	2000-01-02 00:00:00	0
1986	Balance	0	0	12	\N	2003-02-15 00:42:39	0	\N	Financial Reporting	22	\N	Y	Balance	\N	\N	\N	\N	Balance	2000-01-02 00:00:00	0
2344	UpdateBalances	0	0	\N	\N	2004-01-16 22:05:22	0	Update Accounting Balances first (not required for subsequent runs)	Financial Reporting	\N	\N	Y	Update Balances	\N	\N	\N	\N	UpdateBalances	2005-05-08 16:31:41	100
2869	AD_Tree_Account_ID	0	0	18	184	2005-10-23 18:37:29	100	Tree for Natural Account Tree	Financial Reporting	10	\N	Y	Account Tree	\N	\N	\N	\N	Account Tree	2005-10-23 18:44:32	100
53323	JasperProcessing	0	0	28	\N	2008-01-07 21:40:17	100	\N	Financial Reporting	1	\N	Y	Jasper Process Now	\N	\N	\N	\N	Jasper Process Now	2008-01-07 21:40:17	100
53657	IsIncludeNullsOrg	0	0	20	\N	2008-07-10 16:40:08	100	Include nulls in the selection of the organization	Financial Reporting	1	\N	Y	Include Nulls in Org	\N	\N	\N	\N	Include Nulls in Org	2008-07-10 16:41:35	100
53658	IsIncludeNullsElementValue	0	0	20	\N	2008-07-10 16:41:22	100	Include nulls in the selection of the account	Financial Reporting	1	\N	Y	Include Nulls in Account	\N	\N	\N	\N	Include Nulls in Account	2008-07-10 16:41:22	100
53659	IsIncludeNullsBPartner	0	0	20	\N	2008-07-10 16:41:54	100	Include nulls in the selection of the business partner	Financial Reporting	1	\N	Y	Include Nulls in BPartner	\N	\N	\N	\N	Include Nulls in BPartner	2008-07-10 16:42:06	100
53660	IsIncludeNullsProduct	0	0	20	\N	2008-07-10 16:42:37	100	Include nulls in the selection of the product	Financial Reporting	1	\N	Y	Include Nulls in Product	\N	\N	\N	\N	Include Nulls in Product	2008-07-10 16:42:37	100
53661	IsIncludeNullsLocation	0	0	20	\N	2008-07-10 16:42:53	100	Include nulls in the selection of the location	Financial Reporting	1	\N	Y	Include Nulls in Location	\N	\N	\N	\N	Include Nulls in Location	2008-07-10 16:42:53	100
53662	IsIncludeNullsProject	0	0	20	\N	2008-07-10 16:43:13	100	Include nulls in the selection of the project	Financial Reporting	1	\N	Y	Include Nulls in Project	\N	\N	\N	\N	Include Nulls in Project	2008-07-10 16:43:13	100
53663	IsIncludeNullsSalesRegion	0	0	20	\N	2008-07-10 16:43:33	100	Include nulls in the selection of the sales region	Financial Reporting	1	\N	Y	Include Nulls in Sales Region	\N	\N	\N	\N	Include Nulls in Sales Region	2008-07-10 16:43:33	100
53664	IsIncludeNullsActivity	0	0	20	\N	2008-07-10 16:43:50	100	Include nulls in the selection of the activity	Financial Reporting	1	\N	Y	Include Nulls in Activity	\N	\N	\N	\N	Include Nulls in Activity	2008-07-10 16:43:50	100
53665	IsIncludeNullsCampaign	0	0	20	\N	2008-07-10 16:44:04	100	Include nulls in the selection of the campaign	Financial Reporting	1	\N	Y	Include Nulls in Campaign	\N	\N	\N	\N	Include Nulls in Campaign	2008-07-10 16:44:04	100
53666	IsIncludeNullsUserElement1	0	0	20	\N	2008-07-10 16:44:22	100	Include nulls in the selection of the user element 1	Financial Reporting	1	\N	Y	Include Nulls in User Element 1	\N	\N	\N	\N	Include Nulls in User Element 1	2008-07-10 16:44:22	100
53667	IsIncludeNullsUserElement2	0	0	20	\N	2008-07-10 16:44:30	100	Include nulls in the selection of the user element 2	Financial Reporting	1	\N	Y	Include Nulls in User Element 2	\N	\N	\N	\N	Include Nulls in User Element 2	2008-07-10 16:44:30	100
53688	Factor	0	0	17	53285	2008-09-26 17:09:14	100	Scaling factor.	Financial Reporting	1	Numbers are divided by the scaling factor for presentation.  E.g. 123,000 with a scaling factor of 1,000 will display as 123.	Y	Factor	\N	\N	\N	\N	Factor	2008-09-26 17:09:14	100
53716	IsIncludeNullsOrgTrx	0	0	20	\N	2008-12-16 17:51:45	0	Include nulls in the selection of the organization transaction	Financial Reporting	1	\N	Y	Include Nulls in Org Trx	\N	\N	\N	\N	Include Nulls in Org Trx	2008-12-16 17:51:45	0
53821	PA_ReportCube_ID	0	0	13	\N	2009-05-14 11:48:36	100	Define reporting cube for pre-calculation of summary accounting data.	Financial Reporting	22	Summary data will be generated for each period of the selected calendar, grouped by the selected dimensions..	Y	Report Cube	\N	\N	\N	\N	Report Cube	2009-05-14 11:48:36	100
53822	IsActivityDim	0	0	20	\N	2009-05-14 11:52:58	100	Include Activity as a cube dimension	Financial Reporting	1	\N	Y	Activity Dimension	\N	\N	\N	\N	Activity Dimension	2009-05-14 11:52:58	100
53823	IsOrgTrxDim	0	0	20	\N	2009-05-14 11:53:34	100	Include OrgTrx as a cube dimension	Financial Reporting	1	\N	Y	OrgTrx Dimension	\N	\N	\N	\N	OrgTrx Dimension	2009-05-14 11:53:34	100
53824	IsBPartnerDim	0	0	20	\N	2009-05-14 11:54:24	100	Include Business Partner as a cube dimension	Financial Reporting	1	\N	Y	Business Partner Dimension	\N	\N	\N	\N	Business Partner Dimension	2009-05-14 11:54:24	100
53825	IsCampaignDim	0	0	20	\N	2009-05-14 11:55:01	100	Include Campaign as a cube dimension	Financial Reporting	1	\N	Y	Campaign Dimension	\N	\N	\N	\N	Campaign Dimension	2009-05-14 11:55:01	100
53826	IsLocFromDim	0	0	20	\N	2009-05-14 11:55:32	100	Include Location From as a cube dimension	Financial Reporting	1	\N	Y	Location From Dimension	\N	\N	\N	\N	Location From Dimension	2009-05-14 11:55:32	100
53827	IsLocToDim	0	0	20	\N	2009-05-14 11:55:53	100	Include Location To as a cube dimension	Financial Reporting	1	\N	Y	Location To  Dimension	\N	\N	\N	\N	Location To Dimension	2009-05-14 11:55:53	100
53828	IsProjectPhaseDim	0	0	20	\N	2009-05-14 11:56:31	100	Include Project Phase as a cube dimension	Financial Reporting	1	\N	Y	Project Phase  Dimension	\N	\N	\N	\N	Project Phase Dimension	2009-05-14 11:56:31	100
53829	IsProjectTaskDim	0	0	20	\N	2009-05-14 11:56:59	100	Include Project Task as a cube dimension	Financial Reporting	1	\N	Y	Project Task  Dimension	\N	\N	\N	\N	Project Task Dimension	2009-05-14 11:56:59	100
53830	IsProjectDim	0	0	20	\N	2009-05-14 11:57:24	100	Include Project as a cube dimension	Financial Reporting	1	\N	Y	Project Dimension	\N	\N	\N	\N	Project Dimension	2009-05-14 11:57:24	100
53831	IsSalesRegionDim	0	0	20	\N	2009-05-14 11:58:00	100	Include Sales Region as a cube dimension	Financial Reporting	1	\N	Y	Sales Region Dimension	\N	\N	\N	\N	Sales Region Dimension	2009-05-14 11:58:00	100
53832	IsSubAcctDim	0	0	20	\N	2009-05-14 11:58:42	100	Include Sub Acct as a cube dimension	Financial Reporting	1	\N	Y	Sub Acct Dimension	\N	\N	\N	\N	Sub Acct Dimension	2009-05-14 11:58:42	100
53833	IsGLBudgetDim	0	0	20	\N	2009-05-14 11:59:24	100	Include GL Budget as a cube dimension	Financial Reporting	1	\N	Y	GL Budget Dimension	\N	\N	\N	\N	GL Budget Dimension	2009-05-14 11:59:24	100
53834	IsProductDim	0	0	20	\N	2009-05-14 11:59:55	100	Include Product as a cube dimension	Financial Reporting	1	\N	Y	Product Dimension	\N	\N	\N	\N	Product Dimension	2009-05-14 11:59:55	100
53835	IsUser1Dim	0	0	20	\N	2009-05-14 12:00:27	100	Include User 1 as a cube dimension	Financial Reporting	1	\N	Y	User 1 Dimension	\N	\N	\N	\N	User 1 Dimension	2009-05-14 12:00:27	100
53836	IsUser2Dim	0	0	20	\N	2009-05-14 12:02:03	100	Include User 2 as a cube dimension	Financial Reporting	1	\N	Y	User 2 Dimension	\N	\N	\N	\N	User 2 Dimension	2009-05-14 12:02:03	100
53837	LastRecalculated	0	0	16	\N	2009-05-19 23:25:04	100	The time last recalculated.	Financial Reporting	7	\N	Y	Last Recalculated	\N	\N	\N	\N	Last Recalculated	2009-05-19 23:25:04	100
53897	IsUserElement2Dim	0	0	20	\N	2009-08-02 22:41:33	100	Include User Element 2 as a cube dimension	Financial Reporting	1	\N	Y	User Element 2 Dimension	\N	\N	\N	\N	User Element 2 Dimension	2009-08-02 22:44:15	100
53898	IsUserElement1Dim	0	0	20	\N	2009-08-02 22:44:08	100	Include User Element 1 as a cube dimension	Financial Reporting	1	\N	Y	User Element 1 Dimension	\N	\N	\N	\N	User Element 1 Dimension	2009-08-02 22:44:08	100
54061	PAPeriodType	0	0	17	53327	2009-10-02 11:52:35	100	PA Period Type	Financial Reporting	1	The Period Type to report on: Period, Year, Total or Natural. Natural = Year for P & L accounts, Total for Balance Sheet accounts.	Y	Period Type	\N	\N	\N	\N	Period Type	2009-10-02 11:56:58	100
54062	PAAmountType	0	0	17	53328	2009-10-02 11:56:46	100	PA Amount Type for reporting	Financial Reporting	1	The amount type to report on: Quantity, Credit Only, Debit Only, Balance (expected sign) or Balance (accounted sign). "Expected sign" adjusts the sign of the result based on the Account Type and Expected Sign of each Account Element, whereas "accounted sign" always returns DR-CR.	Y	Amount Type	\N	\N	\N	\N	Amount Type	2009-10-02 12:01:36	100
57154	AD_PrintFormatHeader_ID	0	0	\N	\N	2014-05-16 15:46:18	100	\N	Financial Reporting	\N	\N	Y	Header Print Format	\N	\N	\N	\N	Header Print Format	2014-05-16 15:46:18	100
57235	IsIncludeNullsUserList1	0	0	\N	\N	2015-04-27 17:09:06	100	Include nulls in the selection of the User List 1	Financial Reporting	\N	\N	Y	Include Nulls in User List 1	\N	\N	\N	\N	Include Nulls in User List 1	2015-04-27 17:09:06	100
57236	IsIncludeNullsUserList2	0	0	\N	\N	2015-04-27 17:09:34	100	Include nulls in the selection of the User List 2	Financial Reporting	\N	\N	Y	Include Nulls in User List 2	\N	\N	\N	\N	Include Nulls in User List 2	2015-04-27 17:09:34	100
57912	RelativePeriodTo	0	0	\N	\N	2015-03-21 10:07:35	100	Period offset (0 is current)	Financial Reporting	\N	\N	Y	Relative Period To	\N	\N	\N	\N	Relative Period To	2015-03-21 10:07:35	100
58112	sellqty	0	0	\N	\N	2015-08-03 12:37:11	100	\N	Financial Reporting	\N	\N	Y	sellqty	\N	\N	\N	\N	sellqty	2015-08-03 12:37:11	100
58113	unitsellprice	0	0	\N	\N	2015-08-03 12:37:13	100	\N	Financial Reporting	\N	\N	Y	unitsellprice	\N	\N	\N	\N	unitsellprice	2015-08-03 12:37:13	100
58114	extended_sell_price	0	0	\N	\N	2015-08-03 12:37:15	100	\N	Financial Reporting	\N	\N	Y	extended_sell_price	\N	\N	\N	\N	extended_sell_price	2015-08-03 12:37:15	100
58115	invoicing_desc	0	0	\N	\N	2015-08-03 12:37:17	100	\N	Financial Reporting	\N	\N	Y	invoicing_desc	\N	\N	\N	\N	invoicing_desc	2015-08-03 12:37:17	100
58305	borp	0	0	\N	\N	2015-08-30 19:54:41	0	\N	Financial Reporting	\N	\N	Y	borp	\N	\N	\N	\N	borp	2015-08-30 19:54:41	0
58309	UnderlineStrokeType	0	0	\N	\N	2015-09-09 16:25:04	100	\N	Financial Reporting	\N	\N	Y	Underline Stroke Type	\N	\N	\N	\N	Underline Stroke Type	2015-09-09 16:25:04	100
58310	OverlineStrokeType	0	0	\N	\N	2015-09-09 16:25:37	100	\N	Financial Reporting	\N	\N	Y	Overline Stroke Type	\N	\N	\N	\N	Overline Stroke Type	2015-09-09 16:25:37	100
\.
