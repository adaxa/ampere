copy "ad_table" ("ad_table_id", "tablename", "name", "accesslevel", "ad_client_id", "ad_org_id", "ad_val_rule_id", "ad_window_id", "copycolumnsfromtable", "created", "createdby", "description", "entitytype", "help", "importtable", "isactive", "iscentrallymaintained", "ischangelog", "isdeleteable", "ishighvolume", "issecurityenabled", "istscapable", "isview", "loadseq", "po_window_id", "replicationtype", "sqlstatement", "updated", "updatedby") from STDIN;
321	M_Inventory	Phys.Inventory	1	0	0	\N	168	\N	1999-12-19 20:39:32	0	Parameters for a Physical Inventory	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
322	M_InventoryLine	Phys.Inventory Line	1	0	0	\N	168	\N	1999-12-19 20:39:32	0	Unique line in an Inventory document	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2000-01-02 00:00:00	0
323	M_Movement	Inventory Move	1	0	0	\N	170	\N	1999-12-19 20:39:32	0	Movement of Inventory	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	130	\N	L	\N	2007-12-17 07:44:33	0
324	M_MovementLine	Move Line	1	0	0	\N	170	\N	1999-12-19 20:39:32	0	Inventory Move document Line	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2007-12-17 07:23:15	0
325	M_Production	Production	3	0	0	\N	191	\N	1999-12-19 20:39:32	0	Plan for producing a product	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	125	\N	L	\N	2000-01-02 00:00:00	0
326	M_ProductionLine	Production Line	3	0	0	\N	53127	\N	1999-12-19 20:39:32	0	Document Line representing a production	Material Management	\N	\N	Y	Y	Y	Y	N	N	N	N	135	\N	L	\N	2011-07-27 15:14:59	100
363	RV_M_Transaction_Sum	Transaction Sum	3	0	0	\N	\N	\N	2000-05-11 18:21:30	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:56:52	100
364	T_Replenish	T_Replenish	3	0	0	\N	\N	\N	2000-05-11 18:21:30	0	\N	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:07:02	100
385	M_ProductionPlan	Production Plan	3	0	0	\N	191	\N	2000-10-15 11:55:26	0	Plan for how a product is produced	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	130	\N	L	\N	2000-01-02 00:00:00	0
425	RV_M_Transaction	Mat Transaction	3	0	0	\N	\N	\N	2001-02-01 20:51:41	0	With Product_PO info	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:58:34	100
478	T_InventoryValue	T_InventoryValue	3	0	0	\N	\N	\N	2002-01-17 21:09:39	0	\N	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-09-03 12:06:59	100
629	RV_Transaction	Mat Transaction	3	0	0	\N	\N	\N	2003-12-04 23:31:06	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:58:29	100
630	RV_Storage	Warehouse Storage	3	0	0	\N	\N	\N	2003-12-04 23:31:06	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:58:10	100
719	M_DemandLine	Demand Line	2	0	0	\N	329	\N	2004-04-17 11:09:18	0	Material Demand Line	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2005-11-09 14:25:43	100
721	M_DemandDetail	Demand Detail	2	0	0	\N	329	\N	2004-04-17 11:09:19	0	Material Demand Line Source Detail	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-11-09 14:25:38	100
723	M_Demand	Demand	2	0	0	\N	329	\N	2004-04-17 11:09:19	0	Material Demand	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2005-11-09 14:25:34	100
727	M_InOutConfirm	Ship/Receipt Confirmation	1	0	0	\N	330	\N	2004-05-10 17:21:41	0	Material Shipment or Receipt Confirmation	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	135	\N	L	\N	2006-07-23 17:05:27	100
728	M_InOutLineConfirm	Ship/Receipt Confirmation Line	1	0	0	\N	330	\N	2004-05-10 17:21:41	0	Material Shipment or Receipt Confirmation Line	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	140	\N	L	\N	2000-01-02 00:00:00	0
730	RV_InOutConfirm	InOut Confirm	3	0	0	\N	\N	\N	2004-05-18 20:17:51	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:56:19	100
731	RV_InOutLineConfirm	InOut Line Confirm	3	0	0	\N	\N	\N	2004-05-18 20:17:52	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2006-01-22 14:56:34	100
758	T_Transaction	T_Transaction	3	0	0	\N	\N	\N	2005-03-30 01:07:18	0	\N	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-03-30 01:15:55	100
763	M_InventoryLineMA	M_InventoryLineMA	1	0	0	\N	168	\N	2005-03-31 15:17:04	0	\N	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2005-04-26 20:41:07	100
764	M_MovementLineMA	M_MovementLineMA	1	0	0	\N	170	\N	2005-03-31 15:17:04	0	\N	Material Management	\N	\N	Y	Y	N	Y	N	N	N	N	145	\N	L	\N	2007-12-17 07:37:46	0
53247	RV_Storage_Per_Product	Storage per Product	3	0	0	\N	\N	N	2009-12-01 16:57:26	100	\N	Material Management	\N	N	Y	Y	N	N	N	N	N	Y	0	\N	L	\N	2010-05-15 18:16:55	100
53331	M_QualityTestResult	Quality Test Result	3	0	0	\N	53127	\N	2011-07-27 15:15:54	100	\N	Material Management	\N	N	Y	Y	N	Y	N	N	N	N	\N	\N	L	\N	2011-07-27 15:15:54	100
53941	RV_Storage_Locator	Warehouse Storage by Locator	3	0	0	\N	\N	\N	2015-02-12 15:20:31	0	\N	Material Management	\N	\N	Y	Y	N	N	N	N	N	Y	150	\N	L	\N	2016-04-11 16:41:46	0
53950	AX_Seasonality	Seasonality Type	3	0	0	\N	53408	\N	2015-03-24 15:45:46	100	Seasonality Type	Material Management	\N	N	Y	N	N	Y	N	N	N	N	\N	\N	L	\N	2016-04-11 16:47:16	0
54052	M_Production_Batch	Production Batch	3	0	0	\N	53127	N	2015-11-20 15:21:17	100	Production Batch for producing a product	Material Management	\N	N	Y	Y	Y	Y	N	N	N	N	0	\N	L	\N	2016-07-01 00:51:11	0
54170	T_InventoryValueByWarehouse	Inventory Value by Warehouse	3	0	0	\N	\N	N	2016-10-31 16:51:59	100	Report View of Inventory Valuation without Locator	Material Management	\N	N	Y	Y	N	N	N	N	N	Y	0	\N	L	\N	2016-10-31 16:51:59	100
\.
