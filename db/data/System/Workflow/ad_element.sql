copy "ad_element" ("ad_element_id", "columnname", "ad_client_id", "ad_org_id", "ad_reference_id", "ad_reference_value_id", "created", "createdby", "description", "entitytype", "fieldlength", "help", "isactive", "name", "po_description", "po_help", "po_name", "po_printname", "printname", "updated", "updatedby") from STDIN;
1514	Priority	0	0	17	154	2001-01-11 17:05:05	0	Indicates if this request is of a high, medium or low priority.	Workflow	1	The Priority indicates the importance of this request.	Y	Priority	\N	\N	\N	\N	Priority	2000-01-02 00:00:00	0
2312	AD_WF_Process_ID	0	0	19	\N	2004-01-01 23:37:18	0	Actual Workflow Process Instance	Workflow	22	Instance of a workflow execution	Y	Workflow Process	\N	\N	\N	\N	Wf Process	2000-01-02 00:00:00	0
2315	AttributeName	0	0	10	\N	2004-01-01 23:37:18	0	Name of the Attribute	Workflow	60	Identifier of the attribute	Y	Attribute Name	\N	\N	\N	\N	Attribute Name	2000-01-02 00:00:00	0
2317	AttributeValue	0	0	10	\N	2004-01-01 23:37:18	0	Value of the Attribute	Workflow	60	Adempiere converts the (string) field values to the attribute data type.  Booleans (Yes-No) may have the values "true" and "false", the date format is YYYY-MM-DD	Y	Attribute Value	\N	\N	\N	\N	Attribute Value	2005-11-11 18:34:39	100
2326	ResponsibleType	0	0	17	304	2004-01-01 23:37:18	0	Type of the Responsibility for a workflow	Workflow	1	Type how the responsible user for the execution of a workflow is determined	Y	Responsible Type	\N	\N	\N	\N	Responsible Type	2000-01-02 00:00:00	0
2332	WFState	0	0	17	305	2004-01-01 23:37:18	0	State of the execution of the workflow	Workflow	2	\N	Y	Workflow State	\N	\N	\N	\N	Wf State	2000-01-02 00:00:00	0
2474	ElapsedTimeMS	0	0	22	\N	2004-03-22 15:56:19	0	Elapsed Time in milli seconds	Workflow	22	Elapsed Time in milli seconds	Y	Elapsed Time ms	\N	\N	\N	\N	Elapsed Time	2010-01-13 10:38:15	0
2484	EndWaitTime	0	0	16	\N	2004-03-25 22:49:26	0	End of sleep time	Workflow	7	End of suspension (sleep)	Y	End Wait	\N	\N	\N	\N	End Wait	2000-01-02 00:00:00	0
2518	IsAbort	0	0	\N	\N	2004-05-07 23:07:01	0	Aborts the current process	Workflow	\N	\N	Y	Abort Process	\N	\N	\N	\N	Abort Process	2000-01-02 00:00:00	0
2629	DateLastAlert	0	0	16	\N	2004-08-30 19:43:01	0	Date when last alert were sent	Workflow	7	The last alert date is updated when a reminder email is sent	Y	Last Alert	\N	\N	\N	\N	Last Alert	2000-01-02 00:00:00	0
2637	DynPriorityStart	0	0	11	\N	2004-09-01 23:37:00	0	Starting priority before changed dynamically	Workflow	22	\N	Y	Dyn Priority Start	\N	\N	\N	\N	Dyn Priority Start	2000-01-02 00:00:00	0
\.
