copy "ad_table_attribute" ("ad_table_attribute_id", "name", "ad_client_id", "ad_org_id", "ad_table_id", "created", "createdby", "entitytype", "errormsg", "isactive", "tableattributedefinition", "tableattributetype", "tsvdefinition", "updated", "updatedby") from STDIN;
50643	ad_wf_process_pkey	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	PRIMARY KEY (ad_wf_process_id)	CP	\N	2022-11-02 05:57:56.041854	100
50644	aduser_adwfprocess	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_user_id) REFERENCES ad_user(ad_user_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50645	wf_instanceorg	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_org_id) REFERENCES ad_org(ad_org_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50646	wf_instanceclient	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_client_id) REFERENCES ad_client(ad_client_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50647	adworkflow_adwfprocess	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_workflow_id) REFERENCES ad_workflow(ad_workflow_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50648	adtable_adwfprocess	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_table_id) REFERENCES ad_table(ad_table_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50649	adwfprocess_adwfprocess	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_wf_process_id) REFERENCES ad_wf_process(ad_wf_process_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
50650	admessage_adwfprocess	0	0	645	2022-11-02 05:57:56.041854	100	Workflow	\N	Y	FOREIGN KEY (ad_message_id) REFERENCES ad_message(ad_message_id) DEFERRABLE INITIALLY DEFERRED	CF	\N	2022-11-02 05:57:56.041854	100
52923	ad_wf_process_workflow	0	0	645	2022-11-02 06:15:05.061147	100	Workflow	\N	Y	 USING btree (ad_workflow_id)	I	\N	2022-11-02 06:15:05.061147	100
53312	ad_wf_process_processed_check	0	0	645	2022-11-02 12:40:39.392787	100	Workflow	\N	Y	CHECK ((processed = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
53313	ad_wf_process_isactive_check	0	0	645	2022-11-02 12:40:39.392787	100	Workflow	\N	Y	CHECK ((isactive = ANY (ARRAY['Y'::bpchar, 'N'::bpchar])))	CC	\N	2022-11-02 12:40:39.392787	100
\.
