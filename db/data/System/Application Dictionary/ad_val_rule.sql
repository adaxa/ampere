copy "ad_val_rule" ("ad_val_rule_id", "name", "ad_client_id", "ad_org_id", "code", "created", "createdby", "description", "entitytype", "isactive", "type", "updated", "updatedby") from STDIN;
100	AD_Column in AD_Table	0	0	AD_Column.AD_Table_ID=@AD_Table_ID@	1999-05-21 00:00:00	0	Table must be previously defined	Application Dictionary	Y	S	2000-01-02 00:00:00	0
104	AD_Org Security validation	0	0	(AD_Org.IsSummary='N' OR AD_Org.AD_Org_ID=0)	1999-07-01 00:00:00	0	Not Summary - 0 - Organizations of the Client with user acces rights	Application Dictionary	Y	S	2000-01-02 00:00:00	0
115	AD_Reference List or Table	0	0	AD_Reference.ValidationType=CASE WHEN  @AD_Reference_ID@ IN (17,28) THEN 'L' ELSE 'T' END	1999-07-07 00:00:00	0	List or Table validation choices	Application Dictionary	Y	S	2000-01-02 00:00:00	0
128	AD_Process Reports	0	0	AD_Process.IsReport='Y'	2000-02-04 12:16:56	0	\N	Application Dictionary	Y	S	2000-01-02 00:00:00	0
129	AD_Client Trx Security validation	0	0	AD_Client.AD_Client_ID <> 0	2000-02-12 17:15:22	0	\N	Application Dictionary	Y	S	2016-04-11 16:43:03	0
130	AD_Org Trx Security validation	0	0	AD_Org.AD_Org_ID <> 0  AND AD_Org.IsSummary='N'	2000-02-12 17:16:45	0	Not Summary - Not 0	Application Dictionary	Y	S	2000-01-02 00:00:00	0
158	AD_Role of Client	0	0	AD_Role.AD_Client_ID=@#AD_Client_ID@	2003-01-25 13:40:29	0	\N	Application Dictionary	Y	S	2000-01-02 00:00:00	0
163	AD_Tab in Window	0	0	AD_Tab.AD_Window_ID=@AD_Window_ID@	2003-05-08 06:37:08	0	\N	Application Dictionary	Y	S	2000-01-02 00:00:00	0
197	AD_Workflow Process	0	0	AD_Workflow.AD_Table_ID IS NOT NULL	2004-04-10 17:50:33	0	\N	Application Dictionary	Y	S	2000-01-02 00:00:00	0
211	AD_Tab with Table	0	0	AD_Tab.AD_Table_ID=@AD_Table_ID@	2004-08-11 18:17:04	0	\N	Application Dictionary	Y	S	2000-01-02 00:00:00	0
52008	EventType Document or Table	0	0	EventType IN ('D', 'T')	2008-02-01 03:39:00	100	\N	Application Dictionary	Y	S	2008-02-01 03:39:00	100
52046	AD_Column in AD_Table with datatype int/string	0	0	AD_Column.AD_Table_ID=@AD_Table_ID@ AND AD_Column.AD_Reference_ID IN (10,11,13,14,22,36)	2009-02-18 12:51:42	100	Only columns with text or number datatypes	Application Dictionary	Y	S	2009-02-18 12:51:42	100
52056	AD_Column (Parent tab link column)	0	0	/* attempt to get the columns in the parent tab table */\nAD_Column.AD_Table_ID IN (SELECT t.AD_Table_ID FROM AD_Tab t\n                                                   WHERE t.AD_Window_ID = @AD_Window_ID@\n\t                         AND t.TabLevel = @TabLevel@-1\n\t\tAND t.SeqNo < @SeqNo@\n\t\tAND NOT EXISTS (SELECT 1 FROM AD_Tab t2\n                                                      where t2.AD_Window_ID=t.AD_Window_ID\n                                                      AND t2.TabLevel = t.TabLevel\n\t\tAND t2.SeqNo < @SeqNo@\n                                                     AND t2.SeqNo > t.SeqNo) )	2009-06-11 14:53:27	100	\N	Application Dictionary	Y	S	2009-06-11 14:53:27	100
52453	AD_InfoColumn from Info Window	0	0	AD_InfoColumn.AD_InfoWindow_ID=@AD_InfoWindow_ID@	2016-04-20 15:33:43	100	\N	Application Dictionary	Y	S	2016-04-20 15:33:43	100
52461	AD_Process_Para of Link Column Process	0	0	AD_Process_Para.AD_Process_ID=(SELECT AD_Process_ID FROM AD_ColumnProcess WHERE AD_ColumnProcess_ID=@AD_ColumnProcess_ID@)	2016-04-29 10:34:44	100	\N	Application Dictionary	Y	S	2016-07-01 00:51:34	0
400001	FTS_Table_Attribute of AD_Table	0	0	AD_Table_Attribute.AD_Table_ID=@AD_Table_ID@	2022-11-03 13:07:14	100	\N	Application Dictionary	Y	S	2022-11-03 13:07:14	100
\.
