CREATE OR REPLACE FUNCTION ampere.trunc(i interval)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
BEGIN
	RETURN EXTRACT(DAY FROM i);
END;
$function$
