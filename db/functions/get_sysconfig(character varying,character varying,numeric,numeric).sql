CREATE OR REPLACE FUNCTION ampere.get_sysconfig(sysconfig_name character varying, defaultvalue character varying, client_id numeric, org_id numeric)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
DECLARE
 	v_value ad_sysconfig.value%TYPE;
BEGIN
    BEGIN
	    SELECT Value
	      INTO STRICT v_value
	      FROM AD_SysConfig WHERE Name=sysconfig_name AND AD_Client_ID IN (0, client_id) AND AD_Org_ID IN (0, org_id) AND IsActive='Y' 
	     ORDER BY AD_Client_ID DESC, AD_Org_ID DESC
	     LIMIT 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_value := defaultvalue;
    END;
	RETURN v_value;
END;
$function$
