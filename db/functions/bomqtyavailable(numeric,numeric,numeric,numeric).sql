CREATE OR REPLACE FUNCTION ampere.bomqtyavailable(product_id numeric, attributesetinstance_id numeric, warehouse_id numeric, locator_id numeric)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$

BEGIN
	
	RETURN bomQtyOnHand(Product_ID, Attributesetinstance_id, Warehouse_ID, Locator_ID)
		- bomQtyReserved(Product_ID, Attributesetinstance_id, Warehouse_ID, Locator_ID);
	
END;

$function$
